<?php

use app\components\View;
use app\helpers\WidgetHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\PostAsset;
use app\assets\YTExternalAsset;

\app\assets\ImageAsset::register($this);
PostAsset::register($this);
YTExternalAsset::register($this);
?>

<div id="image-page" class="post item-post image"
     data-widget="page"
    >
    <div class="row collapsing-content">
        <div class="col-xs-12 col-sm-12">
            <div class="collapse in"
                 aria-expanded="true">
                <div id="ad2" style="margin-bottom: 10px;">
                    <iframe src="/static/html/iframe.html" scrolling="no" frameBorder="0" allowtransparency="true" width="960px" height="252px"></iframe>
                </div>
                <div id="overflow-wrapper" class="post-expand-wrapper">
                    <h1><?= $label ?></h1>
                </div>
                <center><iframe width="780" scrolling="no" height="210" frameborder="0" marginwidth="0" marginheight="0" src="http://adsbookie.com/y/ad.php?link=aHR0cDovL3YyLmFkc2Jvb2tpZS5jb20vY2RuL3pyODA="></iframe>
                </center>
            </div>
        </div>
    </div>
</div>
