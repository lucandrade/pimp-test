<?php

namespace app\commands;

use app\helpers\DeployHelper;
use yii\console\Controller;

class DeployController extends Controller
{
    public function actionIndex()
    {
        echo implode("\n", DeployHelper::runPull());
        echo implode("\n", DeployHelper::runAfterPullScripts());
    }

    public function actionAfterPull()
    {
        echo implode("\n", DeployHelper::runAfterPullScripts());
    }
}