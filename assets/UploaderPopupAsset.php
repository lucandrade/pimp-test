<?php

namespace app\assets;


use yii\web\AssetBundle;

class UploaderPopupAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
    ];

    public $js = [
        'js/popup-uploader.js',
    ];

    public $depends = [
        'app\assets\FullUploaderAsset'
    ];
    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}