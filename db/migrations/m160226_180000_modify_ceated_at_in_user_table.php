<?php

use yii\db\Migration;
use yii\db\Schema;

class m160226_180000_modify_ceated_at_in_user_table extends Migration
{
    public $table = '{{user}}';

    public function up()
    {
        $this->alterColumn($this->table, 'created_at', 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP');
    }

    public function down()
    {
        $this->alterColumn($this->table, 'created_at', Schema::TYPE_TIMESTAMP);
    }

}
