$(function () {
    // todo: use common code
    $(".action-button").on("click", function (e) {
        e.preventDefault();
        var url = $(this).attr('href'),
            id = $(this).data('id'),
            needConfirmation = $(this).data('need-confirmation') || 0;

        if (needConfirmation) {
            if (confirm('Are you sure?')) {
                sendPostRowAjax(url, id);
            }
        } else {
            sendPostRowAjax(url, id);
        }
        return false;
    });

    var sendPostRowAjax = function (url, ids) {
        $.ajax({
            url: url,
            data: {ids: ids},
            type: "POST",
            success: function () {
                location.reload();
            }
        });
    };


    // datetime bootstrap picker
    $('.datetime-picker, .input-daterange').datepicker();

    // grid selected rows action
    $('[data-index-action]').each(function () {
        var me = $(this), $grid = me.closest('.grid-view');

        me.on(me.data('index-action'), function (e) {
            var ids = $grid.yiiGridView('getSelectedRows'), params = {};

            e.preventDefault();

            if (!ids.length || me.val() == '-') {
                return false;
            }

            $.each(ids, function (key, value) {
                params['selection[' + key + ']'] = value;
            });
            params[me.attr('name')] = me.val();
            params['deleteByOne'] = me.data('delete-by-one') || 0;

            bootboxConfirm(me.data('index-message') || "Are you sure?", function (result) {
                yii.handleAction($('<a>').data('params', params).data('method', 'post'));
            });
        });
    });


    $('.purge-fields').on('click', function () {
        var ids = $('.grid-view').yiiGridView('getSelectedRows'),
            $me = $(this);
        if (ids.length == 0) {
            return false;
        }
        bootboxConfirm($me.data('index-message') || "Are you sure?", function (result) {
            sendPostRowAjax($me.data('url'), ids.join(','))
        });
    });

});

function bootboxConfirm(message, ok, cancel) {
    bootbox.dialog({
        message: message,
        title: "Confirm",
        buttons: {
            success: {
                label: "OK",
                className: "btn-success",
                callback: ok
            },
            danger: {
                label: "Cancel",
                className: "btn-danger",
                callback: cancel
            }
        }
    });
}

$(function () {

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

    function getDateMonthString(date) {

        var monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];

        var dateStr = [];
        dateStr.push(monthNames[date.getMonth()]);
        dateStr.push(('0' + date.getDate()).slice(-2));
        dateStr.push(date.getFullYear());
        return dateStr.join(' ');
    }

    if ($('#filter-statistic-date').length > 0) {

        $('.choose-daterange').click(function (e) {
            e.preventDefault();
        });

        var dateRangeChanged = false;
        var dateRangePeriod;

        if ($('#popover-daterange [name="date_from"]').val() === $('#popover-daterange [name="date_to"]').val()) {
            dateRangePeriod = {
                date_from: new Date($('#popover-daterange [name="date_from"]').val())
            };
        } else {
            dateRangePeriod = {
                date_from: new Date($('#popover-daterange [name="date_from"]').val()),
                date_to: new Date($('#popover-daterange [name="date_to"]').val()),
            };
        }

        function updateRangeInputData($popoverContent, name) {
            var dateFormatted;

            if (typeof dateRangePeriod[name] === 'undefined') {
                dateFormatted = '';
            } else {
                dateFormatted = formatDate(dateRangePeriod[name]);
            }
            $popoverContent.find('form [name="' + name + '"]').val(dateFormatted);
        }

        function updateRangeInput($popoverContent) {
            updateRangeInputData($popoverContent, 'date_from');
            updateRangeInputData($popoverContent, 'date_to');
        }

        var externalDateChanged = true;

        function callInternalDatepickerUpdate(el, callback, data) {
            externalDateChanged = false;
            $(el).datepicker(callback, data);
            externalDateChanged = true;
        }

        function initInternalDatepicker($pickerContainer) {
            callInternalDatepickerUpdate($pickerContainer, 'clearDates');

            var pickerDates = [];
            if (typeof dateRangePeriod.date_from !== 'undefined' && typeof dateRangePeriod.date_to !== 'undefined') {
                pickerDates.push(formatDate(dateRangePeriod.date_from));
                pickerDates.push(formatDate(dateRangePeriod.date_to));
            } else if (typeof dateRangePeriod.date_from !== 'undefined') {
                pickerDates.push(formatDate(dateRangePeriod.date_from));
            } else if (typeof dateRangePeriod.date_to !== 'undefined') {
                pickerDates.push(formatDate(dateRangePeriod.date_to));
            }
            callInternalDatepickerUpdate($pickerContainer, 'setDates', pickerDates);
        }


        function resolveDatePeriodOrder() {
            if (dateRangePeriod.date_to && dateRangePeriod.date_from) {
                if (dateRangePeriod.date_to.getTime() < dateRangePeriod.date_from.getTime()) {
                    var dateFrom = dateRangePeriod.date_from;
                    dateRangePeriod.date_from = dateRangePeriod.date_to;
                    dateRangePeriod.date_to = dateFrom;
                }
            }
        }

        $('.choose-daterange').popover({
            placement: 'bottom',
            html: true,
            content: function () {
                return $('#popover-daterange').clone().html();
            }
        }).on('shown.bs.popover', function () {
            var $popoverContent = $(this).parent().find('.popover');
            var $pickerContainer = $popoverContent.find('.inline-daterange .daterange-container');

            $pickerContainer.datepicker({
                format: "yyyy-mm-dd",
                multidate: true,
                //inputs: $popoverContent.find('form [name="' + $(this).data('input-name') + '"]'),
            }).on('changeDate', function () {
                if (externalDateChanged) {
                    var dateRangeName;

                    if ($popoverContent.find('.date-focused').length > 0) {
                        dateRangeName = $popoverContent.find('.date-focused').attr('name');

                        switch ($(this).datepicker('getDates').length) {
                            case 0:
                                dateRangePeriod = {};
                                break;

                            case 1:

                                dateRangePeriod = {};
                                dateRangePeriod[dateRangeName] = $(this).datepicker('getDate');

                                initInternalDatepicker($pickerContainer);
                                break;
                            case 2:
                                dateRangePeriod[dateRangeName] = $(this).datepicker('getDate');

                                resolveDatePeriodOrder();
                                initInternalDatepicker($pickerContainer);
                                break;

                            case 3:
                                dateRangePeriod[dateRangeName] = $(this).datepicker('getDate');

                                resolveDatePeriodOrder();
                                initInternalDatepicker($pickerContainer);
                                break;

                            default:
                                dateRangePeriod = {};
                                callInternalDatepickerUpdate(this, 'clearDates');
                        }

                        $popoverContent.find('[name="' + dateRangeName + '"]').focus();
                    } else {
                        switch ($(this).datepicker('getDates').length) {
                            case 0:
                                dateRangePeriod = {};
                                break;

                            case 1:
                                dateRangePeriod = {};
                                dateRangePeriod.date_from = $(this).datepicker('getDate');

                                initInternalDatepicker($pickerContainer);
                                break;
                            case 2:
                                var dateTo = $(this).datepicker('getDate');

                                if (dateTo.getTime() < dateRangePeriod.date_from.getTime()) {
                                    dateRangePeriod.date_from = dateTo;
                                } else {
                                    dateRangePeriod.date_to = dateTo;
                                }

                                //resolveDatePeriodOrder();
                                initInternalDatepicker($pickerContainer);
                                break;

                            case 3:
                                dateRangePeriod = {};
                                dateRangePeriod.date_from = $(this).datepicker('getDate');

                                initInternalDatepicker($pickerContainer);
                                break;

                            default:
                                dateRangePeriod = {};
                                callInternalDatepickerUpdate(this, 'clearDates');
                        }
                    }

                    updateRangeInput($popoverContent);
                    dateRangeChanged = true;
                }
            });

            $popoverContent.find('[name="date_from"], [name="date_to"]').focus(function () {
                $(this).addClass('date-focused');
                $(this).closest('.form-group').find('.date-focused').not(this).removeClass('date-focused');
            });

            updateRangeInput($popoverContent);
            initInternalDatepicker($pickerContainer);

            $popoverContent.find('.btn-clear-date').click(function () {
                var $dateInput = $(this).closest('.input-group').find('input');
                var inputName = $dateInput.attr('name');
                if (typeof dateRangePeriod[inputName] !== 'undefined') {
                    delete dateRangePeriod[inputName];
                }

                if (Object.keys(dateRangePeriod).length === 0) {
                    dateRangePeriod = {};
                }

                updateRangeInput($popoverContent);
                initInternalDatepicker($pickerContainer);

                $dateInput.focus();
            });

            $popoverContent.find('[name="date_from"], [name="date_to"]').change(function () {
                var dateValue = Date.parse($(this).val());
                if (!isNaN(dateValue)) {
                    dateRangePeriod[$(this).attr('name')] = new Date(dateValue);
                    resolveDatePeriodOrder();

                    updateRangeInput($popoverContent);
                    initInternalDatepicker($pickerContainer);
                }
            });

            $popoverContent.find('form').submit(function (e) {
                e.preventDefault();

                var filterUrl = $(this).attr('action');
                var dateFrom = $(this).find('[name="date_from"]').val();
                var dateTo = $(this).find('[name="date_to"]').val();

                if (dateFrom !== '' && dateTo !== '') {
                    if (dateFrom === dateTo) {
                        filterUrl += '/' + dateFrom;
                    } else {
                        filterUrl += '/' + dateFrom + '_' + dateTo;
                    }

                } else if (dateFrom !== '') {
                    filterUrl += '/' + dateFrom;
                } else if (dateTo !== '') {
                    filterUrl += '/' + dateTo;
                }

                if (dateFrom !== '' || dateTo !== '') {
                    document.location.href = filterUrl;
                }

            });
        });

        /**
         * charts for admin dashboard
         */
        if (
            typeof dataLogged !== 'undefined' &&
            dataComments !== 'undefined' &&
            $('#chart-logged').length > 0 &&
            $('#chart-comments').length > 0
        ) {
            function drawCharts() {
                // logged charts
                var dataTableLogged = google.visualization.arrayToDataTable(dataLogged);
                var optionsLogged = {
                    legend: 'bottom',
                };
                var chartLogged = new google.visualization.AreaChart(document.getElementById('chart-logged'));
                chartLogged.draw(dataTableLogged, optionsLogged);
                if (typeof origLoggedDate !== 'undefined') {
                    google.visualization.events.addListener(chartLogged, 'select', function () {
                        var loggedSelection = chartLogged.getSelection();
                        if (loggedSelection.length === 1) {
                            if (
                                loggedSelection[0].row != null &&
                                loggedSelection[0].column != null &&
                                typeof  origLoggedDate[loggedSelection[0].row] !== 'undefined'
                            ) {
                                var queryParams = {
                                    date: origLoggedDate[loggedSelection[0].row]
                                };

                                if (loggedSelection[0].column == 1) {
                                    queryParams.type = 'user_visit';
                                } else {
                                    queryParams.type = 'user_registration';
                                }

                                var win = window.open(userStatsUrl + '?' + $.param(queryParams), '_blank');
                                win.focus();
                            }
                        }
                    });
                }

                // comments chart
                var dataTableComments = google.visualization.arrayToDataTable(dataComments);
                var optionsComments = {
                    legend: 'bottom',
                    seriesType: 'bars',
                    series: {1: {type: 'line'}}
                };
                var chartComments = new google.visualization.ComboChart(document.getElementById('chart-comments'));
                chartComments.draw(dataTableComments, optionsComments);
                if (typeof origCommentsDate !== 'undefined') {
                    google.visualization.events.addListener(chartComments, 'select', function () {
                        var commentsSelection = chartComments.getSelection();
                        if (commentsSelection.length === 1) {
                            if (
                                commentsSelection[0].row != null &&
                                commentsSelection[0].column != null &&
                                typeof  origCommentsDate[commentsSelection[0].row] !== 'undefined'
                            ) {
                                var queryParams = {
                                    date: origCommentsDate[commentsSelection[0].row]
                                };

                                if (commentsSelection[0].column == 1) {
                                    //queryParams.type = 'comments';
                                    commentsStatsUrl = commentsStatsUrl.replace('-votesup', '-comments');
                                } else {
                                    commentsStatsUrl = commentsStatsUrl.replace('-comments', '-votesup');
                                    //queryParams.type = 'votesup';
                                }

                                var win = window.open(commentsStatsUrl + '&' + $.param(queryParams), '_blank');
                                win.focus();
                            }
                        }
                    });
                }
            }

            google.charts.load('current', {'packages': ['corechart']});
            google.charts.setOnLoadCallback(drawCharts);
        }
    }

});
