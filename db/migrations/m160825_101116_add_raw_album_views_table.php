<?php

use yii\db\Migration;

class m160825_101116_add_raw_album_views_table extends Migration
{
    public $table = 'raw_album_views';
    public $ref = 'album_id';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            $this->ref => $this->integer(),
            'counted_at' => $this->timestamp()
        ], $tableOptions);

        $this->createIndex($this->ref . "_index", $this->table, $this->ref);
        $this->createIndex('viewed_index', $this->table, ['counted_at']);
        $this->addForeignKey($this->ref . "_{$this->table}_fk", $this->table, $this->ref, 'album', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey($this->ref . "_{$this->table}_fk", $this->table);
        $this->dropTable($this->table);
    }
}
