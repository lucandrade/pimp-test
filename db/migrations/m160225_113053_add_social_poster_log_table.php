<?php

use yii\db\Migration;
use yii\db\Schema;

class m160225_113053_add_social_poster_log_table extends Migration
{
    public $table = 'social_poster_log';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable($this->table, [
            'url' => Schema::TYPE_STRING,
            'social_platform' => Schema::TYPE_STRING,
            'posted_at' => Schema::TYPE_TIMESTAMP
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->table);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
