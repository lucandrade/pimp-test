<?php

namespace app\models;

use yii\db\ActiveRecord;

class JobQueue extends ActiveRecord
{
    public static function tableName()
    {
        return "JobQueue";
    }

    public function rules()
    {
        return [
            ['command', 'string', 'max'=>256],
        ];
    }

    public static function addJob($command)
    {
        $jobQueue = new self;
        $jobQueue->command = $command;
        $jobQueue->save();
        return $jobQueue;
    }


    public static function isInQueue($command)
    {
        return !empty(self::find()->where('command=:command',[':command'=>$command])->asArray()->select('id')->one());
    }


    public static function addedInPeriod($cmd, $period, $timeUnit = "SECOND")
    {
        return self::find()
            ->where("command=:cmd AND added_to_queue_at > DATE_SUB(NOW(), INTERVAL :period $timeUnit)",
                [':cmd'=>$cmd,':period'=>$period])
            ->asArray()->select('id')->count();
    }

}