<?php
/**
 * @var $this \yii\web\View
 * @var $models Notification[]
 * @var $notificationCount integer
 */

use app\assets\NotificationAsset;
use app\models\Notification;
use yii\helpers\Url;

NotificationAsset::register($this);
$this->registerJs('var notificationCount = ' . $notificationCount . ';', $this::POS_HEAD);
$this->registerJs('var activeNotificationTime = ' . time() . ';', $this::POS_HEAD);
$this->registerJs('var updateNotificationUrl = "' . Url::to(['/notification/get-update']) . '";', $this::POS_HEAD);
?>
<ul class="<?= $menuClass ?>">
    <li class="header-notification dropdown">
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <span class="h3<?= $notificationCount ? ' text-danger' : ''; ?>">
                <i class="fa fa-bell"></i>
            </span>
            <?php if ($notificationCount) { ?>
                <span class="label label-danger">
                    <?= $notificationCount ?>
                </span>
            <?php } ?>
        </a>
        <ul class="dropdown-menu">
            <li class="dropdown-header">
                <p class="pull-right">
                    <a href="<?= Url::to(['notification/mark-as-read']) ?>" class="mark-notifications">
                        Mark all as read
                    </a>
                </p>
                <p>Notifications</p>
            </li>
            <li>
                <ul class="list-unstyled list-notification">
                    <?php foreach ($models as $model) { ?>
                        <li class="divider"></li>
                        <li class="post-notification bg-default">
                            <div class="row">
                                <div class="col-xs-3">
                                    <?php if(isset($model->caller)): ?>
                                        <a href="<?= $model->caller->getProfileUrl() ?>">
                                            <img src="<?= $model->caller->avatar ?>" class="img-responsive">
                                        </a>
                                    <?php else: ?>
                                        <?= \yii\helpers\Html::img("@web/static/i/anon_user.png", ['class' => 'img-responsive']) ?>
                                    <?php endif; ?>
                                </div>
                                <div class="col-xs-9">
                                    <p><?= $model->notice ?></p>
                                    <small class="text-muted">
                                        <?= Yii::$app->formatter->asRelativeTime($model->created_at) ?>
                                    </small>
                                </div>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            </li>
            <?php if (count($models) < $notificationCount) { ?>
                <li class="dropdown-header">
                    <p class="text-center">
                        <a href="<?= Url::to(['notification/show-more']) ?>"
                           data-time-end="<?= time() ?>"
                           data-offset="<?= count($models) ?>"
                           class="show-more-notifications btn btn-xs btn-default">
                            <i class="fa fa-spinner fa-pulse" hidden="hidden"></i>
                            Show more
                        </a>
                    </p>
                </li>
            <?php } ?>
        </ul>
    </li>
</ul>