<?php $isLargeLayout = !isset($isLargeLayout) ? true : $isLargeLayout;
$displaySorter = isset($displaySorter) ? $displaySorter : false;
$sorterLayout = $displaySorter ? "{sorter}<div class='clearfix'></div>" : "";
?>


<div>
    <?php
    \yii\widgets\Pjax::begin(['id'=>'pjax-listview', 'linkSelector' => false]);

    $class = 'image-block col-md-4 col-xs-4 col-xxs-6';
    if($isLargeLayout){
        $class .= ' col-lg-3';
    }
    $config = [
        'id' => 'image-list-container',
        "layout" => "$sorterLayout<div id='album-images'>{items}</div>\n <div class='clearfix'></div>{pager}{summary}",

        'itemOptions'=> ['class' => $class],
        'dataProvider' => $dataProvider,
        'itemView' => '_album_image',
        'emptyText' => false,
        'emptyTextOptions' => ['id' => 'album-images'],
        'options' => [
            'data-removeUrl'=> \yii\helpers\Url::to(['image/delete'])
        ],
        'pager' => [
            'class' => \app\widgets\LinkPager::className(),
            'countOnPage' => $dataProvider->getCount(),
        ],
        'summary' => false
    ];

    if($displaySorter)
    {
        $config['sorter'] = [
            'class' => \app\widgets\DropdownSorter::className(),
            'userSortSetting' => \app\models\UserSettings::NAME_IMAGES_SORTING,
            'attributes' => [
                'title',
                'Size' => 'size',
                'Uploaded' => 'createTime',
                'Modified' => 'updateTime'
            ]
        ];
    }



    if(!empty($hoverContent)){
        $config['viewParams'] = ['hoverContent' => $hoverContent];
    }

    echo \app\components\widgets\ListView::widget($config);

    \yii\widgets\Pjax::end();
    ?>

</div>