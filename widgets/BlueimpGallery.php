<?php

namespace app\widgets;


use yii\base\Widget;

class BlueimpGallery extends Widget
{
    public function run(){
        return $this->render('BlueimpGallery');
    }
}