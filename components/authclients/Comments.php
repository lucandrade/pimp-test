<?php

namespace app\components\authclients;

use Yii;


use yii\helpers\Url;
use LogicException;
use yii\authclient\OAuth2;
use app\components\behaviors\ConsoleAuthClientBehavior;


class Comments extends OAuth2
{
    public $tokenFileDir = "@runtime/token_comments";
    public $token;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = ConsoleAuthClientBehavior::className();
        return $behaviors;
    }

    public function init()
    {
        if (isset(Yii::$app->params['commentsHost']) && !empty(Yii::$app->params['commentsHost'])) {
            $this->authUrl = 'http://' . Yii::$app->params['commentsHost'] . '/auth/auth-comments';
            $this->tokenUrl = 'http://' . Yii::$app->params['commentsHost'] . '/auth/comments-token';
        }

        if (isset(Yii::$app->params['commentsClientId']) && !empty(Yii::$app->params['commentsClientId'])) {
            $this->clientId = Yii::$app->params['commentsClientId'];
        }

        if (isset(Yii::$app->params['commentsClientSecret']) && !empty(Yii::$app->params['commentsClientSecret'])) {
            $this->clientSecret = Yii::$app->params['commentsClientSecret'];
        }

        if (is_a(Yii::$app, 'yii\web\Application')) {
            $this->returnUrl = Url::to(['admin/auth-comments-success'], true);
        }
    }

    protected function initUserAttributes()
    {
        return $this->api('get-user', 'GET');
    }

    public function refreshToken($refreshToken = null)
    {
        if (is_null($refreshToken)) {
            if (!isset($this->token['refresh_token'])) {
                throw new LogicException(
                    'refresh token must be passed in or set as part of setAccessToken'
                );
            }
            $refreshToken = $this->token['refresh_token'];
        }

        $params = [
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
            'grant_type' => 'refresh_token',
            'refresh_token' => $refreshToken
        ];

        $response = $this->sendRequest('POST', $this->tokenUrl, $params);
        $created = time();

        if (isset($response['error'])) {
            return [];
        } else {
            $tokenParams = $response;
            $tokenParams['created'] = $created;
        }

        return $tokenParams;
    }
}

