<?php

use app\components\bootstrap\Dropdown;
use app\models\User;
use yii\bootstrap\Nav;
use app\components\bootstrap\NavBar;
use app\widgets\Notifications;
use app\helpers\Html;

NavBar::begin(
    [
        'id' => 'main-navbar',
        'brandLabel' => '',
        'options' => [
            'class' => 'navbar navbar-inverse navbar-fixed-top'
        ],
    ]
);
?>

<?php
$searchForm = $this->render('_search_form_navbar');
$hidealbleItems = [
        [
            'label' => 'Upload',
            'options' => [
                'class' => 'special-item uploader-trigger'
            ]
        ],
        [
            'label' => 'Images',
            'url' => ['album/my']
        ],
        [
            'label' => 'Forum',
            'url' => '/forum'
        ],
        [
            'label' => 'Posts',
            'url' => ['post/index']
        ],
];

echo Nav::widget(
    [
        'id' => 'header-menu',
        'items' => $hidealbleItems,
        'options' => ['class' => 'navbar-nav'],
    ]
);

$hidealbleItems[] =  '<li class="divider"></li>';
$hidealbleItems[] =  $searchForm;

?>

<?php

if (Yii::$app->user->isGuest) {


    echo Nav::widget(
        [
            'encodeLabels' => false,
            'options' => ['class' => 'navbar-nav navbar-right guest-nav'],

            'items' => [
                '<li class="divider"></li>',
                [
                    'label' => '<i class="fa fa-sign-in"></i> Sign-in',
                    'url' => Yii::$app->user->loginUrl,
                ],
            ],
        ]
    );

    echo Nav::widget(
        [
            'id' => 'hided-menu',
            'encodeLabels' => false,
            'options' => ['class' => 'nav navbar-nav'],
            'items' => [
                [
                    'label' =>' <i class="fa fa-navicon"></i>',
                    'url' => '#',
                    'options' => ['class' => 'button-icon'],
                    'items' => $hidealbleItems
                ],
            ],
        ]
    );
} else {
    /**
     * @var $user User
     */
    $user = Yii::$app->user->identity;

    $userMenuItems = array_merge(

        $user->is_admin ? [
            [
                'label' => '<i class="fa fa-gear"></i> Administrator',
                'url' => ['admin/index']
            ],
        ] : [],
        [
            [
                'label' => $user->username,
            ],
            '<li class="divider"></li>',
            [
                'label' => '<i class="fa fa-bar-chart"></i> Rating: ' . $user->rating,
            ],
            [
                'label' => '<i class="fa fa-sticky-note fa-wf"></i> My images',
                'url' => ['album/my']
            ],
            [
                'label' => '<i class="fa fa-copyright"></i> My posts',
                'url' => ['post/index', 'type' => 'my']
            ],
            [
                'label' => '<i class="fa fa-file-archive-o"></i> My archives',
                'url' => ['archive/list']
            ],
            [
                'label' => '<i class="fa fa-heart"></i> Favorite posts',
                'url' => ['post/index', 'type' => 'favorites']
            ],
            [
                'label' => '<i class="fa fa-plus"></i> Add post',
                'url' => ['post/update']
            ],
            [
                'label' => '<i class="fa fa-edit fa-wf"></i> Settings',
                'url' => ['user/settings']
            ],
            [
                'label' => '<i class="fa fa-television fa-wf"></i> Public profile',
                'url' => ['user/profile', 'id' => $user->id]
            ],
            '<li class="divider"></li>',
            [
                'label' => '<i class="fa fa-sign-out"></i> Logout',
                'url' => ['site/logout']
            ]
        ]
    );

    $collapsedItems = $userMenuItems;
    array_unshift($collapsedItems, "<li class='divider'></li>");

    $widget = Yii::createObject([
        'class' => Dropdown::className(),
        'encodeLabels' => false,
        'options' => ['class' => 'dropdown-menu']
    ]);

    if($user->rating <= 400)
    {
        $neededRating = $user->getNextBadgeNeededRating();
        $s = $neededRating == 1 ? "" : "s";
        $badgeTitle = "Only $neededRating point$s missing for your next badge!";
    }else{
        $badgeTitle = "You achieved maxim badge!";
    }

    $userLabel = '<span class="user-info">'
        . $user->headerAvatar .
        '</span>';

    echo Nav::widget(
        [
            'encodeLabels' => false,
            'options' => ['class' => 'nav navbar-nav navbar-right'],
            'items' => [
                [
                    'label' => $userLabel,
                    'url' => '#',
                    'options' => ['class' => 'navbar-user'],
                    'items' => $userMenuItems
                ],
            ],
        ]
    );
    echo Nav::widget(
        [
            'id' => 'hided-menu',
            'encodeLabels' => false,
            'options' => ['class' => 'nav navbar-nav'],
            'items' => [
                [
                    'label' =>' <i class="fa fa-navicon"></i>',
                    'url' => '#',
                    'options' => ['class' => 'button-icon'],
                    'items' => $hidealbleItems
                ],
            ],
        ]
    );

    echo Notifications::widget();
}

?>

<?= $searchForm ?>

<?php NavBar::end(); ?>

