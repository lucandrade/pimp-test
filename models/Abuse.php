<?php

namespace app\models;

use app\helpers\StringHelper;
use Yii;
use yii\base\InvalidConfigException;

/**
 * This is the model class for table "abuse".
 *
 * @property integer $id
 * @property string $reporterIp
 * @property string $reporterName
 * @property string $reporterText
 * @property integer $imageId
 *
 * @property Image $image
 */
class Abuse extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'abuse';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['imageId'], 'integer'],
            [['reporterIp', 'reporterName'], 'string', 'max' => 128],
            [['reporterText'], 'string', 'max' => 255],
            [['imageId'], 'exist', 'skipOnError' => true, 'targetClass' => Image::className(), 'targetAttribute' => ['imageId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reporterIp' => 'Reporter Ip',
            'reporterName' => 'Reporter Name',
            'reporterText' => 'Reporter Text',
            'imageId' => 'Image ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(Image::className(), ['id' => 'imageId']);
    }

    public static function idPatterns()
    {
        return [
            '/.*\/image\/show\?id=(\d+).*/i',
            '/.*\/ftp\/(?:[a-z0-9_]+\/)*(\d+)_/i',
            '/.*\/album\/item\?id=(\d+).*/i' => function($matches){
                if(isset($matches[1])){
                    $images = Image::findAll(['albumId'=>$matches[1]]);
                    return $images;
                }
            },
        ];
    }


    public static function findByIdPatterns($url)
    {
        $patterns = self::idPatterns();
        foreach($patterns as $index => $value)
        {
            if(is_int($index))
            {
                preg_match($value, $url, $result);
                if(isset($result[1]))
                {
                    $images = Image::find()->where(['id'=>$result[1]])->all();
                    if(!empty($images)){
                        return $images;
                    }
                }
            }elseif(is_string($index) && is_callable($value)){
                if(preg_match($index, $url, $matches)){
                    $images = call_user_func($value, $matches);
                    if(!empty($images)){
                        return $images;
                    }
                }
            }else{
                throw new InvalidConfigException("Wrong patterns format");
            }
        }

        return [];
    }


    public static function findImagesByUrl($string)
    {
        $images = self::findByIdPatterns($string);
        $images = array_merge($images, self::findByOldPatterns($string));

        return $images;
    }


    public static function findByOldPatterns($string)
    {
        preg_match('/.*\/image\/(\d+)/i', $string, $result);
        if(isset($result[1])){
            return Image::findAll(['id' => $result[1]]);
        }

        preg_match('/.*\/image\/(\d+)-.*/i', $string, $result);
        if(isset($result[1])){
            return Image::findAll(['id' => $result[1]]);
        }

        preg_match('/.*\/image\/show\/id\/(\d+)/i', $string, $result);
        if(isset($result[1])){
            return Image::findAll(['id' => $result[1]]);
        }

        preg_match('/.*\/album\/(show\/id\/)?(\d+).*/i', $string, $result);
        if(isset($result[1])){
            if($album = Album::findOne(['id'=>$result[2]]))
            {
                if($album->id == 622) {
                    return [];
                }
                $images = Image::findAll(['albumId' => $album->id]);
                return $images;
            }
            return false;
        }

        if(!isset($hash)){
            preg_match('/.*media2?\/image\/.*\/thumbs\/(\w+)_.*/i', $string, $result);
            if(isset($result[1])){
                $hash = $result[1];
            }
        }

        if(!isset($hash)){
            preg_match('/.*media2?\/image\/.*\/(\w+)_.*/i', $string, $result);
            if(isset($result[1])){
                $hash = $result[1];
            }
        }

        if(!isset($hash)){
            preg_match('/.*media2?\/image\/.*\/(\w+)\..*/i', $string, $result);
            if(isset($result[1])){
                $hash = $result[1];
            }
        }

        if(!isset($hash)){
            preg_match('/.*media2?\/simple\/\d+\/thumbs\/(\w+)_.*/i', $string, $result);
            if(isset($result[1])){
                $hash = $result[1];
            }
        }

        if(!isset($hash)){
            preg_match('/.*media2?\/simple\/\d+\/(\w+)_.*/i', $string, $result);
            if(isset($result[1])){
                $hash = $result[1];
            }
        }

        if(!isset($hash)){
            preg_match('/.*media2?\/simple\/\d+\/(\w+)\..*/i', $string, $result);
            if(isset($result[1])){
                $hash = $result[1];
            }
        }

        if(!isset($hash)){
            preg_match('~.*?ist.*?\.filesor\.com/pimpandhost\.com/.*?/.*?/.*?/.*?/.*?/.*?/.*?/.*?/.*?/(.*?)/.*?~i', $string, $result);
            if(isset($result[1])){
                $id62 = $result[1];
                $id =  StringHelper::any2dec($id62);
                if($id)
                    return Image::findAll(['id'=>$id]);
            }
        }

        if(!isset($hash)){
            preg_match('~.*?\.pimpandhost\.com/pimpandhost\.com/.*?/.*?/.*?/.*?/.*?/.*?/.*?/.*?/.*?/(.*?)/.*?~i', $string, $result);
            if(isset($result[1])){
                $id62 = $result[1];
                $id = StringHelper::any2dec($id62);
                if($id)
                    return Image::findAll(['id'=>$id]);
            }
        }


        if(isset($hash) && (strlen($hash) == 12 || strlen($hash) == 32)){
            if($images = Image::findAll(['hash' => $hash])){
                return $images;
            }
        }

        if(isset($id)){
            if($images = Image::findAll(['id'=>$id])){
                return $images;
            }
        }

        return [];
    }

    public static function createByUrl($url)
    {
        $images = self::findImagesByUrl($url);
        $ip = \Yii::$app->request->getUserIP();
        $username = !\Yii::$app->user->isGuest ? \Yii::$app->user->identity->getDisplayName() : "";
        $abuses = [];
        foreach($images as $image){
            $abuse = new self;
            $abuse->setAttributes([
                'imageId' => $image['id'],
                'reporterText' => '',
                'reporterIp' => $ip,
                'reporterName' => $username
            ]);
            if($abuse->save()){
                $abuses[] = $abuse;
            }
        }

        return $abuses;
    }


    public static function purgeAll()
    {
        ini_set('memory_limit', '9999M');
        $imageIds = self::find()->select('imageId')->column();
        $images = Image::findAll(['id' => $imageIds]);
        foreach($images as $image){
            $image->putToQueForPurging(10);
        }

        return self::deleteAll();
    }

}
