(function($){
    $.fn.albumLocker = function(method){
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method "' +  method + '" is not found in jquery.albumLocker' );
        }
    };

    var settings = {};
    var defaultSettings = {
        iconIsClicker:false,
        getAlbumId :function(button){
            return $(button).data('id');
        },
        getIsLocked: function(button){
            return $(button).data('locked');
        }
    };

    var isLocked, albumId, $btnLockAlbum, $lockForm, isInited = false;

    var methods = {
        init:function(options)
        {
            settings = $.extend(true, defaultSettings, options);
            init();
            $(this).on('click', clickHandler);
        },

        process:function(options){
            settings = $.extend(true, defaultSettings, options);
            init();
            processAlbum.call(this);
        }
    };

    var init = function(){
        if(!isInited){
            initForm();
            isInited = true;
        }
    };


    var processAlbum = function(){
        $btnLockAlbum = $(this);

        albumId = settings.getAlbumId(this);
        isLocked = settings.getIsLocked(this);

        if(isLocked)
        {
            $.ajax({
                url: $lockForm.attr('action'),
                data: {albumId:albumId},
                type: "POST",
                dataType: "json",
                success: function(res){
                    if(res.status && res.status == 'ok'){
                        var $icon = settings.iconIsClicker ? $btnLockAlbum : $btnLockAlbum.find('i.fa');
                        $icon.removeClass("fa-lock").addClass('fa-unlock');
                        $btnLockAlbum.data('locked', 0);
                        $btnLockAlbum.trigger('unlockalbum');
                    }else{
                        alert("Something was wrong while unlocking album");
                    }
                },
                fail:function(){
                    alert("Something was wrong while unlocking album");
                }
            });
        }else{
            $(' #lock-album-modal').modal('show');
        }
    };


    var clickHandler = function(e){
        e.preventDefault();
        e.stopPropagation();

        processAlbum.call(this);
    };

    var initForm = function(){

        $lockForm =  $("#album-lock-form");

        $('#btn-set-password').click(function(){

            $lockForm.data('yiiActiveForm').submitting = true;
            $lockForm.yiiActiveForm('validate');
            $(this).attr('disabled','disabled');
            return false;
        });

        $lockForm.on('submit', function(){
            return false;
        });

        $lockForm.on('afterValidate', function(event, messages, errorAttributes){

            $lockForm.data('yiiActiveForm').submitting = false;

            if(errorAttributes.length == 0){

                $.ajax({
                    url :  $lockForm.attr('action'),
                    data : { albumId:albumId, password: $lockForm.find('input[type="password"]').val()},
                    type : "POST",
                    dataType:"json",

                    success:function(res){
                        if(res.status && res.status == 'ok')
                        {
                            var $icon = settings.iconIsClicker ? $btnLockAlbum : $btnLockAlbum.find('i.fa');
                            $icon.removeClass("fa-unlock").addClass('fa-lock');
                            $btnLockAlbum.data('locked', 1);
                            $('#lock-album-modal').modal('hide');
                            $lockForm.yiiActiveForm('resetForm');
                            $lockForm.find('input').val('');
                            $btnLockAlbum.trigger('lockalbum');
                        }
                        else{
                            if(res.status && res.status == 'error'){
                                $lockForm.yiiActiveForm("updateMessages",res.messages);
                            }else{
                                alert("Something was going wrong while setting password for album");
                            }
                        }
                    },
                    fail:function(){
                        alert("Something was going wrong while setting password for album");
                    },
                    complete:function(){
                        $('#btn-set-password').removeAttr('disabled');
                    }
                });
            }
            return false;
        });
    }



})(jQuery);