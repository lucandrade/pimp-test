<?php

namespace app\assets;

use yii\web\AssetBundle;

class PhotoswipeAsset extends AssetBundle
{
	public $sourcePath = '@bower/photoswipe/dist';
	public $css = [
		'photoswipe.css',
		'default-skin/default-skin.css'
	];
    public $js = [
        'photoswipe.js',
        'photoswipe-ui-default.min.js'
    ];
}
