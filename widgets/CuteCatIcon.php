<?php

namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;

class CuteCatIcon extends Widget
{
    public $isShow = true;
    public $options = ['id' => 'cat-icon-wrapper'];
    public $dir = "/static/i/cutecats/128/";

    public function run()
    {
        if ($this->isShow) {
            $img = $this->getRandomImage();
            return Html::tag('div', Html::img($img, ['alt' => $img]), $this->options);
        }
    }


    protected function getRandomImage($mask = "*.*")
    {
        $dir = \Yii::getAlias("@app/web" . $this->dir . $mask);
        $files = glob($dir);
        $index = array_rand($files);
        $file = pathinfo($files[$index], PATHINFO_BASENAME);
        return \Yii::getAlias("@web" . $this->dir . $file);
    }


}