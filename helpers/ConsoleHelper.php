<?php
namespace app\helpers;


class ConsoleHelper
{
    public static function echoWithDate($message)
    {
        echo date("Y-m-d H:i:s") . " " . $message;
    }


    public static function log($message, $logFile = ''){

        if(empty($logFile)){
            $logFile = \Yii::getAlias("@runtime") . "/logs/console.log";
        }

        return file_put_contents(
            $logFile,
            "\n".date('Y-m-d H:i:s') . str_pad(' ',40,'=')."\n" . $message ."\n",
            FILE_APPEND
        );
    }

    public static function exec($command, $logFile = false)
    {
        $out = [];
        exec("$command 2>&1", $out);

        if(!empty($out) && !empty($logFile)){
            self::log(implode("\n", $out), $logFile);
        }

        return $out;
    }

}