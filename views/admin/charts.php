<?php

/**
 * @var $this \yii\web\View
 * @var $dateFrom string
 * @var $dateTo string
 */

use app\assets\GCExternalAsset;
use app\models\Statistic;
use yii\helpers\Json;
use yii\helpers\Url;

GCExternalAsset::register($this);
list($origLoggedDate, $loggedData) = Statistic::getLoggedData($dateFrom, $dateTo, true);
list($origCommentsDate, $commentsData) = Statistic::getCommentsData($dateFrom, $dateTo, true);

$this->registerJs('var userStatsUrl = "' . Url::to(['/admin/users-statistic'], true) . '"', $this::POS_HEAD);
$this->registerJs('var dataLogged = ' . Json::encode($loggedData), $this::POS_HEAD);
$this->registerJs('var origLoggedDate = ' . Json::encode($origLoggedDate), $this::POS_HEAD);

$commentsStatsUrl = 'http://' . Yii::$app->params['commentsHost'] . Url::to(['widgets/manage-comments', 'widget_id' => Yii::$app->params['commentsClientId']]);
$this->registerJs('var commentsStatsUrl = "' . $commentsStatsUrl . '"', $this::POS_HEAD);
$this->registerJs('var dataComments = ' . Json::encode($commentsData), $this::POS_HEAD);
$this->registerJs('var origCommentsDate = ' . Json::encode($origCommentsDate), $this::POS_HEAD);
?>
<?php if (count($loggedData) > 2 && count($commentsData) > 2) { ?>
    <div class="row">
        <div class="col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body text-center">
                    <div id="chart-logged"></div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body text-center">
                    <div id="chart-comments"></div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>