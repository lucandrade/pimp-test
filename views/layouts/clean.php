<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\SiteAsset;
use yii\helpers\Url;
use app\widgets\Alert;
use app\widgets\MinifyWidget;

SiteAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="robots" content="Iindex, follow, all"/>
	<meta name="revisit-after" content="1 days"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title ?: $this->context->title) ?></title>
	<?php $this->head() ?>
</head>
<body class="page-quick-sidebar-over-content page-boxed page-header-fixed">

<?php if(file_exists('alert.php')) include('alert.php');?>

<?php $this->beginBody() ?>

<?= $content ?>

<?php $this->endBody() ?>

<?php if(file_exists('counters.php')) include('counters.php');?>

</body>
</html>
<?php $this->endPage() ?>
