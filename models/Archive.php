<?php

namespace app\models;

use app\helpers\FileHelper;
use Yii;
use yii\helpers\Json;
use yii\helpers\Url;

/**
 * This is the model class for table "archive".
 *
 * @property integer $id
 * @property string $title
 * @property string $request
 * @property string $filename
 * @property integer $status
 * @property string $updateTime
 * @property string $createTime
 * @property integer $imageCount
 * @property integer $size
 * @property integer $authorId
 *
 * @property User $author
 */
class Archive extends \yii\db\ActiveRecord
{

    const STATUS_NORMAL    = 0;
    const STATUS_ARCHIVED  = 1;
    const STATUS_ERROR = 2;

    const SCENARIO_ADDING_TO_QUEUE = 'adding_to_queue';


    const EXPIRE_PERIOD = 1*24*60*60;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'archive';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'required'],
            [['filename'], 'required', 'except' => self::SCENARIO_ADDING_TO_QUEUE],
            [['request'], 'string'],
            [['request'], 'required', 'on' => self::SCENARIO_ADDING_TO_QUEUE],
            [['status', 'imageCount', 'size', 'authorId'], 'integer'],
            [['updateTime', 'createTime'], 'safe'],
            [['title', 'filename'], 'string', 'max' => 255],
            [['authorId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['authorId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'request' => 'Request',
            'filename' => 'Filename',
            'status' => 'Status',
            'updateTime' => 'Update Time',
            'createTime' => 'Create Time',
            'imageCount' => 'Image Count',
            'size' => 'Size',
            'authorId' => 'Author ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'authorId']);
    }


    public function getRequest()
    {
        return Json::decode($this->request);
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord){
            $this->createTime = date('Y-m-d H:i:s');
        }
        return parent::beforeSave($insert);
    }



    public static function rootZipFolder()
    {
        return \Yii::getAlias("@app/web/");
    }

    public function getRelativePath(){
        return "media/archives/" . $this->id . "/";
    }

    public function getZipFolder()
    {
        $dir = self::rootZipFolder() . $this->getRelativePath();
        if(!file_exists($dir)){
            FileHelper::createDirectory($dir);
        }

        return $dir;
    }


    public function removeFile()
    {
        $file = $this->getFullFileName();
        if(file_exists($file) && !unlink($file))
        {
             $this->addError('filename', "Can't remove file $file, error:" . error_get_last());
             return false;
        }

        return true;
    }

    public function beforeDelete()
    {
        return parent::beforeDelete() && $this->removeFile();
    }


    public function getUser()
    {
        return $this->hasOne(User::className(),['id' => 'authorId']);
    }

    public function setFileName($filename = null)
    {
        $this->filename = $filename ? $filename : $this->getZipFileName();
    }


    public function addToQueue()
    {
        return \Yii::$app->pheanstalk->addJob("archive/index $this->id");
    }


    public function getFullFileName()
    {
        return $this->getZipFolder() .  $this->filename . ".zip";
    }

    public function getDownloadLink()
    {
        return  Url::base() . "/" . $this->getRelativePath() . $this->filename . ".zip";
    }


    public function setTitleForAlbum($albumTitle)
    {
        $title = "Archive for album '" . $albumTitle . "'";

        if(strlen($title) > 255){
            $title = substr($title, 0, 251) . "...";
        }

        $this->title = $title;
    }


    public function makeErrored($errorMessage = null)
    {
        if(!empty($errorMessage)){
            $this->title = $errorMessage;
        }
        $this->status = self::STATUS_ERROR;
        return $this->save();
    }



}
