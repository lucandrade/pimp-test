<?php

namespace app\controllers;

use \Yii;
use app\components\Controller;
use yii\filters\AccessControl;
use app\models\Tag;
use yii\web\Response;


class TagController extends Controller
{

    public $layout = 'main';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSearch($name)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $tags = Tag::find()
            ->where(['like', 'name', $name])
            ->orderBy('name')
            ->limit(50)
            ->all();
        $response = array();
        foreach ($tags as $tag) {
            $response[] = $tag->attributes;
        }
        return $response;
    }

}
