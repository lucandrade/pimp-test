<?php

namespace app\assets;

use yii\web\AssetBundle;

class PostAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/plugins/bootstrap-datepicker3.min.css',
        'css/plugins/selectize.bootstrap3.css',
        'css/plugins/medium-editor.css',
        'css/plugins/themes/bootstrap.css',
        'css/post.css',
        'css/comments.css',
        'js/smartbanner/smartbanner.css'
    ];
    public $js = [
//        'js/jplayer/jquery.jplayer.js', excluded it because this file produces error while minification
        'js/plugins/bootstrap-datepicker.js',
        'js/plugins/selectize.js',
        'js/plugins/sortable.js',
        'js/plugins/medium-editor.js',
        'js/plugins/jquery.ui.widget.js',
        'js/plugins/jquery.iframe-transport.js',
        'js/plugins/jquery.fileupload.js',
        'js/plugins/fileupload.dynamic.js',
//        'js/plugins/imagesloaded.pkgd.js',
        'js/smartbanner/settings.js',
        'js/post.js',
        'js/cookie.js',
        'js/smartbanner/smartbanner.js'
    ];
    public $depends = [
        'app\assets\SiteAsset',
        'app\assets\BootboxAsset',
        'app\assets\LightGalleryAsset',
    ];
    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}