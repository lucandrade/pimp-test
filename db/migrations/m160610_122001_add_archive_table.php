<?php

use yii\db\Migration;

class m160610_122001_add_archive_table extends Migration
{
    public $table = 'archive';
    public $userRef = 'authorId';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'request' => $this->text(),
            'filename' => $this->string()->notNull(),
            'status' => "tinyint(1) NOT NULL DEFAULT '0'",
            'updateTime' => $this->timestamp(),
            'createTime' => $this->dateTime(),
            'imageCount' => $this->integer()->notNull()->defaultValue(0),
            'size' => $this->integer()->notNull()->defaultValue(0),
            $this->userRef => $this->integer(11),
        ], $tableOptions);

        $this->createIndex($this->userRef . "_index", $this->table, $this->userRef);
        $this->addForeignKey($this->userRef . "_{$this->table}_fk", $this->table, $this->userRef,'user', 'id');
    }

    public function down()
    {
        $this->dropForeignKey($this->userRef . "_{$this->table}_fk", $this->table);
        $this->dropTable($this->table);
    }
}
