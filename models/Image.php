<?php

namespace app\models;

use app\components\ActiveRecord;
use app\components\behaviors\AlbumImageCommon;
use app\components\behaviors\FtpImageFile;
use app\components\behaviors\HasOwner;
use app\helpers\FileHelper;
use app\helpers\LogHelper;
use app\models\interfaces\CommentsServiceModel;
use Yii;
use app\helpers\ImageHelper;
use yii\helpers\Url;
use app\models\behaviors\Rating;

/**
 * This is the model class for table "image".
 *
 * @property integer $id
 * @property string $title
 * @property string $fileName
 * @property string $hash
 * @property integer $status
 * @property string $updateTime
 * @property string $createTime
 * @property integer $commentCount
 * @property integer $size
 * @property integer $biggestSize
 * @property integer $isGif
 * @property integer $isAdult
 * @property string $removeText
 * @property integer $server
 * @property string $ip
 * @property string $country
 * @property integer $authorId
 * @property integer $albumId
 * @property string $purgedDate
 *
 * @property Album $album
 * @property User $author
 */
class Image extends ActiveRecord implements CommentsServiceModel
{
    const STATUS_NORMAL = 0;
    const STATUS_DELETED = 1;
    const STATUS_PRIVATE = 2;
    const STATUS_DELETED_FOREVER = 3;
    const STATUS_PROCESSING = 5;


    const SIZE_ORIGINAL = 0;
    const SIZE_SMALL = 1;
    const SIZE_MEDIUM = 2;


    const SIZE_0_PX = 250;
    const SIZE_SMALL_PX = 500;
    const SIZE_MEDIUM_PX = 750;



    const SCENARIO_CREATING = 'creating';
    const SCENARIO_PURGING = 'purging';

    const IMAGE_FOLDER = 'uploaded_images';

    const MAX_FILE_SIZE = 5000000;

    const THUMB_SUFFIX = '_thumb';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'image';
    }


    public function afterSave($insert, $changedAttributes)
    {
        if($insert ||
            (isset($changedAttributes['status'])
                && ($this->status === self::STATUS_DELETED
                    || $this->status === self::STATUS_NORMAL
                )
            )
        ){
            $this->recalculateAlbumStats();
        }

        parent::afterSave($insert, $changedAttributes);
    }


    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['ownerBehaviour'] = [
            'class' => HasOwner::className()
        ];

        $behaviors['albumImageCommon'] = [
            'class' => AlbumImageCommon::className(),
        ];

        $behaviors['ftpFile'] = [
            'class' => FtpImageFile::className(),
            'storeFolder' => self::IMAGE_FOLDER,
            'processRoute' => 'picture/process',
            'serversForUpload' => \Yii::$app->params['serversForImages']
        ];

        $behaviors['rating'] =[
            'class' => Rating::className()
        ];

        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'fileName', 'hash', 'server', 'ip', 'authorId'], 'required'],
            ['ip', 'filter', 'filter' => function($value){
                if(is_string($value))
                {
                    if($longIp = ip2long($value)){
                        return $longIp;
                    }else{
                        $this->addError("ip", "Can't convert $value to long ip");
                    }
                }

                return $value;
            }, 'on'=>self::SCENARIO_CREATING],

            [['commentCount', 'size', 'biggestSize', 'isAdult', 'server', 'ip', 'totalViews', 'todayViews'], 'integer'],
            ['isGif', 'boolean'],
            ['status', 'in', 'range' => [self::STATUS_PROCESSING, self::STATUS_NORMAL, self::STATUS_DELETED,  self::STATUS_PRIVATE], 'on' => 'default'],
            [['updateTime', 'createTime'], 'safe'],
            [['title', 'fileName', 'hash'], 'string', 'max' => 255],
            [['removeText'], 'string', 'max' => 100],
            [['country'], 'string', 'max' => 2],
            [['albumId'], 'exist', 'skipOnError' => true, 'targetClass' => Album::className(), 'targetAttribute' => ['albumId' => 'id']],
            [['authorId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['authorId' => 'id']],

            [['purgedDate'], 'safe', 'on' => self::SCENARIO_PURGING],
            [['status'], 'in','range' => [self::STATUS_DELETED_FOREVER],  'on' => self::SCENARIO_PURGING],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'fileName' => 'File Name',
            'hash' => 'Hash',
            'status' => 'Status',
            'updateTime' => 'Update Time',
            'createTime' => 'Create Time',
            'commentCount' => 'Comment Count',
            'size' => 'Size',
            'biggestSize' => 'Biggest Size',
            'isGif' => 'Is Gif',
            'isAdult' => 'Is Adult',
            'removeText' => 'Remove Text',
            'server' => 'Server',
            'ip' => 'Ip',
            'country' => 'Country',
            'authorId' => 'Author ID',
            'albumId' => 'Album ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlbum()
    {
        if(empty($this->albumId)){
            return Album::createUnsorted($this->authorId);
        }
        return $this->hasOne(Album::className(), ['id' => 'albumId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'authorId']);
    }


    public static function createdFromUploadedFile($file, $requestAttributes, $tmpFileName = null)
    {
        $image = new self;
        $image->setScenario(self::SCENARIO_CREATING);
        $image->title = pathinfo($file['original_name'], PATHINFO_FILENAME);
        $image->hash = md5($file['original_name']);
        $image->fileName = FileHelper::clearFilename($file['original_name']);
        $image->createTime = date('Y-m-d H:i:s');
        $image->size = $file['size'];
        $image->status = self::STATUS_PROCESSING;
        $image->setAttributes($requestAttributes);
        $image->setServer();
        if(!$image->saveFile($file, $tmpFileName)){
            LogHelper::log("Error:" . json_encode($image->getErrors()), "@runtime/logs/", 'upload_errors.log');
        }
        $image->putToFtpUploadingQueue();
        return $image;
    }


    public function setServer($server = null)
    {
        if($server){
            $this->server = $server;
        }else{
            $this->server = $this->pickServer(); //rework when there will be several ftp servers to upload
        }
    }

    public function saveFile($file, $tempFullName = null, $copy = false)
    {
        if($this->isNewRecord || empty($this->id)){
            $this->save();
        }

        if(!$tempFullName){

            if(!empty($file['error'])){
                $this->addError('fileName', $file['error']);
                return false;
            }

            if(empty($file['name']) || empty($file['dynamicPath'])){
                $this->addError('fileName', "Params 'name' and 'dynamicPath can't be empty while file saving");
                return false;
            }


            $tempName = $file['dynamicPath'] . "/" . $file['name'];
            $tempFullName = ImageHelper::getTmpImagePath(false) . $tempName;
        }else{
            $tempName = pathinfo($tempFullName, PATHINFO_BASENAME);
        }

        if(!file_exists($tempFullName))
        {
            $this->addError('fileName', "Temp file '$tempFullName' not found");
            return false;
        }

        $newFullName = $this->getFullFileName();

        if (
            ($copy && copy($tempFullName, $newFullName)) ||
            rename($tempFullName, $newFullName)
        ) {

            if(file_exists($tempFullName)){
                unlink($tempFullName);
            }

            if(file_exists($newFullName))
            {
                LogHelper::log("File $newFullName created!", "@runtime/logs/", 'file_upload_created.log');
                $this->setBiggestSize($newFullName);
                return $this->save();
            }
            else{
                $this->addError('fileName', "File was not properly copied to new path");
                return false;
            }
        }
        else{
            $this->addError('file_name', 'Failed move avatar file(class ' . __CLASS__ . ' line ' . __LINE__ . ')');
            return false;
        }
    }


    public function softDelete()
    {
        $this->status = self::STATUS_DELETED;
        return $this->save();
    }

    public function isBelongsTo($userId)
    {
        return $this->authorId == $userId;
    }

    public function moveToAlbum($albumId, $runValidation = true)
    {
        $this->albumId = $albumId;
        $this->status = self::STATUS_NORMAL;
        return $this->save($runValidation);
    }

    public function getNextInAlbum($queryMethod = 'one', $select = 'id')
    {
        return $this->getNeighbor(">", SORT_ASC, $queryMethod, $select);
    }

    public function getPrevInAlbum($queryMethod = 'one', $select = 'id')
    {
        return  $this->getNeighbor("<", SORT_DESC, $queryMethod, $select);
    }


    public function getNeighbor($sign, $sort, $queryMethod = 'one', $select = '*')
    {
        $commonCondition = ['albumId' => $this->albumId, 'status' => self::STATUS_NORMAL];
        $image = self::find()
            ->select($select)
            ->asArray()
            ->where("id $sign :id",[':id' => $this->id])
            ->andWhere($commonCondition)
            ->orderBy(['id' => $sort])->limit(1)->$queryMethod();

        if(!$image){
            $image = self::find()
                ->select($select)
                ->asArray()
                ->where($commonCondition)
                ->andWhere('id<>:id', [':id'=>$this->id])
                ->orderBy(['id' => $sort])->limit(1)
                ->$queryMethod();
        }
        return $image;
    }

    public function getIsDeleted()
    {
        return in_array($this->status, [self::STATUS_DELETED, self::STATUS_DELETED_FOREVER]);
    }


    public function getShareCodes(){

        return [
            'url' => Url::to(['image/show', 'id'=>$this->id], true),
            'src' => [
                Image::SIZE_0_PX => $this->getHttpFtpFileName(Image::SIZE_0_PX, false),
                Image::SIZE_SMALL_PX => $this->getHttpFtpFileName(Image::SIZE_SMALL_PX, false),
                Image::SIZE_MEDIUM_PX => $this->getHttpFtpFileName(Image::SIZE_MEDIUM_PX, false),
            ],
        ];
    }

    public function getSizeInKb()
    {
        return round($this->size/1000);
    }


    public function getExtension()
    {
        $parts = explode('.', $this->fileName);
        return end($parts);
    }

    public function getOriginalName(){
        return $this->title . "." . $this->extension;
    }


    public static function emptyTrash($userId)
    {
        $images = self::find()
            ->where(['status'=>self::STATUS_DELETED , 'authorId' => $userId])
            ->all();

        $removedImages = [];

        foreach($images as $image){
            if($image->putToQueForPurging()){
                $removedImages[] = $image->id;
            }
        }

        return $removedImages;
    }

    public function putToQueForPurging($priority = 10)
    {
        $this->setScenario(self::SCENARIO_PURGING);
        $this->status = self::STATUS_DELETED_FOREVER;
        $this->save();
        return \Yii::$app->pheanstalk->addJob("picture/purge $this->id", 0, $priority);
    }


    public function restore()
    {
        $this->status = self::STATUS_NORMAL;
        return $this->save();
    }



    public function purge()
    {
        $purgeFromDb = true;
        if(!isset(\Yii::$app->params['deleteFromStorage']) || \Yii::$app->params['deleteFromStorage']){
            $purgeFromDb = $this->removeFiles();
        }

        if($purgeFromDb)
        {
            $this->setScenario(self::SCENARIO_PURGING);
            if($this->status !== self::STATUS_DELETED_FOREVER){
                $this->status = self::STATUS_DELETED_FOREVER;
            }

            $this->purgedDate = date('Y-m-d H:i:s');
            return $this->save();
        }else{
            $this->addError('fileName', 'File was not successfully removed from storage');
            return false;
        }
    }

    public function getOwnerId()
    {
        return $this->authorId;
    }

    public function getNotificationType()
    {
        return Notification::PARENT_MODEL_IMAGE;
    }

    /**
     * @param Image[] $images
     * @return array;
     */
    public static function extractShareCodes($images)
    {
        $codes = [];
        foreach($images as $image){
            $codes[] = $image->getShareCodes();
        }

        return $codes;
    }


    public function getUrl($scheme = false)
    {
        return Url::to(['image/show', 'id'=>$this->id], $scheme);
    }

    public function getIsActive()
    {
        return in_array($this->status, [self::STATUS_NORMAL, self::STATUS_PROCESSING]);
    }


    public function getIsPrivate()
    {
        return $this->status == self::STATUS_PRIVATE;
    }


    public function recalculateAlbumStats()
    {
        $album = $this->album;
        if(!$album->getIsUnsorted()){
            return $album->recalculateStats();
        }else{
            return true;
        }
    }


}
