<?php
    use app\widgets\ShareCodes;
    use app\models\Image;
?>
<div id="share-codes">

    <div id="high-content" style="display: none">
        <div>Album link:</div>
        <div class="row">
            <div class="col-xs-12">
                <div class="input-group">
                    <input type="text" id="album-link" class="form-control clipboard-target">
                    <span class="input-group-btn">
                        <button
                            type="button"
                            class="btn btn-default clipboard-btn"
                            data-clipboard-target="#album-link"
                            data-toggle="tooltip"
                            data-placement="bottom"
                            data-trigger="manual">
                            <i class="fa fa-clipboard"></i>
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div id="main-btns" class="btn-group">
        <button id="btn-links" class="btn  active">
            LINKS
        </button>
        <button id="btn-html" class="btn">
            HTML
        </button>
        <button id="btn-bb" class="btn">
            BB CODE
        </button>
    </div>

    <div id="sizes-block" class="image-sizes-wrapper" style="display: none">
        <span class="thumbs-label">Thumbs sizes:</span>
        <div class="btn-group image-sizes row">
            <button id="btn-small" class="btn" data-size="<?= Image::SIZE_0_PX ?>">
                Small
            </button>
            <button id="btn-medium" class="btn" data-size="<?= Image::SIZE_SMALL_PX ?>">
                Medium
            </button>
            <button id="btn-large" class="btn active" data-size="<?= Image::SIZE_MEDIUM_PX ?>">
                Large
            </button>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <textarea
                class="textarea clipboard-target clipboard-target-textarea"
                id="site-codes"
                contenteditable="true"></textarea>
            <button
                data-toggle="tooltip"
                data-placement="bottom"
                data-trigger="manual"
                class="btn btn-default btn-sm btn-clipboard btn-clipboard-textarea"
                type="button"
                data-clipboard-target="#site-codes">
                <i class="fa fa-clipboard"></i>
            </button>
        </div>
    </div>
</div>
