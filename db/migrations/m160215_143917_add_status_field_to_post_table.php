<?php

use app\models\Post;
use yii\db\Migration;

class m160215_143917_add_status_field_to_post_table extends Migration
{
    public $table = 'post';
    public $col = 'status';
    public function up()
    {
        $this->addColumn($this->table, $this->col, $this->smallInteger()->defaultValue(Post::STATUS_ACTIVE));
    }

    public function down()
    {
        $this->dropColumn($this->table, $this->col);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
