<?php

namespace app\controllers;

use Yii;
use app\components\Controller;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Response;
use app\models\Notification;

class NotificationController extends Controller
{
    public $layout = 'main';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'mark-as-read' => ['post'],
                    'get-update' => ['get'],
                ],
            ],
        ];
    }

    public function actionMarkAsRead()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $lastTime = Yii::$app->request->post('lastTime');

        if (!is_numeric($lastTime)) {
            return [
                'status' => 'error',
                'message' => 'Wrong data format'
            ];
        }

        Notification::updateAll(
            ['status' => Notification::STATUS_READED],
            'owner_id = :owner_id AND status = :status AND created_at <= :created_at',
            [
                ':owner_id' => Yii::$app->user->id,
                ':status' => Notification::STATUS_ACTIVE,
                ':created_at' => date('Y-m-d H:i:s', (int)$lastTime)
            ]
        );

        return [
            'status' => 'success',
            'lastTime' => $lastTime
        ];
    }

    public function actionGetUpdate($lastTime = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $notificationQuery = Notification::getActiveNotificationQuery();
        if (is_numeric($lastTime)) {
            $timeDiff = time() - (int)$lastTime;
            $notificationQuery->andWhere([
                '>',
                'created_at',
                new Expression('NOW() - INTERVAL :timeDiff SECOND', [':timeDiff' => $timeDiff])
            ]);
        }
        $models = $notificationQuery->all();
        return [
            'status' => 'success',
            'activeNotificationTime' => time(),
            'activeNotificationCount' => count($models),
            'notificationList' => $this->renderAjax('get_update', ['models' => $models])
        ];
    }

    public function actionShowMore($time_end, $offset)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $notificationQuery = Notification::getActiveNotificationQuery();
        $notificationQuery->andWhere(['<', 'created_at', date('Y-m-d H:i:s', (int)$time_end)]);
        $notificationQuery->limit(Notification::$batchSize);
        $notificationQuery->offset($offset);
        $models = $notificationQuery->all();
        return [
            'status' => 'success',
            'activeNotificationCount' => count($models),
            'notificationList' => $this->renderAjax('get_update', ['models' => $models])
        ];
    }
}
