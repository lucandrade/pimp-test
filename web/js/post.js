onYouTubeIframeAPIReady = function () {
    setTimeout(function () {
        onYouTubeIframeAPIReadyDeferred();
    }, 600);
};

/* Post outputs */
$(function () {


    function clone(obj) {
        var copy;
        // Handle the 3 simple types, and null or undefined
        if (null == obj || "object" != typeof obj) return obj;
        // Handle Date
        if (obj instanceof Date) {
            copy = new Date();
            copy.setTime(obj.getTime());
            return copy;
        }
        // Handle Array
        if (obj instanceof Array) {
            copy = [];
            for (var i = 0, len = obj.length; i < len; i++) {
                copy[i] = clone(obj[i]);
            }
            return copy;
        }
        // Handle Object
        if (obj instanceof Object) {
            copy = {};
            for (var attr in obj) {
                if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
            }
            return copy;
        }
        throw new Error("Unable to copy obj! Its type isn't supported.");
    }

    var maxPostHeight = 450;
    var postExpandHeight = 900;
    var postExpandBottom = '<div class="extend-wrapper">'
        + '<a class="btn btn-expand-post post-content-expand" title="extend post">'
        + '<span class="post-expand extend"><i class="fa fa-bars mybar"></i> Extend</span>'
        + '<span class="post-expand-images extend"><i class="fa fa-picture-o mybar"></i><span class="expand-counter">0</span></span>'
        + '<span class="post-expand-videos extend"><i class="fa fa-caret-square-o-right mybar"></i><span class="expand-counter">0</span></span>'
        + '</a>';
    +'</div>';


    function countExpandBlocks($post) {
        var expandBlocks = {images: 0, videos: 0, text: 0};

        $post.find('.post-block').each(function () {
            if ($(this).position().top + $(this).outerHeight() > maxPostHeight) {
                if ($(this).data('type') === 'image') {
                    expandBlocks.images++;
                } else if ($(this).data('type') === 'video') {
                    expandBlocks.videos++;
                } else if ($(this).data('type') === 'text') {
                    expandBlocks.text++;
                }
            }
        });

        return expandBlocks;
    }

    function initYoutubeIframe($iframe) {
        var youtubeWidth = 560;
        var youtubeHeight = 315;
        $iframe.each(function () {
            this.height = parseInt(youtubeHeight * $('.post-block').closest('.list-view').width() / youtubeWidth);

            var $ytThumb = $('#ytThumb-' + $(this).data('videoid'));
            $ytThumb.css('background-image', 'url("' + $ytThumb.data('thumb-src') + '")');
            $ytThumb.height(this.height);
        });
    }

    function initFacebookVideo($video) {
        var facebookWidth = 750;
        var facebookHeight = 420;
        var height = parseInt(facebookHeight * $('.post-block').closest('.list-view').width() / facebookWidth);
        $video.closest('.fb-video-wrapper').css('height', height + 'px');
    }

    var ytPlayers = [];
    var initializedPlayers = {};
    var ytPlayerReady = false;
    $.fn.initYTPlayer = function (callback) {
        if (ytPlayerReady) {
            // when video ready
            function onPlayerReady(event) {
                var $ytPlayer = $(event.target.getIframe());
                initYoutubeIframe($ytPlayer);
                if (typeof callback === 'function') {
                    callback();
                }
            }

            // when video ends
            function onPlayerStateChange(event) {
                if (event.data === 0) {
                    var $ytPlayer = $(event.target.getIframe());
                    var $ytThumb = $('#ytThumb-' + $ytPlayer.data('videoid'));

                    $ytPlayer.attr('hidden', 'hidden');
                    $ytThumb.removeAttr('hidden');
                }
            }

            if ($(this).length > 0) {
                return this.each(function () {
                    var playerVideoId = $(this).data('videoid');
                    var playerWidth = $(this).data('width');
                    var playerHeight = $(this).data('heght');

                    var player = new YT.Player(this, {
                        height: playerHeight,
                        width: playerWidth,
                        videoId: playerVideoId,
                        playerVars: {
                            rel: 0
                        },
                        events: {
                            'onReady': onPlayerReady,
                            'onStateChange': onPlayerStateChange
                        }
                    });
                    initializedPlayers[$(this).attr('id')] = player;

                    var $ytPlayer = $('#' + $(this).attr('id'));
                    var $ytThumb = $('#ytThumb-' + $(this).data('videoid'));

                    $ytThumb.click(function () {
                        player.playVideo();
                        $(this).attr('hidden', 'hidden');
                        $ytPlayer.removeAttr('hidden');
                    });
                });

            } else if (typeof callback === 'function') {
                callback();
            }

        } else {
            ytPlayers.push({
                playerElement: this,
                callbackFunction: callback
            });
        }
    };

    window.onYouTubeIframeAPIReadyDeferred = function () {
        ytPlayerReady = true;
        $(ytPlayers).each(function () {
            $(this.playerElement).initYTPlayer(this.callbackFunction);
        });
    };

    var autoplayPostGif = false;
    if (typeof userOptions !== 'undefined') {
        if (userOptions.hasOwnProperty('autoplay_gifs')) {
            autoplayPostGif = parseInt(userOptions.autoplay_gifs) === 1;
        }
    }

    $.fn.initGifPlayer = function () {

        if (autoplayPostGif && mobilecheck()) {
            var jPlayerInstances = this;
            window.addEventListener('touchstart', function videoStart() {
                $(jPlayerInstances).jPlayer('play');
                this.removeEventListener('touchstart', videoStart);
            });
        }

        function resizeGifPlayer(gifPlayer) {

            $(gifPlayer).each(function () {
                var playerWidth = $(this).data('width');
                var playerHeight = $(this).data('height');

                playerHeight = parseInt($(this).parent().width() * playerHeight / playerWidth);
                playerWidth = $(this).parent().width();

                var jsize = {
                    width: playerWidth + "px",
                    height: playerHeight + "px"
                };
                $(this).jPlayer('option', 'size', jsize);
            });
        }

        var that = this;
        $(window).resize(function () {
            resizeGifPlayer(that);
        });

        return this.each(function () {
            var gifSupplied = [];
            var gifMedia = {};

            gifMedia['webmv'] = $(this).data('src-orig');
            gifMedia['m4v'] = $(this).data('src-alt');
            gifMedia['poster'] = $(this).data('src-poster');
            gifSupplied.push('m4v');
            gifSupplied.push('webmv');

            var jPlayerContainer = this;
            var gifWrapper = $(jPlayerContainer).closest('.gif-player-wrapper');
            $(jPlayerContainer).jPlayer({
                ready: function () {
                    $(this).jPlayer("setMedia", gifMedia);
                },
                canplay: function () {
                    if (autoplayPostGif && !mobilecheck()) {
                        $(jPlayerContainer).jPlayer("play");
                    }
                },
                playlistOptions: {
                    autoPlay: true
                },
                supplied: gifSupplied.join(', '),
                solution: "html, flash",
                swfPath: "/js/jplayer",
                useStateClassSkin: true,
                autoBlur: false,
                smoothPlayBar: true,
                keyEnabled: false,
                remainingDuration: true,
                toggleDuration: true,
                loop: true,
                click: function () {
                    if ($(this).data().jPlayer.status.paused) {
                        $(this).jPlayer('play', 0);
                    } else {
                        $(this).jPlayer('pause');
                        $(gifWrapper).find('.i-gif-play').show();
                    }
                },
                playing: function () {
                    $(gifWrapper).find('.i-gif-play').hide();
                }
            });
            $(gifWrapper).find('.i-gif-play').click(function () {
                $(jPlayerContainer).jPlayer('play', 0);
            });
            resizeGifPlayer(jPlayerContainer);
        });
    };

    function initPostExpandHeight($expandWrapper) {

        var expandProceed = function () {
            $expandWrapper.find('.post-block').each(function () {
                if ($(this).data('type') === 'video') {
                    if ($(this).find('[data-width]').length > 0 && $(this).find('[data-height]').length > 0) {
                        //var blockHeight = parseInt($(this).find('[data-height]').data('height'));
                        //var blockWidth = parseInt($(this).find('[data-width]').data('width'));
                        //
                        //if (blockWidth > $(this).width()) {
                        //    blockHeight = blockHeight * $(this).width() / blockWidth;
                        //}

                        var blockHeight = $(this).height();
                        $(this).css({height: blockHeight + 'px', 'max-width': '100%'});
                    }
                } else if ($(this).data('type') === 'image') {
                    if ($(this).find('[width]').length > 0 && $(this).find('[height]').length > 0) {
                        var blockHeight = parseInt($(this).find('[height]').attr('height'));
                        var blockWidth = parseInt($(this).find('[width]').attr('width'));
                        if (blockWidth > $(this).width()) {
                            blockHeight = blockHeight * $(this).width() / blockWidth;
                        }
                        $(this).css({height: blockHeight.toFixed(0) + 'px', 'max-width': '100%'});
                    }
                }
            });
        };

        initFacebookVideo($('.post-block[data-type="video"] .fb-video'));
        $expandWrapper.find('[id^="gifPlayer-"]').initGifPlayer();

        if ($expandWrapper.find('[id^="ytPlayer-"]').length > 0) {
            $expandWrapper.find('[id^="ytPlayer-"]').initYTPlayer(expandProceed);
        } else {
            expandProceed();
        }
    }

    $.fn.expandPostList = function () {
        this.each(function () {
            var $post = $(this);
            initPostExpandHeight($post.find('.post-expand-wrapper'));
            var expandHeight = $post.find('.post-expand-wrapper').outerHeight();

            if (expandHeight > postExpandHeight) {
                var $postExpandBottom = $(postExpandBottom).clone();
                var expandBlocks = countExpandBlocks($post);

                if (expandBlocks.images === 0) {
                    $postExpandBottom.find('.post-expand-images').remove();
                } else {
                    $postExpandBottom.find('.post-expand-images .expand-counter').text(expandBlocks.images);
                }

                if (expandBlocks.videos === 0) {
                    $postExpandBottom.find('.post-expand-videos').remove();
                } else {
                    $postExpandBottom.find('.post-expand-videos .expand-counter').text(expandBlocks.videos);
                }

                $post.find('.post-expand-wrapper').after($postExpandBottom);
                $post.find('.post-expand-wrapper').css('max-height', maxPostHeight + 'px');

                $post.find('.btn-expand-post').click(function () {
                    $post.find('.post-expand-wrapper').attr('style', '');
                    $(this).closest('.extend-wrapper').remove();
                });

            } else {
                $post.find('.btn-expand-post').remove();
            }
        });
    };

    if ($('.posts .post').length > 0) {
        if ($('.posts').data('collapse-posts')) {
            $('.posts .post').expandPostList();
        }
    } else {
        $('[id^="gifPlayer-"]').initGifPlayer();
        $('[id^="ytPlayer-"]').initYTPlayer();
        initFacebookVideo($('.post-block[data-type="video"] .fb-video'));
    }

    $.fn.expandPostAjaxList = function () {
        var $absoluteContainer = $('<div/>').css({
            position: 'absolute',
            visibility: 'hidden',
            display: 'block'
        });
        $absoluteContainer.width($('.post').first().width());
        $absoluteContainer.appendTo('body');

        this.each(function () {
            $absoluteContainer.html('').append($(this));
            var $post = $(this);
            initPostExpandHeight($post.find('.post-expand-wrapper'));
            var expandHeight = $post.find('.post-expand-wrapper').outerHeight();

            if (expandHeight > postExpandHeight) {
                var $postExpandBottom = $(postExpandBottom).clone();
                var expandBlocks = countExpandBlocks($post);

                if (expandBlocks.images === 0) {
                    $postExpandBottom.find('.post-expand-images').remove();
                } else {
                    $postExpandBottom.find('.post-expand-images .expand-counter').text(expandBlocks.images);
                }

                if (expandBlocks.videos === 0) {
                    $postExpandBottom.find('.post-expand-videos').remove();
                } else {
                    $postExpandBottom.find('.post-expand-videos .expand-counter').text(expandBlocks.videos);
                }

                $post.find('.post-expand-wrapper').after($postExpandBottom);
                $post.find('.post-expand-wrapper').css('max-height', maxPostHeight + 'px');

            } else {
                $post.find('.btn-expand-post').remove();
            }
        });

        $absoluteContainer.remove();
    };

    $.fn.expandPostAjaxListEvent = function () {
        this.each(function () {
            var $post = $(this);
            $post.find('.btn-expand-post').click(function () {
                $post.find('.post-expand-wrapper').attr('style', '');
                $(this).remove();
            });

            $post.find('[id^="ytThumb-"]').click(function () {
                if (typeof  initializedPlayers[$post.find('iframe').attr('id')] !== 'undefined') {
                    initializedPlayers[$post.find('iframe').attr('id')].playVideo();
                    $(this).attr('hidden', 'hidden');
                    $(this).parent().find('iframe').removeAttr('hidden');
                }
            });
        });
    };

    $(window).resize(function () {
        var defaultWidth = 750;
        var defaultExpandHeight = 580;
        var wrapperWidth = $('.post-expand-wrapper').width();
        $('.post-expand-wrapper').each(function () {
            $(this).find('.post-block[data-type="video"]').css('height', 'auto');
            $(this).find('.post-block[data-type="image"]').css('height', 'auto');

            if ($(this).find('.btn-expand-post').length > 0) {
                $(this).css('max-height', parseInt(defaultExpandHeight * wrapperWidth / defaultWidth) + 'px');
            }
        });
        initYoutubeIframe($('.post-block[data-type="video"] iframe'));
        initFacebookVideo($('.post-block[data-type="video"] .fb-video'));
    });

    function mobilecheck() {
        var check = false;
        (function (a) {
            if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4)))check = true
        })(navigator.userAgent || navigator.vendor || window.opera);
        return check;
    }

    $('.choose-daterange').click(function (e) {
        e.preventDefault();
    });

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

    function getDateMonthString(date) {

        var monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];

        var dateStr = [];
        dateStr.push(monthNames[date.getMonth()]);
        dateStr.push(('0' + date.getDate()).slice(-2));
        dateStr.push(date.getFullYear());
        return dateStr.join(' ');
    }

    var dateRangeChanged = false;

    //var defaultDateRangePeriod = {
    //    date_from: new Date($('#popover-daterange [name="date_from"]').val()),
    //    date_to: new Date($('#popover-daterange [name="date_to"]').val()),
    //};
    var dateRangePeriod;

    if ($('#popover-daterange [name="date_from"]').val() === $('#popover-daterange [name="date_to"]').val()) {
        dateRangePeriod = {
            date_from: new Date($('#popover-daterange [name="date_from"]').val())
        };
    } else {
        dateRangePeriod = {
            date_from: new Date($('#popover-daterange [name="date_from"]').val()),
            date_to: new Date($('#popover-daterange [name="date_to"]').val()),
        };
    }


    function afterUpdateRangeInput($popoverContent) {
        var $rangeLink = $popoverContent.parent().find('.choose-daterange');
        var dateFromMs = Date.parse($popoverContent.find('[name="date_from"]').val());
        var dateToMs = Date.parse($popoverContent.find('[name="date_to"]').val());
        var linkText = '';
        if (!isNaN(dateFromMs) && !isNaN(dateToMs)) {
            var dateFrom = new Date(dateFromMs);
            var dateTo = new Date(dateToMs);

            if (dateFromMs === dateToMs) {
                linkText += formatDate(dateFrom) === formatDate(new Date()) ? 'today' : getDateMonthString(dateFrom);
                $rangeLink.text(linkText);
            } else {
                linkText += getDateMonthString(dateFrom);
                linkText += ' - ';
                linkText += getDateMonthString(dateTo);
                $rangeLink.text(linkText);
            }

        } else if (!isNaN(dateFromMs)) {
            var dateFrom = new Date(dateFromMs);
            linkText += formatDate(dateFrom) === formatDate(new Date()) ? 'today' : getDateMonthString(dateFrom);
            $rangeLink.text(linkText);
        } else if (!isNaN(dateToMs)) {
            var dateTo = new Date(dateToMs);
            linkText += formatDate(dateTo) === formatDate(new Date()) ? 'today' : getDateMonthString(dateTo);
            $rangeLink.text(linkText);
        }

    }

    function updateRangeInputData($popoverContent, name) {
        var dateFormatted;

        if (typeof dateRangePeriod[name] === 'undefined') {
            dateFormatted = '';
        } else {
            dateFormatted = formatDate(dateRangePeriod[name]);
        }

        $popoverContent.find('form [name="' + name + '"]').val(dateFormatted);
    }

    function updateRangeInput($popoverContent) {
        updateRangeInputData($popoverContent, 'date_from');
        updateRangeInputData($popoverContent, 'date_to');

        //afterUpdateRangeInput($popoverContent);
    }

    var externalDateChanged = true;

    function callInternalDatepickerUpdate(el, callback, data) {
        externalDateChanged = false;
        $(el).datepicker(callback, data);
        externalDateChanged = true;
    }

    function initInternalDatepicker($pickerContainer) {
        callInternalDatepickerUpdate($pickerContainer, 'clearDates');

        var pickerDates = [];
        if (typeof dateRangePeriod.date_from !== 'undefined' && typeof dateRangePeriod.date_to !== 'undefined') {
            pickerDates.push(formatDate(dateRangePeriod.date_from));
            pickerDates.push(formatDate(dateRangePeriod.date_to));
        } else if (typeof dateRangePeriod.date_from !== 'undefined') {
            pickerDates.push(formatDate(dateRangePeriod.date_from));
        } else if (typeof dateRangePeriod.date_to !== 'undefined') {
            pickerDates.push(formatDate(dateRangePeriod.date_to));
        }
        callInternalDatepickerUpdate($pickerContainer, 'setDates', pickerDates);
    }


    function resolveDatePeriodOrder() {
        if (dateRangePeriod.date_to && dateRangePeriod.date_from) {
            if (dateRangePeriod.date_to.getTime() < dateRangePeriod.date_from.getTime()) {
                var dateFrom = dateRangePeriod.date_from;
                dateRangePeriod.date_from = dateRangePeriod.date_to;
                dateRangePeriod.date_to = dateFrom;
            }
        }
    }

    $('.choose-daterange').popover({
        placement: 'bottom',
        html: true,
        content: function () {
            return $('#popover-daterange').clone().html();
        }
    }).on('shown.bs.popover', function () {
        var $popoverContent = $(this).parent().find('.popover');
        var $pickerContainer = $popoverContent.find('.inline-daterange .daterange-container');

        $pickerContainer.datepicker({
            format: "yyyy-mm-dd",
            multidate: true,
            //inputs: $popoverContent.find('form [name="' + $(this).data('input-name') + '"]'),
        }).on('changeDate', function () {
            if (externalDateChanged) {
                var dateRangeName;

                if ($popoverContent.find('.date-focused').length > 0) {
                    dateRangeName = $popoverContent.find('.date-focused').attr('name');

                    switch ($(this).datepicker('getDates').length) {
                        case 0:
                            dateRangePeriod = {};
                            break;

                        case 1:

                            dateRangePeriod = {};
                            dateRangePeriod[dateRangeName] = $(this).datepicker('getDate');

                            initInternalDatepicker($pickerContainer);
                            break;
                        case 2:
                            dateRangePeriod[dateRangeName] = $(this).datepicker('getDate');

                            resolveDatePeriodOrder();
                            initInternalDatepicker($pickerContainer);
                            break;

                        case 3:
                            dateRangePeriod[dateRangeName] = $(this).datepicker('getDate');

                            resolveDatePeriodOrder();
                            initInternalDatepicker($pickerContainer);
                            break;

                        default:
                            dateRangePeriod = {};
                            callInternalDatepickerUpdate(this, 'clearDates');
                    }

                    $popoverContent.find('[name="' + dateRangeName + '"]').focus();
                } else {
                    switch ($(this).datepicker('getDates').length) {
                        case 0:
                            dateRangePeriod = {};
                            break;

                        case 1:
                            dateRangePeriod = {};
                            dateRangePeriod.date_from = $(this).datepicker('getDate');

                            initInternalDatepicker($pickerContainer);
                            break;
                        case 2:
                            var dateTo = $(this).datepicker('getDate');

                            if (dateTo.getTime() < dateRangePeriod.date_from.getTime()) {
                                dateRangePeriod.date_from = dateTo;
                            } else {
                                dateRangePeriod.date_to = dateTo;
                            }

                            //resolveDatePeriodOrder();
                            initInternalDatepicker($pickerContainer);
                            break;

                        case 3:
                            dateRangePeriod = {};
                            dateRangePeriod.date_from = $(this).datepicker('getDate');

                            initInternalDatepicker($pickerContainer);
                            break;

                        default:
                            dateRangePeriod = {};
                            callInternalDatepickerUpdate(this, 'clearDates');
                    }
                }

                updateRangeInput($popoverContent);
                dateRangeChanged = true;
            }
        });

        $popoverContent.find('[name="date_from"], [name="date_to"]').focus(function () {
            $(this).addClass('date-focused');
            $(this).closest('.form-group').find('.date-focused').not(this).removeClass('date-focused');
        });

        updateRangeInput($popoverContent);
        initInternalDatepicker($pickerContainer);

        $popoverContent.find('.btn-clear-date').click(function () {
            var $dateInput = $(this).closest('.input-group').find('input');
            var inputName = $dateInput.attr('name');
            if (typeof dateRangePeriod[inputName] !== 'undefined') {
                delete dateRangePeriod[inputName];
            }

            if (Object.keys(dateRangePeriod).length === 0) {
                dateRangePeriod = {};
            }

            updateRangeInput($popoverContent);
            initInternalDatepicker($pickerContainer);

            $dateInput.focus();
        });

        $popoverContent.find('[name="date_from"], [name="date_to"]').change(function () {
            var dateValue = Date.parse($(this).val());
            if (!isNaN(dateValue)) {
                dateRangePeriod[$(this).attr('name')] = new Date(dateValue);
                resolveDatePeriodOrder();

                updateRangeInput($popoverContent);
                initInternalDatepicker($pickerContainer);
            }
        });

        $popoverContent.find('form').submit(function (e) {
            e.preventDefault();

            var filterUrl = $(this).attr('action');
            var dateFrom = $(this).find('[name="date_from"]').val();
            var dateTo = $(this).find('[name="date_to"]').val();

            if (dateFrom !== '' && dateTo !== '') {
                if (dateFrom === dateTo) {
                    filterUrl += '/' + dateFrom;
                } else {
                    filterUrl += '/' + dateFrom + '_' + dateTo;
                }

            } else if (dateFrom !== '') {
                filterUrl += '/' + dateFrom;
            } else if (dateTo !== '') {
                filterUrl += '/' + dateTo;
            }

            if (dateFrom !== '' || dateTo !== '') {
                document.location.href = filterUrl;
            }

        });
    });

    if ($('.posts .post').length > 0) {

        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('.scroll-post-top').fadeIn();
                $('.scroll-post-bottom').fadeIn();
            } else {
                $('.scroll-post-top').fadeOut();
                $('.scroll-post-bottom').fadeOut();
            }
        });

        function currentScrollPost() {
            var $currentPost;
            $('.posts .post').each(function () {
                if ($(this).position().top < $(window).scrollTop()) {
                    $currentPost = $(this);
                }
            });
            return $currentPost;
        }

        function prevScrollPost($currentScrollPost) {
            if (typeof  $currentScrollPost === undefined) {
                $currentScrollPost = currentScrollPost();
            }
            return $currentScrollPost.closest('.item').prev('.item').find('.post');
        }

        function nextScrollPost($currentScrollPost) {
            if (typeof  $currentScrollPost === undefined) {
                $currentScrollPost = currentScrollPost();
            }
            return $currentScrollPost.closest('.item').next('.item').find('.post');
        }

        var scrollPositionOffset = 5;

        $('.scroll-post-top').click(function (e) {
            e.preventDefault();
            var $currentPost = currentScrollPost();
            /**
             * check scroll difference
             */
            var scrollDiff = $(window).scrollTop() - ($currentPost.position().top - scrollPositionOffset);
            if (scrollDiff < scrollPositionOffset * 3) {
                var $prevPost = prevScrollPost($currentPost);
                if ($prevPost.length > 0) {
                    $currentPost = $prevPost;
                }
            }

            $('html, body').animate({scrollTop: $currentPost.position().top - scrollPositionOffset}, 300);
        });

        $('.scroll-post-bottom').click(function (e) {
            e.preventDefault();
            var $currentPost = currentScrollPost();
            var $nextPost = nextScrollPost($currentPost);
            if ($nextPost.length > 0) {
                /**
                 * check scroll difference
                 */
                var scrollDiff = ($nextPost.position().top + scrollPositionOffset) - $(window).scrollTop();
                if (scrollDiff < scrollPositionOffset * 3) {
                    var $afterNextPost = nextScrollPost($nextPost);
                    if ($afterNextPost.length > 0) {
                        $nextPost = $afterNextPost;
                    }
                }

                $('html, body').animate({scrollTop: $nextPost.position().top + scrollPositionOffset}, 300);
            } else {

                $('html, body').animate({scrollTop: $currentPost.position().top + $currentPost.closest('.item').outerHeight()}, 300);
            }
        });

    }

    function notifyUser(message, url) {
        var title = 'Owned.com';
        url = url || 'http://owned.com';

        var notificationClick = function () {
            window.open(url, '_blank');
        };

        var options = {
            icon: 'static/i/logo.png',
            body: message
        };

        // Let's check if the browser supports notifications
        if (!("Notification" in window)) {
            console.log("This browser does not support desktop notification");
        }
        // Let's check whether notification permissions have already been granted
        else if (Notification.permission === "granted") {
            // If it's okay let's create a notification
            var notification = new Notification(title, options);
            notification.onclick = notificationClick;
        }

        // Otherwise, we need to ask the user for permission
        else if (Notification.permission !== 'denied') {
            Notification.requestPermission(function (permission) {
                // If the user accepts, let's create a notification
                if (permission === "granted") {
                    var notification = new Notification(title, options);
                    notification.onclick = notificationClick;
                }
            });
        }

        // At last, if the user has denied notifications, and you
        // want to be respectful there is no need to bother them any more.
    }

    $('body').on('click', '.share-to-socials', function () {
        sharePostClickHandler($(this));
        return false;
    });


    function sharePostClickHandler($me, forcePosting, platforms)
    {
        var $icon = $me.children();
        if ($me.children().hasClass('fa-spin')) {
            return false;
        }
        var data = {id: $me.parents('.post').data('page-key')};

        if(forcePosting){
            data['forcePosting'] = forcePosting;
        }
        if(platforms){
            data['platforms'] = platforms;
        }

        $me.children().addClass("fa-spin fa-spinner").removeClass("fa-share");
        $.ajax({
            url: $me.attr('href'),
            type: "POST",
            dataType: "json",
            data: data,
            success: function (response) {
                $me.children().removeClass("fa-spin fa-spinner").addClass("fa-share");
                var successPosted = [];
                var duplicates = [];
                $.each(response, function (index, value) {
                    if (value == 'ok') {
                        successPosted.push(index);
                    }
                    if(value == 'duplicate'){
                        duplicates.push(index);
                    }
                });

                if(successPosted.length > 0){
                    bootstapAlert('This post was successfully posted to ' + successPosted.join(','), $me.parent(), {right:"-95px",
                        afterHide:function(){
                            if(duplicates.length > 0){
                                if(confirm("Post was already shared to "+duplicates.join(',')+ ". Are you sure you want it shared anyway?")){
                                    sharePostClickHandler($me, true, duplicates.join(','));
                                }
                            }
                        }
                    });
                }else{
                    if(duplicates.length > 0){

                        bootboxConfirm("Post was already shared to "+duplicates.join(',')+ ". Are you sure you want it shared anyway?",
                            function(){
                                sharePostClickHandler($me, true, duplicates.join(','));
                            }
                        )
                    }else{
                        bootstapAlert("Nothing was posted to social due to errors:" + JSON.stringify(response),
                            $me.parent(), {right:"-95px", colorClass:"alert-danger"});
                    }
                }
            },
            fail: function () {
                bootstapAlert("Something was going wrong, try again later",
                    $me.parent(), {right:"-95px", colorClass:"alert-danger"});
                $me.children().removeClass("fa-spin fa-spinner").addClass("fa-share");
            }
        });
    }


    function bootstapAlert(message, $elem, options)
    {
        var defaultOptions = {
            bottom:"25px",
            minWidth:"300px",
            colorClass:"alert-success",
            fadeOutTime:3000
        };
        var settings = $.extend(defaultOptions, options);
        var $alert = $("<div/>").html(message).addClass('alert text-center').addClass(settings.colorClass)
            .css('position','absolute').css('z-index', 10).css('min-width', settings.minWidth);

        if(settings.top){
            $alert.css('top', settings.top);
        }
        if(settings.bottom){
            $alert.css('bottom', settings.bottom);
        }
        if(settings.left){
            $alert.css('left', settings.left);
        }
        if(settings.right){
            $alert.css('right', settings.right);
        }

        $elem.append($alert);
        $elem.find($alert).fadeOut(3000, function(){
            if(options.afterHide && typeof(options.afterHide) == 'function'){
                options.afterHide();
            }
            $(this).remove();
        });
    }

    function bootboxConfirm(message, ok, cancel) {
        bootbox.dialog({
            message: message,
            title: "Confirm",
            buttons: {
                success: {
                    label: "OK",
                    className: "btn-success",
                    callback: ok
                },
                danger: {
                    label: "Cancel",
                    className: "btn-danger",
                    callback: cancel
                }
            }
        });
    }

});

/* Post updates */
$(function () {
    if ($('#editable').length > 0 && typeof postBlockTypes !== 'undefined') {

        var draftDelay = 5000;
        var draftInfoStart;
        var draftTimer = true;
        var draftInfoInterval = true;

        var listItemIndex = 0;
        var postBlockIndex = typeof startBlockIndex === 'undefined' ? 0 : startBlockIndex;

        var $imgFileInputBlock = $('<input name="loadedImages" type="file" multiple="multiple" class="file-control" accept="image/jpeg,image/png,image/gif" hidden="hidden">');

        var $postManage = $('.post-manage').clone();
        $postManage.removeClass('post-manage');

        var textField = '<div class="form-group">'
            + '<label for="" class="control-label" hidden="hidden">Text</label>'
            + '<input type="hidden" name="PostBlock[_i_][type]"  value="' + postBlockTypes.text + '" class="form-control">'
            + '<input type="hidden" name="PostBlock[_i_][position]" value="" class="form-control">'
                /* Medium-editor js */
            + '<div class="medium-eitor">'
            + '<div class="medium-editor-content"><p></p></div>'
            + '</div>'

            + '<textarea name="PostBlock[_i_][text]" class="form-control" rows="2" placeholder="Enter text..." hidden="hidden"></textarea>'
            + '<div class="help-block"></div>'
            + '</div>';

        var imageEmptyField = '<div class="form-group">'
            + '<label for="" class="control-label" hidden="hidden">Image</label>'
            + '<input type="hidden" data-postblock-name="PostBlock[_i_][id]" value="" class="form-control">'
            + '<input type="hidden" data-postblock-name="PostBlock[_i_][type]" value="' + postBlockTypes.image + '" class="form-control">'
            + '<input type="hidden" data-postblock-name="PostBlock[_i_][position]" value="" class="form-control">'
            + '<input type="hidden" data-postblock-name="PostBlock[_i_][upload_name]" value="" class="form-control">'
            + '<input type="hidden" data-postblock-name="PostBlock[_i_][file_name]" value="" class="form-control">'
            + '<div class="help-block">'
            + '<img src="" class="img-responsive preview-thumb" hidden="hidden">'
            + '<div class="progress" hidden="hidden">'
            + '<div class="progress-text">'
            + '<span class="progress-timeleft"></span> sec left,'
            + ' <span class="progress-bitrate"></span>'
            + ' <span class="progress-bitrate-units"></span>'
            + '</div>'
            + '<div class="bar" style="width: 0%;"></div>'
            + '</div>'
            + '</div>'
            + '</div>';

        var videoFormats = ['video/x-flv', 'video/mp4', 'video/quicktime', 'video/x-msvideo', 'video/x-ms-wmv'];

        var videoPlaceholder = '<div class="jumbotron">'
            + '<p class="text-muted"><small>Processing...</small></p>'
            + '<p class="h3 video-info"></p>'
            + '</div>';

        var videoField = '<div class="form-group">'
            + '<label for="" class="control-label" hidden="hidden">Video</label>'
            + '<input type="hidden" data-postblock-name="PostBlock[_i_][type]"  value="' + postBlockTypes.video + '" class="form-control">'
            + '<input type="hidden" data-postblock-name="PostBlock[_i_][position]" value="" class="form-control">'
            + '<div class="input-group video-url-group">'
            + '<span class="input-group-addon"><i class="fa fa-play"></i></span>'
            + '<input type="url" data-postblock-name="PostBlock[_i_][video_url]" class="form-control"  placeholder="Insert source url. Support youtube, facebook and coub video" />'
            + '<span class="input-group-btn"><button class="btn btn-default btn-add-url" type="button">Add</button></span>'
            + '</div>'
            + '<div class="video-file-group">'
            + '<p class="text-center">'
            + '<br>'
            + '<strong>or</strong>&nbsp;&nbsp;&nbsp;'
            + '<button type="button" class="btn btn-default btn-upload-video"><i class="fa fa-upload"></i> Upload video</button>'
            + '</p>'
            + '<input type="hidden" data-postblock-name="PostBlock[_i_][upload_name]" value="" class="form-control">'
            + '<input type="hidden" data-postblock-name="PostBlock[_i_][file_name]" value="" class="form-control">'
            + '<input name="loadedVideos" type="file" multiple="multiple" class="file-control" accept="' + videoFormats.join(',') + '" hidden="hidden">'
            + '</div>'
            + '<div class="progress" hidden="hidden">'
            + '<div class="progress-text">'
            + '<span class="progress-timeleft"></span> sec left,'
            + ' <span class="progress-bitrate"></span>'
            + ' <span class="progress-bitrate-units"></span>'
            + '</div>'
            + '<div class="bar" style="width: 0%;"></div>'
            + '</div>'
            + '<div class="uploaded-files"></div>'
            + '<div class="help-block"></div>'
            + '</div>';

        var videoEmptyField = '<div class="form-group">'
            + '<label for="" class="control-label" hidden="hidden">Video</label>'
            + '<input type="hidden" data-postblock-name="PostBlock[_i_][id]" value="" class="form-control">'
            + '<input type="hidden" data-postblock-name="PostBlock[_i_][type]" value="' + postBlockTypes.video + '" class="form-control">'
            + '<input type="hidden" data-postblock-name="PostBlock[_i_][position]" value="" class="form-control">'
            + '<input type="hidden" data-postblock-name="PostBlock[_i_][upload_name]" value="" class="form-control">'
            + '<input type="hidden" data-postblock-name="PostBlock[_i_][file_name]" value="" class="form-control">'
            + '<input type="text" data-postblock-name="PostBlock[_i_][text]" value="" class="form-control" hidden="hidden" placeholder="Title for video">'
            + '<div class="progress" hidden="hidden">'
            + '<div class="progress-text">'
            + '<span class="progress-timeleft"></span> sec left,'
            + ' <span class="progress-bitrate"></span>'
            + ' <span class="progress-bitrate-units"></span>'
            + '</div>'
            + '<div class="bar" style="width: 0%;"></div>'
            + '</div>'
            + '<div class="help-block"></div>'
            + '</div>';

        var videoImage = '<div class="video-image" style=""></div>';
        var videoEmbed = '<iframe width="100%" height="315" src="" frameborder="0" allowfullscreen></iframe>';
        var videoEmbedFB = '<div class="fb-video" data-allowfullscreen="1" data-href=""></div>';
        var videoValidationTimer = true;

        function resortPostList() {
            $('#editable li').each(function () {
                $(this).find('[name$="[position]"]').val($(this).index());
                $(this).find('[data-postblock-name$="[position]"]').val($(this).index());
            });
        }

        var fileUploading = {
            videoUploading: [],
            imageUploading: []
        };

        function removeArrayValue(arr) {
            var what, a = arguments, L = a.length, ax;
            while (L > 1 && arr.length) {
                what = a[--L];
                while ((ax = arr.indexOf(what)) !== -1) {
                    arr.splice(ax, 1);
                }
            }
            return arr;
        }

        function fileUploadStarted(type, key) {
            fileUploading[type].push(key);
            $('#post-save, #post-save-xs').attr('disabled', 'disabled');
            $('#post-save, #post-save-xs').text('Save post (uploading...)');

            $('.btn-post-preview').attr('disabled', 'disabled');
            $('#post-draft').attr('disabled', 'disabled');
            if (draftTimer > 0) {
                clearTimeout(draftTimer);
            }
            draftTimer = false;
        }

        function fileUploadFinshed(type, key) {
            fileUploading[type] = removeArrayValue(fileUploading[type], key);
            if (fileUploading.videoUploading.length + fileUploading.imageUploading.length === 0) {
                $('#post-save, #post-save-xs').removeAttr('disabled');
                $('#post-save, #post-save-xs').text('Save post');

                $('.btn-post-preview').removeAttr('disabled');
                $('#post-draft').removeAttr('disabled');
                draftTimer = true;
            }
        }

        var editableList = Sortable.create(editable, {
            filter: '.js-remove',
            handle: '.drag-handle',
            animation: 150,
            onFilter: function (evt) {
                var el = editableList.closest(evt.item); // get dragged item

                if ($(el).find('.js-remove').length > 0) {
                    var btn = $(el).find('.js-remove').get(0);
                    if (typeof  btn.removableCall === 'function') {
                        btn.removableCall();
                    }
                    var uploadingType = $(btn).data('uploading-type');
                    var uploadingKey = $(btn).data('uploading-key');
                    if (typeof uploadingType !== 'undefined' && fileUploading[uploadingType].length > 0) {
                        fileUploadFinshed(uploadingType, uploadingKey);
                    }
                }

                el && el.parentNode.removeChild(el);
            },
            onEnd: function () {
                resortPostList();
                makeDraft();
            }
        });

        $('#post-tagids').selectize({
            persist: false,
            delimiter: ',',
            valueField: 'id',
            labelField: 'name',
            searchField: 'name',
            maxItems: 10,
            create: function (input) {
                return {
                    id: 'add_tag_' + input,
                    name: input
                }
            },
            load: function (query, callback) {
                if (!query.length) return callback();
                if (typeof tagsSearchUrl === 'undefined') return callback();

                $.ajax({
                    url: tagsSearchUrl + '?name=' + encodeURIComponent(query),
                    type: 'GET',
                    error: function () {
                        callback();
                    },
                    success: function (res) {
                        callback(res);
                    }
                });
            }
        });

        function addPostItem($btn, before) {
            var $li = $('<li class="list-group-item"></li>');
            before = before === false ? false : true;
            $li.append($postManage.clone().html());
            //editableList.el.appendChild($li.get(0));

            if (before) {
                $li.insertBefore($btn.closest('li'));
            } else {
                $li.insertAfter($btn.closest('li'));
            }

            $li.find('.add-post-item').click(function () {
                addPostItem($(this));
            });
            $li.find('.adding-panel button').click(function () {
                addFieldItem($(this));
            });
            $li.find('.change-post-item').click(function () {
                var attr = $li.find('.adding-panel').attr('hidden');
                if (typeof attr !== 'undefined' && attr !== false) {
                    $li.find('.adding-panel').removeAttr('hidden');
                } else {
                    $li.find('.adding-panel').attr('hidden', 'hidden');
                }
            });
            applyListItemIndex($li);
            return $li;
        }

        function waitingLoadedFile($input, callback) {
            $input.click(function () {

                function docFocus() {
                    $(document.body).unbind('focus', docFocus);
                    var waitingFilesProceessing = 500;

                    setTimeout(function () {
                        if ($input.get(0).files.length) {
                            callback($input, true);
                        } else {
                            callback($input, false);
                        }
                    }, waitingFilesProceessing);

                }

                document.body.onfocus = docFocus;
            });
        }

        function handleVideos($videoInput) {
            $videoInput.on('change', function (e) {
                var $li = $videoInput.closest('li');

                $(e.target.files).each(function (index) {

                    var $videoInfo = $(videoPlaceholder).clone();
                    if (index === 0) {
                        $videoInput.closest('.form-group').find('[data-postblock-name$="[file_name]"]').val(this.name);
                    } else {
                        $li = addPostItem($li.find('.add-post-item'), false);
                        addVideoEmptyField($li);
                        $li.find('[data-postblock-name$="[file_name]"]').val(this.name);
                    }
                    $videoInfo.find('.video-info').text(this.name);
                    $li.find('.form-group').append($videoInfo);
                });
            });
        }

        function cancelImageLoding($input, result) {
            if (!result) {
                $('.image-manage').remove();
            }
        }

        function applyVideoLoding($input, result) {
            if (result) {
                $input.closest('li').find('.video-url-group').remove();
                $input.closest('li').find('.btn-upload-video').closest('p').remove();
            }
        }

        function resolveFieldNameIndex($li) {
            postBlockIndex++;
            $li.find('[name*="[_i_]"]').each(function () {
                //$(this).attr('name', $(this).attr('name').replace(/_i_/, $li.index()));
                $(this).attr('name', $(this).attr('name').replace(/_i_/, postBlockIndex));
            });
            $li.find('[data-postblock-name*="[_i_]"]').each(function () {
                //$(this).attr('name', $(this).attr('name').replace(/_i_/, $li.index()));
                $(this).data('postblock-name', $(this).data('postblock-name').replace(/_i_/, postBlockIndex));
            });
        }

        function applyListItemIndex($li) {
            listItemIndex++;
            $li.attr('data-list-item', listItemIndex);
        }

        $('#editable li').each(function () {
            applyListItemIndex($(this));
        });

        function initMediumEditor($li) {
            var editor = new MediumEditor($li.find('.medium-editor-content').get(0), {
                targetBlank: true,
                toolbar: {
                    buttons: ['bold', 'italic', 'strikethrough', 'anchor'],
                },
                placeholder: {
                    text: 'Type your text...'
                }
            });

            editor.subscribe('blur', function () {
                $li.find('textarea').val($li.find('.medium-editor-content').html());
                makeDraft();
            });

            return editor;
        }

        var slideDownDelayTimeout = 300;

        function addTextField($btn) {
            var $li = $btn.closest('li');
            var $textField = $(textField).clone();
            $li.find('.form-group').html($textField.html());

            resolveFieldNameIndex($li);

            var editor = initMediumEditor($li);
            setTimeout(function () {
                editor.selectElement($li.find('.medium-editor-content p').get(0));
            }, slideDownDelayTimeout);
        }

        function validateVideo(input) {

            if (videoValidationTimer !== true) {
                clearTimeout(videoValidationTimer);
                videoValidationTimer = true;
            }

            if (typeof validatePostVideoUrl !== 'undefined' && input.value !== "") {

                var attributes = {
                    video_url: input.value,
                };

                $.ajax({
                    url: validatePostVideoUrl,
                    type: 'POST',
                    data: {PostBlock: attributes},
                    dataType: 'JSON',
                    success: function (response) {
                        $(input).closest('.form-group').find('.video-error').remove();
                        if (response.status === 'error') {
                            $(input).closest('.form-group').append('<div class="alert alert-danger video-error">' + response.message + '</div>');
                        } else if (typeof response.type !== 'undefined') {

                            $(input).closest('.input-group').attr('hidden', 'hidden');
                            $(input).closest('li').find('.video-file-group').remove();

                            if (response.image === '') {
                                if (response.type === 'facebook') {
                                    var $videoBlock = $(videoEmbedFB).clone();
                                    $videoBlock.attr('data-href', response.video);
                                    $(input).closest('.form-group').append($videoBlock);
                                    if (typeof FB !== 'undefined') {
                                        FB.init({
                                            xfbml: true,
                                            version: 'v2.5'
                                        });
                                    }
                                } else {
                                    var $videoBlock = $(videoEmbed).clone();
                                    $videoBlock.attr('src', response.video);
                                    $(input).closest('.form-group').append($videoBlock);
                                }
                            } else {
                                var $videoBlock = $(videoImage).clone();
                                $videoBlock.css('background-image', 'url(' + response.image + ')');
                                $(input).closest('.form-group').append($videoBlock);
                            }


                            $(input).closest('li').find('[data-postblock-name]').each(function () {
                                $(this).attr('name', $(this).data('postblock-name'));
                            });
                        }
                    }
                });
            }
        }

        function prepareVideoValidation(videoInput, timeout) {
            timeout = timeout || 2000;

            if (videoValidationTimer !== true) {
                clearTimeout(videoValidationTimer);
            }

            videoValidationTimer = setTimeout(function () {
                validateVideo(videoInput);
            }, timeout);
        }

        var progressHandle = {};

        function getProgressHandle(key) {
            if (progressHandle.hasOwnProperty(key)) {
                return {
                    startTime: progressHandle[key],
                    endTime: new Date().getTime()
                };
            }
        }

        function setProgressHandle(key) {
            if (!progressHandle.hasOwnProperty(key)) {
                progressHandle[key] = new Date().getTime();
            }
        }

        function formatBitrate(bitrate) {
            if (parseInt(bitrate / 8000000) > 0) {
                return {
                    value: parseInt(bitrate / 8000000),
                    units: 'MB/s'
                }
            } else if (parseInt(bitrate / 8000) > 0) {
                return {
                    value: parseInt(bitrate / 8000),
                    units: 'kB/s'
                }
            } else {
                return {
                    value: parseInt(bitrate / 8),
                    units: 'B/s'
                }
            }
        }

        function addVideoField($btn) {
            var $li = $btn.closest('li');
            var $videoField = $(videoField).clone();
            $li.find('.form-group').html($videoField.html());
            resolveFieldNameIndex($li);

            $li.find('.btn-add-url').click(function () {
                if (!$(this).closest('.input-group').attr('hidden')) {
                    prepareVideoValidation($li.find('[data-postblock-name$="[video_url]"]').get(0), 50);
                }
            });

            $li.find('[data-postblock-name$="[video_url]"]').keydown(function (e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    if (!$(this).closest('.input-group').attr('hidden')) {
                        prepareVideoValidation(this, 50);
                    }
                }
            });

            if (typeof  uploadVideoUrl !== 'undefined') {
                $li.find('input[type="file"]').attr('data-url', uploadVideoUrl);
                var $currentVideoLi = $li;
                $li.find('input[type="file"]').fileupload({
                    maxChunkSize: 1000000,
                    dataType: 'JSON',
                    add: function (e, data) {
                        if (data.files) {
                            $.each(data.files, function () {
                                $currentVideoLi = addPostItem($currentVideoLi.find('.add-post-item'), false);
                                addVideoEmptyField($currentVideoLi);
                                $currentVideoLi.find('[data-postblock-name$="[file_name]"]').val(this.name);
                                $currentVideoLi.find('.js-remove').data('uploading-key', $currentVideoLi.data('list-item'));
                                $currentVideoLi.find('.js-remove').data('uploading-type', 'videoUploading');
                                fileUploadStarted('videoUploading', $currentVideoLi.data('list-item'));
                                this['progress_holder'] = $currentVideoLi.find('.progress');
                                this['list_item'] = $currentVideoLi.data('list-item');
                            });
                            data.submit();
                            $li.hide();
                        }
                    },
                    done: function (e, data) {
                        if (typeof data.result.loadedVideos !== 'undefined') {
                            $.each(data.result.loadedVideos, function (index, file) {
                                if (typeof data.files[index] !== 'undefined' && typeof data.files[index]['list_item'] !== 'undefined') {
                                    var $fnLi = $('[data-list-item="' + data.files[index]['list_item'] + '"]');
                                    $fnLi.find('[data-postblock-name$="[upload_name]"]').val(file.dynamicPath + '/' + file.name);

                                    var $videoInfo = $(videoPlaceholder).clone();
                                    $videoInfo.find('.video-info').text(data.files[index]['name']);
                                    $fnLi.find('.form-group').append($videoInfo);
                                    $fnLi.find('[data-postblock-name$="[text]"]').removeAttr('hidden');

                                    $fnLi.find('[data-postblock-name]').each(function () {
                                        $(this).attr('name', $(this).data('postblock-name'));
                                    });
                                    fileUploadFinshed('videoUploading', $fnLi.data('list-item'));
                                }
                            });
                        }
                        $li.remove();
                    },
                    progress: function (e, data) {
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        var progress_holder = data.files[0].progress_holder;

                        var handle = getProgressHandle(data.files[0].list_item);
                        if (typeof handle !== 'undefined') {
                            var bitrate = formatBitrate(data.bitrate);
                            var passedTime = (handle.endTime - handle.startTime) / 1000;
                            var passedPerсentage = data.loaded / data.total;
                            var timeleft = parseInt(passedTime / passedPerсentage - passedTime);

                            progress_holder.find('.progress-timeleft').text(timeleft + 1);
                            progress_holder.find('.progress-bitrate').text(bitrate.value);
                            progress_holder.find('.progress-bitrate-units').text(bitrate.units);
                        }
                        setProgressHandle(data.files[0].list_item);

                        progress_holder.removeAttr('hidden');
                        progress_holder.find('.bar').css(
                            'width',
                            progress + '%'
                        );
                        if (progress >= 100) {
                            progress_holder.attr('hidden', true);
                        }
                    },
                    progressall: function (e, data) {
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        $li.find('.progress').removeAttr('hidden');
                        $li.find('.progress .bar').css(
                            'width',
                            progress + '%'
                        );
                    }
                });
            }

            waitingLoadedFile($li.find('input[type="file"]'), applyVideoLoding);
            handleVideos($li.find('input[type="file"]'));

            $li.find('.btn-upload-video').click(function () {
                var $videoInput = $(this).closest('.form-group').find('input[type="file"]');
                $videoInput.click();
            });

            setTimeout(function () {
                $li.find('[data-postblock-name$="[video_url]"]').focus();
            }, slideDownDelayTimeout);


        }

        function addImageField($btn) {
            var $currentImageLi = $btn.closest('li');
            var $imgFileInput = $imgFileInputBlock.clone();

            $currentImageLi.addClass('image-manage');
            $currentImageLi.attr('hidden', 'hidden');

            $currentImageLi.find('[data-postblock-name$="[position]"]').val($currentImageLi.index());
            resolveFieldNameIndex($currentImageLi);

            if (typeof  uploadImageUrl !== 'undefined') {

                var $manageImageLi = $currentImageLi;

                $imgFileInput.attr('data-url', uploadImageUrl);
                waitingLoadedFile($imgFileInput, cancelImageLoding);
                var uploadUrl = $imgFileInput.data('url');

                $imgFileInput.fileupload({
                    maxChunkSize: 1000000,
                    dataType: 'JSON',
                    add: function (e, data) {
                        if (data.files) {
                            $.each(data.files, function () {
                                $currentImageLi = addPostItem($currentImageLi.find('.add-post-item'), false);
                                addImageEmptyField($currentImageLi);
                                $currentImageLi.find('[data-postblock-name$="[file_name]"]').val(this.name);
                                $currentImageLi.find('.js-remove').data('uploading-key', this.name);
                                $currentImageLi.find('.js-remove').data('uploading-type', 'videoUploading');

                                //@todo try to remove browser hang on big image add
                                //var that = this;
                                //setTimeout(function () {
                                //    var $img = $currentImageLi.find('.preview-thumb');
                                //    var reader = new FileReader();
                                //    reader.onload = function (event) {
                                //        $img.attr('src', event.target.result);
                                //        $img.removeAttr('hidden');
                                //    }
                                //    reader.readAsDataURL(that);
                                //}, 200);

                                var $img = $currentImageLi.find('.preview-thumb');
                                var reader = new FileReader();
                                reader.onload = function (event) {
                                    $img.attr('src', event.target.result);
                                    $img.removeAttr('hidden');
                                    $img.width('100%');
                                }
                                reader.readAsDataURL(this);
                                fileUploadStarted('imageUploading', $currentImageLi.data('list-item'));
                                this['progress_holder'] = $currentImageLi.find('.progress');
                                this['list_item'] = $currentImageLi.data('list-item');
                            });
                            data.submit();
                        }
                    },
                    done: function (e, data) {
                        if (typeof data.result.loadedImages !== 'undefined') {
                            $.each(data.result.loadedImages, function (index, file) {
                                if (typeof data.files[index] !== 'undefined' && typeof data.files[index]['list_item'] !== 'undefined') {
                                    var $fnLi = $('[data-list-item="' + data.files[index]['list_item'] + '"]');
                                    $fnLi.find('[data-postblock-name$="[upload_name]"]').val(file.dynamicPath + '/' + file.name);

                                    $fnLi.find('[data-postblock-name]').each(function () {
                                        $(this).attr('name', $(this).data('postblock-name'));
                                    });
                                    fileUploadFinshed('imageUploading', $fnLi.data('list-item'));
                                }
                            });
                        }
                        $manageImageLi.remove();
                        // makeDraft();
                    },
                    progress: function (e, data) {
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        var progress_holder = data.files[0].progress_holder;

                        var handle = getProgressHandle(data.files[0].list_item);
                        if (typeof handle !== 'undefined') {
                            var bitrate = formatBitrate(data.bitrate);
                            var timeleft = parseInt((data.total - data.loaded) / ( 1000000 / ((handle.endTime - handle.startTime) / 1000)));

                            progress_holder.find('.progress-timeleft').text(timeleft + 1);
                            progress_holder.find('.progress-bitrate').text(bitrate.value);
                            progress_holder.find('.progress-bitrate-units').text(bitrate.units);
                        }
                        setProgressHandle(data.files[0].list_item);

                        progress_holder.removeAttr('hidden');
                        progress_holder.find('.bar').css(
                            'width',
                            progress + '%'
                        );
                        if (progress >= 100) {
                            progress_holder.attr('hidden', true);
                        }
                    },
                    progressall: function (e, data) {
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        $manageImageLi.find('.progress').removeAttr('hidden');
                        $manageImageLi.find('.progress .bar').css(
                            'width',
                            progress + '%'
                        );
                    }
                });

                $imgFileInput.click();
            }
        }

        function addImageEmptyField($li) {
            var $imageEmptyField = $(imageEmptyField).clone();
            $li.find('.form-group').html($imageEmptyField.html());
            resolveFieldNameIndex($li);

            $li.find('.form-group').removeAttr('hidden');
            $li.find('.add-post-item').removeAttr('hidden');
            $li.find('.adding-panel').attr('hidden', 'hidden');

            resortPostList();
        }

        function addVideoEmptyField($li) {
            var $videoEmptyField = $(videoEmptyField).clone();
            $li.find('.form-group').html($videoEmptyField.html());
            resolveFieldNameIndex($li);

            $li.find('.form-group').removeAttr('hidden');
            $li.find('.add-post-item').removeAttr('hidden');
            $li.find('.adding-panel').attr('hidden', 'hidden');

            resortPostList();
        }

        function isFunction(functionToCheck) {
            var getType = {};
            return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
        }

        function alertDraftErrors(errors) {
            $('.alert-draft').remove();
            var draftAlert = '<div class="alert alert-danger alert-draft">'
            draftAlert += '<ul class="list-unstyled">';
            for (var key in errors) {
                draftAlert += '<li>';
                draftAlert += '<strong>' + key + '</strong><br />';
                draftAlert += errors[key].join(';<br />') + '.<br />';
                draftAlert += '</li>';
            }
            draftAlert += '</ul>';
            draftAlert += '</div>';
            $('#add-post-form').append(draftAlert);
        }

        function savePost() {
            if (typeof savePostUrl !== 'undefined') {
                if (draftTimer > 0) {
                    clearTimeout(draftTimer);
                }
                var $saveBtn = $('button[name="Post[is_draft]"]');
                $saveBtn.attr('disabled', 'disabled');

                var formData = new FormData($('#add-post-form').get(0));
                $.ajax({
                    method: "POST",
                    url: savePostUrl,
                    data: formData,
                    dataType: 'JSON',
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        if (typeof response.postUrl !== 'undefined') {
                            window.location = response.postUrl;
                        } else if (response.status === 'error' && typeof response.errors !== 'undefined') {
                            alertDraftErrors(response.errors);
                        }
                        $saveBtn.removeAttr('disabled');
                    }
                });
            }
        }

        function storePostDraft(callback) {
            if (typeof storeDraftUrl !== 'undefined') {
                var $draftBtn = $('.btn-default[name="Post[is_draft]"]');
                $draftBtn.attr('disabled', 'disabled');

                callback = callback || successDraft;
                var formData = new FormData($('#add-post-form').get(0));

                $.ajax({
                    method: "POST",
                    url: storeDraftUrl,
                    data: formData,
                    dataType: 'JSON',
                    processData: false, // Don't process the files
                    contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                    success: function (response) {
                        if (draftTimer > 0) {
                            clearTimeout(draftTimer);
                        }
                        draftTimer = true;
                        if (typeof response.status !== 'undefined' &&
                            response.status === 'ok' &&
                            isFunction(callback)
                        ) {
                            callback(response);
                        } else if (response.status === 'error' && typeof response.errors !== 'undefined') {
                            alertDraftErrors(response.errors);
                        }
                        $draftBtn.removeAttr('disabled');
                    }
                });
            }
        }

        function udateDraftCounter() {
            var secondsCount = draftDelay / 1000 - ( (new Date().getTime() / 1000) - draftInfoStart);
            secondsCount = secondsCount.toFixed();
            if (secondsCount > 0) {
                $('.draft-info').text('Autosave in ' + secondsCount + ' seconds');
            } else {
                if (draftInfoInterval > 0) {
                    clearInterval(draftInfoInterval);
                }
                $('.draft-info').text('Auto saved');
            }
        }

        function makeDraft() {
            if (typeof storeDraftUrl !== 'undefined') {
                if (draftTimer === true) {
                    draftTimer = setTimeout(storePostDraft, draftDelay);

                    draftInfoStart = new Date().getTime() / 1000;
                    draftInfoInterval = setInterval(udateDraftCounter, 1000);
                }
            }
        }

        function addFieldItem($btn) {
            var $li = $btn.closest('li');

            switch ($btn.data('type')) {
                case 'text':
                    addTextField($btn);
                    makeDraft();
                    break;
                case 'image':
                    addImageField($btn);
                    break;
                case 'video':
                    addVideoField($btn);
                    break;
                default:
                    break;
            }

            $li.find('.form-group').removeAttr('hidden');
            $li.find('.add-post-item').removeAttr('hidden');
            //$li.find('.change-post-item').removeAttr('hidden');
            $li.find('.adding-panel').attr('hidden', 'hidden');

            resortPostList();
        }

        $('.add-post-item').click(function () {
            addPostItem($(this));
        });
        $('.adding-panel button').click(function () {
            addFieldItem($(this));
        });
        $('.adding-panel-bottom button').click(function () {
            addPostItem($(this));
            var btnType = $(this).data('type');
            $(this).closest('li').prev('li').find('[data-type="' + btnType + '"]').click();
        });
        $('.change-post-item').click(function () {
            var $li = $(this).closest('li');
            var attr = $li.find('.adding-panel').attr('hidden');
            if (typeof attr !== 'undefined' && attr !== false) {
                $li.find('.adding-panel').removeAttr('hidden');
            } else {
                $li.find('.adding-panel').attr('hidden', 'hidden');
            }
        });

        $('.medium-editor-content').each(function () {
            initMediumEditor($(this).closest('li'));
        });

        function resolveDraftedIndex($li, index) {
            $li.find('[name*="PostBlock"]').each(function () {
                $(this).attr('name', $(this).attr('name').replace(/PostBlock\[\d+\]/, 'PostBlock[' + index + ']'));
            });
        }

        function successDraft(response) {
            $('.alert-draft').remove();
            if (typeof  response.post !== 'undefined') {
                var draftAlert = '<div class="alert alert-info alert-draft">';
                draftAlert += '<p> Draft saved: ' + response.post.title + '</p>';
                draftAlert += '</div>';
                $('#add-post-form').append(draftAlert);
                $('.alert-draft').fadeOut(3000);

                if (typeof response.postBlocks !== 'undefined') {
                    $('#editable [name$="[file_name]"]').each(function () {
                        var fileNameInput = this;
                        $.each(response.postBlocks, function () {
                            if (this.file_name === fileNameInput.value) {
                                $(fileNameInput).closest('.form-group').find('[name$="[id]"]').val(this.id);
                                resolveDraftedIndex($(fileNameInput).closest('li'), this.id);
                            }
                        });
                    });
                }
            }
        }

        $('.btn-default[name="Post[is_draft]"]').click(function (e) {
            e.preventDefault();
            storePostDraft();

            if (draftInfoInterval > 0) {
                clearInterval(draftInfoInterval);
                $('.draft-info').text('Draft saved');
            }

        });

        $('#post-save, #post-save-xs').click(function (e) {
            e.preventDefault();
            savePost();
        });

        /* start drafting events */
        $('#post-title').blur(function () {
            makeDraft();
        });
        $('#post-tagids, #post-is_authors, #post-is_adult').change(function () {
            makeDraft();
        });

        $('.btn-post-preview').click(function (e) {
            storePostDraft();
        });

        if (typeof $.fn.datetimepicker === 'function') {
            var defaultLiveAtDate = new Date($('#post-live_at').data('default-time') * 1000);

            $('#post-live_at').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss'
            });

            var localDate = new Date();
            $('#post-timezone_offset').val(localDate.getTimezoneOffset() * 60);

            $('.post-live-at .btn-clear-date').click(function () {
                $('#post-live_at').data("DateTimePicker").clear();
            });

            $('[href="#collapseLiveAt"]').click(function () {
                $('#post-live_at').data("DateTimePicker").date(defaultLiveAtDate);
            });

            $('#collapseLiveAt').on('shown.bs.collapse', function () {
                $('#post-live_at').attr('name', 'Post[live_at]');
                $('#post-timezone_offset').attr('name', 'Post[timezone_offset]');

                $('[href="#collapseLiveAt"]').text('Hide live time');
            });

            $('#collapseLiveAt').on('hidden.bs.collapse', function () {
                $('#post-live_at').removeAttr('name');
                $('#post-timezone_offset').removeAttr('name');

                $('[href="#collapseLiveAt"]').text('Set live time');
            });
        }
    }
});