<?php

use yii\db\Migration;

class m161019_152519_add_parent_model_type_filed_to_notification extends Migration
{
    public $table = 'notification';
    public $col = 'parent_model_type';
    public function up()
    {
        $this->addColumn($this->table, $this->col, "TINYINT(1) UNSIGNED");
    }

    public function down()
    {
        $this->dropColumn($this->table, $this->col);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
