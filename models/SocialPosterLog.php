<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Url;

/**
 * Class Tag
 *
 * @property string $url;
 * @property string $posted_at;
 * @property string $social_platform;
 *
 * @package app\models
 */
class SocialPosterLog extends ActiveRecord
{

    public static function tableName()
    {
        return 'social_poster_log';
    }

    public function rules()
    {
        return [
            [['url', 'social_platform'], 'required'],
            [['url'], 'string'],
            ['social_platform', 'in', 'range'=>['facebook','tumblr','pinterest','stumbleupon']]
        ];
    }

    public function behaviors()
    {
        return [
        ];
    }

    public static function create($url, $socialPlarform)
    {
        $model = new self;
        $model->url = $url;
        $model->social_platform = strtolower($socialPlarform);
        $model->save();
        return $model;
    }


    public static function wasPostedLastDay($url)
    {
        return self::find()
            ->where("url=:url AND posted_at > DATE_SUB(NOW(), INTERVAL 24 HOUR)",
                [':url'=>$url])
            ->asArray()->select('id')->count();
    }

    public static function wasPosted($url, $socialNetwork)
    {
        return !empty(self::find()->asArray()
            ->where(['url'=>$url, 'social_platform' => strtolower($socialNetwork)])->select('url')->one());
    }

}