<?php
use yii\helpers\Url;
use \app\helpers\Html;


\app\assets\AbuseListAsset::register($this);


?>

<div id="list-abuse-page" class="text-center" data-cancel-url="<?= Url::to(['abuse/cancel']) ?>">
    <h1>Reported images</h1>
    <div>
        <h3>1) Batch report</h3>
        <div>
            <div class="form-group">
                <textarea id="abuse-batch-report" class="form-control" rows="5" placeholder="Place urls here"></textarea>
            </div>
            <button id="submit-batch-report-btn" class="btn btn-primary" data-url="<?= \yii\helpers\Url::to(['abuse/ajax-batch-report']) ?>">Batch report</button>
        </div>
    </div>
    <?php if($dataProvider->totalCount > 0): ?>
    <div id="reported-images">
        <h3>2) Reported images
            <button id="purge-all-btn" class="btn btn-danger" data-url="<?= Url::to(['abuse/purge-all']) ?>">Purge All</button>
        </h3>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'id' => 'abuse-list',
            'summary' => false,
            'columns' => [
                [
                    'header' => 'Preview',
                    'value' => function(\app\models\Abuse $model){
                        return Html::img($model->image->getHttpFtpFileName(), ['class'=>'img-responsive']);
                    },
                    'format' => 'html',
                    'contentOptions' => [
                        'class' => 'preview-cell'
                    ]
                ],
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{delete}',
                    'contentOptions' => [
                        'class' => 'buttons-cell'
                    ],
                    'buttons'=> [
                        'delete' => function($url, $model, $key){
                            return Html::button("Cancel", ['class' => 'btn btn-primary cancel']);
                        }
                    ]
                ]

            ]
        ]); ?>

    </div>
    <?php endif; ?>
</div>
