<?php

namespace app\components;


use app\components\data\Pagination;
use SphinxClient;
use Yii;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\helpers\Json;

class SphinxDataProvider extends ActiveDataProvider
{

    const CACHE_PREFIX = 'sphixDP';
    const MAX_SEARCH_RESULTS = 100;

    public $cacheEnabled = true;
    public $cacheDuration = 60;

    public $sphinxClient;

    public $sphinxResult;

    public $indexName;

    public $sphinxQuery;

    public $sortingAttributes;
    public $defaultSortAttribute;

    public $primaryModelKey;

    public $maxMatches = 1000;

    public $useSqlSortMode = false;

    public $ignoreSphinx = false;

    private $_sphinxIds;

    public function init()
    {
        if (empty($this->sphinxClient)) {
            $this->sphinxClient = \Yii::$app->sphinxClient->getClient();
        }
    }


    public function setPagination($value)
    {
        if(is_array($value)){
            if(empty($value['class'])){
                $value['class'] = Pagination::className();
            }
        }
        parent::setPagination($value);
    }


    public function getPagination()
    {
        $pagination = parent::getPagination();
        if($pagination instanceof Pagination && empty($pagination->maxPageNumber))
        {
            $pagination->calculateMaxPageNumber($this->maxMatches);
            $this->setPagination($pagination);
        }

        return $pagination;
    }


    private function convertSortField($field)
    {
        if (empty($field)) {
            return $this->defaultSortAttribute;
        }

        return isset($this->sortingAttributes[$field]) ? $this->sortingAttributes[$field] : $field;
    }

    private function convertSortMode($mode)
    {
        if (empty($mode)) {
            return SPH_SORT_ATTR_ASC;
        }

        if ($this->useSqlSortMode) {
            $modeTransfering = [
                SORT_ASC => "ASC",
                SORT_DESC => "DESC"
            ];
        } else {
            $modeTransfering = [
                SORT_ASC => SPH_SORT_ATTR_ASC,
                SORT_DESC => SPH_SORT_ATTR_DESC
            ];
        }

        return isset($modeTransfering[$mode]) ? $modeTransfering[$mode] : SPH_SORT_ATTR_ASC;
    }

    protected function prepareSortOrders($orders)
    {
        $sphinxOrders = [];
        foreach ($orders as $field => $mode) {
            $newOrder = $this->convertSortMode($mode);
            $newField = $this->convertSortField($field);
            $sphinxOrders[$newField] = $newOrder;
        }

        return $sphinxOrders;
    }

    public function prepareModels()
    {
        if($this->sphinxQuery === false){
            return [];
        }

        $query = clone $this->query;

        $ids = $this->getSphinxIds();
        if ($ids) {
            $expression = new Expression('FIELD( ' . $this->primaryModelKey . ', ' . implode(',', $ids) . " )");
            $query->orderBy([$expression]);
        }
        $query->andWhere([$this->primaryModelKey => $ids]);

        if ($this->cacheEnabled) {
            $key = self::CACHE_PREFIX . $this->buildQueryCacheKey($query);
            $models = Yii::$app->cache->get($key);
            if (!$models) {
                $models = $query->all($this->db);
                Yii::$app->cache->set($key, $models, $this->cacheDuration);
            }
            return $models;
        }

        return $query->all($this->db);
    }


    public function getSphinxIds()
    {
        if (!$this->_sphinxIds) {
            $cl = clone $this->sphinxClient;
            if (!($this->sphinxClient instanceof \SphinxClient)) {
                throw new Exception('sphinxClient must be instance of ');
            }

            if (($pagination = $this->getPagination()) !== false) {
                $pagination->totalCount = $this->getTotalCount();
                $cl->SetLimits($pagination->getOffset(), $pagination->getLimit());
            }

            if (($sort = $this->getSort()) !== false) {
                $orders = $this->prepareSortOrders($sort->getOrders());
                if ($this->useSqlSortMode) {
                    foreach ($orders as $field => $mode) {
                        $arr[] = "$field $mode";
                    }
                    $cl->setSortMode(SPH_SORT_EXTENDED, implode(',', $arr));
                } else {
                    foreach ($orders as $field => $mode) {
                        $cl->setSortMode($mode, $field);
                    }
                }
            } else {
                $cl->setSortMode(SPH_SORT_RELEVANCE);
            }

            if ($this->cacheEnabled) {
                $key = self::CACHE_PREFIX . $this->buildSphinClientCacheKey($cl);
                $res = Yii::$app->cache->get($key);
                if (!$res) {
                    $res = $cl->Query($this->sphinxQuery, $this->indexName);
                    if($res){
                        Yii::$app->cache->set($key, $res, $this->cacheDuration);
                    }
                }

            } else {
                $res = $cl->Query($this->sphinxQuery, $this->indexName);
            }

            if($res == false){
                throw new Exception("Sphinx error:" . $cl->getLastError());
            }

            $this->_sphinxIds = $this->extractIds($res);
        }

        return $this->_sphinxIds;
    }

    protected function extractIds($result)
    {
        $ids = [];
        if (!empty($result['matches'])) {
            foreach ($result['matches'] as $match) {
                $ids[] = $match['id'];
            }
        }

        return $ids;
    }

    public function prepareTotalCount()
    {
        if($this->sphinxQuery === false){
            return 0;
        }
        $cl = $this->sphinxClient;
        $res = $cl->Query($this->sphinxQuery, $this->indexName);
        if($res == false){
            throw new Exception("Sphinx error while geting total found:" . $cl->getLastError());
        }
        return isset($res['total_found']) ? (int)$res['total_found'] : 0;
    }

    public function buildSphinClientCacheKey(SphinxClient $client)
    {
        $clientParams = clone $client;
        $filters = $clientParams->_filters;
        foreach ($filters as $key => $filter) {
            if (
                isset($filter['attr']) &&
                isset($filter['max']) &&
                $filter['attr'] === 'ts_live_at'
            ) {
                $max = $filter['max'];
                $diff = $max % $this->cacheDuration;
                $filters[$key]['max'] = $max - $diff;

                $min = $filter['min'];
                $diff = $min % $this->cacheDuration;
                $filters[$key]['min'] = $min - $diff;
            }
        }
        $clientParams->_filters = $filters;
        return md5($this->sphinxQuery . $this->indexName . Json::encode($clientParams));
    }

    public function buildQueryCacheKey(ActiveQuery $query)
    {
        return md5($query->createCommand()->sql . Json::encode($query->createCommand()->params));

    }
}