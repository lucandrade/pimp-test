<?php

use app\assets\UserAsset;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/**
 * @var \app\models\UserSettingsForm $model
 */

/* @var $user \app\models\User */
$user = Yii::$app->user->identity;

$this->title = 'Profile settings';

UserAsset::register($this);
$this->registerJs('var uploadAvatarUrl = "' . Url::to(['/post/upload-image']) . '";', $this::POS_HEAD);
$this->registerJs('var settingAvatarUrl = "' . Url::to(['/user/setting-avatar']) . '";', $this::POS_HEAD);
?>
<div class="site-reset-password">
    <?php $form = ActiveForm::begin([
        'id' => 'user-settings-form',
        'successCssClass' => false,
        'options' => ['autocomplete' => 'off']
    ]);
    ?>
        <div class="panel panel-default">

            <!-- <div class="panel-heading">-->

            <!-- </div>-->

            <div class="panel-body">
                <h3>User settings</h3>

                <div data-image="avatar">

                </div>

                <hr>
                <?= $form->field($model, 'username')->textInput(['disabled'=>'disabled']); ?>
                <?php
                if ($model->passwordIsSet) {
                    echo $form->field($model, 'currentPassword')->passwordInput(['autocomplete' => 'off']);
                }
                ?>

                <?= $form->field($model, 'newPassword')->passwordInput() ?>
                <?= $form->field($model, 'newPassword2')->passwordInput() ?>

                <?= $form->field($model, 'show_adult')->checkbox() ?>
                <?= $form->field($model, 'autoplay_gifs')->checkbox() ?>
                <?= $form->field($model, 'expand_posts')->checkbox() ?>

                <hr>
                <div class="form-group text-center">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        </div>

        <?= $this->render('social-accounts' , compact('model')); ?>
    <?php ActiveForm::end(); ?>
</div>



