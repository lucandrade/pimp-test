<?php

use yii\db\Expression;
use app\models\User;

$now = new Expression('NOW()');
$demoPass = Yii::$app->security->generatePasswordHash('demo');

return [
	[
		'id' => User::ANONYMOUS_ID,
		'username' => 'anonymous_user',
		'email' => 'anon',
		'created_at' => $now,
		'updated_at' => $now,

		'status'=>0,
		'is_admin'=>0,
		'is_activated_email' => 0,
	],
];

