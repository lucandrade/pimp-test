<?php

namespace app\controllers;


use app\components\Controller;
use app\components\data\Pagination;
use app\components\filters\JsonResponse;
use app\models\Album;
use app\models\forms\AlbumAuth;
use app\models\Image;
use app\models\User;
use app\models\UserSettings;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;


class AlbumController extends Controller
{

    public $layout = '1-column';
    const SESSION_PASSWORD_KEY = 'alpa';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [

                    [
                        'allow' => false,
                        'actions' => ['ajax-create', 'lock', 'remove', 'my', 'trash' ,'unsorted', 'download'],
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true
                    ]
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'ajax-create' => ['post'],
                    'my' => ['get'],
                    'lock' => ['post'],
                    'move-images-to' => ['post']
                ],
            ],
            'jsonResponse' => [
                'class' => JsonResponse::className(),
                'only' => ['remove', 'download', 'move-images-to', 'create-by-uploading', 'get-share-codes','lock']
            ]
        ];
    }

    public function actionMy()
    {
        return $this->renderUserAlbums(\Yii::$app->user->identity);
    }

    public function actionUser($id)
    {

        if($id == User::ANONYMOUS_ID){
            throw new ForbiddenHttpException("Access denied");
        }

        $user = $this->loadUserForDashboard(['id' => $id]);

        return $this->renderUserAlbums($user);
    }


    public function actionUsername($username)
    {
        $user = $this->loadUserForDashboard(['username' => $username]);
        return $this->renderUserAlbums($user);
    }

    protected function loadUserForDashboard($condition)
    {
        $user = User::findOne($condition);
        if(!$user){
            throw new NotFoundHttpException("User not found");
        }

        if($user->id == User::ANONYMOUS_ID){
            throw new ForbiddenHttpException("Access denied");
        }

        return $user;
    }

    protected function renderUserAlbums(User $user){

        $this->layout = '1-column';

        $defaultOrder = ['createTime' => SORT_DESC, 'id' => SORT_DESC];

        $isMine = !\Yii::$app->user->isGuest &&
            (\Yii::$app->user->id == $user->id || \Yii::$app->user->identity->getIsAdmin());


        $statuses = [Album::STATUS_NORMAL];
        if($isMine){
            $statuses[] = Album::STATUS_PRIVATE;
        }

        $query = Album::find()
            ->where(["authorId" => $user->id, 'status' => $statuses]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'class' => Pagination::className(),
                'maxItems' => \Yii::$app->params['maxDisplayAlbumsInDashboard'],
                'userSettingName' => UserSettings::NAME_ALBUMS_PAGE_SIZE
            ],
            'sort' => [
                'defaultOrder' => $defaultOrder,
            ],
        ]);

        return $this->render('dashboard', compact('dataProvider', 'user', 'isMine'));
    }

    public function actionAjaxCreate()
    {
        if(!\Yii::$app->request->isAjax){
            throw new BadRequestHttpException("Wrong request type");
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        $album = new Album();

        if($album->load($_POST))
        {
            $album->authorId = \Yii::$app->user->id;
            $album->setScenario(Album::SCENARIO_CREATING_BY_USER);
            if($album->save()){

                return [
                    'status' => 'ok',
                    'id' => $album->id,
                    'title' => $album->title
                ];
            }else{
                return [
                    'status' => 'error',
                    'messages' => $album->getErrors()
                ];
            }
        }else{
            return [
                'status' => 'error',
                'messages' => ['no data was loaded']
            ];
        }

    }

    public function actionItem($id)
    {
        /**
        * @var $album Album
    */

        $album = Album::findOne(['id' => $id]);
        if(!$album){
            return $this->render("//common/only_label", ['label'=>"Album not found"]);
        }

        if($album->getIsSoftDeleted()){
            return $this->render("//common/only_label", ['label'=>"Album removed"]);
        }

        if(!$album->checkAuthorAccess() && $album->getIsLocked())
        {
            $savedPasswordHash = \Yii::$app->session->get(self::SESSION_PASSWORD_KEY);

            if(!$savedPasswordHash || !$album->validatePasswordHash($savedPasswordHash)){

                $authForm = new AlbumAuth([
                    'album' => $album
                ]);

                if(!$authForm->load(\Yii::$app->request->post()) || !$authForm->validate()){
                    return $this->render('auth_form', compact('authForm','album'));
                }else{
                    \Yii::$app->session->set(self::SESSION_PASSWORD_KEY, $album->password);
                }
            }
        }

        return $this->renderAlbum($album);
    }

    public function actionUnsorted()
    {
        $album = Album::createUnsorted(\Yii::$app->user->id);
        $query = Image::find()->where([
            'albumId' => null,
            'status' => [Image::STATUS_NORMAL, Image::STATUS_PROCESSING, Image::STATUS_PRIVATE],
            'authorId' => \Yii::$app->user->id,
        ]);

        return $this->renderAlbum($album, false, $query);
    }


    protected function renderAlbum(Album $album, $renderPartial = false, $imagesQuery = false)
    {
        if($imagesQuery === false){
            $imagesQuery = Image::find()->where(['albumId' => $album->id,
                'status' => [Image::STATUS_NORMAL, Image::STATUS_PROCESSING, Image::STATUS_PRIVATE]]);
        }


        $order = ['createTime' => SORT_DESC];

        if(!\Yii::$app->user->isGuest){
            $userOrder = \Yii::$app->user->identity->getSortOrder(UserSettings::NAME_IMAGES_SORTING);
            if($userOrder){
                $order = $userOrder;
            }
        }


        $imagesDataProvider = new ActiveDataProvider([
            'query' => $imagesQuery,
            'totalCount' => $album->imageCount,
            'pagination' => [
                'class' => Pagination::className(),
                'userSettingName' => UserSettings::NAME_IMAGES_PAGE_SIZE,
                'maxItems' => \Yii::$app->params['maxDisplayImagesInAlbum'],
            ],
            'sort' => [
                'defaultOrder' => $order
            ]
        ]);

        $data = compact('album', 'imagesDataProvider');

        $data['albumsForMoving'] = [];

        if($album->checkAuthorAccess())
        {
            $albumsForMoving = ArrayHelper::map( \Yii::$app->user->identity->getAlbums()
                ->where(['status'=>Album::STATUS_NORMAL])
                ->select(['id', 'title'])
                ->asArray()
                ->all(), 'id', 'title');

            if(!$album->getIsUnsorted())
            {
                unset($albumsForMoving[$album->id]);
                $albumsForMoving = ArrayHelper::merge(['0'=>'Unsorted'], $albumsForMoving);
            }

            $data['albumsForMoving'] = $albumsForMoving;
        }

        $html = $renderPartial ? $this->renderPartial('item', $data) : $this->render('item', $data);

        return $html;
    }


    public function actionRemove()
    {
        $album = $this->loadAlbum();
        if($album->softDelete()){
            return ['status' => 'ok'];
        }else{
            return ['status' => 'error', 'messages' => $album->getErrors()];
        }
    }


    public function actionDownload()
    {
        $album = $this->loadAlbum(false);
        if($album->addArchiveToQueue(\Yii::$app->user->id)){
            return ['status' => 'ok'];
        }else{
            return ['status' => 'error', 'messages'=>$album->getErrors()];
        }
    }


    public function actionGetShareCodes($ids)
    {
        if(empty($ids)){
            throw new BadRequestHttpException("Ids can't be blank");
        }

        $album = Album::findOne(['id' => $ids]);
        if(!$album){
            throw new NotFoundHttpException("Album #$ids not found");
        }

        list($codes, $moreThanMax) = $album->getShareCodes();

        $data['codes'] = $codes;
        $data['hiTaRows'] = Url::to(['album/item', 'id'=>$album->id], true);

        return [
            'status' => 'ok',
            'codes' => $codes,
            'hiTaRows' => Url::to(['album/item', 'id'=>$album->id], true),
            'addLink' => $moreThanMax
        ];
    }


    public function actionCreateByUploading()
    {
        $id = \Yii::$app->user->getId();
        $album = Album::create([
            'authorId' => $id,
            'title' => Album::TITLE_UNTITLED
        ]);

        $this->layout = 'scripts-only';
        $html = $this->renderAlbum($album);


        return [
            'html' => $html,
            'url' => Url::to(['album/item', 'id' => $album->id]),
            'albumId' => $album->id
        ];
    }


    public function actionLock()
    {
        $album = $this->loadAlbum();

        if($album->getIsLocked()){
            $album->unlock();
        }else{
            $album->lock(\Yii::$app->request->post('password'));
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;

        if(!$album->hasErrors()){
            return ['status' => 'ok'];
        }else{
            return ['status' => 'error', 'messages' => $album->getErrors()];
        }
    }


    public function actionMoveImagesTo()
    {
        $res = [];

        $userId = User::ANONYMOUS_ID;
        $userIsAdmin = false;

        if(!\Yii::$app->user->isGuest){
            $userIsAdmin = \Yii::$app->user->identity->is_admin;
            $userId = \Yii::$app->user->id;
        }

        $albumId = \Yii::$app->request->post('albumId');

        if($albumId){
            if(!$album = Album::findOne(['id' => $albumId])) {
                throw new NotFoundHttpException("Album with such id not found");
            }
        }elseif($albumTitle = \Yii::$app->request->post('albumTitle'))
        {
            $album = Album::create(['title'=>$albumTitle, 'authorId' => $userId]);
            if($album->hasErrors())
            {
                $res['album']['errors'] = $album->getErrors();
                return $res;
            }
        }
        else{
            if(!\Yii::$app->user->isGuest){
                $album = \Yii::$app->user->identity->getUnsortedAlbum();
            }else{
                throw new BadRequestHttpException("album id or album title must be provided");
            }
        }

        if(!$album->checkAuthorAccess()){
            throw new ForbiddenHttpException("You haven't access to this album");
        }

        $ids = explode(',', \Yii::$app->request->post('ids'));

        $movedIds = [];
        $errors = [];

        $images = Image::findAll(['id' => $ids]);
        foreach($images as $image)
        {
            if($userIsAdmin || $image->isBelongsTo($userId))
            {
                if ($image->moveToAlbum($album->id, false)){
                    $movedIds[] = $image->id;
                }else{
                    $errors[$image->id] = $image->getErrors();
                }
            }else{
                $errors[$image->id] = "You can't move this image";
            }
        }

        $res['album']['id'] = $album->getIsUnsorted() ? 0 : $album->id;
        $res['album']['title'] = $album->title;
        $res['album']['url'] = Url::to(['album/item', 'id' => $album->id]);

        $res['ids'] = $movedIds;
        $res['errors'] = $errors;

        return $res;
    }



    public function actionTrash()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Image::find()->where(['status' => Image::STATUS_DELETED, 'authorId'=>\Yii::$app->user->id]),
            'pagination' => [
                'class' => Pagination::className()
            ]
        ]);

        return $this->render("trash-bin", compact('dataProvider'));
    }


    /**
     * @return Album
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    protected function loadAlbum($checkAccess = true)
    {
        $albumId = \Yii::$app->request->post('albumId');
        /**
         * @var $album Album;
         */
        if(empty($albumId)){
            if(!\Yii::$app->user->getIsGuest()){
                $album = \Yii::$app->user->identity->getUnsortedAlbum();
            }else{
                throw new Exception("Anonymous doesn't have unsorted album");
            }
        }else{
            $album = Album::findOne(['id' => $albumId]);
        }


        if(!$album){
            throw new NotFoundHttpException("Album with id '$albumId' not found");
        }

        if($checkAccess && !$album->checkAuthorAccess()){
            throw new ForbiddenHttpException("You haven't access to this album");
        }

        return $album;
    }


}