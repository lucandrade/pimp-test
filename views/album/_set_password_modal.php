<?php
use yii\bootstrap\ActiveForm;
?>

<?php \yii\bootstrap\Modal::begin([
    'id'=>'create-album-modal',
    'header' => '<h4>Set password</h4>',
    'toggleButton' => false,
    'footer' => "<button type='button' class='btn btn-primary' id='btn-set-password'>Save</button>"
]); ?>

<?php $form = ActiveForm::begin([
    'id' => 'album-lock-form',
    'action' => ['album/ajax-create']
]);

$passwordForm = new \app\models\forms\SetPassword();
?>
<?= $form->field($passwordForm, 'password')->passwordInput() ?>
<?php ActiveForm::end(); ?>

<?php \yii\bootstrap\Modal::end(); ?>