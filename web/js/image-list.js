var selectedIds = [];

var imageListUtilities = (function(){
    return {
        getIdsFromWrappers: function($wrappers, asString){
            var ids = [];
            $wrappers.parents('.image-block').each(function(){
                ids.push($(this).data('key'));
            });

            if(asString){
                ids = ids.join(',')
            }
            return ids;
        },
        updateListView : function(){
            $.pjax.reload({container:"#pjax-listview"});
        }
    }
})();


$(document).on('pjax:end', function(){
    pahIniter.imageList.pjaxEnd();
});

$(document).on("ready", function(){
    pahIniter.imageList.ready();
});