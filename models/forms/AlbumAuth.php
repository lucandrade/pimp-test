<?php
namespace app\models\forms;


use app\models\Album;
use yii\base\InvalidConfigException;
use yii\base\Model;

class AlbumAuth extends Model
{

    public $password;
    public $album;


    public function rules()
    {
        return [
            ['password', 'required'],
            ['password', 'validatePassword']
        ];
    }


    public function init()
    {
        if(!$this->album instanceof Album){
            throw new InvalidConfigException("'album' property must be instance of " . Album::className());
        }
    }

    public function validatePassword()
    {
        if(!\Yii::$app->security->validatePassword($this->password, $this->album->password)){
            $this->addError('password', "Wrong password");
        }
    }

    public function authenticate()
    {
        if($this->validate()){
            \Yii::$app->session->set(Album::SESSION_PASSWORD_KEY, $this->album->password);
        }
    }
}