<?php

use yii\db\Schema;
use yii\db\Migration;
use yii\db\Expression;

class m151215_140416_users_init extends Migration
{
	public $table = '{{%user}}';

    public function up()
    {
		$this->createTable($this->table,[
			'id'=>$this->primaryKey(),
			'username'=>$this->string(20),
			'password'=>$this->string(128),
			'email'=>$this->string(128),
			'activkey'=>$this->string(128),
			'status'=>$this->integer(1),
			'is_activated_email'=>$this->integer(1),
			'is_admin'=>$this->integer(1),
			'email_activation_key'=>$this->string(100),
			'current_oauth_provider'=>$this->string(255),
			'password_reset_token'=>$this->string(255),

			'created_at'=>$this->timestamp('CURRENT_TIMESTAMP'),
			'updated_at'=>$this->timestamp('CURRENT_TIMESTAMP'),
			'lastvisit_at'=>$this->timestamp('0000-00-00 00:00:00'),
		]);

		$this->createIndex('email',$this->table,'email',true);
		$this->createIndex('status',$this->table,'status');
		$this->createIndex('is_admin',$this->table,'is_admin');

		$this->db->createCommand()->insert('{{%user}}', [
				'id' => \app\models\User::ANONYMOUS_ID,
				'username' => 'anonymous_user',
				'email' => 'anonymous_user',
				'created_at' => new Expression('NOW()'),
				'updated_at' => new Expression('NOW()'),

				'status'=>0,
				'is_admin'=>0,
				'is_activated_email' => 0,
		])->execute();


    }

    public function down()
    {
		$this->dropTable($this->table);
    }

}
