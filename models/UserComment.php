<?php

namespace app\models;

use yii\db\ActiveRecord;

class UserComment extends ActiveRecord
{

    public static function tableName()
    {
        return 'user_comment';
    }

    public function rules()
    {
        return [
            [['page_id', 'author_id'], 'required'],
            [['page_id', 'author_id'], 'integer'],
            ['page_id', 'exist', 'targetClass' => Post::className(), 'targetAttribute' => 'id'],
            ['author_id', 'exist', 'targetClass' => User::className(), 'targetAttribute' => 'id'],
        ];
    }
}