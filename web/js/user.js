if (typeof  uploadAvatarUrl !== 'undefined' && typeof  settingAvatarUrl !== 'undefined') {
    $(function () {
        var $imgFileInput = $('#user-avatar-input');
        $imgFileInput.attr('data-url', uploadAvatarUrl);
        var $avatarBlock = $imgFileInput.closest('.user-avatar');
        var avatarSrc;
        $imgFileInput.fileupload({
            maxChunkSize: 10000,
            dataType: 'json',
            add: function (e, data) {
                if (data.files && data.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (event) {
                        avatarSrc = event.target.result;
                    }
                    reader.readAsDataURL(data.files[0]);
                    data.submit();
                }
            },
            done: function (e, data) {
                if (typeof data.result.loadedImages !== 'undefined') {
                    $.each(data.result.loadedImages, function (index, file) {
                        $avatarBlock.find('[name$="[upload_name]"]').val(file.dynamicPath + '/' + file.name);
                    });

                    $.ajax({
                        method: 'POST',
                        url: settingAvatarUrl,
                        data: $avatarBlock.closest('form').serialize(),
                        dataType: 'json',
                        success: function (response) {
                            if (response.status === 'success') {
                                $avatarBlock.find('.img-avatar').css({'background-image': 'url(' + avatarSrc + ')'});
                                $avatarBlock.find('img').attr('src', response.avatar);
                                $avatarBlock.closest('.form-group').find('.btn-warning').attr('hidden', 'hidden');
                            }
                            $avatarBlock.find('.progress').attr('hidden', 'hidden');
                        }
                    });

                }
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $avatarBlock.find('.progress').removeAttr('hidden');
                $avatarBlock.find('.progress .bar').css(
                    'width',
                    progress + '%'
                );
            }
        });
    });
}