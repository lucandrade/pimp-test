<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "UserSettings".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $key
 * @property string $value
 * @property integer $create_add
 * @property integer $update_add
 */
class UserSettings extends ActiveRecord
{

    const NAME_ALBUMS_SORTING = 'albums_sorting';
    const NAME_IMAGES_SORTING = 'images_sorting';

    const NAME_ALBUMS_PAGE_SIZE = 'albums_page_size';
    const NAME_IMAGES_PAGE_SIZE = 'images_page_size';



    public static function tableName()
    {
        return '{{user_settings}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public static function get($userId, $name, $default = null)
    {
        $value = self::find()
            ->where(['user_id'=>$userId, 'key' => $name])
            ->select('value')
            ->scalar();

        return isset($value) ? $value : $default;
    }

    public static function set($id, $name, $value)
    {
        $model = self::find()->where([
            'user_id' => $id,
            'key' => $name
        ])->one();

        if (!$model)
        {
            $model = new self;
            $model->user_id = $id;
            $model->key = $name;
        }
        $model->value = $value;
        $model->save();

        return $model;
    }
}
