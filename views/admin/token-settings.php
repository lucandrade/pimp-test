<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php foreach(\Yii::$app->authAdminCollection->getClients() as $client): ?>
    <div class="row">
        <div class="col-sm-3"><?= $client->id ?> token</div>
        <div class="col-sm-3"><?= $client->fileTokenStatus; ?></div>
        <div class="col-sm-6">
            <?php $urlArray = $client->id != 'comments' ? ['admin/auth', 'authclient'=>$client->id] : ['admin/auth-comments']  ?>
            <?= Html::a('Obtain token', $urlArray, ['class'=>'btn btn-primary']); ?>
        </div>
    </div>
    <hr>
<?php endforeach; ?>