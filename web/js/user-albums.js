var initGrid = function(){
    $('.album-thumb').imageSelector({
        enableSelection: false,
        hoverBarContent:function($wrapper){
            return $wrapper.parents('.image-block').find('.hover-donor .hover-bar-content').html();
        }
    });
};

$(document).ready(function(){

    var $container = $('#albums-content');
    if($container.length == 0){
        return;
    }

    $('#upload-btn-copy').on('click', function(){
        $('#header-menu .uploader-trigger').click();
        return false;
    });

    $('.stats-popover').popover({
        placement:"top"
    });

    $container.on('click',' .hover-bar .fa-download', function(e){
        e.stopPropagation();
        e.preventDefault();
        if($container.data('login')){
            window.location.href = $container.data('login');
        }else{
            var $block = $(this).parents('.image-block');
            pahIniter.album.download($block.data('key'),$container.data('downloadurl'),  $container.data('archive-list-url'));
        }
    });

    $container.on('click',' .hover-bar .unlock-icon', function(e){
        e.stopPropagation();
        e.preventDefault();
        $(this).albumLocker('process', {iconIsClicker:true});
    });

    $container.on('lockalbum',' .hover-bar .unlock-icon', function(e){
        $(this).parents('.album-thumb').find('i.thumb-lock-icon').fadeIn();
    });

    $container.on('unlockalbum',' .hover-bar .unlock-icon', function(e){
        $(this).parents('.album-thumb').find('i.thumb-lock-icon').fadeOut();
    });



    $container.on('click',' .hover-bar .fa-share-alt', function(e){
        e.stopPropagation();
        e.preventDefault();
        var id = $(this).parents('.image-block').data('key');
        if(!id){
            throw "Can't get key of this album";
        }

        shareModalWindow.loadAndShow(id, 'album', $(this));
    });

    $container.on('click',' .hover-bar .fa-trash-o', function(e){
        e.stopPropagation();
        e.preventDefault();
        var $block = $(this).parents('.image-block');

        pahIniter.album.remove(
            $block.data('key'),
            function(res){
                if(res.status && res.status == 'ok'){
                    $block.fadeOut("slow", function(){
                        $(this).remove();
                    });
                }
            },
            $container.data('removeurl')
        );

        return false;
    });

    initGrid();
});