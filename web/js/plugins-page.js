$(document).ready(function(){
    var $mainPage = $('#plugins-page');

    if($mainPage.length == 0){
       return;
    }

    $mainPage.find('a.title-link, a.show-instructions').on('click', function(){
        var alias = $(this).parents('.plugin-header').data('alias');
        $mainPage.find('.instructions').hide();
        $mainPage.find('#instructions-'+alias).fadeIn();
        return false;
    });
});