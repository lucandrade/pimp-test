<?php
namespace app\helpers;
use app\models\Post;
use app\models\SocialPosterLog;
use yii\helpers\VarDumper;

class PostHelper
{

    const AUTOPOST_CLIENTNAME_PREFIX = "AP";

    public static function postToSocialPlatforms(Post $post, $forcePosting = false, $platforms = [])
    {
        $results = [];
        $logData = [];

        foreach (\Yii::$app->authAdminCollection->getClients() as $client)
        {
            $clientName = $client->getId();
            if(substr($clientName, 0, 2) == self::AUTOPOST_CLIENTNAME_PREFIX)
            {
                $clientName = str_replace(self::AUTOPOST_CLIENTNAME_PREFIX, '', $clientName);

                if(!empty($platforms) && !in_array($clientName, $platforms)){
                    continue;
                }

                try{
                    if(!$forcePosting && SocialPosterLog::wasPosted($post['url'], $clientName))
                    {
                        $results[$clientName] = 'duplicate';
                        $logData[$clientName] = 'duplicate';
                    }
                    elseif($client->autoPost($post))
                    {
                        SocialPosterLog::create($post['url'], $clientName);
                        $results[$clientName] = 'ok';
                        $logData[$clientName] = 'ok';
                    }
                    else
                    {
                        $results[$clientName] = "Errors: " . json_encode($client->getAutoPostResult());
                        $logData[$clientName] = "Errors: " . json_encode($client->getAutoPostResult())
                            . " \n Curl request options:" . json_encode($client->getCurlRequestOptions());
                    }
                }catch (\Exception $e){
                    $results[$clientName] = $e;
                    $logData[$clientName] = "Exception: " . $e->getMessage()
                        . " \n Curl request options:" . json_encode($client->getCurlRequestOptions());
                }
            }
        }

        if(!empty(\Yii::$app->params['stumbleuponPosterOptions']))
        {
            ob_start();
            try{
                global $ss_post;
                $postData = $post->getSocialPostingData();
                $ss_post = new \stdclass;
                $ss_post->id = $postData['id'];
                $ss_post->title = $postData['title']."\nSee More at http://www.owned.com/";
                $ss_post->excerpt = '';
                $ss_post->content = '';
                $ss_post->url = $postData['url'];
                $ss_post->img = $postData['media_url'];
                $ss_post->tags = $postData['tags'];

                global $options;

                $options = \Yii::$app->params['stumbleuponPosterOptions'];

                $options['pnDefImg'] = $ss_post->img;
                $options['trDefImg'] = $ss_post->img;
                $options['fbDefImg'] = $ss_post->img;
                $options['ogImgDef'] = $ss_post->img;
                define('ogImgDef', $ss_post->img);


                require(\Yii::getAlias("@app/extensions/social_poster/init.php"));
                echo "INITED";
                $plgn_NS_SNAutoPoster->nxs_options['addURLParams'] = '';
                $plgn_NS_SNAutoPoster->nxs_options['useUnProc'] = 1;
                $plgn_NS_SNAutoPoster->nxs_options['pn'] = array();
                $plgn_NS_SNAutoPoster->nxs_options['ogImgDef'] = $ss_post->img;
                $plgn_NS_SNAutoPoster->nxs_options['riHowManyPostsToTrack'] = 0;

                $ret = nxs_doPublishToSU($ss_post->id, $options);

                if($ret == 200){
                    $results['stumbleupon'] = "ok";
                    $logData['stumbleupon'] = 'ok';
                }else{
                    $logData['stumbleupon'] = $ret;
                    $results['stumbleupon']['res'] = $ret;
                }

            }catch (\Exception $e){
                $results['Stumbleupon']['res'] = $e;
                $logData['Stumbleupon'] = $e->getMessage();

            }

            $output =  ob_get_clean();
            if($results['Stumbleupon'] != 'ok'){
                $results['Stumbleupon']['output'] = $output;
            }else{
                SocialPosterLog::create($postData['url'], "stumbleupon");
            }
        }

        \Yii::info("\nPost #" . $post->id . ", title: '" . $post->title
            . "' was autoposted to socials, results:" . VarDumper::dumpAsString($logData), 'share-to-socials');

        return $results;
    }

}
