<?php
/**
 * @var $this \yii\web\View
 * @var $filter \app\models\FilterStatistic
 */

use app\models\Statistic;

?>

    <div class="text-muted filter-statistic">
        Statistic for
        <a href="#" class="choose-daterange">
            <?= $filter->periodString ?>
        </a>
    </div>

    <div id="popover-daterange" hidden="hidden">
        <p class="h4 text-center preset-labels">
            <?php foreach ($filter->presetsLabels as $preset => $label) { ?>
                <a href="<?= $label['url'] ?>" class="label label-default">
                    <?= $label['title'] ?>
                </a>
            <?php } ?>
        </p>
        <br>
        <form action="<?= $filter->formActionUrl ?>"
              class="form-vertical"
              id="filter-statistic-date"
              method="GET">
            <fieldset class="filter-daterange">
                <div class="form-group">
                    <div class="block-inline block-inline-date">
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" name="date_from"
                                   value="<?= $filter->getDateFrom('Y-m-d', time()) ?>">
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-clear-date" type="button">
                                <i class="fa fa-remove"></i>
                            </button>
                        </span>
                        </div>
                    </div>
                    <div class="block-inline">
                        <div class="input-group">
                            <span class="btn">to</span>
                        </div>
                    </div>
                    <div class="block-inline block-inline-date">
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" name="date_to"
                                   value="<?= $filter->getDateTo('Y-m-d', time()) ?>">
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-clear-date" type="button">
                                <i class="fa fa-remove"></i>
                            </button>
                        </span>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="inline-daterange">
                    <div class="daterange-container"></div>
                </div>
                <hr>
                <div class="form-group text-center">
                    <button class="btn btn-primary" type="submit">
                        Show
                    </button>
                </div>
            </fieldset>
        </form>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-6 col-lg-3">
            <div class="panel panel-default">
                <a href="<?= $filter->getUserStatisticUrl(Statistic::TYPE_USER_REGISTRATION) ?>" target="_blank"
                   class="panel-link">
                    <div class="panel-body text-center">
                        <?php if (isset($statistic[Statistic::TYPE_USER_REGISTRATION])) { ?>
                            <h3 class="text-success counter">
                                <?= $statistic[Statistic::TYPE_USER_REGISTRATION]->value ?>
                            </h3>
                        <?php } else { ?>
                            <h3 class="text-muted counter">
                                0
                            </h3>
                        <?php } ?>
                        <p class="text-muted">Registered</p>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3">
            <div class="panel panel-default">
                <a href="<?= $filter->getUserStatisticUrl(Statistic::TYPE_USER_VISIT) ?>" target="_blank"
                   class="panel-link">
                    <div class="panel-body text-center">
                        <?php if (isset($statistic[Statistic::TYPE_USER_VISIT])) { ?>
                            <h3 class="text-primary counter">
                                <?= $statistic[Statistic::TYPE_USER_VISIT]->value ?>
                            </h3>
                        <?php } else { ?>
                            <h3 class="text-muted counter">
                                0
                            </h3>
                        <?php } ?>
                        <p class="text-muted">Logged in</p>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3">
            <div class="panel panel-default">
                <a href="<?= $filter->getExternalCommentsUrl() ?>" target="_blank"
                   class="panel-link">
                    <div class="panel-body text-center">
                        <?php if (isset($statistic[Statistic::TYPE_COMMENTS])) { ?>
                            <h3 class="text-info counter">
                                <?= $statistic[Statistic::TYPE_COMMENTS]->value ?>
                            </h3>
                        <?php } else { ?>
                            <h3 class="text-muted counter">
                                0
                            </h3>
                        <?php } ?>
                        <p class="text-muted">Comments</p>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-sm-6 col-lg-3">
            <div class="panel panel-default">
                <div class="panel-body text-center">
                    <?php if (isset($statistic[Statistic::TYPE_VOTES_UP]) && isset($statistic[Statistic::TYPE_VOTES_DOWN])) { ?>
                        <h3 class="text-warning counter">
                            <?= $statistic[Statistic::TYPE_VOTES_UP]->value ?>
                            /
                            <?= $statistic[Statistic::TYPE_VOTES_DOWN]->value ?>
                        </h3>
                    <?php } else { ?>
                        <h3 class="text-muted counter">
                            0 / 0
                        </h3>
                    <?php } ?>
                    <p class="text-muted">Upvotes / Downvotes</p>
                </div>
            </div>
        </div>
    </div>

<?= $this->render('charts', ['dateFrom' => $filter->dateFrom, 'dateTo' => $filter->dateTo]) ?>