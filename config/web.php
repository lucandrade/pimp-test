<?php

return [
    'id' => 'posts.pah',
    'defaultRoute' => 'site/index',
    'bootstrap' => array_merge(['log', 'redirectManager'], YII_DEBUG ? ['debug'] : []),
    'modules' => [
        'debug' => [
            'class' => 'yii\debug\Module',
            'allowedIPs'=>['*']
        ]
    ],
    'components' => [
        'session' => [
            'class' => 'app\components\Session',
            'cookieParams' => ['httponly' => true, 'lifetime' => 1 * 24 * 60 * 60],
            'timeout' => 1 * 24 * 60 * 60,
            'useCookies' => true,
        ],
        'view' => [
            'class' => 'app\components\View'
        ],
        'request' => [
            'cookieValidationKey' => 'YvRMxGGensHQ8jeET9W7-ou3M_DG_Jbqd',
        ],
        'sphinxClient' => [
            'class' => 'app\extensions\SphinxWrapper',
            'port' => 9312,
            'host' => 'sphinx',
        ],

        'redirectManager' => [
            'class' => 'app\components\RedirectManager',
        ],

        'authManager' => [
            'class' => 'app\components\PhpManager',
            'defaultRoles' => ['guest'],
        ],
        'user' => [
            'class' => 'app\components\WebUser',
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => [
        'identityCookieDomain' => '.pah.loc',
        'Post' => [
            'liveAtAdditive' => 30 //minutes
        ]
    ],
];
