<?php

namespace app\commands;

use app\helpers\ConsoleHelper;
use PDO;
use Yii;
use yii\console\Controller;
use yii\helpers\Json;
use app\models\JobLog;
use app\components\Daemon;

class WorkerController extends Controller
{
    /**
     * @var \app\components\Pheanstalk;
     */
    private $_pheanstalk;

    public $startTime;
    public $defaultTube = 'testtube';

    private function __interval($str)
    {
        echo "\n";
        ConsoleHelper::echoWithDate("###(" . number_format((microtime(true) - $this->startTime), 2) . " $str)\n");
        $this->startTime = microtime(true);
    }

    public function actionIndex($tube = null)
    {
        $this->runWoker($tube);
    }

    protected function runWoker($tube = null)
    {
        $pheanstalk = $this->getPheanstalk();
        $pheanstalkClient = $pheanstalk->getClient();

        $tube = isset($tube) ? $tube : $this->defaultTube;

        while ($job = $pheanstalkClient->watch($tube)->ignore('default')->reserve()) {
            $jsonData = $job->getData();
            $jobId = $job->getId();
            $data = Json::decode($jsonData);

            $pheanstalkClient->delete($job);

            $period = !empty($data['period']) ? $data['period'] : 0;
            if (!$period || !JobLog::addedInPeriod($data['cmd'], $period)) {
                $this->__interval("Executing work for $jsonData...");
                $start = microtime(true);
                $output = $this->runCmd($data['cmd']);
                $end = microtime(true);
                JobLog::addLog([
                    'command_output' => $this->getCommandOutput($output),
                    'command' => $data['cmd'],
                    'execute_time' => $end - $start,
                    'job_id' => $jobId
                ]);
                $this->__interval("Output of the command");
            } else {
                $this->__interval("\nThis job was added to queue less than $period seconds ago and thus is not executed");
            }
        }
    }

    private function getCommandOutput($output)
    {
        $outputStr = implode(PHP_EOL, $output);
        return $outputStr;
    }

    private function runCmd($cmd)
    {
        chdir(Yii::getAlias("@app"));
        $command = PHP_BINARY . " yii " . $cmd;
        exec($command, $output);
        return $output;
    }

    public function actionStart()
    {
        $maxWorkers = 25;
        $daemon = new Daemon(['maxProcesses' => $maxWorkers]);
        $worker = $this;
        $daemon->run(function ($childPid) use ($worker) {
            ConsoleHelper::echoWithDate("Run worker in child  process #$childPid" . PHP_EOL);
            $worker->runWoker();
        });
    }

    public function actionStop()
    {
        Daemon::stop();
    }

    protected function getPheanstalk()
    {
        if (!$this->_pheanstalk) {
            $this->_pheanstalk = Yii::$app->pheanstalk;
        }
        return $this->_pheanstalk;
    }
}