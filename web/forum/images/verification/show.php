<?php
#########################################################
##                                                     ##
##    Enhanced Captcha Image Verification v1.2062711   ##
##               Idea by Jason Williams                ##
##      Coded by Andy Calderbank & Jason Williams      ##
##       (C) 2006 May only be distributed from         ##
##      VBulletin.org - all other rights reserved      ##
##                                                     ##
#########################################################
session_start();
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

$d = dir('.');
while (false !== ($entry = $d->read())) 
    if ($entry != '.' && $entry != '..' && $entry != 'show.php' && $entry != '.htaccess' && $entry != 'Thumbs.db')
        $answers[] .= $entry; 

$image = $_SESSION['show_images'][$_SERVER['QUERY_STRING']-1];
$im = imagecreatefromjpeg($answers[$image-1]);
header("Content-type: image/jpeg");
imagejpeg($im);
imagedestroy($im);
?>