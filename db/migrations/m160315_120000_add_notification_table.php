<?php

use yii\db\Schema;
use yii\db\Migration;

class m160315_120000_add_notification_table extends Migration
{
    public $table = 'notification';

    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => Schema::TYPE_PK,
            'owner_id' => $this->integer(11)->notNull(),
            'type' => $this->smallInteger()->notNull(),
            'type_id' => $this->integer(11)->notNull(),
            'caller_id' => $this->integer(11)->notNull(),
            'status' => $this->smallInteger()->notNull(),
            'created_at' => $this->dateTime()->notNull()
        ], $tableOptions);

        $this->createIndex('ix_Owner', $this->table, 'owner_id');
    }

    public function down()
    {
        $this->dropTable($this->table);
    }
}
