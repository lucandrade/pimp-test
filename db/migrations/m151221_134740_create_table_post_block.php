<?php

use yii\db\Schema;
use yii\db\Migration;

class m151221_134740_create_table_post_block extends Migration
{
    public function up()
    {
        $this->createTable('{{%post_block}}',[
            'id'           => Schema::TYPE_PK,
            'post_id'      => Schema::TYPE_INTEGER,
            'type'         => Schema::TYPE_STRING,
            'text'         => Schema::TYPE_TEXT,
            'position'     => Schema::TYPE_INTEGER,
            'file_name'    => Schema::TYPE_STRING,
            'file_size'    => Schema::TYPE_STRING,
            'media_width'  => Schema::TYPE_STRING,
            'media_height' => Schema::TYPE_STRING,
        ]);
    }

    public function down()
    {
       $this->dropTable('{{%post_block}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
