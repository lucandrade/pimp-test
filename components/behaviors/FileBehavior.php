<?php

namespace app\components\behaviors;


use yii\base\Behavior;
use yii\base\Exception;
use app\helpers\ImageHelper;
use app\helpers\FileHelper;
use app\helpers\StringHelper;

class FileBehavior extends Behavior
{
    public $ownerProperty = 'id';
    public $globalPath = '@app/media_protected/';
    public $storeFolder;

    public function getPath(){

        $fullPath = $this->globalPath . $this->storeFolder . "/";
        if(empty($this->globalPath)){
            throw new Exception("Property 'globalPath' can't be empty");
        }

        $path = \Yii::getAlias($fullPath);
        $id = $this->owner->{$this->ownerProperty};
        $path .= ImageHelper::getPathPrefix($id);
        $path .=  FileHelper::hashDir(StringHelper::dec2any($this->owner->{$this->ownerProperty}))
        . StringHelper::dec2any($this->owner->{$this->ownerProperty}) . "/";
        FileHelper::createDirectory($path, 0777);
        return $path;
    }
}