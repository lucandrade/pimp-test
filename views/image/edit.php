<div class="img-preview" style="display: none"  data-title="<?= $image->title ?>" data-album-id="<?= $image->albumId ?>">
    <?= \yii\helpers\Html::img($image->getHttpFtpFileName(),['crossorigin'=>"anonymous"]); ?>
    <?= \app\widgets\Uploader::widget([
        'uploadUrl' => \yii\helpers\Url::to(['image/upload-file'], true),
    ]); ?>
</div>