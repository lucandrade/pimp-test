<?php

namespace app\widgets;

use yii\base\Exception;
use yii\base\Widget;
use yii\web\View;

class ModalCaptcha extends Widget
{
    public $imgId = 'captcha-img';

    public $invokerSelector;
    public $invokerEvent = 'click';
    public $successCallback = 'function(){}';

    public $jsPosition = View::POS_READY;



    public function init()
    {
        if(empty($this->invokerSelector)){
            throw new Exception("Please specify jquery selector by acting upon captcha modal will be displayed");
        }
    }


    public function run()
    {
        return $this->render('ModalCaptcha');
    }
}