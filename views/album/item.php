<?php
/**
 * @var $album app\models\Album
 * @var $imagesDataProvider \yii\data\ActiveDataProvider;
 * @var $this \yii\web\View
 *
 */

use app\components\View;
use app\helpers\WidgetHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\YTExternalAsset;
use app\models\Image;

\app\assets\AlbumAsset::register($this);
YTExternalAsset::register($this);
$isMine = !\Yii::$app->user->isGuest && $album->authorId == \Yii::$app->user->id;
$lockStatus = false;
if(!$album->getIsUnsorted())
{
    $this->title = Html::encode($album->title) . ' | ' . \Yii::$app->name;
    $lockStatus = $album->getIsLocked() ? ["lock", "Unlock"] : ["unlock", "Lock"];

    $config = WidgetHelper::getConfig(2);
    $ck = WidgetHelper::getScriptInView('//js/widget', ['config' => $config]);
    $this->registerJs($ck, View::POS_END);
}

$totalCount = $album->getImageCount();
$models = $imagesDataProvider->getModels();

if(isset($models[0]))
{
    $this->registerMetaTag(['property' => 'og:image', 'content' => $models[0]->getHttpFtpFileName(Image::SIZE_0_PX)]);
    $this->registerMetaTag(['property' => 'og:image:width', 'content' => Image::SIZE_0_PX]);
    $this->registerMetaTag(['property' => 'og:image:height', 'content' => Image::SIZE_0_PX]);
}

$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Pimp and host'
], 'description');

$this->registerMetaTag([
    'name' => 'keywords',
//    'content' => $album->getTagList(',') ?: 'owned,jokes,fun,humour,funny videos,epic,fail,win,funny pics,like a boss,forever alone,meme,gifs,funny gifs,trolling'
], 'keywords');

if(!$album->getIsUnsorted() && $album->checkAuthorAccess())
{
    echo \app\widgets\AlbumLocker::widget();
}

echo \app\widgets\ShareLinksModal::widget();
echo \app\widgets\BlueimpGallery::widget();
echo \app\widgets\AlbumPicker::widget([
    'albumIds' => $albumsForMoving
]);
?>
<div id="album-page" class="post item-post album"
     data-widget="page"
     data-page-key="album_<?= $album->id ?>"
     data-key="<?= $album->id ?>"
     data-remove-url="<?= Url::to(['album/remove']); ?>"
     data-remove-redirect-url="<?= Url::to(['album/my']); ?>"
     data-download-url="<?= Url::to(['album/download']) ?>"
     data-archive-list-url="<?= Url::to(['/archive/list']) ?>"
>

    <div class="clearfix"></div>
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="post-header-main">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="author-header">

                            <?php if($isMine): ?>
                                <div class="pull-right">
                                   <?= \app\widgets\UploaderPopup::widget([

                                           'modalId' => 'album-popup-uploader',
                                           'buttonOptions' => [
                                               'id' => 'add-image-btn',
                                               'class' => 'btn btn-pah add-image-btn',
                                               'label'=> '<i class="fa fa-plus-circle fa-2x"> </i> <span>Add image</span>',
                                               'title'=> "Add image to this album"
                                           ],

                                           'clicker' => false,
                                           'modalTitle' => "Upload images to '" . $album->title . "'",

                                           'fullUploaderOptions' => [
                                           'albumId' => $album->id,
                                           'uploaderOptions' => [
                                               'registerAsset' => false,
                                               'buttonOptions' => [
                                                   'id' => 'album-fb'
                                               ],
                                               'js' => [
                                                   'previewsContainer' => '#album-images',
                                                   'clicker' => '#album-fb',
                                               ],
                                           ],

                                           'loadAlbumPage' => false,
                                       ]
                                   ]);?>

                                </div>
                            <?php elseif(!$album->getIsUnsorted()): ?>
                                <div class="widget-rating widget-rating-scores">
                                    <div data-rating="page"
                                         data-rating-type="scores"
                                         data-rating-key="album_<?= $album->id ?>"
                                         data-rating-value="<?= $album->rating ?>">
                                    </div>
                                </div>
                            <?php endif; ?>

                            <?= \app\widgets\BreadcrumbsHeader::widget([
                                'author' => $isMine ? \Yii::$app->user->identity : $album->author,
                                'links' => [
                                    [
                                        'label' => \app\widgets\Editable::widget(['model' => $album]),
                                    ]
                                ],
                                'afterLinksContent' => $this->render('_title_icons', [
                                    'isMine' => $isMine,
                                    'album' => $album,
                                    'lockStatus' => $lockStatus
                                ])
                            ]) ?>
                        </div>

                        <?= $this->render("//common/_header_post_info", [
                            'model' => $album,
                            'totalCount' => $totalCount
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="album-ctrl pull-right" <?= $totalCount == 0 ? "style='display:none'": "" ?>>
         <span id="slide-show"
               href="#"
               class="pah-color"
               title="Run slideshow for this album">
                    <i class="fa fa-th"></i> <span>Slideshow</span>
        </span>
    </div>

    <?= \app\widgets\SelectionBar::widget([
        'show' =>$imagesDataProvider->totalCount > 0,
        'showAuthorIcons' => $album->checkAuthorAccess(),
    ]); ?>


    <div class="row collapsing-content">
        <div class="col-xs-12 col-sm-12">
            <div class="collapse in"
                 id="collapsePost-<?= $album->id ?>"
                 aria-expanded="true">

                <?= $this->render('_image_list', ['dataProvider' => $imagesDataProvider, 'displaySorter' => true]) ?>

            </div>
            <div class="clearfix"></div>
            <div class="post-bottom">
                <div class="pull-right manage-right-icons">
                    <?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->is_admin): ?>
                        <a href="<?= Url::to(["admin/post-to-socials"]); ?>"
                           class="share-to-socials"
                           title="share to socials">
                            <i class="fa fa-2x fa-share"></i>
                        </a>
                    <?php endif; ?>
                </div>
                <?php if(!$isMine  && !$album->getIsUnsorted()): ?>
                    <span class="widget-rating">
                        <span data-rating="page"
                              data-rating-type="buttons"
                              data-rating-key="page_<?= $album->id ?>"
                              data-rating-value="<?= $album->rating ?>">
                        </span>
                    </span>
                <?php endif; ?>
                <?php if(!$album->getIsUnsorted()): ?>
                    <div class="post-btn-group pull-left">
                        <div class="comm">
                            <a class="post-comments" href="<?= $album->commentsUrl ?>" title="comments">
                                <i class="fa fa-comment"></i>
                                <span data-page="comments-count"></span>
                            </a>
                        </div>
                        <div class="mys">
                            <div id="rrssb-<?= $album->id ?>-bottom">
                                <span class="btn"
                                      title="share"
                                      data-rrssb="button"
                                      data-rrssb-container="#rrssb-<?= $album->id ?>-bottom"
                                      data-rrssb-url="<?= $album->getUrl(true) ?>"
                                      data-rrssb-title="<?= $album->title ?>"
                                </span>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div id="comments-block"></div>
    <div data-comment="comment-widget" id="comments"></div>
</div>
