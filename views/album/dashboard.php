<?php
use yii\helpers\Url;
use app\models\Image;

$this->title =  $isMine ? 'My albums and images'
    : "Album and images of " . $user->username;

$this->title .= ' | ' . \Yii::$app->name;
\app\assets\AlbumsAsset::register($this);
\app\assets\PostAsset::register($this);

echo \app\widgets\ShareLinksModal::widget();
if($isMine){
    echo \app\widgets\AlbumLocker::widget();
}
?>

<div class="author-header">
    <div class="pull-right">
        <button id="upload-btn-copy" class="btn btn-pah add-image-btn" title="Create Album">
            <i class="fa fa-plus-circle fa-2x"> </i> <span>Create Album</span>
        </button>
    </div>

    <?= \app\widgets\BreadcrumbsHeader::widget([
        'author' => $user,
        'links' => ['dashboard']
    ]) ?>
</div>
<div id="albums-content" class="collapsing-content"
     <?= \Yii::$app->user->isGuest ? 'data-login="' . Url::to(['site/login']) . '"' : ""; ?>
    data-removeurl="<?= Url::to(['album/remove']) ?>"
    data-downloadurl="<?= Url::to(['album/download']) ?>"
     data-archive-list-url="<?= Url::to(['/archive/list']) ?>"
>
    <div class="col-xs-12 col-sm-12">
        <div class="collapse in" aria-expanded="true">
            <div class="row">
                <?php
                \yii\widgets\Pjax::begin(['id'=>'pjax-albums', 'linkSelector' => false]);
                    $config = [
                        "layout" => "{sorter}<div class='clearfix'></div><div id='album-images'>{items}</div>\n <div class='clearfix'></div>{pager}{summary}",
                        'itemOptions'=> ['class' => 'image-block col-md-4 col-lg-3 col-xs-4 col-xxs-6 album-thumb-item'],
                        'dataProvider' => $dataProvider,
                        'itemView' => '_album',
                        'viewParams' => compact("isMine", 'user'),
                        'sorter' => [
                            'class' => \app\widgets\DropdownSorter::className(),
                            'userSortSetting' => \app\models\UserSettings::NAME_ALBUMS_SORTING,
                            'attributes' => [
                                'Last Uploaded' => 'createTime',
                                'Album name' => 'title',
                                'Album Size' => 'imageSize',
                                'Last Modified' => 'updateTime'
                            ]
                        ],
                        'pager' => [
                            'class' => \app\widgets\LinkPager::className(),
                            'countOnPage' => $dataProvider->getCount(),
                        ],
                        'summary' => false,
                        'emptyText' => false,
                        'options' => [
                            'class'=> 'list-view'
                        ]
                    ];


                    if($isMine){
                        $beforeItemsContent = \app\widgets\SpecialListThumb::widget([
                            'queryCount' => Image::find()
                                ->where(['status'=>Image::STATUS_DELETED, 'authorId'=>$user->id]),
                            'url' => ['album/trash'],
                            'thumb' => 'trashbin.jpg',
                            'emptyThumb' => 'empty_trashbin.jpg'
                        ]);

                        $beforeItemsContent .= \app\widgets\SpecialListThumb::widget([
                            'queryCount' => Image::find()
                                ->where(['authorId' => $user->id, 'albumId' => null])
                                ->andWhere("status=:processing OR status=:active", [':processing'=>Image::STATUS_PROCESSING,
                                    ':active'=>Image::STATUS_NORMAL]),
                            'url' => ['album/unsorted'],
                            'thumb' => 'unsorted_1.jpg'
                        ]) . "<div class='clearfix'></div>";


                        $config['beforeItemsContent'] = $beforeItemsContent;

                    }
                    echo \app\components\widgets\ListView::widget($config);
                \yii\widgets\Pjax::end();
                ?>
            </div>
        </div>
    </div>
</div>