<?php

namespace app\components\behaviors;
use app\helpers\FileHelper;
use app\models\Image;
use yii\base\Exception;
use yii\helpers\Url;
use app\helpers\ImageHelper;
use app\helpers\ConsoleHelper;

class ImageFileBehavior extends FileBehavior
{
    public $filenameProperty = 'fileName';

    public $thumbSuffix = "_thumb";

    public $biggestSizeAttr = 'biggestSize';

    public $thumbSizes  = [Image::SIZE_0_PX, Image::SIZE_SMALL_PX, Image::SIZE_MEDIUM_PX];

    const OLD_GIF_THUMB_SIZE = 'p2';

    public $oldSuffixes = [
        self::OLD_GIF_THUMB_SIZE => ["_p2.gif", 0],
        Image::SIZE_0_PX => ["_0.jpg", 0],
        Image::SIZE_SMALL_PX => ['_s.jpg', Image::SIZE_SMALL],
        Image::SIZE_MEDIUM_PX => ['_m.jpg', Image::SIZE_MEDIUM],


    ];

    public function getImgSrc($format = 'original')
    {

//        if($this->owner->getIsProcessing()){
//            return \Yii::getAlias("@web/static/i/processing.png");
//        }

        if (file_exists($this->owner->getFullFileName())) {

            $filename = $this->owner->{$this->filenameProperty};
            $ownerId = $this->owner->{$this->ownerProperty};

            if (!in_array($format, ImageHelper::$availableFormats)) {
                $format = ImageHelper::$defaultSrcFormat;
            }
            $src = Url::base(true) . '/media/?';
            $src .= $format;
            $src .= "?/$this->storeFolder/";
            $src .= ImageHelper::getPathPrefix($ownerId);
            $src .= $filename;

            return $src;
        }

        return null;
    }

    public function createThumb($size)
    {
        $fullFileName = $this->getFullFileName();
        $pathInfo = pathinfo($fullFileName);
        $thumbName = $pathInfo['dirname'] . DIRECTORY_SEPARATOR . $this->getThumbFileName($size);

        $sizeCmd = "'$size'";

//        if($size == Image::SIZE_MEDIUM_PX){
//        }else{
//            $sizeCmd = "'$size"."x"."$size^>' -gravity center -crop '$size"."x"."$size+0+0'";
//        }

        $background = ImageHelper::isAnimatedGif($fullFileName) ? " -background white " : "";
        $cmd  = \Yii::$app->params['convertCmdPrefix'] . " convert " . escapeshellarg($fullFileName . '[0]') . "$background -quality 90 -resize $sizeCmd "
            . escapeshellarg($thumbName);
        $out = ConsoleHelper::exec($cmd);
        if(!empty($out)){
            $this->owner->addError("fileName", "Can't create thumb:" . implode("\n", $out));
        }

        if(pathinfo($thumbName,PATHINFO_EXTENSION) === 'png'){
            $outPng = ImageHelper::compressPng($thumbName);
            if(!empty($outPng)){
                $this->owner->addError('fileName', "Coudn't compress png");
            }
        }

        return file_exists($thumbName);
    }


    public function getThumbFileName($size)
    {
        $pathInfo = pathinfo($this->owner->{$this->filenameProperty});
        return $pathInfo['filename'] . $this->getThumbSuffix($size, $pathInfo['extension']);
    }

    protected function getThumbSuffix($size, $defaultExtension)
    {
        return $this->thumbSuffix . $size . "." . $defaultExtension;
    }


    public function getFilename($thumbSize = false)
    {

        return $thumbSize !== false ? $this->getThumbFileName($thumbSize) : $this->owner->{$this->filenameProperty};
    }


    public function createThumbs()
    {
        foreach($this->thumbSizes as $size){

            if(!$this->createThumb($size)){
                return false;
            }
        }

        return true;
    }

    public function allSizes()
    {
        $sizes = $this->thumbSizes;
        $sizes[] = false;
        return $sizes;
    }

    public function getFullFileName($thumbSize = false)
    {
        $filename = $this->getFilename($thumbSize);
        return $this->getPath() . $filename;
    }

    public  function setBiggestSize($fileName)
    {
        if(!file_exists($fileName)){
            throw new Exception("$fileName not found");
        }

        list($width, $height) = getimagesize($fileName);

        $sizes = $this->thumbSizes;
        sort($sizes, SORT_ASC);
//         if($width <= $sizes[0]){
//             $this->owner->{$this->biggestSizeAttr} = Image::SIZE_ORIGINAL;
//         }elseif($width <= $sizes[1]){
//             $this->owner->{$this->biggestSizeAttr} = Image::SIZE_SMALL;
//         }else{
//             $this->owner->{$this->biggestSizeAttr} = Image::SIZE_MEDIUM;
//         }
        if($width <= $sizes[1]){
            $this->owner->{$this->biggestSizeAttr} = Image::SIZE_ORIGINAL;
        }elseif($width <= $sizes[2]){
            $this->owner->{$this->biggestSizeAttr} = Image::SIZE_SMALL;
        }else{
            $this->owner->{$this->biggestSizeAttr} = Image::SIZE_MEDIUM;
        }
    }



}