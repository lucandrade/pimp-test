<?php

return [
    'id' => 'console',
    'controllerNamespace' => 'app\commands',
    'controllerMap' => [
        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationPath' => '@app/db/migrations',
        ],
        'fixture' => [
            'class' => 'yii\console\controllers\FixtureController',
            'namespace' => 'app\db\fixtures',
        ]
    ],
    'components' => [

    ],
    'params' => [
        'commentsHost' => 'comments.kirs.name',
        'import' => [
            'donorDbName' => 'oldowned',
            'donorDbHost' => 'localhost',
            'imagesPathAlias' => '@app/../../../mnt/data/oldowned/htdocs/media/images/',
            'videoPathAlias' => '@app/../../../mnt/data/oldowned/htdocs/media/videos/'
        ],
    ],
];
