<?php
/**
 * Created by PhpStorm.
 * User: mactra
 * Date: 11.07.16
 * Time: 17:31
 */

namespace app\widgets;


use app\assets\SelectionBarAsset;
use yii\bootstrap\Widget;

class SelectionBar extends Widget
{
    public $showAuthorIcons = false;
    public $show = true;
    public $registerAsset = true;
    public $id = 'selection-bar';


    public $icons = [];

    protected function defaultIcons(){
        return [
            'remove' => [
                'title' => 'Remove selected images',
                'icon' => 'fa-trash',
                'show' => $this->showAuthorIcons
            ],
            'move-to' => [
                'title' => 'Move images to another album',
                'icon' => 'fa-copy',
                'show' => $this->showAuthorIcons
            ],
            'share' => [
                'title' => 'Share selected images',
                'icon' => 'fa-share-alt',
            ],
        ];
    }


    public function run(){

        $icons = empty($this->icons) ? $this->defaultIcons() : $this->icons;

        if($this->registerAsset){
            SelectionBarAsset::register($this->getView());
        }
        return $this->render('SelectionBar', compact('icons'));
    }
}