<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Url;

/**
 * Class Tag
 *
 * @property string $name;
 *
 * @package app\models
 */
class Tag extends ActiveRecord
{

    public $usedNumber;

    public static function tableName()
    {
        return 'tag';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'trim'],
            [['name'], 'unique', 'targetClass' => Tag::className(), 'targetAttribute' => 'name'],

        ];
    }

    public function behaviors()
    {
        return [
        ];
    }

    public function getUrl()
    {
        return Url::to(['/site/search', 'cat'=> MainSearchForm::POSTS_SEARCH_CATEGORY_ALIAS, 'q' => "tag_" . str_replace(' ', '_', $this->name)]);
    }

    public static function getRecent()
    {
        $tags = self::find()
            ->from('tag t')->select('t.*, count(pt.post_id) as usedNumber')
            ->innerJoin('post_to_tag pt', 't.id=pt.tag_id')
            ->innerJoin('post p', "pt.post_id=p.id")
            ->where('p.live_at>:time', [':time' => date('Y-m-d 00:00:00', strtotime("-7 DAY"))])
            ->orderBy("usedNumber DESC")->groupBy('t.id')
            ->limit(25)
            ->all();

        return $tags;
    }

    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['id' => 'post_id'])
            ->viaTable('post_to_tag', ['tag_id' => 'id']);
    }

    public function getPostCount()
    {
        return $this->hasMany(Post::className(), ['id' => 'post_id'])
            ->viaTable('post_to_tag', ['tag_id' => 'id'])->count();
    }

}