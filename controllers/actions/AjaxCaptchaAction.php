<?php
namespace app\controllers\actions;

use yii\captcha\CaptchaAction;

class AjaxCaptchaAction extends CaptchaAction
{
    public function run()
    {
        if(!empty($_POST['code'])){
            $input = $_POST['code'];
            $res = $this->validate($input, false);
            return $res ? "ok" : 'no';
        }else{
            $res = parent::run();
            return $res;
        }
    }
}