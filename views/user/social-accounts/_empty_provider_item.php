<?php
use yii\helpers\Url;
/**
 * @var $provider string;
 */

?>
<div class="col-sm-4 ouath-item">
    <h4><?= ucfirst($provider) ?></h4>
    <div class="social-image-wrapper">
        <a href="<?= Url::toRoute(['site/auth', 'authclient'=>$provider]) ?>"
           class="socicon-btn socicon-btn-circle socicon-lg socicon-solid bg-<?= $provider ?>-color bg-hover-grey-salsa font-white bg-hover-white socicon-<?=$provider?> tooltips" data-original-title="Facebook"></a>
    </div>
    <div>
        <a class="btn btn-primary" href="<?= Url::toRoute(['site/auth', 'authclient'=>$provider]) ?>">Link</a>
    </div>

</div>