<?php

namespace app\components;

use Yii;
use yii\web\IdentityInterface;
use yii\base\Exception;
use yii\helpers\Json;
use yii\web\User;
use yii\web\Cookie;
use app\models\Statistic;
use yii\base\InvalidValueException;
use yii\web\Request;

class WebUser extends User
{
    public $excludeReturnUrlPatterns = [
        "(login|logout|auth|widget|css|js)"
    ];


    public $returnUrlRoutes = [
        'image/show',
        'album/item',
        'album/user',
        'post/index',
        'post/item',
        'post/update',
        'post/preview',
        'site/faq',
        'site/search',
        'site/privacy',
        'admin/index',
        'admin/users',
        'admin/posts',
        'admin/token-settings',
        'user/settings',
        'user/profile',
        'user/social-accounts',
        'user/reset-password',
        'user/set-default',
        'user/set-default-avatar'
    ];




    public function login(IdentityInterface $identity, $duration = 0)
    {
        if ($identity->updateLastVisit()) {
            $identity->getForumUser()->updateLastVisit();
            return parent::login($identity, $duration);
        } else {
            throw new Exception("Can't save lastvisit time. Errors " . Json::encode($identity->getErrors()));
        }
    }


    public function getId()
    {
        if($this->getIsGuest()){
            return \app\models\User::ANONYMOUS_ID;
        }

        return parent::getId();
    }


    protected function afterRenewAuthStatus()
    {
        if (!$this->getIsGuest()) {
            if ($this->getIdentity()->isBanned()) {
                $this->logout();
                Yii::$app->session->setFlash('danger', 'Your account is banned!');
            } else {
                Statistic::updateUserLogin($this->getId());
            }
        }
    }

    protected function sendIdentityCookie($identity, $duration)
    {
        $cookie = new Cookie($this->identityCookie);
        $cookie->value = json_encode([
            $identity->getId(),
            $identity->getAuthKey(),
            $duration,
            $identity->email
        ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        if($duration > 0){
            $cookie->expire = time() + $duration;
        }

        if(!empty(\Yii::$app->params['identityCookieDomain'])){
            $cookie->domain = \Yii::$app->params['identityCookieDomain'];
        }

        Yii::$app->getResponse()->getCookies()->add($cookie);
    }

    protected function renewAuthStatus()
    {
        if ($this->getIsGuest()) {
            $this->loginByCookie();
        } elseif ($this->autoRenewCookie) {
            $this->renewIdentityCookie();
        }

        $this->afterRenewAuthStatus();
    }

    protected function renewIdentityCookie()
    {
        $name = $this->identityCookie['name'];
        $value = Yii::$app->getRequest()->getCookies()->getValue($name);
        if ($value !== null)
        {
            $data = json_decode($value, true);
            if (is_array($data) && isset($data[2]))
            {
                $cookie = new Cookie($this->identityCookie);
                $cookie->value = $value;
                $cookie->expire = time() + (int) $data[2];
                if(!empty(\Yii::$app->params['identityCookieDomain'])){
                    $cookie->domain = \Yii::$app->params['identityCookieDomain'];
                }
                Yii::$app->getResponse()->getCookies()->add($cookie);
            }
        }
    }

    protected function loginByCookie()
    {
        $value = Yii::$app->getRequest()->getCookies()->getValue($this->identityCookie['name']);
        if ($value === null) {
            return;
        }

        $data = json_decode($value, true);
        if (count($data) < 3 || !isset($data[0], $data[1], $data[2])) {
            return;
        }

        list ($id, $authKey, $duration) = $data;
        /* @var $class IdentityInterface */
        $class = $this->identityClass;
        $identity = $class::findIdentity($id);
        if ($identity === null) {
            return;
        } elseif (!$identity instanceof IdentityInterface) {
            throw new InvalidValueException("$class::findIdentity() must return an object implementing IdentityInterface.");
        }

        if ($identity->validateAuthKey($authKey)) {
            if ($this->beforeLogin($identity, true, $duration)) {
                $this->switchIdentity($identity, $this->autoRenewCookie ? $duration : 0);
                $ip = Yii::$app->getRequest()->getUserIP();

                Yii::info("User '$id' logged in from $ip via cookie.", __METHOD__);
                $this->afterLogin($identity, true, $duration);
            }
        } else {
            Yii::warning("Invalid auth key attempted for user '$id': $authKey", __METHOD__);
        }
    }


    public function switchIdentity($identity, $duration = 0)
    {
        $this->setIdentity($identity);

        if (!$this->enableSession) {
            return;
        }

        $session = Yii::$app->getSession();
        if (!YII_ENV_TEST) {
            $session->regenerateID(true);
        }
        $session->remove($this->idParam);
        $session->remove($this->authTimeoutParam);

        if ($identity)
        {
            $session->set($this->idParam, $identity->getId());
            if ($this->authTimeout !== null) {
                $session->set($this->authTimeoutParam, time() + $this->authTimeout);
            }
            if ($this->absoluteAuthTimeout !== null) {
                $session->set($this->absoluteAuthTimeoutParam, time() + $this->absoluteAuthTimeout);
            }
            $this->sendIdentityCookie($identity, $duration);
            \app\models\forum\User::login($identity,$duration);

        } else{
            $cookie = new Cookie($this->identityCookie);
            if(!empty(\Yii::$app->params['identityCookieDomain'])){
                $cookie->domain = \Yii::$app->params['identityCookieDomain'];
            }
            Yii::$app->getResponse()->getCookies()->remove($cookie);
            \app\models\forum\User::removeLoginCookies();
        }
    }



    public function updateReturnUrl()
    {
        if(\Yii::$app->request->isAjax){
            return false;
        }
        $url = \Yii::$app->request->getUrl();

        foreach($this->excludeReturnUrlPatterns as $pattern){
            if(preg_match("/$pattern/", $url)){
                return false;
            }
        }

        $this->setReturnUrl($url);
    }


    public function getFiltredReturnUrl()
    {
        $returnUrl = Yii::$app->user->returnUrl;
        $params = Yii::$app->urlManager->parseRequest(new Request(['url' => $returnUrl]));

        $route = reset($params);

        if(in_array($route, $this->returnUrlRoutes)){
            return $returnUrl;
        }

        return \Yii::$app->getHomeUrl();
    }


    public function afterLogout($identity)
    {
        $identity->getForumUser()->logout();
        parent::afterLogout($identity);
    }

}