<?php

$commonConfig =  [
    'name' => 'pimpandhost.com',
    'basePath' => dirname(__DIR__),
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'except' => [
                        'yii\web\HttpException:404',
                    ],

                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => [
                        'share-to-socials'
                    ],
                    'logVars' => [],
                    'logFile' => '@runtime/logs/share-to-socials.log'
                ],
                [
                    'class' => 'app\components\logs\NotFoundTarget',
                    'categories' =>  ['yii\web\HttpException:404'],
                    'logVars' => [],
                    'logFile' => '@runtime/logs/404.log',
                ]
            ],
        ],

		'cdnApi' => [
			'class' => 'app\extensions\CdnApi'
		],

        'forumDb' => [
            'class' => 'yii\db\Connection',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
        ],

        'pahDb' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=pah_v2',
            'username' => 'root',
            'password' => 'root',
            'charset' => 'utf8',
        ],

        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<action:(signup|auth|login|logout|captcha|error|activate|about|reset)>' => 'site/<action>',
                [
                    'pattern' => '<action:contact|privacy>',
                    'route' => 'site/<action>',
                    'suffix' => '/',
                ],
                'posts' => 'post/index',
                'plugins' => 'plugin/index',

				'site/botBatchReportAbuse' => 'abuse/bot-batch-report',

                'album/<id:\d+>' => 'album/item',
                'album/user/<id:\d+>' => 'album/user',

                'album/username/<username:\w+>/page/<page:\d+>' => 'album/username',
                'album/username/<username:\w+>/' => 'album/username',

                'image/<id:\d+>'  => 'image/show',

                'image/<action:(edit)>/<id:\d+>'  => 'image/<action>',

                '<type:(new|best|my|favorites)>/<period:(today|week|month|all)>' => 'post
                /index',
                '<type:(new|best|my|favorites)>/<date_from:\d{4}-\d{2}-\d{2}>' => 'post/index',
                '<type:(new|best|my|favorites)>/<date_from:\d{4}-\d{2}-\d{2}>_<date_to:\d{4}-\d{2}-\d{2}>' => 'post/index',
                '<type:(new|best|my|favorites)>' => 'post/index',
                '<period:(today|week|month|all)>' => 'post/index',
                '/page/<page:\d+>' => 'post/index',
                'toggle-favorite/<post_id:\d+>' => 'post/toggle-favorite',

                '/p/<alias:.+>-<id:\d+>' => 'post/item',
                '/s/<cat:(ew|p|i|my)>/<q>' => 'site/search',

                '/s/<q>' => 'site/search',

                '/tags/find' => 'tag/search',
                '/u/<id:\d+>/<type:(my|favorites|overview|commented)>' => 'user/profile',
                '/u/<id:\d+>' => 'user/profile',
                'users/<username:\w+>'  => 'user/profile',
                'user/<action>' => 'user/<action>',
                'admin' => 'admin/index',
                'admin/users' => 'admin/users',
                'admin/users/create' => 'admin/users-create',
                'admin/users/<id:[0-9]+>' => 'admin/users-edit',
                'admin/users/<id:[0-9]+>/delete' => 'admin/users-delete',
                'admin/statistics' => 'admin/statistics',

                'admin/<date_from:\d{4}-\d{2}-\d{2}>' => 'admin/index',
                'admin/<date_from:\d{4}-\d{2}-\d{2}>_<date_to:\d{4}-\d{2}-\d{2}>' => 'admin/index',
                'admin/<period:(today|week|month|all)>' => 'admin/index',
                [
                    'pattern' => '<alias:[\p{L}\p{N}\-]+(/)?>',
                    'route' => 'post/old-item'
                ],

                'sitemap.xml' => 'site/sitemap',
                '<username:\w+>'  => 'user/profile',
            ]
        ],

        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '',
                    'clientSecret' => '',
                ],
                'google' => [
                    'class' => 'yii\authclient\clients\GoogleOAuth',
                    'clientId' => '',
                    'clientSecret' => '',
                ],
                'twitter' => [
                    'class' => 'yii\authclient\clients\Twitter',
                    'consumerKey' => '',
                    'consumerSecret' => '',
                ],
            ],
        ],

        'authAdminCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'ytadmin' => [
                    'class' => 'app\components\authclients\GoogleYoutubeAdmin',
                    'scope' => 'https://www.googleapis.com/auth/youtube email https://www.googleapis.com/auth/youtubepartner https://www.googleapis.com/auth/youtube.force-ssl',
                    'clientId' => '',
                    'clientSecret' => ''
                ],
                'comments' => [
                    'class' => 'app\components\authclients\Comments'
                ],
            ]
        ],

        'pheanstalk' => [
            'class' => 'app\components\Pheanstalk'

        ],

//		'formatter' => [
//			'class' => 'yii\i18n\Formatter',
//			'dateFormat' => 'M/dd/Y',
//			'datetimeFormat' => 'M/dd/Y h:m a',
//			'timeFormat' => 'h:m a',
//			'thousandSeparator' => ' ',
//			'timeZone'=>'GMT',
//		],
    ]
];
$commonConfig['params']= [
    'uploadingEnabled' => true,
    'adminEmail' => 'admin@' . $commonConfig['name'],
    'activationEmailFrom' => 'no-reply@' . $commonConfig['name'],
    'resetPasswordEmailFrom' => 'no-reply@' . $commonConfig['name'],
    'facebookAppId' => '',
    'yahooClientId' => '',
    'yahooSecret' => '',


//    'serversForImages' => [32],
//    'serversForPosts' => [56],

    'deleteFromStorage' => true,

    'itemsPerPage' => 20,

    'imagesPerPage' => 100,

    'maxDisplayImagesInAlbum' => 1000,
    'maxDisplayAlbumsInDashboard' => 1000,
    'maxImagesInArchive'=> 1000,


    'convertCmdPrefix' => 'nice -n 19 ionice -c3',

    'commentsHost' => 'comments.kirs.name',
    'commentsClientId' => '',
    'commentsClientSecret' => '',
    'servers' => [
        55 => [
//             'type' => 'ver2',
            'url' => 'http://pah.kaktuz.net/ftp/',
            'path' => '/ftp_folder/',
            'server' => '89.238.172.153',
            'username' => 'pah',
            'password' => 'AU6eMjsZqUjE2',
        ],
        56 => [
            'url' => 'http://pah.kaktuz.net/ftp/',
            'path' => '/ftp_folder/',
            'server' => '89.238.172.153',
            'username' => 'pah',
            'password' => 'AU6eMjsZqUjE2',
        ],
    ]
];

return $commonConfig;
