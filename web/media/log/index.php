<?php

/**
 * use ImageMagic
 *
 * convert <input_image> <commands> <out_image>
 * composite <commands> <input_image> <out_image>
 */

function logRequest()
{
    $message = '';
    foreach ($_SERVER as $k => $v) {
        $message .= $k . ' : ' . $v . PHP_EOL;
    }
    $message .= date('Y-m-d H:i:s') . PHP_EOL;
    $message .= str_repeat('=', 32) . PHP_EOL;
    file_put_contents(__DIR__ . DIRECTORY_SEPARATOR . 'server.log', $message, FILE_APPEND);
}
logRequest();

$time_page = microtime(true);

$allowed_extensions = array("jpeg", "jpg", "gif", "png");

define('low', 'nice -n 19 ionice -c3 ');

//define('low', ' ');
//define('low', '/usr/local/bin/');

$presets = array(
    'default' => function ($file, $new_file) {
        __exec(low . "convert " . escapeshellarg($file . '[0]') . " -quality 90 -resize 150x150^ -gravity center -extent 150x150 " . escapeshellarg($new_file));
    },
    'original' => function ($file, $new_file) {
        __exec(low . "convert " . escapeshellarg($file . '[0]') . " " . escapeshellarg($new_file));
    },
    'adjusted' => function ($file, $new_file) {
        __exec(low . "convert " . escapeshellarg($file . '[0]') . " -quality 90 -resize 685 " . escapeshellarg($new_file));
    },
    'thumb_50' => function ($file, $new_file) {
        __exec(low . "convert " . escapeshellarg($file . '[0]') . " -quality 90 -resize 50x50^ -gravity center -extent 50x50 " . escapeshellarg($new_file));
    },
    'thumb_video' => function ($file, $new_file) {
        __exec(low . "convert " . escapeshellarg($file . '[0]') . " -quality 90 -resize 640x360^ -gravity center -extent 640x360 " . escapeshellarg($new_file));
    },
);

$presetsGif = array(
    'default' => function ($file, $new_file) {
        __exec(low . "convert " . escapeshellarg($file) . " -coalesce " . escapeshellarg($new_file));
        __exec(low . "convert " . escapeshellarg($new_file) . " -resize 150x150 " . escapeshellarg($new_file));
    },
    'original' => function ($file, $new_file) {
        __exec(low . "convert " . escapeshellarg($file) . " -coalesce " . escapeshellarg($new_file));
    },
    'adjusted' => function ($file, $new_file) {
        __exec(low . "convert " . escapeshellarg($file) . " -coalesce " . escapeshellarg($new_file));
        __exec(low . "convert " . escapeshellarg($new_file) . " -resize 685 " . escapeshellarg($new_file));
    },
    'thumb_50' => function ($file, $new_file) {
        __exec(low . "convert " . escapeshellarg($file) . " -coalesce " . escapeshellarg($new_file));
        __exec(low . "convert " . escapeshellarg($new_file) . " -resize 50x50 " . escapeshellarg($new_file));
    },
    'thumb_video' => function ($file, $new_file) {
        __exec(low . "convert " . escapeshellarg($file) . " -coalesce " . escapeshellarg($new_file));
        __exec(low . "convert " . escapeshellarg($new_file) . " -resize 640x360 " . escapeshellarg($new_file));
    },
);

function presets($preset = 'default', $file, $new_file)
{

    global $presets;
    global $presetsGif;

    if (strtolower(pathinfo($file, PATHINFO_EXTENSION)) === 'gif') {

        if (isset($presetsGif[$preset])) {
            $presetsGif[$preset]($file, $new_file);
        } elseif (isset($presetsGif['default'])) {
            $presetsGif['default']($file, $new_file);
        }

    } else {

        if (isset($presets[$preset])) {
            $presets[$preset]($file, $new_file);
        } elseif (isset($presets['default'])) {
            $presets['default']($file, $new_file);
        }

    }
}

function defaults($preset = 'default', $file = 'no_image.gif')
{
    switch ($preset) {
        default:
            $url = '/media/?/' . $file;
            break;
    }
    header('location: ' . $url);
    die();
}

//@todo clean up this mess
//$preset = isset($_GET['preset'])?$_GET['preset']:0;
if (!preg_match('~/media/log/\?(.+\?)?/(.+)~si', urldecode($_SERVER['REQUEST_URI']), $match)) {
    __error(400);
}

$preset = !empty($match[1]) ? trim($match[1], '?') : 'default';

if (!isset($presets[$preset])) {
    $preset = 'default';
}

$path = urldecode(parse_url($match[2], PHP_URL_PATH));
$file = __DIR__ . '/../../../media_protected/' . $path;

if (preg_match('~media/_cache/(.+)~si', $file)) {
    __error(400);
}

if (!in_array(strtolower(pathinfo($file, PATHINFO_EXTENSION)), $allowed_extensions)) {
    __error(403);
}

if (!file_exists($file)) {
    defaults($preset, 'no_image.jpg');
    die();
}

//$new_file = '../media/_cache/'.$preset.'/'.$path.'-'.filemtime($file).filesize($file).'.png';

if (strtolower(pathinfo($file, PATHINFO_EXTENSION)) === 'gif') {
    $new_url = '/media/_cache/' . $preset . '/' . $path;
} else {
    $new_url = '/media/_cache/' . $preset . '/' . $path . '.png';
}

$new_file = '../..' . $new_url;
__mkdir($new_file);


if (!file_exists($new_file)) {

    header('X-Preset#: ' . $preset);

    // not allowed format
    $output = __exec(low . 'identify  -verbose ' . escapeshellarg($file) . ' | grep ' . escapeshellarg('Format:'));

    if (isset($output[0]) && preg_match('~Format: (\w+)~', $output[0], $matches)) {
        $format = $matches[1];
    }
    if (!isset($format) || !in_array(strtolower($format), $allowed_extensions)) {
        __error(400);
    }

    presets($preset, $file, $new_file);

    //foreach(explode("\n", $headers) as $header) header($header);
    //header('Content-Length: '.filesize($file));
    header('X-Gentime: ' . __generated() . ' sec');
}

header('location: ' . $new_url);

function __error($nr = 404)
{
    $headers = array(
        400 => "HTTP/1.1 400 Bad Request",
        403 => "HTTP/1.1 403 Host is not allowed",
        404 => "HTTP/1.1 404 Not Found"
    );

    header($headers[$nr]);
    die();
}

function __mkdir($dir, $mode = 0777)
{
    if (file_exists($dir)) {
        return true;
    }

    $dir = pathinfo($dir, PATHINFO_DIRNAME);
    return mkdir($dir, $mode, true);
}

function __generated()
{
    global $time_page;
    return number_format((microtime(true) - $time_page), 2);
}

function __exec($cmd, $log_file = null)
{
    l($cmd, $log_file);
    exec($cmd, $output);
    l($output, $log_file);
    return $output;
}

function l($text = '...', $file = null)
{
    if (empty($file)) {
        $file = 'temp.log';
    }
    if (gettype($text) == 'string') {
        error_log($text . "\n", 3, $file);
    } else {
        error_log(var_export($text, true) . "\n", 3, $file);
    }
}

