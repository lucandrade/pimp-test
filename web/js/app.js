var seenPostIds = [];
var deltaScroll = 0;
var $currentElem;
var scrollTimeout;
var orientationTimeout;

var isMobileChrome = false;

$(document).ready(function () {

    window.onpopstate = function(event){
        window.location.href = window.location.href;
    };

    var uaParts = navigator.userAgent.match(/(CriOS)|(Chrome[.0-9]* Mobile)/);
    isMobileChrome = uaParts && (uaParts.length > 1);

    $(window).on('scroll', function () {

        if (!isMobileChrome) {
            return true;
        }

        if (scrollTimeout) {
            // clear the timeout, if one is pending
            clearTimeout(scrollTimeout);
        }

        scrollTimeout = setTimeout(function () {
            var topViewPort = $(window).scrollTop(),
                botViewPort = topViewPort + $(window).height(),
                postInViewPort = false;


            $('.post').each(function () {
                var $me = $(this),
                    meTop = $me.offset().top,
                    meBot = meTop + $me.height();

                if ((meTop >= topViewPort && meTop <= botViewPort) ||
                    ((meTop < topViewPort) && (meBot > botViewPort))
                ) {
                    $currentElem = $me;
                    deltaScroll = (meTop - topViewPort) / (botViewPort - topViewPort);
                    postInViewPort = true;
                    return false;
                }
            });

        }, 100);
    });

    $('#toggle-seen-posts').on('click', function () {
        var newStatus = $(this).data('status') == 'Show' ? "Hide" : "Show";
        $(this).html($(this).data('status'));
        $(this).data('status', newStatus);
        toggleSeenPosts($('.item'), newStatus);
        return false;
    });


    $('.container').on('click', '.list-post .save-history', function () {
        var $post = $(this).parents('.post');
        var url = "?page=" + $post.data('page') + "&p=" + $post.data('page-key');
        window.history.replaceState({}, document.title, url);
    });

    $(window).on('orientationchange', orientationChangeHandler);

    function orientationChangeHandler(e) {

        if (isMobileChrome) {
            if (orientationTimeout) {
                clearTimeout(orientationTimeout);
            }
            orientationTimeout = setTimeout(function () {
                $(window).trigger('resize');
                var offsetElem = $currentElem && $currentElem.offset() ? $currentElem.offset().top : 0;
                scrollToElement(offsetElem - (deltaScroll * $(window).height()), 1000);
            }, 200);
        }
    }
});


function updateSeenPostsCount($items) {
    var $seenPosts = $('#seen-posts-count'),
        seenPostsNumber = parseInt($seenPosts.html()) || 0;

    $items.each(function () {
        var isSeen = parseInt($(this).find(".post").data('isseen')) || 0;
        if (isSeen) {
            seenPostsNumber++;
        }
    });

    setNewSeenPostNumber(seenPostsNumber);
}


function setNewSeenPostNumber(value) {
    if (value > 0 && $("#manage-seen-posts-ctrl").is(":hidden")) {
        $('#manage-seen-posts-ctrl').show();
    }

    $('#seen-posts-count').html(value);
}

function initProccessSeenPosts($items) {
    updateSeenPostsCount($items);
    var status = $('#toggle-seen-posts').data('status') || "Hide";
    toggleSeenPosts($items, status);
}


function toggleSeenPosts($items, targetStaus) {
    $items.each(function () {
        var isSeen = parseInt($(this).find(".post").data('isseen')) || 0;
        if (isSeen) {
            if (targetStaus == 'Show') {
                $(this).show();
            } else {
                //$(this).hide();
            }
        }
    })
}

function scrollPostsHandler() {
    deltaScroll = $(window).scrollTop();

    $(".item").each(function () {
        var $me = $(this);

        var isSeen = parseInt($me.find(".post").data('isseen')) || 0;
        if (isSeen) {
            return;
        }
        var bottomBorderOffset = $(window).scrollTop() - $me.offset().top - $me.height() - 50;
        var lastPosition = $me.data('lastposition') || bottomBorderOffset;

        if (bottomBorderOffset > lastPosition) {
            if (bottomBorderOffset >= 0 ||
                $(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                console.log("Post#" + $me.data('key') + " is scrolled");

                $me.find(".post").data('isseen', 1);

                $.ajax({
                    url: $("[name='make-seen-url']").val(),
                    data: {post_id: $me.data('key')},
                    type: "POST",
                    success: function (data) {
                        if (data.status != 'ok') {
                            $me.find(".post").data('isseen', 0)
                        }
                    }
                });
            }
        }

        $me.data('lastposition', bottomBorderOffset);
    });
}

function addToFavoriteHandler() {
    var $me = $(this);
    $.ajax({
        url: $("[name='add-to-favorite-url']").val(),
        data: {post_id: $me.parents('.post').data('key')},
        type: "POST",
        success: function (data) {
            if (data.status != 'ok') {
                console.log(data.errors);
            } else {
                if (data.result == '-1') {
                    $me.find('i.fa').removeClass('active');
                } else {
                    if (data.result == '1') {
                        $me.find('i.fa').addClass('active');
                    }
                }
            }
        }
    });
    return false;
}

function scrollToElement($elem, speed, easing, complete) {
    if (speed === undefined) {
        speed = 0;
    }

    if ($elem instanceof jQuery) {
        if ($elem.length > 0) {
            scrollTop = $elem.offset().top - 50;
        } else {
            return;
        }
    } else {
        scrollTop = $elem;
    }


    $('html, body').animate({
        scrollTop: scrollTop
    }, speed, easing, complete);
};


$('#additional-menu-header > a').on('click', function () {
    $('#additional-menu-content').toggle();
    return false;
});

$('#additional-menu-content').on('click', function (e) {
    e.stopPropagation();
});
$('html').on('click', function () {
    $('#additional-menu-content').hide();
});

$(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scroll-to-top').fadeIn();
        } else {
            $('.scroll-to-top').fadeOut();
        }

        if ($('#sidebar-footer').length > 0) {
            if ($(this).scrollTop() + 70 > $('#sidebar-footer').offset().top) {
                $('#sidebar-footer > div').addClass('sidebar-fixed');
                $('#sidebar-footer > div').width($('#sidebar-footer').width());
            } else {
                $('#sidebar-footer > div').removeClass('sidebar-fixed');
                $('#sidebar-footer > div').width('auto');
            }
        }
    });

    $('.scroll-to-top').click(function (e) {
        e.preventDefault();
        $('html, body').animate({scrollTop: 0}, 300);
    });

    $('body').on('click', '.toggle-post', function () {
        var $me = $(this),
            $content = $me.parents('.post').find('.collapsing-content'),
            isCollapsed = $content.is(':hidden'),
            title = isCollapsed ? "Collapse post" : "Expand post";

        $me.find('i.fa').toggleClass('fa-minus-square-o', isCollapsed);
        $me.find('i.fa').toggleClass('fa-plus-square-o', !isCollapsed);
        $content.toggle();
        $me.attr('title', title);

        return false;
    });

    var resizeTimer;
    $(window).resize(function () {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function () {

        }, 100);
    });

    $('.search-form').submit(function (e) {
        var searchKeyword = $(this).find('input[type="text"]').val();
        e.preventDefault();
        if (searchKeyword !== '') {
            var searchAction = $(this).attr('action');
            searchAction = searchAction.replace('keyword', searchKeyword);
            var cat = $(this).find('select').val();
            if(cat !== 'everywhere'){
                searchAction = searchAction.replace('ew', cat);
            }else{
                searchAction = searchAction.replace('/ew', '');
            }
            window.location = searchAction;
        }
    });
    $('.search-form .btn-search').click(function () {
        $(this).parents('.search-form').submit();
    });
});



// Lazy load images
var lazyloadTimer;

$(window).on('DOMContentLoaded load resize scroll', function () {;
	//@todo add force trigger param
	clearTimeout(lazyloadTimer);
	lazyloadTimer = setTimeout(function () {
		var images = $("img.lazyload");
		// load images that have entered the viewport
		$(images).each(function (index) {
			if (inViewport(this, 100)) {
				$(this).removeClass("lazyload");
				console.log('loaded ' + $(this).attr("data-src"));
				$(this).attr("src",$(this).attr("data-src"));
				$(this).removeAttr("data-src");
			}
		})
		// if all the images are loaded, stop calling the handler
		//   if (images.length == 0) {
		//     $(window).off('DOMContentLoaded load resize scroll')
		//   }
	}, 100);
})

function inViewport (el, threshold) {

    var r, html;
    if ( !el || 1 !== el.nodeType ) { return false; }
    html = document.documentElement;
    r = el.getBoundingClientRect();

    return ( !!r 
      && r.bottom >= 0 + threshold
      && r.right >= 0 + threshold
      && r.top <= html.clientHeight + threshold
      && r.left <= html.clientWidth + threshold
    );

}
