<?php

namespace app\widgets;

use Yii;
use yii\helpers\Html;
use yii\authclient\widgets\AuthChoiceAsset;
use yii\authclient\widgets\AuthChoiceStyleAsset;

class AuthChoice extends \yii\authclient\widgets\AuthChoice
{
    /**
     * Initializes the widget.
     */
    public function init()
    {
        $view = Yii::$app->getView();
        if ($this->popupMode) {
            AuthChoiceAsset::register($view);
            $view->registerJs("\$('#" . $this->getId() . "').authchoice({ popup:{width: 480, height: 640} });");
        } else {
            AuthChoiceStyleAsset::register($view);
        }
        $this->options['id'] = $this->getId();
        echo Html::beginTag('div', $this->options);
    }
}
