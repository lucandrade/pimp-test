<?php

namespace app\widgets;


use app\assets\LightGalleryAsset;
use app\helpers\ArrayHelper;
use yii\base\Widget;
use yii\bootstrap\Html;

class ZoomImage extends Widget
{
    public $originSrc;
    public $thumbSrc;
    public $parentSelector = '#overflow-wrapper';
    public $selector;

    public $options = [];
    public $defaultOptions = [
        'id' => 'light-gallery',
    ];
    public $imgOptions = [];

    public function run()
    {
        $options = ArrayHelper::merge($this->defaultOptions, $this->options, [
        	'data-src' => $this->originSrc,
        ]);
        $view = $this->getView();
        LightGalleryAsset::register($view);
        $selector = empty($this->selector) ? '#' . $options['id'] : $this->selector;
        $view->registerJs(
            "$('$this->parentSelector').lightGallery({'selector': '$selector'});
            pahIniter.widgets.lightGallery($('$this->parentSelector'));"
        );

        return
            Html::tag('div',
                    Html::img($this->thumbSrc, $this->imgOptions),
                $options
            );
    }
}