$(document).ready(function(){

    if($('#abuse-report-page').length == 0){
        return false;
    }

    $('#abuse-report-page #abuse-btn').on('click', function(){
        var hash = String(Math.random()).substring(2, 15);
        var request = {
            hash : hash,
            url : $('#abuse-url').text()
        };

        $.getJSON(
            $(this).data('url'),
            {req:JSON.stringify(request)},
            function(res){
                if(res.code !== undefined && res.code == 0){
                    $('#abuse-report-page').text('Thank you for your report!');
                }else{
                    bootbox.alert("We couldn\'t recognize image by your request");
                }
            });
    });

});