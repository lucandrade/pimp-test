<?php
namespace app\models\interfaces;


interface CommentsServiceModel
{
    public function getOwnerId();

    public function getUrl();

    public function getIsActive();
}