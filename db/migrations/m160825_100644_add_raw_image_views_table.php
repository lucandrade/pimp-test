<?php

use yii\db\Migration;

class m160825_100644_add_raw_image_views_table extends Migration
{
    public $table = 'raw_image_views';
    public $imageRef = 'image_id';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            $this->imageRef => $this->integer(),
            'counted_at' => $this->timestamp()
        ], $tableOptions);

        $this->createIndex($this->imageRef . "_index", $this->table, $this->imageRef);
        $this->createIndex('viewed_index', $this->table, ['counted_at']);
        $this->addForeignKey($this->imageRef . "_{$this->table}_fk", $this->table, $this->imageRef, 'image', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey($this->imageRef . "_{$this->table}_fk", $this->table);
        $this->dropTable($this->table);
    }
}
