<?php
namespace app\commands;

use app\helpers\LogHelper;
use app\models\Post;
use Yii;
use app\components\VideoUploader;
use app\helpers\FileHelper;
use app\helpers\ImageHelper;
use app\helpers\VideoHelper;
use app\models\PostBlock;
use yii\console\Controller;
use yii\console\Exception;
use yii\helpers\Console;
use app\models\Video;
use yii\helpers\Json;

class VideoController extends Controller
{
    public $chunkSizeBytes = 1 * 1024 * 1024;

    public function actionUpload($postId, $file = null, $admin = null)
    {
        /** Implement this once we post creation process will be ready */
        $postBlock = PostBlock::findOne(['id' => $postId]);

        if ($postBlock === null) {
            echo $this->ansiFormat("Post block with such id was not found\n", Console::FG_RED);
            return;
        }

        $fileName = $postBlock->isGIF() ? $postBlock->mediaFileName : $postBlock->videoFileName;
        $file = empty($fileName) ? $file : $fileName;

        if (!file_exists($file)) {
            echo "Can't find file $file";
            return 0;
        }

        if (exif_imagetype($file) == IMAGETYPE_GIF) {
            $webmFile = FileHelper::replaceExtension($file, 'webm');

            if (file_exists($webmFile)) {
                unlink($webmFile);
            }

            $output = ImageHelper::convertGifToWebm($file, $webmFile);

            if (file_exists($webmFile)) {
                $file = $webmFile;
            } else {
                throw new Exception("Coudn't convert $file to webm! Convert logs:\n" . join('\n', $output));
            }

        }

        $thumbFile = $postBlock->thumbFileName;
        $tmpThumbFileName = $this->getTmpFileName($thumbFile);
        $output = VideoHelper::createVideoThumbnail($file, $tmpThumbFileName);

        if (file_exists($tmpThumbFileName)) {
            rename($tmpThumbFileName, $thumbFile);
            echo $this->ansiFormat("Thumbnail was successfully cretaed(png)!\n", Console::FG_GREEN);
        } else {
            echo $this->ansiFormat("Coudn't convert $file to png!\n", Console::FG_RED);
        }

        if (!file_exists($thumbFile)) {
            throw new Exception("Error while convert $file! Convert logs:\n" . join('\n', $output));
        }

        $collection = Yii::$app->authAdminCollection;
        $ytadmin = $collection->getClient('ytadmin');
        $status = $ytadmin->getFileTokenStatus();
        if (strtolower($status) !== 'ok') {
            echo $this->ansiFormat("\n Token is expired, please renew it for uploading!", Console::FG_RED);
            return;
        }
        $token = $ytadmin->getTokenFromFile();

        $videoUploader = new VideoUploader(['chunkSizeBytes' => $this->chunkSizeBytes]);

        if (empty($postBlock->text)) {
            $title = pathinfo($postBlock->file_name, PATHINFO_BASENAME);
            $title = str_replace('.' . pathinfo($postBlock->file_name, PATHINFO_EXTENSION), '', $title);
        } else {
            $title = $postBlock->text;
        }

        $status = $videoUploader->uploadToYoutube($file, $title, $token, [
            'privacyStatus' => $admin ? 'public' : "private"
        ]);


        if (!$status || $videoUploader->hasErrors()) {
            echo $this->ansiFormat("Errors while video uploading:\n", Console::FG_RED);
            echo join('\n', $videoUploader->getErrors());
            return;
        }

        if ($postBlock->updateVideoData($status)) {
            echo $this->ansiFormat("Video was successfully uploaded!", Console::FG_GREEN);
        } else {
            echo $this->ansiFormat("Errors while video data updating:\n", Console::FG_RED);
            echo join('\n', $postBlock->getErrors());
        }

        if (
            (int)$postBlock->post->status === Post::STATUS_PROCESSING &&
            !$postBlock->post->isPostProcessing()
        ) {
            $postBlock->post->status = Post::STATUS_ACTIVE;
            $postBlock->post->update(true, ['status']);
        }

        /**
         * @var \app\components\authclients\autopost\Facebook $client
         *//*Temporary switched off uploading to facebook*/;
//        $client = \Yii::$app->authAdminCollection->getClient("APFacebook");
//        $this->stdout(PHP_EOL . "Starting uploading to facebook..." . PHP_EOL);
//        $result = $client->uploadVideo($file, [
//                'title' => $title,
//                'call_to_action' => Json::encode([
//                    'type' => 'WATCH_MORE',
//                    'value'=>[
//                        'link' => 'http://owned.com/',
//                        'link_caption' => 'owned.com'
//                    ]
//                ])
//            ]);
//        if($result['success']){
//            $this->stdout("Video was successfully uploaded, id " . $result['video_id'] . PHP_EOL, Console::FG_GREEN );
//        }else{
//            $this->stdout("Errors while uploading video to facebook: " . $result['error'] . PHP_EOL, Console::FG_RED);
//        }
    }

    public function actionConvert($postId, $file = null)
    {
        $postBlock = PostBlock::findOne($postId);
        if ($postBlock === null) {
            echo $this->ansiFormat("Post block with such id was not found\n", Console::FG_RED);
            return;
        }

        $file = $postBlock->isGIF() ? $postBlock->mediaFileName : $file;

        if (!file_exists($file)) {
            echo $this->ansiFormat("Can't find file $file\n", Console::FG_RED);
            return;
        }

        if (exif_imagetype($file) === IMAGETYPE_GIF) {

            $webmFile = $postBlock->webmFileName;
            if (file_exists($webmFile)) {
                unlink($webmFile);
            }

            $tmpFileName = $this->getTmpFileName($webmFile);
            $output = ImageHelper::convertGifToWebm($file, $tmpFileName);

            if (file_exists($tmpFileName)) {
                rename($tmpFileName, $webmFile);
                echo $this->ansiFormat("Gif was successfully converted to webm!\n", Console::FG_GREEN);
            } else {
                echo $this->ansiFormat("Coudn't convert $file to webm!\n", Console::FG_RED);
            }

            $m4vFile = $postBlock->m4vFileName;
            if (file_exists($m4vFile)) {
                unlink($m4vFile);
            }

            $tmpM4vFileName = $this->getTmpFileName($m4vFile);
            $output[] = ImageHelper::convertGifToM4v($file, $tmpM4vFileName);

            if (file_exists($tmpM4vFileName)) {
                rename($tmpM4vFileName, $m4vFile);
                echo $this->ansiFormat("Gif was successfully converted to m4v!\n", Console::FG_GREEN);
            } else {
                echo $this->ansiFormat("Coudn't convert $file to m4v!\n", Console::FG_RED);
            }

            if (!file_exists($webmFile) || !file_exists($m4vFile)) {
                throw new Exception("Error while convert $file! Convert logs:\n" . join('\n', $output));
            }

            $thumbFile = $postBlock->thumbFileName;
            $tmpThumbFileName = $this->getTmpFileName($thumbFile);
            $output[] = ImageHelper::createGifThumbnail($file, $tmpThumbFileName);

            if (file_exists($tmpThumbFileName)) {
                rename($tmpThumbFileName, $thumbFile);
                echo $this->ansiFormat("Gif was successfully converted to png!\n", Console::FG_GREEN);
            } else {
                echo $this->ansiFormat("Coudn't convert $file to png!\n", Console::FG_RED);
            }

            if (!file_exists($thumbFile)) {
                throw new Exception("Error while convert $file! Convert logs:\n" . join('\n', $output));
            }

            if (
                (int)$postBlock->post->status === Post::STATUS_PROCESSING &&
                !$postBlock->post->isPostProcessing()
            ) {
                $postBlock->post->status = Post::STATUS_ACTIVE;
                $postBlock->post->update(true, ['status']);
            }

        } else {
            echo $this->ansiFormat("Wrong file type\n", Console::FG_RED);
            return;
        }

    }

    public function getTmpFileName($fileName)
    {
        $baseName = basename($fileName);
        return str_replace($baseName, 'tmp_' . $baseName, $fileName);
    }
}