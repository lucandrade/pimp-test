<?php
use yii\helpers\Url;

?>
<hr>
<!-- Footer -->
<footer>
    <div class="row">
        <div class="col-lg-12 text-center">
            <a href="/">Main Page</a>
            &nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="<?= Url::to('/forum') ?>" target="_blank">Forums</a>
            &nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="<?= Url::to(['plugin/index']) ?>">Forum Plugins</a>
            &nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="<?= Url::to(['site/tos']) ?>">Terms of Service</a>
            &nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="http://www.pimpandhost.com/2257.html" target="_blank">2257</a>
            &nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="http://www.rtalabel.org" target="_blank" rel="nofollow">RTA</a>
            &nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="<?= Url::to(['site/abuse']) ?>">Abuse</a>
        </div>
    </div>
</footer>

<?php if (Yii::$app->controller->id === 'post') { ?>
    <a href="#" class="scroll-post-top" title="scroll to post top">Scroll to post top</a>
    <a href="#" class="scroll-post-bottom" title="scroll to post bottom">Scroll to post bottom</a>
<?php } ?>

<a href="#" class="scroll-to-top" title="scroll to top">Scroll to top</a>
