<?php

namespace app\widgets;


use app\assets\AlbumLockerAsset;
use app\models\forms\LockAlbum;
use yii\base\Widget;

class AlbumLocker extends Widget
{
    private $_lockForm;

    public function init()
    {
        $this->_lockForm = new LockAlbum();
    }

    public function getLockForm()
    {
        return $this->_lockForm;
    }

    public function run()
    {
        AlbumLockerAsset::register($this->getView());
        return $this->render('AlbumLocker');
    }
}