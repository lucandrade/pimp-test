<?php

namespace app\assets;


use yii\web\AssetBundle;

class AbuseReportAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/abuse-report.css',
    ];

    public $js = [
        'js/abuse-report.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
        'app\assets\BootboxAsset'
    ];

    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}