<?php

namespace app\controllers\actions;
use yii\web\NotFoundHttpException;


use yii\authclient\AuthAction;

class AdminAuthAction extends AuthAction
{
    public function run()
    {
        if (!empty($_GET[$this->clientIdGetParamName])) {
            $clientId = $_GET[$this->clientIdGetParamName];
            /* @var $collection \yii\authclient\Collection */
            $collection = \Yii::$app->get($this->clientCollection);
            if (!$collection->hasClient($clientId)) {
                throw new NotFoundHttpException("Unknown auth client '{$clientId}'");
            }

            $client = $collection->getClient($clientId);

            if(!empty($client->ownAuthMethod)){
                $client->auth();

            }else{
                return $this->auth($client);
            }
        } else {
            throw new NotFoundHttpException();
        }
    }
}