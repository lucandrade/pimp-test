<?php

namespace app\components;


class ActiveRecord extends \yii\db\ActiveRecord
{
    public function getTableName(){
        return static::tableName();
    }
}