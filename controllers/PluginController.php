<?php

namespace app\controllers;


use app\assets\UploaderPluginVbullet;
use app\components\Controller;
use yii\base\InvalidConfigException;
use yii\filters\HttpCache;
use Yii;

class PluginController extends Controller
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors[] = [
            'class' => HttpCache::className(),
            'only' => ['uploader'],
            'lastModified' => function ($action, $params) {
                return time();
            },
            'cacheControlHeader' => 'no-cache'
        ];

        return $behaviors;
    }

    public function actionUploader()
    {
        $jsFile = Yii::getAlias('@webroot/assets/' . UploaderPluginVbullet::$jsTargetFilename);
        if(file_exists($jsFile)){
            return file_get_contents($jsFile);
        }else{
            throw new InvalidConfigException("Plugin file is not found, please run command to compress the bundle to generate it");
        }
    }


    public function actionTest()
    {
        $this->layout = 'empty';
        return $this->render('test');
    }

    public function actionIndex()
    {
        $this->layout = '1-column';
        $plugins = [
            [
                'shortName' => 'vBulletin Plug-in',
                'fullName' => 'vBulletin uploader plugin',
                'alias' => 'vb',
                'steps' => [
                    "<strong>UPGRADE:</strong> Please Uninstall the old one!",
                    "<strong>IMPORTANT:</strong> Web root dir has to have write permissions in order to create the bridging php file!",
                    "Log-in to admin panel",
                    "Go to <strong>Plugins & products</strong>",
                    "Select <strong>Manage products</strong>",
                    "Click on <strong>Add/Import Product</strong>",
                    "Unizp & Browse to the <strong>XML</strong> file and click <strong>Import</strong>",
                    "All done!"
                ],
            ]
        ];


        return $this->render('index', compact('plugins'));
    }
}