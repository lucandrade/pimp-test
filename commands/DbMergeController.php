<?php

namespace app\commands;

use app\models\PostBlock;
use Yii;
use yii\console\Controller;
use yii\db\Connection;
use yii\db\Query;
use app\models\User;
use app\models\Post;

class DbMergeController extends Controller
{
    public $donorDbHost;
    public $donorDbName;
    public $donorDbUser;
    public $donorDbPassword;

    public $importImagesPathAlias;
    public $importVideoPathAlias;

    private $_donorDb;

    public function init()
    {
        parent::init();
        if (
            isset(Yii::$app->params['import']['imagesPathAlias']) &&
            isset(Yii::$app->params['import']['videoPathAlias'])
        ) {
            $this->importImagesPathAlias = Yii::$app->params['import']['imagesPathAlias'];
            $this->importVideoPathAlias = Yii::$app->params['import']['videoPathAlias'];
        }
        if (
            isset(Yii::$app->params['import']['donorDbName']) &&
            isset(Yii::$app->params['import']['donorDbHost'])
        ) {
            $this->donorDbName = Yii::$app->params['import']['donorDbName'];
            $this->donorDbHost = Yii::$app->params['import']['donorDbHost'];
        }
    }

    public function options($actionID)
    {
        return ['donorDbName', 'donorDbUser', 'donorDbPassword', 'donorDbHost'];
    }

    private function prepareDonorDb()
    {
        $db = Yii::$app->db;
        $this->_donorDb = new Connection([
            'charset' => 'utf8',
            'username' => empty($this->donorDbUser) ? $db->username : $this->donorDbUser,
            'password' => empty($this->donorDbPassword) ? $db->password : $this->donorDbPassword,
            'dsn' => "mysql:host={$this->donorDbHost};dbname=" . $this->donorDbName
        ]);

    }

    private function importUsersTable($batchSize = 100)
    {
        $importedUsers = 0;
        $usersQuery = (New Query())->from('tbl_users');
        Yii::$app->db->createCommand("SET foreign_key_checks = 0")->execute();
        Yii::$app->db->createCommand()->truncateTable('user')->execute();

        foreach ($usersQuery->batch($batchSize, $this->_donorDb) as $users) {
            foreach ($users as $batchUser) {
                $user = new User();
                $user->setAttributes($batchUser, false);
                $user->created_at = $batchUser['create_at'];
                $user->is_admin = $batchUser['superuser'];
                if ($user->save()) {
                    echo "\r" . ++$importedUsers . " record(s) were imported from table `tbl_users`";
                } else {
                    echo "\nUser #$user->id were not imported because of " . json_encode($user->getErrors()) . "\n";
                }
            }
        }

    }

    public function actionMigrateUsers()
    {
        $this->prepareDonorDb();
        $this->importUsersTable();
    }

    public function dir_prefix($str)
    {
        $str = preg_replace('/[^\d\w-]/', '', $str);
        $arr = str_split($str);
        $result = '';
        $result .= (isset($arr[0]) ? $arr[0] : '_') . '/';
        $result .= (isset($arr[1]) ? $arr[1] : '_') . '/';
        $result .= (isset($arr[2]) ? $arr[2] : '_') . '/';
        $result .= (isset($arr[3]) ? $arr[3] : '_') . '/';
        return $result;
    }

    public function string_to_filename($string, $separator = '_')
    {
        $filename = preg_replace('~[^\p{L}\p{N}]~u', ' ', $string);
        $filename = preg_replace('/[^A-Za-z0-9]+/', ' ', $filename);
        $filename = preg_replace('~ +~', $separator, trim($filename));
        return $filename;
    }

    public function createImportedPost($batchPost)
    {
        $post = new Post(['scenario' => 'import']);
        $post->id = $batchPost['id'];
        $post->title = $batchPost['title'];
        $post->user_id = (int)$batchPost['author_id'];
        $post->is_authors = $batchPost['exclusive'];
        $post->is_adult = $batchPost['adult'];
        $post->is_draft = $batchPost['status'] == 1 ? 0 : 1;
        $post->created_at = $batchPost['create_time'];
        $post->updated_at = $batchPost['update_time'];
        $post->live_at = $batchPost['publish_time'];
        $post->rating = $batchPost['voted'];
        return $post;
    }

    public function combinePostImages($posts, $images)
    {
        foreach ($posts as $id => $post) {
            $post['images'] = [];
            foreach ($images as $key => $image) {
                if ($image['post_id'] === $post['id']) {
                    $post['images'][$image['id']] = $image;
                    unset($images[$key]);
                }
            }
            $posts[$id] = $post;
        }

        return $posts;
    }

    private function importPostsTImage($batchSize = 100)
    {
        $importedPosts = 0;
        /**
         * accepted types
         * type 1 = jpg
         * type 3 = gif
         * type 4 = png(mem)
         */
        $postsQuery = (New Query())
            ->indexBy('id')
            ->from('tbl_post p')
            ->where('p.type = 1 OR p.type = 3 OR p.type = 4');

        foreach ($postsQuery->batch($batchSize, $this->_donorDb) as $posts) {

            $images = (New Query())
                ->indexBy('id')
                ->from('tbl_images i')
                ->where(['post_id' => array_keys($posts)])
                ->all($this->_donorDb);

            $posts = $this->combinePostImages($posts, $images);
            foreach ($posts as $batchPost) {
                $postID = $batchPost['id'];
                $post = $this->createImportedPost($batchPost);
                $post->detachBehavior('timestampBehavior');

                $imageFileExist = false;
                foreach ($batchPost['images'] as $key => $image) {
                    $path = Yii::getAlias($this->importImagesPathAlias);
                    $fileName = $path . $this->dir_prefix($image['id']) . $image['id'] . '/' . $image['name'] . '.' . $image['ext'];

                    if (file_exists($fileName)) {
                        $image['fileName'] = $fileName;
                        $imageFileExist = true;
                    }
                    $batchPost['images'][$key] = $image;
                }

                if (!$imageFileExist) {
                    $post->addError('title', 'Image file(original) not exist');
                } else if ($post->save()) {

                    $post->importTags($batchPost['tags']);
                    $post->importImages($batchPost['images']);

                    echo "\r" . ++$importedPosts . " record(s) type image were imported from table `tbl_post`";
                } else {
                    echo "\nPost #$postID were not imported because of " . json_encode($post->getErrors()) . "\n";
                }
            }
        }
    }

    public function combinePostVideo($posts, $videos)
    {
        foreach ($posts as $id => $post) {
            $post['video'] = [];

            foreach ($videos as $key => $video) {
                if ($video['post_id'] === $post['id']) {
                    $video['video_url'] = $post['content'];
                    $post['video'][$video['id']] = $video;
                    unset($videos[$key]);
                    break;
                }
            }

            $posts[$id] = $post;
        }

        return $posts;
    }

    private function importPostsTVideo($batchSize = 100)
    {
        $importedPosts = 0;
        /**
         * accepted types
         * type 2 = webm
         */
        $postsQuery = (New Query())
            ->indexBy('id')
            ->from('tbl_post p')
            ->where('p.type = 2');

        foreach ($postsQuery->batch($batchSize, $this->_donorDb) as $posts) {

            $videos = (New Query())
                ->indexBy('id')
                ->from('tbl_video v')
                ->where(['post_id' => array_keys($posts)])
                ->all($this->_donorDb);

            $posts = $this->combinePostVideo($posts, $videos);
            foreach ($posts as $batchPost) {
                $postID = $batchPost['id'];
                $post = $this->createImportedPost($batchPost);

                $videoFileExist = false;
                foreach ($batchPost['video'] as $key => $video) {
                    $path = Yii::getAlias($this->importVideoPathAlias);
                    $videoName = $this->string_to_filename(strtolower($post->title), '_');
                    $fileName = $path . $this->dir_prefix($video['id']) . $video['id'] . '/' . $videoName . '.' . $video['ext'];
                    if (file_exists($fileName)) {
                        $video['fileName'] = $fileName;
                        $videoFileExist = true;
                    } else {
                        $postBlock = new PostBlock();
                        $postBlock->video_url = $video['video_url'];
                        if ($postBlock->validate(['video_url'])) {
                            $videoFileExist = true;
                        }
                    }
                    $batchPost['video'][$key] = $video;
                }

                if (!$videoFileExist) {
                    $post->addError('title', 'Video file(original) not exist');
                } else if ($post->save()) {

                    $post->importTags($batchPost['tags']);
                    $post->importVideo($batchPost['video']);

                    echo "\r" . ++$importedPosts . " record(s) type video were imported from table `tbl_post`";
                } else {
                    echo "\nPost #$postID were not imported because of " . json_encode($post->getErrors()) . "\n";
                }
            }
        }
    }

    public function actionMigratePosts()
    {
        $this->prepareDonorDb();

        Yii::$app->db->createCommand("SET foreign_key_checks = 0")->execute();
        Yii::$app->db->createCommand()->truncateTable('post_to_tag')->execute();
        Yii::$app->db->createCommand()->truncateTable('tag')->execute();
        Yii::$app->db->createCommand()->truncateTable('post_block')->execute();
        Yii::$app->db->createCommand()->truncateTable('user_seen_post')->execute();
        Yii::$app->db->createCommand()->truncateTable('post')->execute();
        Yii::$app->db->createCommand()->truncateTable('JobLog')->execute();
        Yii::$app->db->createCommand()->truncateTable('JobQueue')->execute();

        $this->importPostsTImage();
        $this->importPostsTVideo();
    }

    public function actionFillActivKey($batchSize = 1000)
    {
        $filledUsers = 0;
        $usersQuery = User::find()->where('activkey IS NULL OR activkey = ""');
        foreach ($usersQuery->batch($batchSize) as $users) {
            foreach ($users as $batchUser) {
                /**
                 * @var $batchUser \app\models\User;
                 */
                $batchUser->generateAuthKey();
                if ($batchUser->update(true, ['activkey'])) {
                    echo "\r" . ++$filledUsers . " record(s) were imported from table `tbl_users`";
                } else {
                    echo "\nUser #$batchUser->id were not filled because of " . json_encode($batchUser->getErrors()) . "\n";
                }
            }

        }
    }
}