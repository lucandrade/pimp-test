<?php

namespace app\components;

use app\helpers\ConsoleHelper;
use yii\base\Object;

pcntl_signal_dispatch();

class Daemon extends Object
{

    public $maxProcesses = 1;
    protected $stopServer = false;
    protected $currentJobs = [];


    public static function mainPidFile()
    {
        return self::getMainDir() . "main.pid";
    }

    public static function workersPidFile()
    {
        return self::getMainDir() . "workers.pid";
    }

    public static function getMainConsoleFile()
    {
        return self::getMainDir() . "main_console.log";
    }

    public static function getErrorConsoleFile()
    {
        return self::getMainDir() . "main_error.log";
    }


    public static function getMainDir()
    {
        $dir = \Yii::getAlias("@app/runtime/workers/");
        if(!file_exists($dir)){
            mkdir($dir);
        }

        return $dir;
    }

    public function __construct($config = [])
    {
        ConsoleHelper::echoWithDate("Сonstructed daemon controller".PHP_EOL);
        pcntl_signal(SIGTERM, array($this, "childSignalHandler"));
        pcntl_signal(SIGCHLD, array($this, "childSignalHandler"));
        parent::__construct($config);
    }



    public function run(callable $jobScript)
    {
        ConsoleHelper::echoWithDate("Running daemon controller".PHP_EOL);

        if(file_exists(self::mainPidFile()))
        {
            $pid = file_get_contents(self::mainPidFile());
            ConsoleHelper::echoWithDate("Daemon is already running, its pid is #$pid. Please stop it before running new one.");
            exit();
        }


        $child_pid = \pcntl_fork();
        if ($child_pid) {

            // Выходим из родительского, привязанного к консоли, процесса
            exit();
        }
        // Делаем основным процессом дочерний.
        posix_setsid();


        fclose(STDIN);
        fclose(STDOUT);
        fclose(STDERR);
        $STDIN = fopen('/dev/null', 'r');
        $STDOUT = fopen(self::getMainConsoleFile(), 'ab');
        $STDERR = fopen(self::getErrorConsoleFile(), 'ab');


        $posixPid = posix_getpid();
        ConsoleHelper::echoWithDate("Main daemon pid Id is ". $posixPid);
        file_put_contents(self::mainPidFile(), $posixPid);
        while (!$this->stopServer)
        {
            \pcntl_signal_dispatch();
            if(count($this->currentJobs) <= $this->maxProcesses){
                $this->launchJob($jobScript);
            }else{
                sleep(1);
            }
        }

        $this->removeFiles();
    }


    protected function removeFiles()
    {
        if($this->stopServer)
        {
            unlink(self::mainPidFile());
            unlink(self::getMainConsoleFile());
            unlink(self::getErrorConsoleFile());
        }
    }

    protected function launchJob(callable $jobScript)
    {
        $pid = pcntl_fork();
        if ($pid == -1) {
            error_log('Could not launch new job, exiting');
            return FALSE;
        }
        elseif ($pid) {
            //parent process
            $this->currentJobs[$pid] = TRUE;
            $this->updateWorkersPidFile();
        }
        else {
            //child process

            call_user_func($jobScript, getmypid());
            exit();
        }
        return TRUE;
    }


    public function childSignalHandler($signo, $pid = null, $status = null)
    {
        switch($signo)
        {
            case SIGTERM:
                $this->stopServer = true;
                unlink(self::mainPidFile());
                break;
            case SIGCHLD:
                if (!$pid) {
                    $pid = pcntl_waitpid(-1, $status, WNOHANG);
                }
                while ($pid > 0) {
                    if ($pid && isset($this->currentJobs[$pid])) {
                        unset($this->currentJobs[$pid]);
                    }

                    $this->updateWorkersPidFile();
                    $pid = pcntl_waitpid(-1, $status, WNOHANG);
                }
                break;
            default:
                // все остальные сигналы
        }
    }


    protected function updateWorkersPidFile()
    {
        $file = self::workersPidFile();
        $pids = implode(',',array_keys($this->currentJobs));
        file_put_contents($file,$pids);
    }



    public static function stop()
    {
        if(file_exists(self::mainPidFile()))
        {
            ConsoleHelper::echoWithDate(" KIlling main processs...".PHP_EOL);
            $mainPid = file_get_contents(self::mainPidFile());
            if(\posix_kill($mainPid, SIGTERM)){
                ConsoleHelper::echoWithDate("Main daemon process #" .$mainPid . " was killed" . PHP_EOL);
            }else
            {
                $errorNo = posix_get_last_error();

                if($errorNo == 3){
                    unlink(self::mainPidFile());
                }
                ConsoleHelper::echoWithDate("Coudn't kill main process #$mainPid. Error no $errorNo. Error message:" . posix_strerror($errorNo) . PHP_EOL);
            }
        }else{
            ConsoleHelper::echoWithDate("Main pid file not found. Make sure process is running." . PHP_EOL);
        }

        if(file_exists(self::workersPidFile()))
        {
            $pids = file_get_contents(self::workersPidFile());
            $pids = explode(',',$pids);
            ConsoleHelper::echoWithDate("Killing child worker processes...:" . PHP_EOL);
            foreach($pids as $pid)
            {
                if(\posix_kill($pid, SIGKILL)){
                    ConsoleHelper::echoWithDate("Child process #$pid was killed" . PHP_EOL);
                }else{
                    $errorNo = posix_get_last_error();
                    ConsoleHelper::echoWithDate("Coudn't kill child process #$pid. Error no $errorNo. Error message:" . posix_strerror($errorNo) . PHP_EOL);
                }
            }
            unlink(self::workersPidFile());
        }
        else{
            ConsoleHelper::echoWithDate("Workers pid file not found. Make sure child processes are running." . PHP_EOL);
        }
    }








}