var pahIniter = (function(){


    var initWrapper = function($wrapper){

        if($wrapper.length == 0){
            $('#selection-bar').hide();
            return false;
        }

        $wrapper.imageSelector({
            hoverBarContent:function($wrapper){
                return $wrapper.parents('.image-block').find('.hover-donor .hover-bar-content').html();
            },

            hoverInfoContent:function($wrapper){
                return $wrapper.parents('.image-block').find('.hover-donor .hover-info-content').html();
            },

            removeSettings:{
                url:$("#image-list-container").data('removeurl'),
                getIds:function($selectedWrappers){
                    return imageListUtilities.getIdsFromWrappers($selectedWrappers);
                },
                successRemoved:function(ids, $wrappers){
                    pahIniter.widgets.albumMetrics.removeWrappers($wrappers);
                    $wrappers.fadeOut(function(){
                        $(this).parents('.image-block').remove();
                    });
                }
            }
        });

        $wrapper.on('click', '.hover-bar .fa-download', function(e){
            e.stopPropagation();
            if($(this).hasClass('icon-disabled')){
                return false;
            }
            var $wrapper = $(this).parents('.image-wrapper'),
                url = $wrapper.data('origin-url'),
                filename  = $wrapper.data('filename');
            $wrapper.imageSelector('download');
            return false;
        });

        $wrapper.on('click',' .hover-bar .fa-trash-o', function(e){
            e.stopPropagation();
            $(this).parents('.image-wrapper').imageSelector('remove');
            return false;
        });

        $wrapper.on('click', ' .hover-bar .fa-picture-o', function(e){
            e.stopPropagation();
            window.location.href = $(this).parents('a.image-wrapper').attr('href');
        });

        $wrapper.on('click',' .hover-bar .fa-edit', function(e){
            e.stopPropagation();
            if($(this).hasClass('icon-disabled')){
                return false;
            }
            var win = window.open($(this).data('url'), '_blank');
            win.focus();
            return false;
        });

        $wrapper.on('click',' .hover-bar .fa-share-alt', function(e){
            e.stopPropagation();
            var id = $(this).parents(' .image-block').data('key');
            shareModalWindow.loadAndShow([id], 'image', $(this));
            return false;
        });

        if(selectedIds.length > 0){
            $wrapper.each(function(){
                var id = imageListUtilities.getIdsFromWrappers($(this))[0];
                if(selectedIds.indexOf(id) > -1){
                    $(this).imageSelector('selectAll');
                }
            });
        }
    };


    var initPluginSelector = function(){

        var $imageWrappers  =  $('#image-list-container  .image-wrapper');
        $('#selection-bar').selectionBar("reset");
        $imageWrappers.imageSelector('unselectAll');
        initWrapper($imageWrappers);
    };

    var resetShareButtonCache = function(){
        $('.share-chain').data('codes', false);
    };

    return {
        albumPage:{
            _imageGallery: false,
            ready:function(imageGallery){
                var self = this;
                var pageSize = parseInt($('#page-size').text()) || 0;
                var $body = $('body');
                var $albumPage = $('#album-page');


                if($albumPage.length == 0){
                    return;
                }



                $('#btn-lock-album').albumLocker();

                $albumPage.on('click', '.title-icons .fa-trash', function(){
                    var pi = window.pahIniter;
                    pi.album.remove($albumPage.data('key'), function(res){
                        if(res.status && res.status == 'ok')
                        {
                            bootbox.alert("Album was successfully removed!", function(){
                                window.location.href = $albumPage.data('remove-redirect-url');
                            });
                        }
                    }, $albumPage.data('remove-url'));
                    return false;
                });


                $albumPage.on('click',' .title-icons .download-link', function(e){
                   if($(this).attr('href') == '#'){
                        window.pahIniter.album.download($albumPage.data('key'), $albumPage.data('download-url'),  $albumPage.data('archive-list-url'));
                       return false;
                   }
                });

                $body
                    .off('click', '.share-chain')
                    .on('click', '.share-chain', function(e){
                    var $me = $(this);
                        e.stopPropagation();
                        e.preventDefault();
                        shareModalWindow.loadAndShow($albumPage.data('key'), 'album', $me);
                    return false;
                });

                $('.stats-popover').popover();

                $('#slide-show').on('click', function(e){
                    var $me = $(this);
                    e.preventDefault();
                    var list = [];
                    $('.image-wrapper').each(function(){
                        list.push({
                            href:$(this).data('src'),
                            settings:{id: $(this).parents('.image-block').data('key')}
                        });
                    });

                    self._imageGallery = blueimp.Gallery(list, {
                        onnext:function(index){
                            if(!this.loadedPages){
                                return true;
                            }
                            var pageIndex = index % pageSize;

                            if(pageIndex == ((pageSize) - 1))
                            {
                                var nextPage = Math.floor(index/pageSize) + Math.min.apply(Math, this.loadedPages);
                                var $link = $('.pagination li a[data-page="'+nextPage+'"]');

                                if($link.length > 0){
                                    url = $link.attr('href');
                                    if(url){
                                        $.pjax({url:url, container:"#pjax-listview"});
                                        self._imageGallery['addToBeginning'] = false;
                                        return false;
                                    }
                                }
                            }

                            return true;
                        },

                        onprev:function(index){

                            if(!this.loadedPages){
                                return true;
                            }
                            var pageIndex = index % pageSize;

                            if(pageIndex === 0){
                                var prevPage = Math.floor(index/pageSize) + Math.min.apply(Math, this.loadedPages) - 2;
                                var $prevLink = $('.pagination li a[data-page="'+prevPage+'"]');
                                if($prevLink.length > 0){
                                    url = $prevLink.attr('href');
                                    if(url){
                                        $.pjax({url:url, container:"#pjax-listview"});
                                        self._imageGallery['addToBeginning'] = true;
                                        return false;
                                    }
                                }
                            }
                            return true;
                        },

                        startSlideshow:true,
                        slideshowInterval:4000
                    });
                    self._imageGallery['loadedPages'] = [parseInt($('.pagination li.active a').text())] || undefined;
                });


                $body.on('onAppended', '.image-wrapper', function(){
                    var $wrapper = $(this);
                    initWrapper($wrapper);
                   resetShareButtonCache();
                });
            },
            pjaxEnd:function(){
                if($('#album-page').length == 0){
                    return;
                }

                resetShareButtonCache();
                var imageGallery = this._imageGallery;

                if(imageGallery instanceof blueimp.Gallery){
                    var activePage = parseInt($('.pagination li.active a').text());
                    if(activePage){
                        if(imageGallery.loadedPages && imageGallery.loadedPages.indexOf(activePage)<0)
                        {
                            var urls = [];
                            $('.image-wrapper').each(function(){
                                urls.push($(this).data('src'));
                            });
                            imageGallery.loadedPages.push(activePage);
                            imageGallery.add(urls ,imageGallery.addToBeginning);
                        }

                        if(imageGallery.addToBeginning){
                            imageGallery.prev(true)
                        }else{
                            imageGallery.next(true)
                        }
                    }
                }
            },

        },

        imageList:{
            ready:function(){
                if($('#image-list-container ').length == 0){
                    return;
                }

                $('#selection-bar').selectionBar();
                initPluginSelector();

                $('#selection-bar .share').on('click', function(){
                    var ids = [];
                    $('#image-list-container .image-wrapper.selected').each(function(){
                        ids.push($(this).parents('.image-block').data('key'));
                    });

                    shareModalWindow.loadAndShow(ids,'image');
                });

                $('#selection-bar .move-to').on('click', function(){
                    $('.image-wrapper.selected').parents('.image-block').moveToAlbum({
                        successMoving:function(ids){
                            imageListUtilities.updateListView();
                        }
                    });
                    return false;
                });


                $('#selection-bar .remove').on('click', function(){
                    $('.image-wrapper').imageSelector('removeSelected');
                });
            },

            pjaxEnd: function(){
                if($('#image-list-container').length == 0){
                    return;
                }
                initPluginSelector();
            }
        },

        widgets:{
            xeditable:function(){
                $('.editable-wrapper .editable-content')
                    .off('hidden')
                    .on('hidden', function(){
                        $(this).siblings('i.fa').removeClass('pah-green').hide();
                    });

                $('.editable-wrapper .editable-content')
                    .off('shown')
                    .on('shown', function(){
                        $(this).siblings('i.fa').addClass('pah-green');
                    });
            },

            albumMetrics:{
                addTotalCount:function(count){
                    var $totalCount = $('#total-images-count'),
                        totalCount = parseInt($totalCount.text());

                    totalCount = totalCount + count;
                    $totalCount.text(totalCount);
                },

                addTotalSize: function(size){
                    var $totalSize = $('#total-album-size'),
                        totalSize = $totalSize.data('total-size');

                    totalSize = totalSize ? parseInt(totalSize) : 0;
                    totalSize += size;
                    $totalSize.data('total-size', totalSize);
                    $totalSize.text(pahIniter.utils.humanFileSize(totalSize));
                },
                removeWrappers:function($wrappers){
                    this.addTotalCount($wrappers.length*(-1));
                    var size = pahIniter.utils.getSize($wrappers);
                    this.addTotalSize(size*(-1));
                }
            },

            lightGallery:function($lightGallery){
                $lightGallery.on('onAfterOpen.lg', function(){
                    $('.lg-outer .lg-close').attr('title', 'Close');
                    $('.lg-outer #lg-download').attr('title', 'Download image');
                    $('.lg-outer #lg-zoom-in').attr('title', 'Zoom in');
                    $('.lg-outer #lg-zoom-out').attr('title', 'Zoom out');
                    $('.lg-outer #lg-actual-size').attr('title', 'See actual size');

                });
            }
        },

        album:{
            remove:function(albumId ,success, removeUrl){
                bootbox.confirm("Are you sure?", function(confirmed){
                    if(confirmed)
                    {
                        $.post(removeUrl,
                            {albumId:albumId},
                            success,'json');
                    }
                });
            },
            download:function(albumId, downloadUrl, archiveListUrl)
            {
                $.post(downloadUrl, {albumId:albumId},
                    function(data){
                        if(data.status && data.status == 'ok'){
                            bootbox.confirm("Your Album Is now being Archived.We will alert you the second the upload is ready via email & your control panel.Please make sure to download the file within 24H from the email, otherwise it will be deleted from our system.Do you want to go to Archive list?",
                                function(confirmed){
                                    if(confirmed){
                                        window.location.href = archiveListUrl
                                    }
                                });
                        }
                    },
                    'json'
                )

            }
        },

        utils:{
            getSize:function($wrappers){
                var size = 0;
                $wrappers.each(function(){
                   size += parseInt($(this).data('size'));
                });

                return size;
            },
            humanFileSize: function(size, unit){
                if( (!unit && size >= 1<<30) || unit == "GB")
                    return this.numberFormat(size/(1<<30),2)+"GB";
                if( (!unit && size >= 1<<20) || unit == "MB")
                    return this.numberFormat(size/(1<<20),2)+"MB";
                if( (!unit && size >= 1<<10) || unit == "KB")
                    return this.numberFormat(size/(1<<10),2)+"KB";
                return this.numberFormat(size)+" bytes";
            },
            numberFormat:function(number, decimals, dec_point, thousands_sep){	// Format a number with grouped thousands
                //
                // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
                // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
                // +	 bugfix by: Michael White (http://crestidg.com)

                var i, j, kw, kd, km;

                // input sanitation & defaults
                if( isNaN(decimals = Math.abs(decimals)) ){
                    decimals = 2;
                }
                if( dec_point == undefined ){
                    dec_point = ".";
                }
                if( thousands_sep == undefined ){
                    thousands_sep = " ";
                }

                i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

                if( (j = i.length) > 3 ){
                    j = j % 3;
                } else{
                    j = 0;
                }

                km = (j ? i.substr(0, j) + thousands_sep : "");
                kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
                //kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
                kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");


                return km + kw + kd;
            }
        }
    }
})();