<?php

namespace app\components\data;
use \Yii;
use yii\base\InvalidConfigException;
use yii\web\Request;


class Pagination extends \yii\data\Pagination
{
    public $maxPageNumber = false;

    public $maxItems = 1000;

    public $pageSizeLimit = false;

    public $defaultPageSize = false;

    public $userSettingName;

    public $allValue = 'all';

    public function getPageCount()
    {
        if($this->maxItems && $this->totalCount > $this->maxItems){
            $this->totalCount = $this->maxItems;
        }
        $count = parent::getPageCount();
        if($this->maxPageNumber && $count > $this->maxPageNumber){
            return $this->maxPageNumber;
        }
        return $count;
    }


    public function getLimit()
    {
        $limit =  parent::getLimit(); // TODO: Change the autogenerated stub
        if($this->maxItems){
            $offset = $this->getOffset();
            if(($offset+$limit) > $this->maxItems){
                return $this->maxItems - $offset;
            }

        }
        return $limit;
    }

    public function init()
    {
        if(!empty($this->userSettingName)){
            $pageSize = \Yii::$app->params['imagesPerPage'];
            $pageSizeFromParam = $this->getQueryParam($this->pageSizeParam, null);
            if(!empty($pageSizeFromParam)){
                if($pageSizeFromParam == $this->allValue){
                    $pageSizeFromParam = $this->maxItems;
                }
                $pageSize = $pageSizeFromParam;
            }

            if(!\Yii::$app->user->isGuest)
            {
                $user= \Yii::$app->user->getIdentity();

                if(!empty($pageSizeFromParam)) {
                    $user->setSettings($this->userSettingName, $pageSize);
                }else
                {
                    $value = $user->getSettings($this->userSettingName);
                    if(empty($value))
                    {
                        $value = $pageSize;
                        $user->setSettings($this->userSettingName, $value);
                    }else{
                        $pageSize = $value;
                    }
                }

            }
            $this->setPageSize($pageSize);
        }
        parent::init();
    }

    public function calculateMaxPageNumber($maxItems)
    {
        $pageSize = $this->getPageSize();
        $this->maxPageNumber = floor($maxItems/ $pageSize);
    }



    public function createUrl($page, $pageSize = null, $absolute = false)
    {
        $page = (int) $page;
        if (($params = $this->params) === null) {
            $request = Yii::$app->getRequest();
            $params = $request instanceof Request ? $request->getQueryParams() : [];
        }
        if ($page > 0 || $page >= 0 && $this->forcePageParam) {
            $params[$this->pageParam] = $page + 1;
        } else {
            unset($params[$this->pageParam]);
        }
        if (empty($pageSize)) {
            $pageSize = $this->getPageSize();
        }
        if ($pageSize != $this->defaultPageSize) {
            $params[$this->pageSizeParam] = $pageSize;
        } else {
            unset($params[$this->pageSizeParam]);
        }
        $params[0] = $this->route === null ? Yii::$app->controller->getRoute() : $this->route;
        $urlManager = $this->urlManager === null ? Yii::$app->getUrlManager() : $this->urlManager;
        if ($absolute) {
            return $urlManager->createAbsoluteUrl($params);
        } else {
            return $urlManager->createUrl($params);
        }
    }

    public function getPage($recalculate = false)
    {
        $page =  parent::getPage($recalculate);

        if(!empty($this->maxPageNumber) && $page > $this->maxPageNumber){
            return $this->maxPageNumber;
        }else{
            return $page;
        }
    }
}