<?php

/**
 * @var $this \yii\web\View
 */

use app\widgets\CuteCatIcon;
use app\widgets\RecentTags;
use app\widgets\Facebook;
use app\widgets\Adv;
use app\widgets\RelatedPosts;
use yii\bootstrap\Html;
use yii\helpers\Url;

?>

<?= Html::a("Upload image", ['/'], ['class' => 'btn btn-pah btn-lg center-block top-sibebar-block']) ?>
<?= Html::a("Submit post", ['post/update'], ['class' => 'btn btn-primary btn-lg center-block top-sibebar-block']) ?>
<br>
<br>
<div id='popular-tags-widget' class="well">
    <h4>Recent tags</h4>
    <?= RecentTags::widget(['tags' => $this->context->recentTags]) ?>
</div>
<p>
    <a href="<?= Url::to(['site/contact']) ?>" class="rlinks">
        Contact
    </a>
    <a href="<?= Url::to(['site/contact']) . '?adv' ?>" class="rlinks">
        Advertising
    </a>
    <a href="<?= Url::to(['site/privacy']) ?>" class="rlinks">
        Privacy
    </a>
</p>
<br>
<?php if (isset($this->params['postModel'])) { ?>

    <?php for ($adv = 1; $adv <= 3; $adv++) { ?>
        <?php if (RelatedPosts::hasModels()) { ?>
            <div class='adv-widget'><?= Adv::widget() ?></div>
            <br>
            <div id="similar-posts-widget">
                <?=
                RelatedPosts::widget([
                    'postModel' => $this->params['postModel']
                ])
                ?>
            </div>
            <br>
        <?php } ?>
    <?php } ?>

<?php }  ?>
<div id="sidebar-footer">
    <div class="">
        <br>
        <hr>
        <p>Copyright &copy; pimpandhost.com <?= date('Y') ?></p>
    </div>
</div>
