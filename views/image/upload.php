<?php
use \yii\helpers\Url;

\app\assets\MainPageAsset::register($this);
\app\assets\PostAsset::register($this);
\app\assets\YTExternalAsset::register($this);
echo \app\widgets\ShareLinksModal::widget();
$uploadUrl = Url::to(['image/upload-file'], true);
?>
<div id="main-page"">

    <?= \app\widgets\UploaderPopup::widget(); ?>

    <div class="row">
        <div id="latest-images" class="col-lg-6 col-md-6">
            <h2 class="text-center">Best images</h2>
            <?= $this->render('//album/_image_list', ['dataProvider' => $imagesDataProvider, 'isLargeLayout' => true]) ?>
        </div>

        <div class="clearfix hidden-lg hidden-md"></div>

        <div id="latest-posts" class="col-lg-6 col-md-6">
            <h2 class="text-center">Best posts</h2>
            <?= $this->render("//post/_posts", ['dataProvider' => $postsDataProvider]); ?>
        </div>
    </div>


</div>


