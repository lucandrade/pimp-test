<?php

namespace app\components;


use app\components\authclients\autopost\Facebook;
use app\helpers\FileHelper;
use yii\base\Exception;
use yii\base\Object;
use yii\helpers\ArrayHelper;

class VideoUploader extends Object
{
    public $chunkSizeBytes = 1 * 1024 * 1024;
    private $_errors = [];


    public function getErrors()
    {
        return $this->_errors;
    }

    public function hasErrors()
    {
        return !empty($this->_errors);
    }


    protected function addError($message)
    {
        $this->_errors[] = $message;
    }


    protected function checkFileExists($fileName)
    {
        if (!file_exists($fileName)) {
            $this->addError("$fileName No such file or directory");
            return false;
        }
        return true;
    }


    public function uploadToFacebook()
    {
        /**
         * @var Facebook $client
         */
        $client = \Yii::$app->authAdminCollection->getClient("APFacebook");
        $client->fetchPageAccessToken();


//        if(($status = $client->loadTokenFromFile()) != 'Ok'){
//            throw new \yii\console\Exception("Some errors with token file:" . $status);
//        }


//        $client->api('')
    }


    public function uploadToYoutube($fileName, $title, $token, array $fileData = [])
    {
        if (!$this->checkFileExists($fileName)) {
            return false;
        }
        for ($attempt = 1; $attempt <= 5; $attempt++) {
            try {
                $description = 'Get more crazy videos on ' . \Yii::$app->urlManager->hostInfo . PHP_EOL;
                $description .= ' Join our official Facebook group: https://www.facebook.com/ownedcom' . PHP_EOL;
                $fileData = ArrayHelper::merge($fileData, [
                    'title' => $title,
                    'description' => $description,
                    'categoryId' => '23',//'Comedy' category id
                    'privacyStatus' => 'public'
                ]);

                $client = new \Google_Client();

                $client->setAccessToken($token);
                $client->setAccessType('offline');

                $youtube = new \Google_Service_YouTube($client);

                $snippet = new \Google_Service_YouTube_VideoSnippet();

                $snippet->setTitle($fileData['title']);
                $snippet->setDescription($fileData['description']);
                $snippet->setCategoryId($fileData['categoryId']);

                $status = new \Google_Service_YouTube_VideoStatus();
                $status->privacyStatus = $fileData['privacyStatus'];

                $video = new \Google_Service_YouTube_Video();
                $video->setSnippet($snippet);
                $video->setStatus($status);

                $client->setDefer(true);

                $insertRequest = $youtube->videos->insert("status, snippet", $video);

                $media = new \Google_Http_MediaFileUpload(
                    $client,
                    $insertRequest,
                    "video/webm",
                    null,
                    true,
                    $this->chunkSizeBytes
                );

                $media->setFileSize(filesize($fileName));
                // Read the media file and upload it chunk by chunk.
                $status = false;

                $handle = fopen($fileName, "rb");
                while (!$status && !feof($handle)) {
                    $chunk = fread($handle, $this->chunkSizeBytes);
                    $status = $media->nextChunk($chunk);
                }
                fclose($handle);

                return $status;
            } catch (\Google_Service_Exception $e) {
                if ($e->getCode() === 503 && $attempt === 5) {
                    $this->addError($e->getMessage());
                } else if ($e->getCode() === 503) {
                    sleep(10);
                } else {
                    $this->addError($e->getMessage());
                    break;
                }
            } catch (\Google_Exception $e) {
                if ($e->getCode() === 503 && $attempt === 5) {
                    $this->addError($e->getMessage());
                } else if ($e->getCode() === 503) {
                    sleep(10);
                } else {
                    $this->addError($e->getMessage());
                    break;
                }
            }
        }
        return false;
    }
}