<?php

namespace app\controllers\admin;

use Yii;
use yii\base\Action;
use yii\base\ActionEvent;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\helpers\Html;

/*
 * todo: add closure param support
 */

class CreateAction extends Action {

	const BEFORE_CREATE_SAVE = 'beforeCreateSave';
	const AFTER_CREATE_SAVE = 'afterCreateSave';
	const BEFORE_CREATE_RENDER = 'beforeCreateRender';

	public $viewName = 'edit';
	public $modelClass;
	public $scenario = 'default';
	public $redirectSuccessSave = ['index'];

	public $messageSuccessSave = 'Row create';

	public function init() {
		if ($this->modelClass === null) {
			throw new InvalidConfigException('Model class must be setted');
		}

		parent::init();
	}

	public function run() {
		/** @var ActiveRecord $model */
		$model = new $this->modelClass;
		$model->setScenario($this->scenario);

		if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
			if ($this->beforeSave($model) && $model->save()) {
				Yii::$app->session->setFlash('success', $this->messageSuccessSave);

				$this->afterSave($model);

				return $this->controller->redirect($this->redirectSuccessSave);
			}
		}

		if ($model->hasErrors()) {
			Yii::$app->session->setFlash('error', Html::errorSummary($model));
		}

		return $this->controller->render($this->viewName, $this->beforeRender(compact('model')));
	}

	public function beforeSave($result) {
		$event = new ActionEvent($this);
		$event->result = $result;
		$this->controller->trigger(self::BEFORE_CREATE_SAVE, $event);

		return $event->isValid;
	}

	public function afterSave($result) {
		$event = new ActionEvent($this);
		$event->result = $result;
		$this->controller->trigger(self::AFTER_CREATE_SAVE, $event);

		return $event->result;
	}

	public function beforeRender($result) {
		$event = new ActionEvent($this);
		$event->result = $result;
		$this->controller->trigger(self::BEFORE_CREATE_RENDER, $event);

		return $event->result;
	}

}