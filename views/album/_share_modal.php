<div class="modal fade" id="share-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h5>Get Uri for <span id="this">this</span> image<span id="s-ending"><span></h5>
                <textarea class="textarea" id="codes" contenteditable="true"></textarea>
                <div class="btn-group">
                    <button id="btn-html" class="btn active">
                        GET HTML CODE
                    </button>
                    <button id="btn-bb" class="btn pull-right">
                        GET BB CODE
                    </button>
                </div>
                <h5>Copy the code below and place it on your website</h5>
                <textarea class="textarea" id="site-codes" contenteditable="true"></textarea>
            </div>
        </div>
    </div>
</div>