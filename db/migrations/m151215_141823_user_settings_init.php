<?php

use yii\db\Migration;

class m151215_141823_user_settings_init extends Migration
{
	public $table = '{{%user_settings}}';

    public function up()
    {
		$this->createTable($this->table, [
			'id' => $this->primaryKey(),
			'user_id' => $this->integer(),
			'key' => $this->string(),
			'value' => $this->string(),
			'created_at' => $this->dateTime(),
			'updated_at' => $this->dateTime(),
		]
		);
    }

    public function down()
    {
        $this->dropTable($this->table);
    }
}
