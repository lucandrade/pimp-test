<?php

namespace app\assets;


use app\components\View;
use app\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\AssetBundle;

class UploaderPluginVbullet extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public static $mainJsFilename = 'assets/tmp/uploader-vbullet-plugin.js';
    public static $jsTargetFilename = 'js/pah-vbullet-plugin.js';

    public $css = [
        'css/uploader-plugin/vbullet.css'
    ];

    public $js = [
    ];

    public $depends = [
        'app\assets\UploaderAsset'
    ];

    public $jsOptions = [
    ];

    public $cssOptions = [
    ];

    public function init()
    {
        $this->initMainJsFile();
        parent::init();
    }

    protected function initMainJsFile()
    {
        $fullFilename = \Yii::getAlias($this->basePath . "/" . self::$mainJsFilename);
        $this->generateMainJsFile($fullFilename);

        $this->js[] = self::$mainJsFilename;
    }

    protected function generateMainJsFile($fullFilename)
    {
        $stylesLink = Url::base(true) . \Yii::getAlias($this->baseUrl . "/assets/css/pah-vbullet-plugin.css");
        $url = Url::to(['image/upload-file'], true);
        $wrongImage = Url::base(true) . \Yii::getAlias($this->baseUrl . "/static/i/wrong.png");
        $view = new View();
        $js = $view->render("//plugin/uploader", compact('stylesLink', 'url', 'wrongImage'));
        $dir = pathinfo($fullFilename, PATHINFO_DIRNAME);
        if(!file_exists($dir)){
            FileHelper::createDirectory($dir);
        }
        return file_put_contents($fullFilename, $js);
    }


}