<?php

use yii\db\Schema;
use yii\db\Migration;

class m151229_180300_add_queueu_table extends Migration
{
    public $table = 'JobQueue';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable($this->table, [
            'id' => Schema::TYPE_PK,
            'command' => Schema::TYPE_STRING,
            'added_to_queue_at' => Schema::TYPE_TIMESTAMP
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable($this->table);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
