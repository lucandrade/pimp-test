<?php

use yii\db\Migration;

/**
 * Handles adding views_count to table `image_table`.
 */
class m160705_151152_add_views_count_to_image_table extends Migration
{
    /**
     * @inheritdoc
     */

    public $table = 'image';
    public $col1 = 'viewsCount';
    public $col2 = 'rating';


    public function up()
    {
        $this->addColumn($this->table, $this->col1, $this->integer()->defaultValue(0));
        $this->addColumn($this->table, $this->col2, $this->integer()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn($this->table, $this->col1);
        $this->dropColumn($this->table, $this->col2);
    }
}
