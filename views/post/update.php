<?php
/**
 * @var $this yii\web\View
 * @var $model app\models\Post
 */


use app\assets\DatetimepickerAsset;
use yii\helpers\Html;
use yii\helpers\Json;
use app\models\PostBlock;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\assets\PostAsset;
use app\assets\YTExternalAsset;

PostAsset::register($this);
YTExternalAsset::register($this);

$this->title = $model->is_draft == 1 ? 'Add post' : 'Update post';

$maxIds = empty($blockIds) ? 0 : max($blockIds);
$this->registerJs('var startBlockIndex = "' . $maxIds . '";', $this::POS_HEAD);
$this->registerJs('var postBlockTypes = ' . Json::encode(array_flip(PostBlock::getTypes())) . ';', $this::POS_HEAD);

$this->registerJs('var validatePostVideoUrl = "' . Url::to(['/post/validate-video']) . '";', $this::POS_HEAD);
$this->registerJs('var uploadVideoUrl = "' . Url::to(['/post/upload-video']) . '";', $this::POS_HEAD);
$this->registerJs('var stopUploadVideoUrl = "' . Url::to(['/post/stop-upload-video']) . '";', $this::POS_HEAD);
$this->registerJs('var uploadImageUrl = "' . Url::to(['/post/upload-image']) . '";', $this::POS_HEAD);
$this->registerJs('var stopUploadImageUrl = "' . Url::to(['/post/stop-upload-image']) . '";', $this::POS_HEAD);
$this->registerJs('var tagsSearchUrl = "' . Url::to(['/tag/search']) . '";', $this::POS_HEAD);
$this->registerJs('var savePostUrl = "' . Url::to(['/post/save', 'id' => $model->id]) . '";', $this::POS_HEAD);
if ($model->is_draft == 1) {
    $this->registerJs('var storeDraftUrl = "' . Url::to(['/post/save', 'id' => $model->id, 'draft' => true]) . '";', $this::POS_HEAD);
}

if (Yii::$app->user->can('admin')) {
    DatetimepickerAsset::register($this);
}
?>

<?php
$form = ActiveForm::begin([
    'id' => 'add-post-form',
    'options' => [
        'class' => 'form-vertical',
        'enctype' => 'multipart/form-data'
    ]
]);
?>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <?= $form->errorSummary($model, ['class' => 'alert alert-danger']); ?>
        <?= $form->field($model, 'title')->textInput(['placeholder' => 'Title']); ?>
    </div>
</div>

<div class="block__list">
    <ul id="editable" class="list-group">
        <?php foreach ($model->postBlocks as $postBlock) { ?>
            <li class="list-group-item">
                <?php
                switch ($postBlock->type) {

                    case $postBlock::BLOCK_TYPE_IMAGE:
                        if ($postBlock->isGIF()) {
                            echo $this->render('_update_videoblock', ['model' => $postBlock]);
                        } else {
                            echo $this->render('_update_imageblock', ['model' => $postBlock]);
                        }
                        break;

                    case $postBlock::BLOCK_TYPE_VIDEO:
                        echo $this->render('_update_videoblock', ['model' => $postBlock]);
                        break;

                    default:
                        echo $this->render('_update_textblock', ['model' => $postBlock]);
                        break;
                }
                ?>
            </li>
        <?php } ?>
        <li class="post-manage list-group-item" hidden="hidden">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="post-block-manage">
                        <button type="button" class="js-remove btn btn-sm btn-danger pull-right">
                            <i class="fa fa-trash"></i>
                        </button>
                        <span class="drag-handle text-muted h4">&#9776;</span>
                        &nbsp;
                        <button type="button" class="btn btn-sm btn-default add-post-item">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                    <div class="form-group" hidden="hidden"></div>
                    <div class="adding-panel text-center">
                        <div class="btn-group">
                            <button type="button" data-type="text"
                                    class="btn btn-primary" title="Add text">
                                <i class="fa fa-indent"></i>
                            </button>
                            <button type="button" data-type="image"
                                    class="btn btn-primary" title="Add images">
                                <i class="fa fa-image"></i>
                            </button>
                            <button type="button" data-type="video"
                                    class="btn btn-primary" title="Add videos">
                                <i class="fa fa-youtube-play"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li class="post-manage-bottom list-group-item">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="adding-panel-bottom text-center">
                        <div class="btn-group">
                            <button type="button" data-type="text"
                                    class="btn btn-primary" title="Add text">
                                <i class="fa fa-indent"></i>
                            </button>
                            <button type="button" data-type="image"
                                    class="btn btn-primary" title="Add images">
                                <i class="fa fa-image"></i>
                            </button>
                            <button type="button" data-type="video"
                                    class="btn btn-primary" title="Add videos">
                                <i class="fa fa-youtube-play"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <?= $form->field($model, 'tagIds')->listBox(ArrayHelper::map($model->tags, 'id', 'name'), ['placeholder' => 'Tags', 'multiple' => 'multiple', 'autocomplete' => 'off'])->label(false) ?>
        <div class="form-group">
            <?= $form->field($model, 'is_authors')->hint('rights', ['class' => 'text-muted'])->checkbox(['label' => 'Is author']); ?>
        </div>
        <div class="form-group">
            <?= $form->field($model, 'is_adult')->hint('18+', ['class' => 'text-muted'])->checkbox(['label' => 'Is adult']); ?>
        </div>
        <?php if (Yii::$app->user->can('admin')) { ?>
            <?php $model->live_at = empty($model->live_at) ? $liveAt : $model->live_at; ?>
            <div class="form-group post-live-at">
                <a href="#collapseLiveAt"
                   role="button"
                   data-toggle="collapse"
                   aria-expanded="false"
                   aria-controls="collapseLiveAt">
                    Set live time
                </a>
                <div class="collapse" id="collapseLiveAt">
                    <div class="input-group">
                        <?=
                        Html::activeTextInput($model, 'live_at', [
                            'class' => 'form-control',
                            'placeholder' => 'Live at',
                            'name' => 'live_at',
                            'data-default-time' => strtotime($model->live_at)
                        ])
                        ?>
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-default btn-clear-date">
                                <i class="fa fa-remove"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </div>
            <?=
            Html::activeHiddenInput($model, 'timezone_offset', [
                'class' => 'form-control',
                'name' => 'timezone_offset'
            ])
            ?>
        <?php } ?>
        <hr>
        <div class="form-group">
            <p class="text-muted">
                <small class="draft-info"></small>
            </p>
            <div>
                <?php if ($model->is_draft == 1) { ?>
                    <div class="pull-right">
                        <a target="_blank" href="<?= $model->getPreviewUrl() ?>"
                           class="btn btn-default btn-post-preview">
                            <i class="fa fa-eye"></i>
                            Preview
                        </a>
                    </div>
                <?php } ?>
                <span class="hidden-xs">
                    <?= Html::button('Save post', ['id' => 'post-save', 'class' => 'btn btn-lg btn-primary', 'name' => $model->formName() . '[is_draft]', 'value' => 0]) ?>
                </span>
                <?php if ($model->is_draft == 1) { ?>
                    &nbsp;
                    <?= Html::button('Save draft', ['id' => 'post-draft', 'class' => 'btn btn-default', 'name' => $model->formName() . '[is_draft]', 'value' => 1]) ?>
                <?php } ?>
            </div>
            <div class="visible-xs-block">
                <br>
                <p class="text-center">
                    <?= Html::button('Save post', ['id' => 'post-save-xs', 'class' => 'btn btn-lg btn-primary', 'name' => $model->formName() . '[is_draft]', 'value' => 0]) ?>
                </p>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end() ?>

