<?php

namespace app\models;

use app\models\User;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\helpers\Json;
use yii\validators\EmailValidator;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model
{
    public $loginId;
    public $password;
    public $rememberMe = true;


    const EMAIL_ATTR_NAME = 'email';
    const USERNAME_ATTR_NAME = 'username';

    public $findField;

    /**
     * @var User
     */
    private $_user = false;


    public function attributeLabels()
    {
        return [
            'rememberMe' => 'Remember'
        ];
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['loginId', 'password'], 'required'],
            [['loginId'], 'defineFindField'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword',]
        ];
    }



    public function defineFindField()
    {
        $emailValidator = new EmailValidator();
        $this->findField = $emailValidator->validate($this->loginId)
            ? self::EMAIL_ATTR_NAME : self::USERNAME_ATTR_NAME;
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors())
        {
            $user = $this->getUser();

            if($this->hasErrors()){
                return false;
            }

            if(!$user)
            {
                $forumUser = \app\models\forum\User::findOne([$this->findField => $this->loginId]);

                if($forumUser && $forumUser->validatePassword($this->password)){
                    $this->_user = User::createFromForumData($forumUser);
                    if($this->_user->hasErrors()){
                        throw new Exception("Coudn't create posts user from forum data, errors:" . Json::encode($this->_user->getErrors()));
                    }
                }else{
                    $this->addError($attribute, "Incorrect username or password.");
                }

            }else
            {
                if(empty($user->password))
                {
                    $forumUser = \app\models\forum\User::findOne(['username' => $user->username]);
                    if(!$forumUser || !$forumUser->validatePassword($this->password)){
                        $this->addError($attribute, 'Incorrect username or password.');
                    }
                }
                else
                {
                    if(!$user->validatePassword($this->password)){
                        $this->addError($attribute, 'Incorrect username or password');
                    }
                }
            }
        }
    }


    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 5 * 24 * 60 * 60 : 0);
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findOne([$this->findField => $this->loginId]);
            if ($this->_user !== null && $this->_user->isBanned()) {
                $this->addError('email', 'Your account is banned!');
            }
        }

        return $this->_user;
    }
}
