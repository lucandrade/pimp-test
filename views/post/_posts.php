<?php
/**
 * @var $dataProvider \app\components\SphinxDataProvider
 */
use yii\helpers\Json;
use app\components\ListView;
use app\helpers\WidgetHelper;
use yii\web\View;
use app\widgets\ScrollPager;
use yii\bootstrap\Html;
use yii\helpers\Url;

$config = WidgetHelper::getConfig(2);
$jsonConfig = Json::encode($config);
$ck = WidgetHelper::getScriptInView('//js/widget', ['config' => $config]);
$this->registerJs($ck, View::POS_END);

if($dataProvider->pagination){
    $pageParamm =  $dataProvider->pagination->pageParam;
    $pageNumber = !empty($_GET[$pageParamm]) ? $_GET[$pageParamm] : 1;
}else{
    $pageNumber = 1;
}

$link = '';
if (!\Yii::$app->user->isGuest) {
    echo Html::hiddenInput('add-to-favorite-url', Url::to(['post/toggle-favorite']));
    $this->registerJs("$('body').on('click', '.add-to-favorite', addToFavoriteHandler)");
}

if($postId = \Yii::$app->request->get('p')){
    $this->registerJs("scrollToElement($('[data-page-key=\"$postId\"]'))", View::POS_READY);
    if($pageNumber>1){
        $postsNumber = ($pageNumber-1) * $dataProvider->pagination->pageSize;
        $link = Html::a("Show previous $postsNumber posts", parse_url(\Yii::$app->request->getUrl(),PHP_URL_PATH));
    }
}

$collapsePosts = \Yii::$app->user->isGuest || !(bool) \Yii::$app->user->identity->getSettings('expand_posts');
$jsAjaxListEvent = $collapsePosts ?
    "if(typeof $.fn.expandPostAjaxListEvent === \"function\"){
        $(this).expandPostAjaxListEvent();
     }" : "";

?>

<div class="posts" data-collapse-posts="<?= (int)$collapsePosts ?>">
    <?php
    echo $link;
    echo ListView::widget([
        'id'=>'main-list-view',
        'summary' => false,
        'dataProvider' => $dataProvider,
        'itemView' => '_post',

        'itemOptions' => [
            'class' => ''
        ],

        'viewParams' => [
            'seenIds' => isset($seenPostIds) ? $seenPostIds : [],
            'page' => $pageNumber
        ],

        'pager' => [
            'class' => ScrollPager::className(),
            'item' => '.item',
            'enabledExtensions' => [
                ScrollPager::EXTENSION_PAGING,
                ScrollPager::EXTENSION_SPINNER
            ],
            'eventOnRender' => $collapsePosts ? 'function(items){
                 $(items).each(function() {
                    if(typeof $.fn.expandPostAjaxList === "function"){
                        $(this).expandPostAjaxList();
                    }
                });
            }' : null,

            'eventOnRendered' => "function(items) {
                var \$items = $(items);
                initProccessSeenPosts(\$items);
                \$items.each(function() {
                     if (typeof $.fn.initGifPlayer === 'function') {
                         $('[id^=\"gifPlayer-\"]', this).initGifPlayer();
                     }
                     $jsAjaxListEvent
                });
            }"
        ]
    ]);
    ?>
</div>