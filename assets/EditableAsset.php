<?php

namespace app\assets;


use yii\web\AssetBundle;

class EditableAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/plugins/bootstrap-editable.css',
        'css/editable-widget.css'
    ];

    public $js = [
        'js/plugins/bootstrap-editable.js',
        'js/editable-widget.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}