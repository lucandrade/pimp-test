$(document).ready(function(){

    var $body = $('body');

    var wrapperTO, popoverTO;

    $body.on('click', '.editable-trigger', function(e){
        var $container = $(this).parents('.popover').prev();
        if($container.data('has-access')){
            e.stopPropagation();
            $container.popover('hide');
            $container.find('.editable-link').editable('show')
        }
    });

    $body.on('mouseenter','.editable-wrapper', function(){
        clearTimeout(popoverTO);
        if($(this).data('has-access')
            && $(this).find('.editable-container').length == 0
            && !$(this).next().hasClass('popover')){
            $(this).popover('show');
        }
    }).on('mouseleave','.editable-wrapper', function(){
        if($(this).data('has-access')){
            var $this = $(this);
            wrapperTO = setTimeout(function(){
                $this.popover('hide');
            },400);
        }
    });

    $body
        .on('mouseenter', '.editable-wrapper ~.popover', function(){
        clearTimeout(wrapperTO);
    })
        .on('mouseleave', '.editable-wrapper ~.popover', function(){
            var $wrapper = $(this).prev();
            popoverTO = setTimeout(function(){
                $wrapper.popover('hide');
            }, 300);
    });


    if(pahIniter){
        pahIniter.widgets.xeditable();
    }



});