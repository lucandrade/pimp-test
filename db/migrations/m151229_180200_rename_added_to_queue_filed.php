<?php

use yii\db\Schema;
use yii\db\Migration;

class m151229_180200_rename_added_to_queue_filed extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `JobLog` CHANGE COLUMN `added_to_queue_at` `removed_from_queue_at` TIMESTAMP;");
    }

    public function down()
    {
        $this->execute("ALTER TABLE `JobLog` CHANGE COLUMN `removed_from_queue_at` `added_to_queue_at` TIMESTAMP;");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
