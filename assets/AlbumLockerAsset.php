<?php

namespace app\assets;


use yii\web\AssetBundle;

class AlbumLockerAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
    ];

    public $js = [
        'js/jquery.albumLocker.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
    ];
    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}