<?php $this->layout = 'widget';?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title></title>
<style>
	body{background-color:white;padding:0;margin:0;font-size:10px;font-family:verdana;color:#535353}
	h1{margin:0;padding:0}
		h1 a{color:#2367a1;font-family:georgia;letter-spacing:-2px;text-decoration:none;}
		h1 a:hover{color:#418bca;}
	.col1{float:left;width:100px;height:20px;line-height:20px;}
	.col2{float:left;width:100px;height:20px;line-height:20px;}
	.col3{float:left;width:135px;height:20px;line-height:20px;}
	.col4{float:left;width:110px;height:20px;line-height:20px;}

	.colr1{float:left;width:110px;height:20px;line-height:20px;color:#5082b6;font-size:11px;font-weight:bold}
	.colr2{float:left;width:110px;height:20px;line-height:20px;color:#a11e1e;font-size:11px;font-weight:bold}
	.colr3{float:left;width:75px;height:20px;line-height:20px;color:#dc5b29;font-size:11px;font-weight:bold}
	.colr4{float:left;width:95px;height:20px;line-height:20px;}
		.colr4 img{margin-top:6px;}
</style>
</head>
<body>


<div style="border:1px solid #a3a3a3;width:218px;margin:auto;">
<div style="padding:3px;">
<h1><a href="http://<?php echo $domain->domain;?>" target="top"><?php echo $domain->domain;?></a></h1>
<div style="height:5px;"></div>
  <div class="col1">Alexa Rank:</div> <div class="colr1" id="alexa_rank">#<?php echo isset($info->alexa->rank)?$info->alexa->rank:'';?></div> 
  <div class="clear:both"></div>
  <div class="col2">Unique visits:</div> <div class="colr2" id="statbrain_visits"><?php echo isset($info->statbrain->visits)?$info->statbrain->visits:'';?></div>
  <div class="clear:both"></div>
  <div class="col3">Aprox. incom per day:</div> <div class="colr3" id="statbrain_income">$<?php echo isset($info->statbrain->income)?$info->statbrain->income:'';?></div> 
  <div class="clear:both"></div>
  <div class="col4">Google Pagerank:</div> <div class="colr4" id="google_pr"><img src="/i/pr/<?php echo isset($info->google->pr)?$info->google->pr:'0';?>.gif"/></div>
  <div class="clear:both"></div>

<?php $this->beginClip('extraJS'); ?>
<script>
<?php if(!$info):?>
  jQuery(document).ready(function() {
    info.load('<?php echo $domain->domain?>');
  });
<?php endif;?>
</script>
<?php $this->endClip(); ?>

</div>
	<a href="http://www.hqindex.com" target="top"><img src="/i/footer.gif" border="0"/></a>
</div>
</body>
