<?php
namespace app\models\forum;


/**
 * Class User
 *
 * @property string $email;
 * @property string $username;
 * @property string $password;
 *
 * @package app\models
 */

use yii\db\ActiveRecord;
use yii\web\Cookie;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface
{
    
    const COOKIE_NAME_ID = 'bbuserid';
    const COOKIE_NAME_PASSWORD = 'bbpassword';
    const COOKIE_NAME_SESSIONHASH = 'bbsessionhash';


    const USER_GROUP_ID_REGISTERED = 2;


    public static function tableName()
    {
        return 'user';
    }

    public static function getDb()
    {
        return \Yii::$app->forumDb;
    }


    public function rules()
    {
        return [
            [['username'], 'required'],
            [['username', 'password'], 'string', 'min'=>"2"],
            [['joindate', 'usergroupid', 'lastvisit'], 'number', 'integerOnly' => true],
            ['usergroupid', 'default', 'value' => self::USER_GROUP_ID_REGISTERED]
        ];
    }

    public static function findIdentity($id)
    {
        return self::findOne(['userid'=>$id]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
    }


    public function getId(){
        return $this->userid;
    }

    public function validateAuthKey($authKey)
    {
        return true;
    }

    public function getAuthKey()
    {
    }

    public function validatePassword($password)
    {
        return $this->password == self::hashPassword($password, $this->salt);
    }

    public static function findByEmail($email)
    {
        return self::findOne(['email' => $email]);
    }

    public function updateLastVisit()
    {
        $this->lastvisit = time();
        return $this->save(true, ['lastvisit']);
    }


    public function setPassword($password)
    {
        $this->password = self::hashPassword($password, $this->salt);
        $this->passworddate = date('Y-m-d');
    }


    public function generateSalt()
    {
        $this->salt = \Yii::$app->security->generateRandomString();
    }

    public function getIsEmailActivated()
    {
        return true;
    }

    public static function hashPassword($password, $salt)
    {
        return md5(md5($password) . $salt);
    }


    public static function login(\app\models\User $identity, $duration)
    {
        $forumUser = $identity->getForumUser();

        self::removeCookie(self::COOKIE_NAME_SESSIONHASH);

        $forumUser->setCookies($duration);
        return $forumUser;
    }


    public function setCookies($duration)
    {
        $expire = $duration > 0 ? time() + $duration : 0;

        $idCookie = new Cookie(['name' => self::COOKIE_NAME_ID]);
        $passCookie = new Cookie(['name' => self::COOKIE_NAME_PASSWORD]);

        if(!empty(\Yii::$app->params['identityCookieDomain'])){
            $idCookie->domain = \Yii::$app->params['identityCookieDomain'];
            $passCookie->domain = \Yii::$app->params['identityCookieDomain'];
        }

        setcookie($idCookie->name, $this->getId(), $expire, $idCookie->path, $idCookie->domain, $idCookie->secure, $idCookie->httpOnly);
        setcookie($passCookie->name, md5($this->password . 'mGidBmHNmdZbVhivwpauPd1rBVi6EQ'), $expire, $passCookie->path, $passCookie->domain, $passCookie->secure, $passCookie->httpOnly);
    }



    public function logout(){
        $this->updateLastVisit();
        self::removeLoginCookies();
    }



    public static function removeLoginCookies(){
        $names = [self::COOKIE_NAME_ID, self::COOKIE_NAME_PASSWORD, self::COOKIE_NAME_SESSIONHASH];

        foreach($names as $name){
            self::removeCookie($name);
        }

    }


    protected static function removeCookie($name)
    {
        $cookie = new Cookie(['name' => $name]);
        if(!empty(\Yii::$app->params['identityCookieDomain'])) {
            $cookie->domain = \Yii::$app->params['identityCookieDomain'];
        }
        \Yii::$app->getResponse()->getCookies()->remove($cookie);
    }

}