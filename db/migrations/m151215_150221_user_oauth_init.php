<?php

use yii\db\Schema;
use yii\db\Migration;

class m151215_150221_user_oauth_init extends Migration
{
	public $table = '{{%user_oauth}}';

	public function up()
	{
		$this->createTable($this->table,[
			'user_id'=>$this->integer(),
			'provider'=>$this->string(45),
			'identifier'=>$this->string(64),
			'profile_cache'=>$this->text(),
			'session_data'=>$this->text(),
			'avatar_url'=>$this->string(255),
			'display_name'=>$this->string(255),
		]);

		$this->addPrimaryKey('',$this->table,['provider','identifier']);
		$this->createIndex('unic_user_id_name',$this->table,['user_id','provider'],true);
		$this->createIndex('oauth_user_id',$this->table,['user_id']);
	}

	public function down()
	{
		$this->dropTable($this->table);
	}
}
