<?php

namespace app\widgets;


use app\assets\UploaderPopupAsset;
use app\helpers\ArrayHelper;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\Json;

class UploaderPopup extends Widget
{

    public $albumId;
    public $modalId = 'uploader-modal';
    public $modalTitle = 'Upload images';
    public $fullUploaderOptions = [];


    /**
     * @var FullUploader;
     */
    private $_uploader;

    public $buttonOptions = [
        'id' => 'uploader-trigger',
        'class' => 'btn btn-primary',
        'label' => 'Upload image'
    ];

    public $clicker = '.uploader-trigger';


    protected function defaultFullUploaderOptions(){
        return [
            'uploaderOptions'=> [
                'js' => [
                    'fileuploadOptions' => [
                        'dropZone' => "#$this->modalId"
                    ],
                ]
            ],
        ];
    }

    public function init()
    {
        $params  = ArrayHelper::merge($this->defaultFullUploaderOptions(), $this->fullUploaderOptions);
        $params['class'] = FullUploader::className();
        $this->_uploader = \Yii::createObject($params);
        $this->_uploader->containerId = $this->modalId;
    }

    public function getUploader(){
        return $this->_uploader;
    }

    public function run()
    {
        $pluginOptions = ['modalId' => $this->modalId];
        $pluginOptions = Json::encode($pluginOptions);

        $clicker = $this->clicker ? $this->clicker : '#' . $this->buttonOptions['id'];
        UploaderPopupAsset::register($this->getView());

        $this->getView()->registerJs("$('$clicker').click(function(){
            var uploader = $(this).data('uploader')
            if(!uploader){
                    uploader = new popupUploader({$pluginOptions})
                    $(this).data('uploader', uploader);
                }
                uploader.show();
                return false;
            });
         $( window ).on('beforeunload', function() {
             if($('.progress').is(':visible')){
                return 1;
             }
          })");

        return $this->render('UploaderPopup');
    }



    protected function getSourceFile($type)
    {
        $configFile = \Yii::getAlias("@app/config/generated-assets/$this->assetConfig.php");
        if(!file_exists($configFile)){
            throw new InvalidConfigException("Config file /$this->assetConfig.php not found");

        }
        $config = require $configFile;
        if(empty($config['all'])
            || empty($config['all']['baseUrl'])
            || empty($config['all'][$type])){
            throw new InvalidConfigException("Wrong format of config file /$this->assetConfig.php not found");
        }

        $config = $config['all'];

        $file = \Yii::getAlias($config['baseUrl'] . "/" . $config[$type][0]);
        return $file;
    }
}