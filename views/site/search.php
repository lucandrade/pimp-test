<?php
/**
 * @var \app\models\MainSearchForm $searchForm
 * @var \app\components\SphinxDataProvider $dataProvider
 */

\app\assets\SearchPageAsset::register($this);
$totalFound  = $dataProvider->getTotalCount();
$title = $totalFound > 0
    ? 'Search results for "' . preg_replace("/^(my:)/", "", $searchForm->q) . '"'
    : 'Nothing was found for "' . $searchForm->q . '"';
$this->title = $title;

?>
<div id="search-page">

    <h3><?= $title ?></h3>
    <?php if($totalFound > 0): ?>

        <?= \app\widgets\SelectionBar::widget([
            'show' => $dataProvider->totalCount > 0,
            'showAuthorIcons' => !\Yii::$app->user->isGuest &&  \Yii::$app->user->identity->is_admin
        ]) ?>
        <?= \app\widgets\ShareLinksModal::widget(); ?>
        <?= $this->render("//album/_image_list", compact('dataProvider')); ?>
    <?php endif; ?>
</div>
