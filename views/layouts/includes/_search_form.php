<?php
use yii\bootstrap\ActiveForm;
use app\models\MainSearchForm;
use yii\helpers\Url;
?>

<?php
    $model = new MainSearchForm();
    $form = ActiveForm::begin([
        'id' =>'search-form',
        'method'=>'get',
        'successCssClass' => false,
        'action' => Url::to(['site/search']),
        'options' => ['class'=>'search-form']
]);?>

<div class="input-group">
    <?= $form->field($model, 'q', ['template'=>'{input}'])
        ->textInput(['placeholder'=>'Search...', 'class'=>'form-control', 'name'=>'q'])?>
    <span class="input-group-btn">
        <button class="btn btn-default" type="button" onclick="$('#search-form').submit()">
            <span class="glyphicon glyphicon-search"></span>
        </button>
    </span>
</div>
<?php ActiveForm::end(); ?>
