<?php

namespace app\controllers;

use app\components\Controller;
use yii\data\ActiveDataProvider;
use app\models\Image;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\components\filters\JsonResponse;

class TrashBinController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'empty' => ['post'],
                ],
            ],
            'jsonResponse' => [
                'class' => JsonResponse::className(),
                'only' => ['empty']
            ]
        ];
    }

    public function actionEmpty()
    {
        $ids = Image::emptyTrash(\Yii::$app->user->id);
        return $ids;
    }



}