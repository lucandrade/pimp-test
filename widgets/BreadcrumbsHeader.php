<?php

namespace app\widgets;


use app\helpers\Html;
use yii\base\Widget;
use yii\widgets\Breadcrumbs;

class BreadcrumbsHeader extends Breadcrumbs
{
    public $author;
    public $authorAvatarSize = 35;
    public $tag = "span";
    public $itemTemplate = "<span>{link}</span>";
    public $activeItemTemplate = "<span>{link}</span>";
    public $delimeter = '<span class="path-delimiter">/</span>';
    public $options = [
       'class' => 'header-breadcrumbs'
    ];

    public $afterLinksContent = '';

    public $encodeLabels = false;


    public function run()
    {
        $links = [];

        foreach ($this->links as $link) {
            if (!is_array($link)) {
                $link = ['label' => $link];
            }

            $links[] = $this->renderItem($link, isset($link['url']) ? $this->itemTemplate : $this->activeItemTemplate);
        }

        $linksBlock = Html::tag('div', implode($this->delimeter, $links) . $this->afterLinksContent, ['class'=>'link-block']);

        return Html::tag($this->tag, $this->renderAuthor() . $linksBlock,
            $this->options);
    }


    protected function renderAuthor()
    {
        if(!is_null($this->author) && !$this->author->getIsAnon()){
            $content = Html::a(Html::img($this->author->avatar,[
                    'alt' => $this->author->avatar,
                    'width' => $this->authorAvatarSize,
                    'height' => $this->authorAvatarSize
                ]) . Html::encode($this->author->username),
                ['album/user', 'id' => $this->author->id],
                [
                    'class'=>'post-author'
                ]
            );
        }else{
            $content = "Anonymous";
        }

        return Html::tag('div',$content. $this->delimeter, [
            'class' => 'author-block'
        ]);
    }








}