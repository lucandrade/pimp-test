<?php

namespace app\components\authclients\autopost;


use app\extensions\tumblroauth\TumblrOAuth;
use app\helpers\TokenHelper;
use app\models\Post;
use Tumblr\API\Client;
use yii\authclient\OAuth1;
use yii\authclient\OAuthToken;
use yii\base\Exception;
use yii\bootstrap\Html;
use app\components\behaviors\ConsoleAuthClientBehavior;

class Trumblr extends OAuth1
{
    /**
     * @inheritdoc
     */
    public $authUrl = 'https://www.tumblr.com/oauth/authorize';
    /**
     * @inheritdoc
     */
    public $requestTokenUrl = 'https://www.tumblr.com/oauth/request_token';
    /**
     * @inheritdoc
     */
    public $requestTokenMethod = 'POST';
    /**
     * @inheritdoc
     */
    public $accessTokenUrl = 'https://www.tumblr.com/oauth/access_token';
    /**
     * @inheritdoc
     */
    public $accessTokenMethod = 'POST';

    public $blogName = 'alexeyrudakov';

    public  $tokenFileDir = "@runtime/tumblr";

    private $_autoPostResult;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = ConsoleAuthClientBehavior::className();
        return $behaviors;
    }

    public function fetchAccessToken(OAuthToken $requestToken = null, $oauthVerifier = null, array $params = [])
    {
        if (!is_object($requestToken)) {
            $requestToken = $this->getState('requestToken');
            if (!is_object($requestToken)) {
                throw new Exception('Request token is required to fetch access token!');
            }
        }
        $thumlrOAuth = new TumblrOAuth($this->consumerKey, $this->consumerSecret,
            $requestToken->getToken(), $requestToken->getTokenSecret());

        if ($oauthVerifier === null) {
            if (isset($_REQUEST['oauth_verifier'])) {
                $oauthVerifier = $_REQUEST['oauth_verifier'];
            }
        }

        $tokenParams = $thumlrOAuth->getAccessToken($oauthVerifier);
        $token = $this->createToken([
            'params' => $tokenParams
        ]);
        $this->setAccessToken($token);

        return $token;
    }


    public function autoPost(Post $post)
    {
        $tokenData = $this->getTokenFromFile();

        $tumblrClient = new Client($this->consumerKey, $this->consumerSecret,
            $tokenData['oauth_token'], $tokenData['oauth_token_secret']);

        $postData['body'] = $post->getSocialPostingData()['title'];
        $postData['tags'] = $post->getSocialPostingData()['tags'];

        $postData['type'] = 'text';
        $res = $tumblrClient->createPost($this->blogName, $postData);
        $this->_autoPostResult = json_decode(json_encode($res),true);

        return !empty($this->_autoPostResult['id']);
    }

    /**
     * @return mixed
     */
    public function getAutoPostResult()
    {
        return $this->_autoPostResult;
    }


    protected function apiInternal($accessToken, $url, $method, array $params, array $headers)
    {
        $params['oauth_consumer_key'] = $this->consumerKey;
        $params['oauth_token'] = $accessToken->getToken();
        $response = $this->sendSignedRequest($method, $url, $params, $headers);

        if(strtoupper($method) == 'GET' ){
            $url = $this->composeUrl($url, $params);
        }
        $this->populateCurlRequestData($url, $method, $params, $headers);

        return $response;
    }


}