<?php
namespace app\helpers;

use Yii;
use Google_Client;
use yii\helpers\Json;
use app\components\authclients\Comments;

class TokenHelper
{
    public static $tokenFilePathAlias = '@runtime/token';

    public static function getTokenFile()
    {
        $dir = Yii::getAlias(self::$tokenFilePathAlias);
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);;
        }

        return $dir . DIRECTORY_SEPARATOR . "token.txt";
    }

    public static function getRefreshTokenFile()
    {
        $dir = Yii::getAlias(self::$tokenFilePathAlias);
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        return $dir . DIRECTORY_SEPARATOR . "refresh_token.txt";
    }

    public static function saveTokenToFile(array $tokenParams, $saveRefreshToken = false)
    {
        if ($saveRefreshToken) {
            if (!empty($tokenParams['refresh_token'])) {
                file_put_contents(self::getRefreshTokenFile(), $tokenParams['refresh_token']);
            }
        }

        return file_put_contents(self::getTokenFile(), Json::encode($tokenParams));
    }


    public static function getTokenFromFile()
    {
        if (file_exists(self::getTokenFile())
            && $tokenStr = file_get_contents(self::getTokenFile())
        ) {
            return Json::decode($tokenStr);
        }
        return false;
    }

    public static function updateYoutubeToken()
    {
        if (!file_exists(self::getRefreshTokenFile())) {
            $exeptionInfo = __CLASS__ . '::' . __FUNCTION__ . '(line ' . __LINE__ . ')';
            throw new \yii\console\Exception('Token file not exist ' . $exeptionInfo);
        }

        $refreshToken = file_get_contents(self::getRefreshTokenFile());
        $client = new Google_Client();
        $client->setClientId(Yii::$app->authAdminCollection->getClient('ytadmin')->clientId);
        $client->setClientSecret(Yii::$app->authAdminCollection->getClient('ytadmin')->clientSecret);

        $token = $client->refreshToken($refreshToken);

        if (empty($token)) {
            $exeptionInfo = __CLASS__ . '::' . __FUNCTION__ . '(line ' . __LINE__ . ')';
            throw new \yii\console\Exception('Token empty ' . $exeptionInfo);
        }
        if(isset($token['error'])){
            throw new \yii\console\Exception("Youtube token was not refreshed, error:" . $token['error']
                . ", error description: " . $token['error_description']);
        }

        return self::saveTokenToFile($token);
    }

    public static function updateCommentsToken()
    {
        if (!file_exists(self::getRefreshTokenFile())) {
            $exeptionInfo = __CLASS__ . '::' . __FUNCTION__ . '(line ' . __LINE__ . ')';
            throw new \yii\console\Exception('Token file not exist ' . $exeptionInfo);
        }

        $refreshToken = file_get_contents(self::getRefreshTokenFile());
        $client = new Comments();
        $tokenParams = $client->refreshToken($refreshToken);

        if (empty($tokenParams)) {
            $exeptionInfo = __CLASS__ . '::' . __FUNCTION__ . '(line ' . __LINE__ . ')';
            throw new \yii\console\Exception('Token params empty ' . $exeptionInfo);
        }

        return self::saveTokenToFile($tokenParams, true);
    }


}