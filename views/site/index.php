<?php \app\assets\MainPageAsset::register($this); ?>
<div id="main-page">
    <?= \app\widgets\FullUploader::widget([
        'uploaderOptions' => [
            'disabledBeforeLoad' => true,
            'buttonOptions' => [
                'id' => 'main-button'
            ],
            'fileInputInsideContainer' => false,
            'js' => [
                'clicker' => '#main-button',
                'fileInput' => "#main-page-file-input",
                'fileInputInsideContainer' => false,
            ]
        ]
    ]); ?>
</div>
