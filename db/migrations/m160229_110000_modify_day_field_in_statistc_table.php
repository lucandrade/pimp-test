<?php

use yii\db\Migration;

class m160229_110000_modify_day_field_in_statistc_table extends Migration
{
    public $table = '{{statistic}}';

    public function up()
    {
        $this->renameColumn($this->table, 'day', 'date');
        $this->alterColumn($this->table, 'date', $this->dateTime()->notNull());
    }

    public function down()
    {
        $this->renameColumn($this->table, 'date', 'day');
        $this->alterColumn($this->table, 'day', $this->date());
    }

}
