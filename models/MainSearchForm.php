<?php

namespace app\models;

use Yii;
use yii\base\Model;

class MainSearchForm extends Model
{
    public $q;
    public $cat = 0;

    private $_sphinxQuery = '';
    private $_sphinxClient;
    public $queryAttr = 'title';

    const POSTS_SEARCH_CATEGORY_ALIAS = 'p';





    public static $searchCategories = [
        'everywhere' => 'Everywhere',
        'i' => 'Images/Albums',
        self::POSTS_SEARCH_CATEGORY_ALIAS => 'Posts',
        'my' => 'My images'
    ];



    public function rules()
    {
        return [
            ['q', 'string'],
            ['cat', 'filter', 'filter' => function($value){
                if(!in_array($value, array_keys(self::$searchCategories)) || empty($value)){
                    return 'everywhere';
                }

                return $value;
            }]
        ];
    }


    public function init()
    {
        $this->_sphinxClient = Yii::$app->sphinxClient->getClient();
        $this->load($_GET, '');
    }


    public function prepareQuery()
    {
        $this->_sphinxQuery = false;

        if ($this->validate()) {
            if (!empty($this->q))
            {
                if($this->cat === self::POSTS_SEARCH_CATEGORY_ALIAS){
                    $this->processPostsRequest();
                }else{
                    $this->processImagesRequest();
                }
            }
        }
    }


    protected function processPostsRequest(){

        if (\Yii::$app->user->identity && !Yii::$app->user->identity->isShowAdult()) {
            $this->_sphinxClient->SetFilter('is_adult_attr', [0]);
        }

        $this->_sphinxQuery = '' . $this->q;
        if (preg_match('/^user_(\d+)/', $this->q, $m)) {
            $this->_sphinxQuery = '"' . $this->q . '"';
        } else if (preg_match('/^tag_([\w\d\s]+)/', $this->q, $m)) {
            $this->_sphinxQuery = '"' . $this->q . '"';
        } else {
            $this->_sphinxQuery = '' . $this->q;
        }
    }

    protected function processImagesRequest()
    {
        if((substr($this->q, 0, 3) === 'my:' || $this->cat == 'my') && !\Yii::$app->user->isGuest)
        {
            if(substr($this->q, 0, 3) === 'my:'){
                $query = substr($this->q, 3);
            }else {
                $query = $this->q;
            }
            $this->_sphinxClient->setFilter('i_authorid_attr', [\Yii::$app->user->id]);
        }else{
            $query = $this->q;
        }


        if($this->isAbuseSearch($query)){
            return null;
        }

        $query_parts = preg_split('~[^\w\d]~', $query);
        $new_query = array();
        foreach($query_parts as $k=>$v) {
            if(strlen($v) >= 3){
                $new_query[] = $v;
            }
        }

        $query = implode(' ', $new_query);
        if(empty($query)){
            return null;
        }
        $this->_sphinxQuery = "@(i_title_attr) $query | @(a_title_attr) $query";
    }

    protected function isAbuseSearch($query){
        if(preg_match('~lsm|jl|xlover79|sib|sandokan~si', $query)){
           return false;
        }

        $abuseSearches = AbuseSearch::find()->select('value')->column();

        if(empty($abuseSearches)){
            return false;
        }

        array_map(function($val){
            return preg_quote($val, '~');
        }, $abuseSearches);

        return preg_match("~" .implode($abuseSearches) ."~", $query);
    }

    /**
     * @return mixed
     */
    public function getSphinxClient()
    {
        return $this->_sphinxClient;
    }

    /**
     * @return string
     */
    public function getSphinxQuery()
    {
        return $this->_sphinxQuery;
    }


    public function getIsImagesRequest()
    {
        return $this->cat !== self::POSTS_SEARCH_CATEGORY_ALIAS;
    }


}