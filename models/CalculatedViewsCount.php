<?php

namespace app\models;

use app\helpers\CommentsService;
use Yii;

/**
 * This is the model class for table "calculated_views_count".
 *
 * @property integer $model_id
 * @property integer $model_type
 * @property integer $raw
 * @property integer $unique
 * @property string $day
 */
class CalculatedViewsCount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */



    public static function tableName()
    {
        return 'calculated_views_count';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model_id', 'model_type'], 'required'],
            [['model_type'], 'in', 'range' => [CommentsService::MODEL_TYPE_ALBUM, CommentsService::MODEL_TYPE_IMAGE]],
            [['model_id', 'raw', 'unique'], 'integer'],
            [['day'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'model_id' => 'Model ID',
            'model_type' => 'Model Type',
            'raw' => 'Raw',
            'unique' => 'Unique',
            'day' => 'Day',
        ];
    }



    public static function processData($data)
    {
        $processed = [];
        $unprocessed = [];

        foreach($data as $row)
        {
            $key = $row['pageKey'] . "_" . $row['day'];
            if(self::processRow($row)){
                $processed[] = $key;
            }else{
                $unprocessed[$key] = $row['error'];
            }

        }

        return [$processed, $unprocessed];
    }


    public static function processRow(&$row)
    {
        list($parentModel, $modelType) = CommentsService::getModelByKey($row['pageKey']);
        if(!$parentModel){
            $row['error'] = "Can't find model by key " . $row['pageKey'];
            return false;
        }

        $tr = self::getDb()->beginTransaction();

        if($parentModel->updateViewsStats($row['raw']))
        {
            $self = self::updateOrCreateByStats($parentModel->id, $modelType, $row['raw'], $row['unique'], $row['day']);
            if(!$self->hasErrors())
            {
                $tr->commit();
                return true;
            }else{
                $row['error'] = "Can't save data to " . self::tableName() . " because:"  . json_encode($self->getErrors());
            }
        }else{
            $row['error'] = "Can't update view stats on model #$parentModel->id of $modelType because:" . json_encode($parentModel->getErrors());
        }

        return false;
    }


    public static function updateOrCreateByStats($modelId, $modelType, $raw, $unique, $day)
    {
        $condition = [
            'model_id' => $modelId,
            'model_type' => $modelType,
            'day' => $day
        ];

        $model = self::findOne($condition);
        if(!$model){
            $model = new self;
            $model->setAttributes($condition);
        }
        $model->unique = $unique;
        $model->raw = $raw;

        $model->save();

        return $model;
    }
}
