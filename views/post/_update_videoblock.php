<?php
/**
 * @var $this yii\web\View
 * @var $model app\models\PostBlock
 */
use yii\bootstrap\Html;

?>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="post-block-manage">
            <button type="button" class="js-remove btn btn-sm btn-danger pull-right">
                <i class="fa fa-trash"></i>
            </button>
            <span class="drag-handle text-muted h4">&#9776;</span>
            &nbsp;
            <button type="button" class="btn btn-sm btn-default add-post-item">
                <i class="fa fa-plus"></i>
            </button>
        </div>
        <div class="form-group">
            <?= Html::activeHiddenInput($model, '[' . $model->id . ']id', ['class' => 'form-control']) ?>
            <?= Html::activeHiddenInput($model, '[' . $model->id . ']position', ['class' => 'form-control']) ?>

            <?php if ($model->isUploadedVideo()) { ?>
                <?=
                Html::activeTextInput($model, '[' . $model->id . ']text', [
                    'class' => 'form-control',
                    'placeholder' => 'Title for video'
                ])
                ?>
            <?php } ?>

            <?php if (!empty($model->videoImage)) { ?>

                <div class="video-image" style="background-image: url(<?= $model->videoImage ?>);"></div>

            <?php } else if ($model->isGIF() && !empty($model->webmSrc)) { ?>

                <div id="gifPlayer-<?= $model->id ?>"
                     class="gif-player"
                     data-src-orig="<?= $model->webmSrc ?>"
                     data-src-alt="<?= $model->m4vSrc ?>"
                     data-src-poster="<?= $model->getThumbSrc('original') ?>"
                     data-width="<?= $model->media_width ?>"
                     data-height="<?= $model->media_height ?>">
                </div>

            <?php } else if (empty($model->videoUrl)) { ?>

                <div class="jumbotron">
                    <p class="text-muted">
                        <small>Processing...</small>
                    </p>
                    <p class="h3 video-info">
                        <?= empty($model->text) ? $model->file_name : $model->text ?>
                    </p>
                </div>

            <?php } else { ?>

                <?php if ($model->videoType === $model::VIDEO_TYPE_FACEBOOK) { ?>
                    <div class="fb-video"
                         data-allowfullscreen="1"
                         data-href="<?= $model->videoUrl ?>">
                    </div>
                <?php } else { ?>
                    <iframe width="100%" height="315" src="<?= $model->videoUrl ?>" frameborder="0"
                            allowfullscreen></iframe>
                <?php } ?>

            <?php } ?>
        </div>
    </div>
</div>