<?php

namespace app\components\behaviors;


use yii\base\Behavior;
use app\helpers\TokenHelper;

class ConsoleAuthClientBehavior extends Behavior
{
    public $dirNameAttribute  = 'tokenFileDir';

    private $_curlRequestOptions;

    /**
     * @return mixed
     */
    public function getCurlRequestOptions()
    {
        return $this->_curlRequestOptions;
    }


    public function populateCurlRequestData($url, $method, array $params, array $headers)
    {
        $curlOptions = [
            'CURLOPT_URL' => $url,
            'method' => $method,
            'params' => $params,
        ];

        if(strtoupper($method) == "POST"){
            $curlOptions['CURL_POSTFIELDS'] = http_build_query($params, '', '&', PHP_QUERY_RFC3986);
        }

        $this->_curlRequestOptions = $curlOptions;
    }


    public function getTokenFromFile()
    {
        TokenHelper::$tokenFilePathAlias = $this->owner->{$this->dirNameAttribute};
        $tokenArr = TokenHelper::getTokenFromFile();
        return $tokenArr;
    }

    public function getFileTokenStatus()
    {
        $tokenArr = $this->getTokenFromFile();
        if(empty($tokenArr)){
            return "Token file not found";
        }

        return
            (!empty($tokenArr['access_token']) || !empty($tokenArr['oauth_token']))
            &&
            (empty($tokenArr['expires_in']) || (int)($tokenArr['expires_in'] + $tokenArr['created']) > time())
            ? "Ok" :"Expired";
    }


    public function loadTokenFromFile()
    {

        $fileStatus = $this->getFileTokenStatus();
        if(strtolower($fileStatus) == 'ok'){

            $tokenArr = $this->getTokenFromFile();
            $this->owner->setAccessToken(['params'=>$tokenArr]);
        }

        return $fileStatus;
    }



}