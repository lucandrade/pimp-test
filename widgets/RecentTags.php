<?php

namespace app\widgets;


use yii\base\Exception;
use yii\base\Widget;
use yii\helpers\Html;

class RecentTags extends Widget
{
    public $tags;

    public function init()
    {
        if (!isset($this->tags)) {
            throw new Exception(self::className() . "::tag must be set");
        }
        parent::init();
    }


    public function run()
    {
        $tags = [];
        foreach ($this->tags as $tag) {
            $label = $tag->name . '&nbsp;' . Html::tag('span', $tag->usedNumber, ['class' => 'badge']);
            $tags[] = Html::a($label, $tag->getUrl(), ['class' => 'btn btn-xs btn-default tag-badge']);
        }
        return $this->render('RecentTags', ['tags' => $tags]);
    }
}