<?php

namespace app\components\widgets;


class ListView extends \yii\widgets\ListView
{
    public $afterItemContent;

    public $afterItemsContent;

    public $beforeItemsContent;

    public function renderItem($model, $key, $index)
    {
        $item = parent::renderItem($model, $key, $index);
        if(is_callable($this->afterItemContent)){
            $item .= call_user_func($this->afterItemContent, $model, $key, $index);
        }

        return $item;
    }


    public function renderItems()
    {
        $content = parent::renderItems();

        if(!empty($this->beforeItemsContent))
        {
            $beforeItemsContent = is_callable($this->beforeItemsContent) ? call_user_func($this->beforeItemsContent) : $this->beforeItemsContent;
            $content = $beforeItemsContent . $this->separator . $content;
        }

        if(is_callable($this->afterItemsContent)){
            $content .= $this->separator . call_user_func($this->afterItemsContent);
        }
        return $content;
    }
}