<?php
/**
 * Configuration file for the "yii asset" console command.
 */

// In the console environment, some path aliases may not exist. Please define these:
Yii::setAlias('@webroot', dirname(dirname(__DIR__)) . '/web');
Yii::setAlias('@web', '/');

return [
    // The list of asset bundles to compress:
    'bundles' => [
        'app\assets\PixieAsset',
    ],
    // Asset bundle for compression output:
    'targets' => [
        'all' => [
			'class' => 'yii\web\AssetBundle',
            'basePath' => '@webroot/assets',
            'baseUrl' => '@web/assets',
            'js' => 'js/image-editor-{hash}.js',
            'css' => 'css/image-editor-{hash}.css'
        ],
    ],
    // Asset manager configuration:
    'assetManager' => [
        'basePath' => '@webroot/assets',
        'baseUrl' => '@web/assets'
    ],
];