<?php

use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\grid\CheckboxColumn;
use yii\bootstrap\Html;
use yii\grid\ActionColumn;
use app\models\Post;

/**
 *
 * @var $dataProvider ActiveDataProvider
 * @var $filter string
 * @var $changeFields array
 * @var $modelClass string
 *
 */

$this->title = 'Posts';
$this->params['path'] = [
    ['label' => 'Posts']
];

// todo: create link - take from action link(?)
// todo: reset link - take from current route
// todo: delete selected button - move on top

$dataProvider->sort->attributes['author'] = [
    'asc' => ['user.username' => SORT_ASC],
    'desc' => ['user.username' => SORT_DESC],
];
$dataProvider->sort->defaultOrder = ['live_at' => SORT_DESC];

echo GridView::widget(
    [
        'layout' => $this->render('_index_grid_layout', [
            'filter' => $filter,
            'changeFields' => $changeFields,
            'modelClass' => $modelClass
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover responsive'
        ],
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model, $key, $index, $grid){
            if($model->isSoftDeleted){
                return ['class'=>'soft-deleted'];
            }
            return [];
        },
        'columns' => [
            [
                'class' => CheckboxColumn::className(),
                'headerOptions' => [
                    'class' => 'col-static',
                ],
                'contentOptions' => [
                    'class' => 'col-static',
                ]
            ],
            [
                'attribute' => 'id',
                'headerOptions' => [
                    'class' => 'col-static',
                ],
                'contentOptions' => [
                    'class' => 'col-static',
                ]
            ],
            [
                'attribute' => 'title',
                'format' => 'raw',
                'contentOptions' => [
                    'style' => 'max-width: 400px'
                ],
                'value' => function ($model) {
                    return Html::a($model->title, $model->getUrl());
                }
            ],
            [
                'attribute' => 'blocks',
                'format' => 'raw',
                'contentOptions' => [
                    'class' => ''
                ],
                'value' => function ($model) {
                    $icons = [];
                    foreach ($model->postBlockCounts as $type => $typeCount) {
                        switch ($type) {

                            case 'text':
                                $icons[] = '<span class="label label-default"><i class="fa fa-file-text-o"></i>&nbsp;' . $typeCount . '</span>';
                                break;

                            case 'image':
                                $icons[] = '<span class="label label-primary"><i class="fa fa-file-image-o"></i>&nbsp;' . $typeCount . '</span>';
                                break;

                            case 'video':
                                $icons[] = '<span class="label label-info"><i class="fa fa-file-video-o"></i>&nbsp;' . $typeCount . '</span>';
                                break;
                        }
                    }
                    return implode('&nbsp;', $icons);
                }
            ],
            [
                'attribute' => 'is_draft',
                'label' => 'Draft',
                'format' => 'raw',
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function ($model) {
                    if ($model->is_draft) {
                        return '<i class="fa fa-check text-success"></i>';
                    }
                    return '';
                }
            ],
            [
                'attribute' => 'is_adult',
                'label' => 'Adult',
                'format' => 'raw',
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function ($model) {
                    if ($model->is_adult) {
                        return '<i class="fa fa-check text-success"></i>';
                    }
                    return '';
                }
            ],
            [
                'attribute' => 'is_authors',
                'label' => 'Authors',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->is_authors) {
                        return '<i class="fa fa-check text-success"></i>';
                    }
                    return '';
                }
            ],
            [
                'attribute' => 'author',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->author) {
                        return Html::a($model->author->username, $model->author->getProfileUrl());
                    } else {
                        return $model->user_id;
                    }
                }
            ],
            [
                'attribute' => 'live_at',
                'format' => 'raw',
                'contentOptions' => [
                    'style' => 'width: 200px'
                ],
                'value' => function (Post $model) {

                    if (!$model->live_at) {
                        return ' - ';
                    }

                    $liveDate = $model->getLiveDate();
                    if (strtotime($model->live_at) > time()) {
                        $liveDate = '<i class="fa fa-clock-o"></i>&nbsp;' . $liveDate;
                    }
                    $liveDate = '<span class="small">'
                        . Yii::$app->formatter->asDatetime($model->live_at, 'php:Y-m-d H:i')
                        . '</span>'
                        . '<br>'
                        . $liveDate;

                    return $liveDate;
                }
            ],
            [
                'class' => ActionColumn::className(),
                'template' => '{update} {delete}',
                'contentOptions' => [
                    'class' => 'text-right nowrap',
                ],
                'buttons' => [
                    'update' => function ($url, $model, $key) {

                        $html =  $model->isSoftDeleted
                            ? Html::a('<i class="fa fa-reply"></i> Restore',
                                ['admin/posts-restore', 'ids'=>$model->id],
                                [
                                    'class' => 'btn btn-xs btn-info action-button',
                                    'data-id' => $model->id
                                ]) . " "
                            :"";
                            $html .= Html::a('<i class="fa fa-edit"></i> Edit', $model->getUpdateUrl(), [
                            'class' => 'btn btn-xs btn-success'
                        ]);

                        return $html;
                    },
                    'delete' => function ($url, $model, $key) {

                        return $model->isSoftDeleted ?
                            Html::a('<i class="fa fa-remove"></i> Purge', [
                                'admin/posts-purge',
                                ], [
                                'class' => 'btn btn-xs btn-danger delete-button action-button',
                                'data-id' => $model->id,
                                'data-need-confirmation' => 1
                                ]
                            )
                            :Html::a('<i class="fa fa-trash"></i> Delete', [
                                'admin/posts-delete',
                                ], [
                                    'class' => 'btn btn-xs btn-danger delete-button action-button',
                                    'data-id' => $model->id,
                                    'data-need-confirmation' => 1
                                ]
                            );
                    }
                ]
            ]
        ]
    ]
);