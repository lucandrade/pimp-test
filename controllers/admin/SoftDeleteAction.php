<?php

namespace app\controllers\admin;


use yii\base\Action;

class SoftDeleteAction extends PostGridAction
{
    public $appliedActionName = 'deleted';

    protected function processModel($model)
    {
        return $model->softDelete();
    }
}