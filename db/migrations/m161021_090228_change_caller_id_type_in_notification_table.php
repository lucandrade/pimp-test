<?php

use yii\db\Migration;

class m161021_090228_change_caller_id_type_in_notification_table extends Migration
{
    public $tb = 'notification';
    public $col = 'caller_id';

    public function up()
    {
        $this->alterColumn($this->tb, $this->col, $this->integer());
    }

    public function down()
    {
        $this->alterColumn($this->tb, $this->col, $this->integer()->notNull());
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
