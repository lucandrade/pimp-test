<?php

namespace app\helpers;


class SortHelper
{
    public static function valueToArrayOrder($value)
    {
        $direction = SORT_ASC;
        if (strncmp($value, '-', 1) === 0)
        {
            $direction = SORT_DESC;
            $value = substr($value, 1);
        }

        return [
            $value => $direction
        ];
    }
}