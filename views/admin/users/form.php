<?php

use app\models\User;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var $model User
 */
$button = $model->isNewRecord ? 'Create' : 'Update';

$this->title = $button . ' User';
$this->params['path'] = [
    ['label' => 'Users', 'url' => ['users']],
    ['label' => $button],
];

$form = ActiveForm::begin(
    [
        'id' => '',
        'method' => 'POST',
        'successCssClass' => false,
//		'action' => [($model->id ? 'admin/users/' . $model->id : 'admin/users-create')],
    ]
);
?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?php
            echo $form->field($model, 'username', ['options' => ['value' => ($model->username ?: '')]]);
            echo $form->field($model, 'email', ['options' => ['value' => ($model->email ?: '')]]);
            echo $form->field($model, 'password')->input('password');
            echo $form->field($model, 'is_activated_email')->checkbox([($model->status ? ['checked ' => ''] : '')]);
            echo $form->field($model, 'is_admin')->checkbox();
            ?>

            <div class="checkbox">
                <label>
                    <?= Html::hiddenInput($model->formName() . '[status]', User::STATUS_ACTIVE); ?>
                    <?= Html::checkbox($model->formName() . '[status]', $model->isBanned(), ['value' => User::STATUS_BANNED]); ?>
                    Is banned
                </label>
            </div>
        </div>

        <div class="panel-footer text-center">
            <?= Html::submitButton($button, ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
<?php
ActiveForm::end();