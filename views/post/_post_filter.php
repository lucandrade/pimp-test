<?php


/**
 * @var $this yii\web\View
 * @var $filter app\models\FilterPost
 */

use yii\helpers\Url;
use yii\helpers\Html;

?>

<div class="text-muted filter-posts">
    for
    <a href="#" class="choose-daterange">
        <?= $filter->periodString ?>
    </a>

    <?php if (0 && !Yii::$app->user->isGuest): //?>
        <div id="manage-seen-posts-ctrl"
             class="pull-right" <?= count($seenPostIds) == 0 ? "style='display:none'" : "" ?>>
            <span id="seen-posts-count""><?= count($seenPostIds) ?></span> seen posts were found
            <a id="toggle-seen-posts" href="#" data-status="Hide">Show</a>
        </div>
    <?php endif; ?>
</div>


<div id="popover-daterange" hidden="hidden">
    <p class="h4 text-center preset-labels">
        <?php foreach ($filter->presetsLabels as $preset => $label) { ?>
            <a href="<?= $label['url'] ?>" class="label label-default">
                <?= $label['title'] ?>
            </a>
        <?php } ?>
    </p>
    <br>
    <form action="<?= $filter->formActionUrl ?>"
          class="form-vertical"
          id="filter-posts-date"
          method="GET">
        <fieldset class="filter-daterange">
            <div class="form-group">
                <div class="block-inline block-inline-date">
                    <div class="input-group input-group-sm">
                        <input type="text" class="form-control" name="date_from"
                               value="<?= $filter->getDateFrom('Y-m-d', time()) ?>">
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-clear-date" type="button">
                                <i class="fa fa-remove"></i>
                            </button>
                        </span>
                    </div>
                </div>
                <div class="block-inline">
                    <div class="input-group">
                        <span class="btn">to</span>
                    </div>
                </div>
                <div class="block-inline block-inline-date">
                    <div class="input-group input-group-sm">
                        <input type="text" class="form-control" name="date_to"
                               value="<?= $filter->getDateTo('Y-m-d', time()) ?>">
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-clear-date" type="button">
                                <i class="fa fa-remove"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </div>
            <hr>
            <div class="inline-daterange">
                <div class="daterange-container"></div>
            </div>
            <hr>
            <div class="form-group text-center">
                <button class="btn btn-primary" type="submit">
                    Show
                </button>
            </div>
        </fieldset>
    </form>
</div>