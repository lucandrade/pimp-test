<?php
/**
 * Configuration file for the "yii asset" console command.
 */

// In the console environment, some path aliases may not exist. Please define these:
Yii::setAlias('@webroot', dirname(dirname(__DIR__)) . '/web');
Yii::setAlias('@web', '/');

return [
    // The list of asset bundles to compress:
    'bundles' => [
        'yii\widgets\ActiveFormAsset',
        'yii\grid\GridViewAsset',
        'yii\validators\ValidationAsset',
        'app\assets\ImageAsset',
        'app\assets\MainPageAsset',
        'app\assets\AlbumPickerAsset',
        'app\assets\AlbumAsset',
        'app\assets\AlbumsAsset',
        'app\assets\DatetimepickerAsset',
        'app\assets\PostAsset',
        'app\assets\YTExternalAsset',
        'app\assets\PhotoswipeAsset',
        'app\assets\InfiniteAjaxScrollAsset',
        'app\assets\NotificationAsset',
        'app\assets\FabricjsAsset',
        'app\assets\SearchPageAsset',
        'app\assets\TrashBinAsset',
        'app\assets\AbuseListAsset',
        'app\assets\AbuseReportAsset',
        'app\assets\EditableAsset',
        'app\assets\UploaderPopupAsset',
        'app\assets\PluginsPageAsset',
        \yii\widgets\PjaxAsset::className(),
        \app\assets\MainPageAsset::className(),

    ],
    // Asset bundle for compression output:
    'targets' => [
        'all' => [
			'class' => 'yii\web\AssetBundle',
            'basePath' => '@webroot/assets',
            'baseUrl' => '@web/assets',
            'js' => 'js/all-{hash}.js',
            'css' => 'css/all-{hash}.css'
        ],
    ],
    // Asset manager configuration:
    'assetManager' => [
        'basePath' => '@webroot/assets',
        'baseUrl' => '@web/assets'
    ],
];