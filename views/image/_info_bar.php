<?php
/**
 * @var $image \app\models\Image;
 */
?>

<div>
    Title: <?= $image->title ?>
</div>

<div>
    Size: <?= $image->size ?> bytes
</div>

<div>
    Link: <?= \yii\helpers\Url::to(['image/show', 'id'=>$image->id], true) ?>
</div>
