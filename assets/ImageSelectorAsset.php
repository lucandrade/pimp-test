<?php

namespace app\assets;


use yii\web\AssetBundle;

class ImageSelectorAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/image.selector.css'
    ];

    public $js = [
        'js/jquery.imageSelector.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];
    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}