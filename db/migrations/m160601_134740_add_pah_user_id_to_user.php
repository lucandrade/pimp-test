<?php

use yii\db\Migration;

class m160601_134740_add_pah_user_id_to_user extends Migration
{
    public $table = 'user';
    public $col = 'pah_user_id';

    public function up()
    {
        $this->addColumn($this->table, $this->col, $this->integer());
    }

    public function down()
    {
        $this->dropColumn($this->table, $this->col);
    }
}
