<?php
namespace app\components;

use Yii;

class PhpManager extends \yii\rbac\PhpManager
{
    public function init()
    {
        $this->itemFile = Yii::getAlias('@app/data/rbac') . '.php';
        $this->assignmentFile = Yii::getAlias('@app/data/assigments') . '.php';
        $this->ruleFile = Yii::getAlias('@app/data/rules') . '.php';

        parent::init();

        $ratingRule = new RatingRule();
        $this->add($ratingRule);

        if (!Yii::$app->user->isGuest) {
            $role = Yii::$app->user->identity->is_admin ? $this->getRole('admin') : $this->getRole('user');
            $this->assign($role, Yii::$app->user->identity->id);
        }
    }

    protected function saveToFile($data, $file)
    {
//        file_put_contents($file, "<?php\nreturn " . VarDumper::export($data) . ";\n", LOCK_EX);
    }
}