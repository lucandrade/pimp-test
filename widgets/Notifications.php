<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;
use app\models\Notification;

class Notifications extends Widget
{
    public $menuClass = 'nav navbar-nav navbar-right';

    public function run()
    {
        if (Yii::$app->user->identity !== null) {
            $notificationQuery = Notification::getActiveNotificationQuery();
            $notificationQuery->limit(Notification::$batchSize);
            return $this->render('notifications', [
                'menuClass' => $this->menuClass,
                'models' => $notificationQuery->all(),
                'notificationCount' => Notification::getActiveNotificationCount()
            ]);
        }
    }
}