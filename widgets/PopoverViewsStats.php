<?php

namespace app\widgets;


use app\helpers\ArrayHelper;
use app\helpers\Html;
use yii\base\InvalidConfigException;
use yii\bootstrap\Widget;

class PopoverViewsStats extends Widget
{
    public $stats;

    public $content;
    public $defaultOptions = [
        'role' => 'button',
        'class' => 'stats-popover underlined-link',
        'title' => 'Views statistic',
        'data-trigger' => 'hover',
        'data-html' => 1,
    ];

    public $options = [];

    public function ini(){
        if(empty($this->stats)){
            throw new InvalidConfigException("'stats' param can't be empty");
        }
        if(empty($this->content)){
            throw new InvalidConfigException("'content' param can't be empty");
        }
    }

    public function run()
    {
        $options = ArrayHelper::merge($this->options, $this->defaultOptions);
        $options['data-content'] = $this->render('PopoverViewsStats', ['stats' => $this->stats]);
        return Html::a($this->content, false, $options);
    }

}