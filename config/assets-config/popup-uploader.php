<?php
/**
 * Configuration file for the "yii asset" console command.
 */

// In the console environment, some path aliases may not exist. Please define these:
Yii::setAlias('@webroot', dirname(dirname(__DIR__)) . '/web');
Yii::setAlias('@web', '/');

return [

    'loadDependencies' => false,
    // The list of asset bundles to compress:
    'bundles' => [
        'app\assets\UploaderAsset',
        'app\assets\FullUploaderAsset',
        'app\assets\UploaderPopupAsset',
    ],
    // Asset bundle for compression output:
    'targets' => [
        'all' => [
			'class' => 'yii\web\AssetBundle',
            'basePath' => '@webroot/assets',
            'baseUrl' => '@web/assets',
            'js' => 'js/popup-uploader-{hash}.js',
            'css' => 'css/popup-uploader-{hash}.css'
        ],
    ],
    // Asset manager configuration:
    'assetManager' => [
        'basePath' => '@webroot/assets',
        'baseUrl' => '@web/assets'
    ],
];