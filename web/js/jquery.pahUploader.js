(function ($) {
    $.fn.pahUploader = function(method){
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method "' +  method + '" is not found in jquery.pahUploader' );
        }
    };

    var defaultSettings = {
        clicker: '#fileinput-button',
        fileInput: '.file-input',
        fileInputInsideContainer:true,
        albumIdSource: false,
        albumId: false,
        previewsContainer: '.previews',
        showProgress: true,
        showAfterUploadingInfo: true,
        showProgressText: true,
        previewContainerClass: "preview-container",
        maxFileSize: 5000000,
        renderPreviewImage : true,
        prependPreviews:true,
        createPreviewContainer:function(file, settings){
            var  $previewContainer = $("<div></div>").addClass(settings.previewContainerClass);
            $previewContainer.attr('id', 'preview_' + file.fileId);

            if(settings.renderPreviewImage){
                var $img = $('<img/>').addClass("img-responsive").hide();
                $previewContainer.append($img);
            }

            if (settings.showProgress) {
                var $progress = $(
                    '<div class="absolute-progress">' +
                    '<div class="progress" data-uploader-file="' + file.name + '">' +
                    '<div class="progress-text">' +
                    '<span class="progress-percent">0%</span> - ' +
                    '<span class="progress-loaded">0kb</span>' +
                    ' (<span class="progress-bitrate">0</span>' +
                    '<span class="progress-bitrate-units">kb</span>)'+
                    '</div>' +
                    '<div class="bar"></div>' +
                    '   </div>' +
                    '</div>');

                $previewContainer.append($progress);
                $previewContainer.append(
                    "<i class='fa fa-2x fa-spin fa-spinner loading-indincator'></i>"
                );
            }

            if(!file.error)
            {
                if(settings.renderPreviewImage){
                    var reader = new FileReader();
                    reader.onload = function (event) {
                        $img.attr('src', event.target.result);
                        $img.show();
                    };
                    reader.readAsDataURL(file);
                }
            }else{
                $img.addClass('wrong-format');
            }

            settings.addError($previewContainer, file);

            return $previewContainer;
        },
        addError:function($previewContainer, file){
            if(file.error){
                displayError($previewContainer, file.error)
            }
        }
    };

    var settings = {};
    var $clicker = false, $fileInput, $albumInput, $previewsContainer, backendUrl, $uploadContainer;

    var initElems = function(options){

        settings = {};
        settings = $.extend({}, defaultSettings, options || {});

        $uploadContainer = $(this);

        if(settings.clicker){
            $clicker = $(settings.clicker);
        }

        this.$fileInput = $fileInput = getFileInput();
        $albumInput = settings.albumIdSource ? $(settings.albumIdSource) : [];
        backendUrl = options.url || $fileInput.data('url');
    };

    var methods = {
        init:function(options)
        {
            initElems.call(this, options);

            var submitDone = options.submitDone || function (data) {
                    if (data.files) {
                        var file = data.files[0];
                        var $previewContainer = $('#preview_' + file.fileId);

                        if (settings.showAfterUploadingInfo) {
                            var $bar = $(data.files[0].image.htmlBar).hide();
                            $previewContainer.append($bar);
                            $bar.fadeIn('slow');
                        }
                        if (settings.showProgress) {
                            $previewContainer.find(".loading-indincator").hide();
                        }

                        if (settings.successUploaded && typeof settings.successUploaded == 'function') {
                            settings.successUploaded(file, $previewContainer);
                        }
                    } else {
                        alert("Coudn't upload picture");
                    }
                };

            this.fileuploadOptions = $.extend ({
                url: backendUrl,
                dataType: "JSON",

                maxChunkSize: 1000000,
                add: function (e, data) {

                    $previewsContainer = $(settings.previewsContainer);
                    if ($previewsContainer.length == 0) {
                        $previewsContainer = $("<div/>").addClass('previews');
                        $uploadContainer.append($previewsContainer);
                    }

                    if(!data.formData)
                    {
                        var formData = {};

                        if (settings.albumId) {
                            formData.albumId = settings.albumId;
                        } else if ($albumInput.length > 0) {
                            formData.albumId = $albumInput.val();
                        }

                        data.formData = formData;
                    }

                    if (data.files) {
                        $.each(data.files, function () {
                            var file = this;
                            var matches = file.type.match(/^image\/(gif|jpe?g|png)$/i);
                            if(!matches){
                                file.isError = true;
                                file.error = {
                                    type:"wrong-format",
                                    title:"Error: file '" + file.name + "' is not image",
                                    description:"Only images in formats png/jpeg/gif are allowed"
                                }

                            }

                            if(file.size > settings.maxFileSize){
                                file.error = {
                                    title:"Error: file '" + file.name + "' is too big",
                                    description:"Size "+ parseInt(file.size/1000) + " kB (Maximum allowed:" + parseInt(settings.maxFileSize/1000) + "kB)"
                                }
                            }

                            var $previewContainer;

                            file.fileId = Math.random().toString(36).replace(/[^a-z]+/g, '');

                            data.formData.fileId = file.fileId;

                            $previewContainer = settings.createPreviewContainer(file, settings);

                            if (settings.beforeAppendPreviewContainer && typeof(settings.beforeAppendPreviewContainer) == 'function') {
                                settings.beforeAppendPreviewContainer($previewContainer, $previewsContainer);
                            }

                            if(settings.prependPreviews){
                                $previewsContainer.prepend($previewContainer);
                            }else{
                                $previewsContainer.append($previewContainer);
                            }

                            if (settings.afterAppendPreviewContainer && typeof(settings.afterAppendPreviewContainer) == 'function') {
                                settings.afterAppendPreviewContainer($previewContainer, $previewsContainer);
                            }

                            if (!$previewContainer.data('stop-submit')) {
                                if(typeof(data.submitDone) === 'function'){
                                    data.submit().done(data.submitDone);
                                }else{
                                    data.submit().done(submitDone);
                                }
                            }
                        });
                    }
                },

                progress: function (e, data) {
                    if (!settings.showProgress) {
                        return
                    }
                    var $preview = $('#preview_' + data.formData.fileId);
                    updateProgress($preview, data.loaded, data.total, data.bitrate);
                }

            }, options.fileuploadOptions);

            if($clicker){
                $clicker.on('click', function (e) {
                    e.preventDefault();
                    var $input = getFileInput();
                    $input.click();
                    return false;
                });
            }

            this.$fileInput.fileupload(this.fileuploadOptions);
        },

        reInitElems:function(options){
            var newSettings = $.extend({}, settings, options || {});
            initElems.call(this, newSettings);
        },

        getPluginUploader:function(){
            this.$fileInput.fileupload(this.fileuploadOptions);
            return this.$fileInput;
        },

        formatBitrate:function(bitrate){
            return formatBitrate(bitrate);
        },

        addError:function($previewContainer, error){
            displayError($previewContainer, error)
        }
    };


    var getFileInput = function(){
        return settings.fileInputInsideContainer ?  $uploadContainer.find(settings.fileInput) : $(settings.fileInput);
    };

    var updateProgress = function($preview, loaded, total, bitrate){
        var progress = parseInt(loaded / total * 100, 10);
        var $progress = $preview.find(".progress");

        if (settings.showProgressText) {
            bitrate = formatBitrate(bitrate);
            $progress.find('.progress-percent').text(progress + "%");
            $progress.find('.progress-loaded').text(parseInt(loaded / 1000) + "kB");
            $progress.find('.progress-bitrate').text(bitrate.value);
            $progress.find('.progress-bitrate-units').text(bitrate.units);
        }

        $progress.show();
        $progress.find(".bar").css(
            'width',
            progress + '%'
        );
    };

    var displayError = function($previewContainer, error)
    {
        $previewContainer.data('stop-submit', true);
        $previewContainer.addClass("uploader-error");
        $previewContainer.attr('title','Hide');
        $previewContainer.on('click', function(){
            $(this).fadeOut("slow", function(){
                $(this).remove();
            });
        });

        if(error.imageUrl) {
            $previewContainer.find('.im-wr').css('background-image', 'url("' + error.imageUrl + '")')
        };
        var $contentTitle = $previewContainer.find('.content-title');
        if($contentTitle.length > 0){
            $contentTitle.html(
                "<div class='content-header'>" + error.title + "</div>"+
                "<div class='content-info'>"+error.description+"</div>"
            );
        }else{
            $previewContainer.append(
                "<div class='content-title'>" +
                "<div class='content-header'>" + error.title + "</div>"+
                "<div class='content-info'>" + error.description + "</div>" +
                "</div>");
        }

    };

    var formatBitrate = function (bitrate) {
        if (parseInt(bitrate / 8000000) > 0) {
            return {
                value: parseInt(bitrate / 8000000),
                units: 'MB/s'
            }
        } else if (parseInt(bitrate / 8000) > 0) {
            return {
                value: parseInt(bitrate / 8000),
                units: 'kB/s'
            }
        } else {
            return {
                value: parseInt(bitrate / 8),
                units: 'B/s'
            }
        }
    };
})(jQuery);
