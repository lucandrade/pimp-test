<?php

namespace app\models;

use app\helpers\SortHelper;
use \Yii;
use yii\base\Exception;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use app\helpers\FileHelper;
use app\helpers\ImageHelper;
use yii\db\Transaction;
use app\helpers\WidgetHelper;
use app\helpers\ArrayHelper;

/**
 * Class User
 *
 * @property string $email;
 * @property string $username;
 * @property string $password;
 * @property int $status;
 * @property boolean $is_activated_email
 * @property string $email_activation_key;
 * @property string $current_oauth_provider
 *
 * @package app\models
 */
class User extends BaseModel implements \yii\web\IdentityInterface
{
    const EMAIL_NOT_ACTIVATED = 0;
    const EMAIL_ACTIVATED = 1;
    const TYPE_USER = 0;
    const TYPE_ADMINISTRATOR = 1;
    const SCENARIO_SETTINGS = 'settings';

    const ANONYMOUS_ID = 1;


    private $_avatar;
    protected $_oauthErrors = [];
    private $_oauth;
    private $_favoritePostIds;
    private $_transaction;
    private $_forumUser;



    public static function ratingMinEdges() {
        return [
            'red' => 400,
            'blue' => 200,
            'yellow' => 100,
            'green' => 20,
            'gray' => 0,
        ];
    }


    public function getCurrentBadgeColor()
    {
        foreach(self::ratingMinEdges() as $color => $edge){
            if($this->rating >= $edge){
                return $color;
            }
        }

        return 'gray';
    }

    public function getNextBadgeNeededRating()
    {
        $edges = self::ratingMinEdges();
        sort($edges, SORT_ASC);
        $nextEdge = 0;
        foreach($edges as $edge){
            if($this->rating < $edge)
            {
                $nextEdge = $edge;
                break;
            }
        }

        return $nextEdge - $this->rating;
    }

    private $_linkedAccounts = false;
    private $_settings = [
        'show_adult' => null,
        'expand_posts' => null
    ];
    public $file_name;

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_SETTINGS] = ['username', 'password'];
        return $scenarios;
    }

    public function rules()
    {
        return [

            [['username'], 'filter', 'filter' => 'trim'],
            [['username'], 'filter', 'filter' => function($value){
                return str_replace(' ', '_', $value);
            }],
            [['username'], 'string', 'min' => 2, 'max' => 50],

//            [['password'], 'string', 'min' => 6, 'max' => 16],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => "Email address '{value}' has already been taken."],

            ['status', 'number', 'integerOnly' => true],

            ['current_oauth_provider', 'in', 'range' => UserOauth::possibleSocialAccounts()],

            ['is_activated_email', 'default', 'value' => self::EMAIL_NOT_ACTIVATED],
            ['is_activated_email', 'boolean'],

            ['is_admin', 'default', 'value' => self::TYPE_USER],
            ['is_admin', 'boolean'],

            ['lastvisit_at', 'safe'],
            [['profile', 'settings'], 'safe'],
            [['gender','money_make'], 'number', 'integerOnly' => true],

            [['payout_type', 'wallet', 'full_name', 'icq',
                'payment_address', 'payment_city', 'payment_postcode','payment_country','payment_phone'], 'string']
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord && empty($this->username)) {
            $this->username = $this->getEmailName();
        }
        if ($this->scenario === self::SCENARIO_SETTINGS) {
            $this->saveSettings();
        }

        if($insert && !isset($this->status)){
            $this->status = self::STATUS_ACTIVE;
        }
        return parent::beforeSave($insert);
    }

    public function saveSettings()
    {
        foreach ($this->_settings as $name => $value) {

        }
    }

    public function beforeDelete()
    {
        $this->_transaction = $this->getDb()->beginTransaction();

        foreach ($this->posts as $post) {
            $post->delete();
        }

        UserFavoritePost::deleteAll(['user_id' => $this->id]);
        UserSeenPost::deleteAll(['user_id' => $this->id]);

        return parent::beforeDelete();
    }

    public function removeRemoteComments()
    {
        $config = WidgetHelper::getConfig('', 2);
        $url = $config['mainUrl'] . "/v1/user/delete";
        unset($config['mainUrl']);
        $config['id'] = $this->id;
        $ch = curl_init();
        $dataString = http_build_query($config);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($config));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($ch);

        $errorNumber = curl_errno($ch);
        $errorMessage = curl_error($ch);
        curl_close($ch);

        if (!$errorNumber) {
            $result = Json::decode($result);
            if (isset($result['status'])
                && ($result['status'] == 'ok' || (isset($result['code']) && $result['code'] == '404'))
            ) {
                return true;
            } else {
                throw new Exception("Coudn't remove remote comments user, request failed, response - " . json_encode($result));
            }
        }
        throw new Exception('Curl error requesting "' . $url . '": #' . $errorNumber . ' - ' . $errorMessage);
    }

    public function afterDelete()
    {
        if ($this->_transaction instanceof Transaction
            && $this->removeRemoteComments()
        ) {
            $this->_transaction->commit();
        }
        parent::afterDelete();
    }

    public function afterSave($insert, $changedAttributes)
    {
        if (
            isset($changedAttributes['status']) &&
            (int)$this->status === self::STATUS_BANNED
        ) {
            $this->setPostsBanned();
        } else if (
            isset($changedAttributes['status']) &&
            (int)$changedAttributes['status'] === self::STATUS_BANNED &&
            (int)$this->status === self::STATUS_ACTIVE
        ) {
            $this->unsetPostsBanned();
        }

        parent::afterSave($insert, $changedAttributes);
    }


    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::findOne(['activkey' => $token, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findIdentityByEmailKey($key)
    {
        return self::findOne(['email_activation_key' => $key, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByPasswordResetToken($token)
    {
        return self::findOne(['password_reset_token' => $token, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByEmail($email)
    {
        return self::findOne(['email' => $email]);
    }


    /**
     * Finds user by username
     *
     * @param  string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne(['username' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->activkey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->activkey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->activkey = Yii::$app->security->generateRandomString();
    }

    public function generateEmailKey()
    {
        $this->email_activation_key = Yii::$app->security->generateRandomString();
    }

    public function getEmailKey()
    {
        return $this->email_activation_key;
    }

    public function activateEmail()
    {
        $this->is_activated_email = self::EMAIL_ACTIVATED;
        return $this->save();
    }

    public static function getByOauth($attributes, $provider)
    {
        $attributes = self::normalizeAttributes($attributes, $provider);

        $userOauth = UserOauth::findOne(['identifier' => $attributes['id'], 'provider' => $provider]);
        if ($userOauth !== null) {
            $user = $userOauth->user;
            if ($user !== null) {
//                $user->setCurrentOauth($userOauth);
                return $user;
            }
        }

        if (isset($attributes['email'])) {
            $user = self::findByEmail($attributes['email']);
            if (!empty($user)) {
                if (!$user->addSocialAccount($attributes, $provider)) {
                    \Yii::$app->session->setFlash('error', 'Some errors occurred while adding new social account to existing user');
                }
                return $user;
            }
        }

        return self::createBySocial($attributes, $provider);
    }

    public static function normalizeAttributes($attributes, $provider)
    {
        $method = 'normalizeBy' . ucfirst($provider);

        if (!method_exists(self::className(), $method)) {
            return $attributes;
        }
        return self::$method($attributes);
    }

    public static function normalizeByFacebook($attributes)
    {
        return [
            'username' => $attributes['name'],
            'id' => $attributes['id'],
            'email' => $attributes['email']
        ];
    }

    public static function normalizeByGoogle($attributes)
    {
        return [
            'username' => $attributes['displayName'],
            'email' => !empty($attributes['emails'][0]['value']) ? $attributes['emails'][0]['value'] : null,
            'id' => $attributes['id'],
            'avatar' => !empty($attributes['image']['url']) ? $attributes['image']['url'] : ""
        ];
    }

    public static function normalizeByTwitter($attributes)
    {
        return [
            'username' => $attributes['screen_name'],
            'id' => $attributes['id'],
            'avatar' => !empty($attributes['profile_image_url']) ? $attributes['profile_image_url'] : ""
        ];
    }

    public static function createBySocial($attributes, $provider)
    {
        $user = new self;

        $user->setAttributes([
            'username' => $attributes['username'],
            'email' => !empty($attributes['email']) ? $attributes['email'] : null,
        ]);

        $user->generateEmailKey();
        $user->generateAuthKey();

        $trans = $user->getDb()->beginTransaction();

        if (
            $user->save() &&
            $user->addSocialAccount($attributes, $provider)&&
            !empty($user->getForumUser())
            && !$user->getForumUser()->hasErrors()
        )
        {
            $trans->commit();
        }

        return $user;
    }


    /**
     * @return \app\models\forum\User;User
     */
    public function getForumUser($registerIfEmpty = true)
    {
        if(!$this->_forumUser){
            $this->_forumUser = \app\models\forum\User::findOne(['username' => $this->username]);
            if(!$this->_forumUser && $registerIfEmpty){
                $this->registerForumUser();
            }
        }
        return $this->_forumUser;
    }


    public function registerForumUser($password = null)
    {
        $user = new \app\models\forum\User();
        $user->email = $this->email;
        $user->username = $this->username;
        $user->joindate = time();
        $user->passworddate = date('Y-m-d');

        if($password){
            $user->setPassword($password);
        }
        $user->save();
        $this->_forumUser = $user;
        return $this->_forumUser;
    }

    public function addSocialAccount($attributes, $provider, $setAsCurrent = true)
    {
        if ($oauth = UserOauth::findOne(['identifier' => $attributes['id'] . "", 'provider' => $provider])) {
            $this->_oauthErrors['endUserMessage'] = "This " . ucfirst($provider) . " account is already linked to another user";
            return false;
        }
        $userOauth = new UserOauth();
        $userOauth->provider = $provider;
        $userOauth->identifier = $attributes['id'] . "";
        $userOauth->user_id = $this->id;

        if (!empty($attributes['avatar'])) {
            $userOauth->avatar_url = $attributes['avatar'];
        }

        $userOauth->display_name = $attributes['username'];

        $userOauth->save();
        $this->_oauthErrors = $userOauth->getErrors();
        if ($setAsCurrent) {
            $this->setCurrentOauth($userOauth);
        }
        return !$userOauth->hasErrors();
    }


    
    public function generateDisplayNameFromSocialAttributes($attributes)
    {
        $displayName = '';    
        if (isset($attributes['username'])) {
            $displayName = $attributes['username'];
        } else if (isset($attributes['displayName'])) {
            $displayName = $attributes['displayName'];
        } else if (isset($attributes['screen_name'])) {
            $displayName = $attributes['screen_name'];
        } else if (isset($attributes['name'])) {
            $displayName = $attributes['name'];
        }

        return $displayName;
    }


    public function getOauthErrors($attribute = null)
    {
        if ($attribute === null) {
            return $this->_oauthErrors;
        } else {
            if (isset($this->_oauthErrors[$attribute])) {
                return $this->_oauthErrors[$attribute];
            } else {
                return null;
            }
        }
    }

    public function getCurrentOauth()
    {
        if (!isset($this->_oauth)) {

            if (empty($this->current_oauth_provider)) {
                $this->_oauth = false;
            } else {
                $this->_oauth = UserOauth::findOne(['provider' => $this->current_oauth_provider, 'user_id' => $this->id]);
                if (empty($this->_oauth)) {
                    throw new Exception("We can't find oauth for you");
                }
            }
        }

        return $this->_oauth;
    }

    public function setCurrentOauth(UserOauth $oauth, $updateInDb = true)
    {
        $this->_oauth = $oauth;
        $this->current_oauth_provider = $oauth->provider;

        if ($updateInDb) {
            $this->save();
        }
    }

    public function getDisplayName()
    {
        return empty($this->username) ? $this->getEmailId() : $this->username;
    }

    public function getEmailId()
    {
        $explode = explode("@", $this->email);
        array_pop($explode);
        return implode('@', $explode);
    }

    public function getAvatarPath()
    {
        $path = Yii::getAlias('@app/media_protected/avatar/');
        $path .= ImageHelper::getPathPrefix($this->id);
        FileHelper::createDirectory($path, 0777);
        return $path;
    }

    public function getEmailName()
    {
        $explode = explode("@", $this->email);
        return reset($explode);
    }

    public function generateAvatarName($file)
    {
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        return $this->id . '_' . md5(Yii::$app->security->generateRandomString(4)) . '.' . $ext;
    }

    public function getAvatarFileName()
    {
        if (!empty($this->getSettings('avatar'))) {
            return $this->avatarPath . $this->getSettings('avatar');
        }

        return null;
    }

    public function saveLoadedAvatar($avatarFileName, $copy = false)
    {

        if (file_exists($avatarFileName)) {
            if (in_array(mime_content_type($avatarFileName), ImageHelper::$imageMimeTypes)) {
                $this->setSettings('avatar', $this->generateAvatarName($avatarFileName));

                if (
                    ($copy && copy($avatarFileName, $this->avatarFileName)) ||
                    rename($avatarFileName, $this->avatarFileName)
                ) {
                    return true;

                } else {
                    $this->addError('file_name', 'Failed move avatar file(class ' . __CLASS__ . ' line ' . __LINE__ . ')');
                    return false;
                }

            } else {

                $logData = [
                    'user_id' => $this->id,
                    'fileName' => $avatarFileName,
                    'mime' => mime_content_type($avatarFileName),
                ];
                ImageHelper::log($logData);

            }

        } else {
            $this->addError('file_name', 'Image file not exist(class ' . __CLASS__ . ' line ' . __LINE__ . ')');
            return false;
        }

    }

    public function getAvatarSrc($format = 'thumb_50')
    {

        if (file_exists($this->avatarFileName)) {
            if (!in_array($format, ImageHelper::$availableFormats)) {
                $format = ImageHelper::$defaultSrcFormat;
            }
            $src = Url::base(true) . '/media/?';
            $src .= $format;
            $src .= '?/avatar/';
            $src .= ImageHelper::getPathPrefix($this->id);
            $src .= $this->getSettings('avatar');
            return $src;
        }

        return null;
    }

    public function getProfileAvatar($format = 'thumb_50')
    {
        if ($this->getAvatarSrc($format)) {
            return $this->getAvatarSrc($format);
        }
        return $this->getGravatar(50);
    }

    public function getAvatar($size = null)
    {
        if (!empty($this->current_oauth_provider)) {
            $oauth = $this->getCurrentOauth();
            if ($oauth) {
                return $oauth->getAvatarUrl($size);
            }
        }

        return $this->getProfileAvatar();
    }

    /**
     * Get either a Gravatar URL or complete image tag for a specified email address.
     *
     * @param string $s Size in pixels, defaults to 80px [ 1 - 2048 ]
     * @param string $d Default imageset to use [ 404 | mm | identicon | monsterid | wavatar ]
     * @param string $r Maximum rating (inclusive) [ g | pg | r | x ]
     * @param boolean $img True to return a complete IMG tag False for just the URL
     * @param array $atts Optional, additional key/value attributes to include in the IMG tag
     * @return String containing either just a URL or a complete image tag
     * @source http://gravatar.com/site/implement/images/php/
     */
    public function getGravatar($s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array())
    {
        $url = 'http://www.gravatar.com/avatar/';
        $url .= md5(strtolower(trim($this->email)));
        $url .= "?s=$s&d=$d&r=$r";
        if ($img) {
            $url = '<img src="' . $url . '"';
            foreach ($atts as $key => $val)
                $url .= ' ' . $key . '="' . $val . '"';
            $url .= ' />';
        }
        return $url;
    }

    public function getLinkedAccounts()
    {
        if (!$this->_linkedAccounts) {
            $this->_linkedAccounts = $this->hasMany(UserOauth::className(), ['user_id' => 'id']);
        }

        return $this->_linkedAccounts;
    }

    public function getSettings($name, $default = null)
    {
        if (isset($this->_settings[$name])) {
            return $this->_settings[$name];
        }

        return $this->_settings[$name] = UserSettings::get($this->id, $name, $default);
    }

    public function setSettings($name, $value)
    {
        if (isset($this->_settings[$name])) {
            if ($this->_settings[$name] === $value) {
                return true;
            }
        }

        $settings = UserSettings::set($this->id, $name, $value);
        if ($settings->hasErrors()) {
            return false;
        }

        $this->_settings[$name] = $value;
        return true;
    }

    public function getUserSettings()
    {
        $this->hasMany(UserSettings::className(), ['user_id' => 'id']);
    }

    public function getLinkedProviders()
    {
        $providers = [];
        foreach ($this->linkedAccounts as $account) {
            $providers[] = $account->provider;
        }

        return $providers;

    }

    public function getLinkedProviderIcons()
    {
        $socialProviders = [];

        foreach ($this->linkedProviders as $provider) {
            switch ($provider) {
                case 'facebook':
                    if ($provider === $this->current_oauth_provider) {
                        $socialProviders[] = '<span class="text-primary"><i class="fa fa-facebook"></i></span>';
                    } else {
                        $socialProviders[] = '<i class="fa fa-facebook"></i>';
                    }
                    break;
                case 'google':
                    if ($provider === $this->current_oauth_provider) {
                        $socialProviders[] = '<span class="text-success"><i class="fa fa-google"></i></span>';
                    } else {
                        $socialProviders[] = '<i class="fa fa-google"></i>';
                    }
                    break;
                case 'twitter':
                    if ($provider === $this->current_oauth_provider) {
                        $socialProviders[] = '<span class="text-info"><i class="fa fa-twitter"></i></span>';
                    } else {
                        $socialProviders[] = '<i class="fa fa-twitter"></i>';
                    }
                    break;
                default:
                    $socialProviders[] = $provider;
            }
        }

        return implode('&nbsp;', $socialProviders);
    }

    public function getLinkedAccount($provider)
    {
        foreach ($this->linkedAccounts as $account) {
            if ($account->provider == $provider) {
                return $account;
            }
        }
    }

    public function isDefaultProvider($provider)
    {
        return $this->current_oauth_provider == $provider;
    }

    public function isLinkedAccount($provider)
    {
        return in_array($provider, $this->getLinkedProviders());
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function unlinkSocialAccount($provider)
    {
        $trans = \Yii::$app->db->beginTransaction();
        $resetedCurrentOauth = true;

        if ($this->current_oauth_provider == $provider) {
            $this->current_oauth_provider = '';
            $resetedCurrentOauth = $this->save();
        }

        $deleted = UserOauth::deleteAll(['user_id' => $this->id, 'provider' => $provider]);

        if ($deleted && $resetedCurrentOauth) {
            $trans->commit();
            return true;
        }

        return false;
    }

    public function getAuthorUrl()
    {
        return Url::to(['site/search', 'q' => "user_" . $this->id]);
    }

    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['user_id' => 'id']);
    }

    public function addPostToSeen($postId)
    {
        $model = UserSeenPost::findOne(['post_id' => $postId, 'user_id' => $this->id]);
        return $model ? $model : UserSeenPost::create($this->id, $postId);
    }

    public function toggleFavoritePost($postId, $addOnly = false)
    {
        $model = UserFavoritePost::findOne(['post_id' => $postId, 'user_id' => $this->id]);
        if ($model) {
            if ($addOnly || $model->delete()) {
                return -1;
            }
        } else {
            $model = UserFavoritePost::create($this->id, $postId);
            if (!$model->hasErrors()) {
                return 1;
            }
        }
        return $model->getErrors();
    }

    public function getSeenPostIds($contextCondition = '', $contextParams = [])
    {
        $ids = UserSeenPost::find()->where('user_id=:user_id', [':user_id' => $this->id])
            ->andWhere($contextCondition, $contextParams)->asArray()->select('post_id')
            ->column();

        return $ids;
    }

    /**
     * @return mixed
     */
    public function getFavoritePostIds()
    {
        if (!$this->_favoritePostIds) {
            $this->_favoritePostIds = UserFavoritePost::find()->where('user_id=:user_id', [':user_id' => $this->id])
                ->asArray()->select('post_id')->column();
        }

        return $this->_favoritePostIds;
    }

    public function getCommentedPost()
    {
        $commentedPost = UserComment::find()
            ->where('author_id=:author_id', [':author_id' => $this->id])
            ->select('page_id')
            ->column();

        return $commentedPost;
    }

    public function getOwnedPostIds()
    {
        $ownedPostIds = Post::find()
            ->where('user_id=:user_id', [':user_id' => $this->id])
            ->select('id')
            ->column();
        
        return $ownedPostIds;
    }

    public function isFavoritePost($postId)
    {
        return in_array($postId, $this->favoritePostIds);
    }

    public function setPostsBanned()
    {
        Post::updateAll([
            'status' => Post::STATUS_BANNED
        ], [
            'status' => Post::STATUS_ACTIVE,
            'user_id' => $this->id
        ]);
    }

    public function unsetPostsBanned()
    {
        Post::updateAll([
            'status' => Post::STATUS_ACTIVE
        ], [
            'status' => Post::STATUS_BANNED,
            'user_id' => $this->id
        ]);
    }

    public function getProfileUrl($scheme = false)
    {
        return Url::to(['user/profile', 'id' => $this->id], $scheme);
    }

    public function isShowAdult()
    {
        return (int)$this->getSettings('show_adult') === 1;
    }

    public function getHeaderAvatar()
    {
        return Html::img($this->avatar, ['class' => 'img-circle']);
    }

    public function getPublicProfileAvatar()
    {
        return Html::img($this->avatar, ['class' => 'header-avatar']);
    }

    public static function countVisit()
    {
        return self::find()
            ->where('DATE(lastvisit_at) = CURDATE()')
            ->count();
    }

    public static function countCurrentRegistred()
    {
        return self::find()
            ->where('DATE(created_at) = CURDATE()')
            ->andWhere('HOUR(created_at) = HOUR(NOW())')
            ->count();
    }

    public static function findAllRegistrationId($dateFrom = null, $dateTo = null)
    {
        $query = self::find()
            ->indexBy('id')
            ->select(['id', 'created_at']);

        if ($dateFrom !== null && $dateTo !== null) {
            $datetimeFrom = strlen($dateFrom) === 10 ? $dateFrom . ' 00:00:00' : $dateFrom;
            $datetimeTo = strlen($dateTo) === 10 ? $dateFrom . ' 23:59:59' : $dateTo;
            $query->andWhere(['between', 'created_at', $datetimeFrom, $datetimeTo]);
        }

        $models = $query->all();
        return array_keys($models);
    }

    public function getLastVisitDate()
    {
        return Yii::$app->formatter->asRelativeTime($this->lastvisit_at);
    }

    public static function createFromForumData(\app\models\forum\User $forumUser)
    {
        $user = new self;
        $user->username = $forumUser->username;
        $user->email = $forumUser->email;
        $user->generateAuthKey();
        $user->save();
        return $user;
    }

    public function updateLastVisit()
    {
        $this->lastvisit_at = date('Y-m-d H:i:s');
        return $this->save(true, ['lastvisit_at']);
    }

    public function getIsEmailActivated()
    {
        return $this->is_activated_email;
    }


    public function getAlbums()
    {
        return $this->hasMany(Album::className(), ['authorId'=>'id']);
    }


    public function getImages($statuses = [Image::STATUS_NORMAL, Image::STATUS_PROCESSING])
    {
        $query = $this->hasMany(Image::className(), ['authorId'=>'id']);
        if(!empty($statuses)){
            $query->andWhere(['status' => $statuses]);
        }

        return $query;
    }

    public function getPreparedAlbums()
    {
        return ArrayHelper::map( $this->getAlbums()
            ->select(['id', 'title'])
            ->asArray()
            ->all(), 'id', 'title');
    }


    public function getIsAnon()
    {
        return $this->id == self::ANONYMOUS_ID;
    }


    public function transferForumErrors()
    {
        $forumErrors = $this->getForumUser()->getErrors();
        foreach($forumErrors as $attribute => $message)
        {
            if($this->hasAttribute($attribute)){
                $this->addError($attribute, "Forum user:" . $message);
            }
        }
    }


    public function getIsActive()
    {
        return $this->status === self::STATUS_ACTIVE;
    }


    public function getUsernameLink($scheme = false)
    {
        return Html::a($this->getDisplayName(), $this->getProfileUrl($scheme));
    }


    public function getUnsortedAlbum()
    {
        return Album::createUnsorted($this->id);
    }

    public function getPageSize($optionName)
    {
        $value = $this->getSettings($optionName);
        if(empty($value)){
            $value = \Yii::$app->params['imagesPerPage'];
        }

        return $value;
    }


    public function getIsAdmin()
    {
        return !empty($this->is_admin);
    }


    public function getSortOrder($optionName){
        $value = $this->getSettings($optionName);
        if(!empty($value)){
            return SortHelper::valueToArrayOrder($value);
        }else{
            return null;
        }
    }

}
