<?php

use yii\db\Migration;

class m160317_140000_add_text_field_to_notification_table extends Migration
{
    public $table = "notification";

    public function up()
    {
        $this->addColumn($this->table, 'text', 'varchar(255) NOT NULL AFTER type_id');
    }

    public function down()
    {
        $this->dropColumn($this->table, 'text');
    }
}
