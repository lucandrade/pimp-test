<div id="fb-root"></div>
<script>
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<style>
    .fb_iframe_widget {
        width: 100%;
        max-width: 100%;
        overflow: hidden;
    }
    .fb_iframe_widget span {
        display: block;
        margin: 0 auto;
    }
</style>
<div class="fb-page"
     data-href="https://www.facebook.com/ownedcom/"
     data-small-header="true"
     data-adapt-container-width="true"
     data-hide-cover="false"
     data-show-facepile="true">
    <div class="fb-xfbml-parse-ignore">
        <blockquote cite="https://www.facebook.com/ownedcom/">
            <a href="https://www.facebook.com/ownedcom/">Owned.com</a>
        </blockquote>
    </div>
</div>