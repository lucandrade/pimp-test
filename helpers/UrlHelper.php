<?php

namespace app\helpers;


class UrlHelper
{
    public static function stringToUrl($string, $separator = '-')
    {
        $url = preg_replace('~[^\p{L}\p{N}]~u', ' ', $string);
        $url = preg_replace('~ +~', $separator, trim($url));
        $url = strtolower($url);
        return $url;
    }
}