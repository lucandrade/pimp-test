<?php

use yii\db\Schema;
use yii\db\Migration;

class m151221_131133_create_table_post extends Migration
{
    public function up()
    {
        $this->createTable('{{%post}}',[
            'id'         => Schema::TYPE_PK,
            'title'      => Schema::TYPE_STRING,
            'is_authors' => Schema::TYPE_STRING,
            'is_adult'   => Schema::TYPE_STRING,
            'is_draft'   => Schema::TYPE_STRING,
            'user_id'    => Schema::TYPE_INTEGER,
            'created_at' => Schema::TYPE_DATETIME,
            'updated_at' => Schema::TYPE_DATETIME,
            'live_at'   => Schema::TYPE_DATETIME,
        ]);

    }

    public function down()
    {
        $this->dropTable('{{%post}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
