<?php

namespace app\helpers;


class StringHelper extends \yii\helpers\StringHelper
{
    public static function toFilename($string, $separator='_')
    {
        $filename = preg_replace('~[^\p{L}\p{N}\.]~u',' ',$string);
        $filename = preg_replace('/[^A-Za-z0-9\.]+/', ' ',$filename);
        $filename = preg_replace('~ +~',$separator, trim($filename));
        return $filename;
    }

    public static function filenameToUrl($filename)
    {
        $filename = html_entity_decode($filename);
        $filename = str_replace('&', ' and ', $filename);

        $filename = preg_replace('~[ \-=_]+~',' ',$filename);
        $filename = preg_replace('~[^a-z0-9 \.]~i','',$filename);
        $file = trim(pathinfo($filename, PATHINFO_FILENAME));
        $ext = strtolower(trim(pathinfo($filename, PATHINFO_EXTENSION)));
        $filename = $file.($ext?'.'.$ext:'');
        $filename = preg_replace('~ ~','-',$filename);

        return $filename;
    }

    public static function dec2any( $num, $base=62, $index=false ) {
        if (! $base ) {
            $base = strlen( $index );
        } else if (! $index ) {
            $index = substr( "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,0 ,$base );
        }
        $out = "";
        for ( $t = floor( log10( $num ) / log10( $base ) ); $t >= 0; $t-- ) {
            $a = floor( $num / pow( $base, $t ) );
            $out = $out . substr( $index, $a, 1 );
            $num = $num - ( $a * pow( $base, $t ) );
        }
        return $out;
    }


    public static function any2dec( $num, $base=62, $index=false ) {
        if (! $base ) {
            $base = strlen( $index );
        } else if (! $index ) {
            $index = substr( "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", 0, $base );
        }
        $out = 0;
        $len = strlen( $num ) - 1;
        for ( $t = 0; $t <= $len; $t++ ) {
            $out = $out + strpos( $index, substr( $num, $t, 1 ) ) * pow( $base, $len - $t );
        }
        return $out;
    }

}