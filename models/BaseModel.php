<?php

namespace app\models;

use yii\db\ActiveRecord;

class BaseModel extends ActiveRecord
{
    const STATUS_ACTIVE = 0;
    const STATUS_DELETED = 5;
    const STATUS_BANNED = 6;


    public function softDelete()
    {
        $this->status = static::STATUS_DELETED;
        return $this->save();
    }

    public function restore()
    {
        $this->status = static::STATUS_ACTIVE;
        return $this->save();
    }

    public function getIsSoftDeleted()
    {
        return $this->status == static::STATUS_DELETED;
    }

    public function isBanned()
    {
        return $this->status === self::STATUS_BANNED;
    }
}