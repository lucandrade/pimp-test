<?php

use yii\db\Migration;

class m160218_145301_change_all_user_status_to_0 extends Migration
{
    public function up()
    {
        \app\models\User::updateAll(['status'=>\app\models\User::STATUS_ACTIVE]);
    }

    public function down()
    {

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
