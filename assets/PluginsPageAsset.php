<?php

namespace app\assets;


use yii\web\AssetBundle;

class PluginsPageAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/plugins-page.css',
    ];

    public $js = [
        'js/plugins-page.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
    ];
    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}