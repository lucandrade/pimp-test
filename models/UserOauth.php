<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_oauth".
 *
 * @property integer $user_id
 * @property string $provider
 * @property string $identifier
 * @property string $profile_cache
 * @property string $session_data
 */
class UserOauth extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_oauth';
    }


    public static function possibleSocialAccounts()
    {
        $clients = \Yii::$app->authClientCollection->getClients();
        $clientIds = [];
        foreach ($clients as $client) {
            $clientIds[] = $client->id;
        }

        return $clientIds;
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'provider', 'identifier'], 'required'],
            [['user_id'], 'integer'],
            [['profile_cache', 'session_data'], 'string'],
            [['provider'], 'string', 'max' => 45],
            [['avatar_url'], 'string', 'max' => 255],
            [['identifier'], 'string', 'max' => 64],
            [['user_id', 'provider'], 'unique', 'targetAttribute' => ['user_id', 'provider'], 'message' => 'You already have this account linked']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'provider' => 'Provider',
            'identifier' => 'Identifier',
            'profile_cache' => 'Profile Cache',
            'session_data' => 'Session Data',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getAvatarUrl($size = null)
    {
        if ($this->provider == 'facebook') {
            return $this->getFacebookAvatar($size);
        }

        return $this->avatar_url;
    }

    public function getFacebookAvatar($size = null)
    {
        return "https://graph.facebook.com/{$this->identifier}/picture";
    }

}
