<?php

namespace app\components;


class AssetBundle extends \yii\web\AssetBundle
{
    public $jsFolders = [];
    public $cssFolders = [];


    public function init()
    {
        foreach ($this->jsFolders as $folder)
        {
            $fullFolder = \Yii::getAlias($this->basePath . DIRECTORY_SEPARATOR . $folder);
            $files = scandir($fullFolder);
            foreach ($files as $file) {
                $parts = explode('.', $file);
                if (end($parts) == 'js') {
                    $this->js[] = $folder . "/" . $file;
                }
            }
        }

        foreach ($this->cssFolders as $folder)
        {
            $fullFolder = \Yii::getAlias($this->basePath . DIRECTORY_SEPARATOR . $folder);
            $files = scandir($fullFolder);
            foreach($files as $file)
            {
                $parts = explode('.',$file);
                if(end($parts) == 'css'){
                    $this->css[] = $folder . "/" . $file;
                }
            }
        }

        parent::init();
    }
}