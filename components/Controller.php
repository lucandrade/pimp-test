<?php

namespace app\components;
use app\models\Tag;
use yii\web\Response;
use yii\bootstrap\ActiveForm;


class Controller extends \yii\web\Controller
{
    public $adClass = '';

    public $layout = '2-columns';

    public $title;

    private $_recentTags;

    /**
     * @return mixed
     */

    public function init()
    {
        if(empty($this->title)){
            $this->title = \Yii::$app->name . ' - Free image hosting and sharing!';
        }
        \Yii::$app->user->updateReturnUrl();
        parent::init();
    }

    protected function performAjaxValidation($model)
    {
        if(\Yii::$app->request->isAjax && isset($_POST['ajax']))
        {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        else
            return false;
    }


    public function getIsMainPage()
    {
        return $this->getRoute() == 'site/index';
    }


    public function getRecentTags()
    {
        if(!$this->_recentTags){
            $this->_recentTags = \Yii::$app->db->cache(function($db){
                return Tag::getRecent();
            });
        }
        return $this->_recentTags;
    }
}