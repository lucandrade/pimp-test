<?php

use yii\db\Schema;
use yii\db\Migration;

class m160120_070023_add_table_user_favorite_posts extends Migration
{
    public $table = 'user_favorite_post';
    public $userRef = 'user_id';
    public $postRef = 'post_id';

    public function up()
    {
        $this->createTable($this->table, [
            $this->userRef => $this->integer(11),
            $this->postRef => $this->integer(11),
        ]);
        $this->addPrimaryKey('pk_' . $this->table, $this->table, [$this->userRef, $this->postRef]);

        $this->createIndex($this->userRef . "_index", $this->table, $this->userRef);
        $this->createIndex($this->postRef. "_index", $this->table, $this->postRef);

        $this->addForeignKey($this->userRef . "_{$this->table}_fk", $this->table, $this->userRef,'user', 'id');
        $this->addForeignKey($this->postRef . "_{$this->table}_fk", $this->table, $this->postRef,'post', 'id');

    }

    public function down()
    {
        $this->dropForeignKey($this->postRef . "_{$this->table}_fk", $this->table);
        $this->dropForeignKey($this->userRef . "_{$this->table}_fk", $this->table);

        $this->dropTable($this->table);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
