<?php

namespace app\commands;


use app\models\Domain;
use app\models\DomainList;
use app\models\DomainListItem;
use app\models\User;
use app\models\UserOauth;
use yii\console\Controller;
use yii\db\Connection;
use yii\db\Query;

class DbController extends Controller
{

    public $donorDbName = 'donordb';
    public $donorDbUser;
    public $donorDbPassword;
    public $donorDbHost = 'localhost';
    private $_donorDb;


    public function options($actionID)
    {
        return ['donorDbName', 'donorDbUser', 'donorDbPassword', 'donorDbHost'];
    }

    public function actionMigrateUsers()
    {
        $this->prepareDonorDb();

        $this->importUsersWithOauth();
        
        $this->importUserTable();
    }


    private function importUserTable()
    {
        $importedUsers = 0;
        $usersBuf = (New Query())->from('User')->all($this->_donorDb);

        foreach($usersBuf as $userBuf)
        {
            $userId = $userBuf['id'];
            unset($userBuf['id']);
            $user = new User();
            $user->setAttributes($userBuf, false);
            if($user->save()){
                echo  "\r" . ++$importedUsers . " were imported from User Table";
            }else{
                echo "\nUser #$userId were not imported because of " . json_encode($user->getErrors()). "\n";
            }
        }
    }


    private function importUsersWithOauth()
    {
        $usersBuf = (New Query())->from('users')->all($this->_donorDb);
        $importedUsers = 0;
        foreach($usersBuf as $userBuf)
        {
            $user = new User();
            $userId = $userBuf['id'];
            unset($userBuf['id']);
            $user->setAttributes($userBuf, false);
            if($user->save())
            {
                echo  "\r" . ++$importedUsers . " were imported from Users Table";

                $oldOauths = UserOauth::find()->where(['user_id'=>$userId])->all($this->_donorDb);

                foreach($oldOauths as $oauthOld)
                {
                    $oauthNew = new UserOauth();
                    $oauthNew->setAttributes($oauthOld->getAttributes());
                    $oauthNew->user_id = $user->id;
                    if(!$oauthNew->save()){
                        echo "\nOauth #{$oauthOld->id} was not imported because of " . json_encode($oauthNew->getErrors()). "\n";
                    }
                }

                $oldLists = DomainList::find()->where(['authorId'=>$userId])->with('items')->all($this->_donorDb);

                foreach($oldLists as $oldList)
                {
                    $newList = new DomainList();
                    $newList->title = $oldList->title;
                    $newList->createTime = $oldList->createTime;
                    $newList->updateTime = $oldList->updateTime;
                    $newList->authorId = $user->id;
                    if($newList->save())
                    {
                        $items = $oldList->getItems()->all($this->_donorDb);
                        foreach($items as $oldItem)
                        {
                            $oldListDomain = Domain::find()->where(['id'=>$oldItem->domainId])->one($this->_donorDb);
                            if($oldListDomain)
                            {
                                $newDomain = Domain::find()->where(['domain'=>$oldListDomain->domain])->one();
                                if($newDomain)
                                {
                                    $newListItem = new DomainListItem();
                                    $newListItem->domainId = $newDomain->id;
                                    $newListItem->listId = $newList->id;
                                    if(!$newListItem->save())
                                    {
                                        echo "\nItem of list " . json_encode($newList->getAttributes()) . " was not saved because of "
                                            . json_encode($newListItem->getErrors()) . "\n";
                                    }
                                }
                                else{
                                    echo "\nCan't find domain in new database with name " . $oldListDomain->domain . "\n";
                                }
                            }
                            else{
                                echo "\nCan't find domain in donor database by id from DomainItemList table" . $oldItem->domainId . "\n";
                            }
                        }
                    }
                    else{
                        echo "\nCan't save list because of " . json_encode($newList->getErrors()) . "\n";
                    }
                }
            }else{
                echo "\nUser #$userId were not imported because of " . json_encode($user->getErrors()). "\n";
            }
        }

    }

    private function prepareDonorDb()
    {
        $db = \Yii::$app->db;
        
        $this->_donorDb = new Connection([
            'username' => !empty($this->donorDbUser) ? $this->donorDbUser : $db->username,
            'password'=> !empty($this->donorDbPassword) ? $this->donorDbPassword : $db->password,

        ]);

        $this->_donorDb->dsn = "mysql:host={$this->donorDbHost};dbname=" . $this->donorDbName;
    }
}