<?php

use app\widgets\Notifications;
use app\widgets\RecentTags;

?>

<?php echo Notifications::widget(['menuClass' => 'nav navbar-nav pull-right nav-notification visible-xs']); ?>

<div id='additional-menu-header' class='visible-xs'>
    <a href='#'>Menu <b class="caret"></b></a>
</div>
<div id='additional-menu-content'>
    <div class="menu-content-wrapper">
        <h3>Recent tags</h3>
        <?= RecentTags::widget(['tags' => $this->context->recentTags]) ?>
    </div>
</div>

