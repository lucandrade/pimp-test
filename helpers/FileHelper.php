<?php

namespace app\helpers;


class FileHelper extends \yii\helpers\FileHelper
{
    public static function replaceExtension($filename, $newExtension)
    {
        $info = pathinfo($filename);
        return $info['dirname']
        . DIRECTORY_SEPARATOR
        . $info['filename']
        . '.'
        . $newExtension;
    }

    public static function hashDir($dir)
    {
        $arr = str_split($dir);
        $result  = '';
        $result .= (isset($arr[0])?$arr[0]:'_').'/';
        $result .= (isset($arr[1])?$arr[1]:'_').'/';
        $result .= (isset($arr[2])?$arr[2]:'_').'/';
        $result .= (isset($arr[3])?$arr[3]:'_').'/';
        return $result;
    }

    public static function generateFileName($oldName, $prefix,  $length)
    {
        $ext = strtolower(pathinfo($oldName, PATHINFO_EXTENSION));
        return $prefix . '_' . md5(\Yii::$app->security->generateRandomString($length)) . '.' . $ext;
    }

    public static function getRemoteFileSize($url)
    {
        $result = -1;

        $curl = curl_init( $url );

        // Issue a HEAD request and follow any redirects.
        curl_setopt( $curl, CURLOPT_NOBODY, true );
        curl_setopt( $curl, CURLOPT_HEADER, true );
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt( $curl, CURLOPT_USERAGENT, \Yii::$app->request->getUserAgent());

        $data = curl_exec( $curl );
        curl_close( $curl );

        if( $data ) {
            $content_length = "unknown";
            $status = "unknown";

            if( preg_match( "/^HTTP\/1\.[01] (\d\d\d)/", $data, $matches ) ) {
                $status = (int)$matches[1];
            }

            if( preg_match( "/Content-Length: (\d+)/", $data, $matches ) ) {
                $content_length = (int)$matches[1];
            }

            // http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
            if( $status == 200 || ($status > 300 && $status <= 308) ) {
                $result = $content_length;
            }
        }

        return $result;
    }


    public static function humanFileSize($size,$unit="")
    {
        if( (!$unit && $size >= 1<<30) || $unit == "GB")
            return number_format($size/(1<<30),2)."GB";
        if( (!$unit && $size >= 1<<20) || $unit == "MB")
            return number_format($size/(1<<20),2)."MB";
        if( (!$unit && $size >= 1<<10) || $unit == "KB")
            return number_format($size/(1<<10),2)."KB";
        return number_format($size)." bytes";
    }

    public static function removeFile($filename)
    {
        if(file_exists($filename)){
            return unlink($filename);
        }

        return true;
    }

    public static function clearFilename($filename)
    {
        $validCharsRegex = '.A-Z0-9_ !&()+-';
        return preg_replace('/[^'.$validCharsRegex.']|\.+$/i', "_", $filename);
    }

}