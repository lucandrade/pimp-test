<?php

use yii\db\Migration;
use yii\db\Schema;

class m160506_082749_table_for_users_comments extends Migration
{

    public $table = 'user_comment';
    public $author_id = 'author_id';
    public $page_id = 'page_id';

    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable($this->table, [
            'id' => Schema::TYPE_PK,
            $this->author_id => $this->integer(11),
            $this->page_id => $this->integer(11),
            'created_at' => $this->dateTime(),
        ], $tableOptions);

        $this->createIndex($this->author_id . "_index", $this->table, $this->author_id);
        $this->createIndex($this->page_id. "_index", $this->table, $this->page_id);

        $this->addForeignKey($this->author_id . "_{$this->table}_fk", $this->table, $this->author_id, 'user', 'id');
        $this->addForeignKey($this->page_id . "_{$this->table}_fk", $this->table, $this->page_id, 'post', 'id');
        
    }

    public function down()
    {
        $this->dropForeignKey($this->postRef . "_{$this->table}_fk", $this->table);
        $this->dropForeignKey($this->userRef . "_{$this->table}_fk", $this->table);
        
        $this->dropTable($this->table);
    }
}
