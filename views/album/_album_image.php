<?php

use app\helpers\Html;
use app\helpers\HtmlPurifier;
use yii\helpers\Url;

/**
 * @var $this yii\web\View
 * @var $model \app\models\Image;
 *
 */

?>
<?= Html::inlineImageStyle($model->id, $model->getHttpFtpFileName(\app\models\Image::SIZE_SMALL_PX)) ?>

<div class="ar">
    <div class="grid-content">
    <?php
        echo Html::a("",
            Url::to(['image/show', 'id' => $model->id], true),
            [
                'class' => 'image-wrapper center-cropped im-wr image_' . $model->id,
                'data-src' => $model->getHttpFtpFileName(false, false),
                'data-filename' => $model->fileName,
                'data-size' => $model->size,
                'target' => '_blank'
        ]);
    ?>
    </div>
</div>


<?php

$disabledClass = $model->getIsProcessing() ? " icon-disabled" : "";
$disabledTitle = $model->getIsProcessing() ? " (disabled while processing)" : "";

if(empty($hoverContent)){
    $hoverContent = $model->checkAuthorAccess(false)
        ?
        '<i title="Download image' . $disabledTitle. '" class="fa fa-download' . $disabledClass . '"></i>'.
        '<i title="Remove image" class="fa fa-trash-o"></i>' .
        '<i title="Edit image' . $disabledTitle . '" data-url="'. Url::to(['image/edit', 'id'=>$model->id]).'" class="fa fa-edit' . $disabledClass . '"></i>' .
        '<i title="Share image" class="fa fa-share-alt"></i>'.
        '<i title="See full image" class="fa fa-picture-o"></i>'
        :
        '<i title="Download image" class="fa fa-download"></i>'.
        '<i title="Share image" class="fa fa-share-alt"></i>'.
        '<i title="See full image" class="fa fa-picture-o"></i>';
}
?>

<div class="hover-donor" style="display: none">
    <div class="hover-bar-content"><?= $hoverContent ?></div>

    <div class="hover-info-content">
        <div class="content-header" title="<?= $model->getOriginalName() ?>"><?= $model->getOriginalName() ?></div>
        <div class="content-info">Size <?= $model->getSizeInKb() ?> kB <?php
            $stats = $model->getViewsStatistic();
            echo \app\widgets\PopoverViewsStats::widget([
                'stats' => $stats,
                'options' => [
                    'data-placement' => 'top'
                ],
                'content'=> $stats['unique']['total']
            ])
            ?>  view<?= $stats['unique']['total'] !== 1 ? "s" : "" ?></div>
    </div>

</div>


