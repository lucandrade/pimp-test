<?php

namespace app\assets;


use app\components\AssetBundle;

class FabricjsAsset extends AssetBundle
{
	public $sourcePath = '@bower/fabric';
	public $js = [
		"dist/fabric.min.js"
	];
}
