<?php
namespace app\db\fixtures;

use yii\test\ActiveFixture;

class AlbumFixture extends ActiveFixture {

	public $modelClass = 'app\models\Album';
	public $depends = [
		'app\db\fixtures\UserFixture'
	];
}