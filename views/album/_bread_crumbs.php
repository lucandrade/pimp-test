<?php
use yii\helpers\Url;

$urlParams = \Yii::$app->user->isGuest || $album->authorId != \Yii::$app->user->id ?
    ['album/user', 'id'=>$album->authorId] : ['album/my'];

$breadcrumbsLinks = [];

if($album->uploadedByGuest()){
    if(!$album->getIsUnsorted()){
        $breadcrumbsLinks = array_merge($breadcrumbsLinks,[['label' => $album->title]]);
    }
}else
{
    $urlParams = \Yii::$app->user->isGuest || $album->authorId != \Yii::$app->user->id ?
        ['album/user', 'id'=>$album->authorId] : ['album/my'];

    $breadcrumbsLinks = array_merge($breadcrumbsLinks, [
        ['label' => $album->author->username, 'url' => $urlParams],
        ['label' => $album->title]
    ]);
}



?>
<?= \app\components\widgets\Breadcrumbs::widget([
    'links' => $breadcrumbsLinks
]) ?>