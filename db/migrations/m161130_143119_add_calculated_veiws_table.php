<?php

use yii\db\Migration;

class m161130_143119_add_calculated_veiws_table extends Migration
{
    public $table = 'calculated_views_count';

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'model_id' => $this->integer(11),
            'model_type' => $this->smallInteger(1),
            'raw' => $this->integer()->unsigned(),
            'unique' => $this->integer()->unsigned(),
            'day' => $this->date(),
        ], $tableOptions);

    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
