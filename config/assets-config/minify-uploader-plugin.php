<?php
/**
 * Configuration file for the "yii asset" console command.
 */

// In the console environment, some path aliases may not exist. Please define these:
Yii::setAlias('@webroot', dirname(dirname(__DIR__)) . '/web');
Yii::setAlias('@web', '/');

return [
    // The list of asset bundles to compress:
    'bundles' => [
        'app\assets\UploaderPluginVbullet'
    ],
    // Asset bundle for compression output:
    'targets' => [
        'uploader' => [
			'class' => 'yii\web\AssetBundle',
//            'class' => 'app\assets\CompressAsset',
            'basePath' => '@webroot/assets',
            'baseUrl' => '@web/assets',
			'css' => 'css/pah-vbullet-plugin.css',
            'js' => \app\assets\UploaderPluginVbullet::$jsTargetFilename,
        ],
    ],
    // Asset manager configuration:
    'assetManager' => [
        'basePath' => '@webroot/assets',
        'baseUrl' => '@web/assets',
    ],
];