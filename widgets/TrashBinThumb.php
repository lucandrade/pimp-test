<?php

namespace app\widgets;


use app\models\Image;
use yii\base\Widget;

class TrashBinThumb extends Widget
{
    public function run()
    {
        $count = Image::find()
            ->where(['status'=>Image::STATUS_DELETED, 'authorId'=>\Yii::$app->user->id])
            ->count('id');

        return $this->render('TrashBinThumb', compact('count'));
    }


}