<?php

namespace app\helpers;


use yii\base\InvalidConfigException;

class Html extends \yii\helpers\Html
{
    public static function userBadge($color, $options = [])
    {
        $class = "user-badge $color-user-badge";
        if(!empty($options['class'])){
            $options['class'] .= ' ' . $class;
        }else{
            $options['class'] = $class;
        }

        return self::tag("span","<i class='fa fa-star'></i>", $options);
    }


    public static function inlineImageStyle($id, $url){
        return "<style> .image_$id { background-image: url(\"$url\");}</style>";
    }



    public static function bootstarpDropdown($buttonOptions, $items, $options = [], $ulOptions = [])
    {
        if(!isset($buttonOptions['label'])){
            throw new InvalidConfigException("Field 'label' in button options must be provided");
        }
        $buttonLabel = $buttonOptions['label'];
        unset($buttonOptions['label']);

        $buttonOptions = ArrayHelper::merge([
            'data-toggle' => 'dropdown',
            'class' => 'btn btn-default dropdown-toggle'
        ], $buttonOptions);

        $ulOptions = ArrayHelper::merge([
            'class' => 'dropdown-menu',
            'encode' => false
        ], $ulOptions);

        return self::tag('div',
            self::button($buttonLabel, $buttonOptions) . self::ul($items, $ulOptions),
            $options
        );

    }
}