function commentWidget(options){

    var mainUrl = options.mainUrl || 'http://comments.loc.com:7888';
    var token;

    var selector = options.selector || "#comment-widget";
    var $container = $(selector);

    if(options.auth_type == 1){
        authv1();
    }else{
        authv2()
    }

    function authv1()
    {

        token = getCookie('atoken');


        $container.append("<div id='user-data'></div>")

        var $logoutLink = $('<a/>');
        $logoutLink.attr('id', 'logout-link').html('Logout').addClass('btn btn-primary')
        $logoutLink.hide();
        $container.append($logoutLink);

        var $loginLink = $('<a/>');
        $loginLink.attr('id', 'login-link').html('login').addClass('btn btn-primary');
        $loginLink.hide();
        $container.append($loginLink);

        if(token != ''){
            getUser(token);
            return;
        }

        $loginLink.show();

        var clientId = options.widget_id;

        $('body').on('click', '#login-link', function(e){

            e.preventDefault();

            var authChoicePopup = $container.data('authChoicePopup');

            if (authChoicePopup) {
                authChoicePopup.close();
            }

            var url = mainUrl+"/auth/auth?response_type=token&client_id="+clientId+"&redirect_uri="+window.location.href;
            var redirectUrl = mainUrl+"/auth/auth?response_type=token&client_id="+clientId+"&redirect_uri="+window.location.href;
            var popupOptions = $.extend({}, options.popup); // clone

            var localPopupWidth = 700;
            if (localPopupWidth) {
                popupOptions.width = localPopupWidth;
            }
            var localPopupHeight = 500;
            if (localPopupWidth) {
                popupOptions.height = localPopupHeight;
            }

            popupOptions.left = (window.screen.width - popupOptions.width) / 2;
            popupOptions.top = (window.screen.height - popupOptions.height) / 2;

            var popupFeatureParts = [];
            for (var propName in popupOptions) {
                if (popupOptions.hasOwnProperty(propName)) {
                    popupFeatureParts.push(propName + '=' + popupOptions[propName]);
                }
            }
            var popupFeature = popupFeatureParts.join(',');

            authChoicePopup = window.open(window.location.href, 'yii_auth_choice', popupFeature);
            authChoicePopup.focus();
            authChoicePopup.location.href = redirectUrl;

            window.addEventListener("message", function(event)
            {
                token = event.data;
                setCookie('atoken',token,2);
                authChoicePopup.close();
                getUser(token);

            }, false);


            $container.data('authChoicePopup', authChoicePopup);
        });

    };

    function authv2(){
        $.ajax({
            url: mainUrl + "/client-auth/init",
            data:{auth:options.auth, widget_id:options.widget_id, parent_id:options.parent_id},
            dataType:"json",
            type:"POST",
            success:function(data){
                console.log(data);
                if(data.result == 'error'){
                    $container.html("<h2>"+ data.message +"</h2>")
                    return;
                }

                if(data.user){
                    var str = JSON.stringify(data.user);
                    $container.append("<div>User registered, data:"+str+"</div>");
                }else{
                    $container.append("<div>User is not registred</div>");
                }

                renderComments(data.comments);
            }
        });
    }

    var renderForm = function()
    {
        //var $form = $("<form/>");
        //$form.data('user', userId)
        //$form.append("<textarea id='comment-text' placeholder='enter comment here'/>")
        //$form.append("<button id='add-comment-btn'>Submit</button>");
        //$container.append($form);
    };

    var renderComments = function(comments)
    {
        var $commentsContainer = $container.append("<div/>");
        $commentsContainer.attr('id', 'comments-container');
        $.each(comments, function(){
            renderComment(this);
        });
    };


    var renderComment = function(comment){

        var $comment = $('#comments-container').append("<div/>");
        $comment.addClass('comment-container');
        $comment.append("<div><i>" + comment.author.username + ":</i></div>");
        $comment.append("<div>" + comment.text + "</div>");

        return $comment;
    };

    $('body').on('click', '#add-comment-btn', function(){

        $.ajax({
            url: mainUrl + "/client-auth/add-comment",
            data:{comment:$('#comment-text').val(), parent_id:options.parent_id, auth:options.auth},
            type:"POST",
            dataType:"json",
            success:function(data){
                if(data.result=='ok'){
                    renderComment(data.comment);
                }
            }

        });
    });


    function getUser(token)
    {
        $.ajax({
            url:mainUrl + "/api/get-user?access_token="+token,
            dataType:"json",
            type:"POST",
            success:function(data){
                var str = JSON.stringify(data);
                $('#user-data').html("User data:" + str);
                $('#login-link').hide();
                $('#logout-link').show();
            }
        });
    }


    $('body').on('click', '#logout-link', function(){
        $('#user-data').html('');
        deleteCookie('atoken');
        $('#login-link').show();
        $(this).hide();
    });

    function setCookie(cname, cvalue, exdays)
    {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    }


    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
        }
        return "";
    }

    function deleteCookie(cname)
    {
        setCookie(cname, "",0);
    }

}