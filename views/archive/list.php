<?php
use app\models\Archive;
?>
<div>
    <?php foreach($archives as $archive):?>
    <li><?php echo $archive->title?>:
        <?php if($archive->status == Archive::STATUS_ARCHIVED):?>
            <a href="<?= $archive->getDownloadLink() ?>">
                <?php echo $archive->imageCount?> images,
                <?php echo \app\helpers\FileHelper::humanFileSize($archive->size)?>,
                expires <?= \Yii::$app->formatter->asRelativeTime(strtotime($archive->createTime) + Archive::EXPIRE_PERIOD) ?>
                download
            </a>
        <?php endif?>
        <?php if($archive->status == Archive::STATUS_ERROR):?>
            Error!
        <?php elseif($archive->status != Archive::STATUS_ARCHIVED):?>
            In que
        <?php endif?>
    <?php endforeach?>
</div>