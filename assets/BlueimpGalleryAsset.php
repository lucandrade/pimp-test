<?php

namespace app\assets;

use yii\web\AssetBundle;

class BlueimpGalleryAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/plugins/blueimp-gallery.min.css'
    ];

    public $js = [
        'js/plugins/imageGallery/blueimp-gallery.js',
        'js/plugins/imageGallery/blueimp-gallery-indicator.js',
        'js/plugins/imageGallery/blueimp-gallery-prepend.js'
    ];

    public $depends = [
    ];

    public $jsOptions = [
    ];

    public $cssOptions = [
    ];
}