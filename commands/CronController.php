<?php

namespace app\commands;

use app\helpers\CommentsService;
use app\models\CalculatedViewsCount;
use app\models\UserComment;
use Yii;
use yii\base\Exception;
use yii\console\Controller;
use yii\db\Expression;
use yii\helpers\Console;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use app\helpers\CronHelper;
use app\models\Post;
use app\models\User;
use app\models\Statistic;
use app\models\Notification;
use app\models\PostBlock;

class CronController extends Controller
{
    public function getExternalData($url, $params = [])
    {
        $jsonData = Json::encode($params);
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS => 10,     // stop after 10 redirects
            CURLOPT_ENCODING => "",     // handle compressed
            CURLOPT_USERAGENT => "", // name of client
            CURLOPT_AUTOREFERER => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT => 120,    // time-out on response
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $jsonData,
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($jsonData)
            ]

        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);

        $content = curl_exec($ch);

        curl_close($ch);
        return Json::decode($content);
    }

    public function log($data, $lastTime = null)
    {
        $content = [];
        if ($lastTime) {
            $content[] = 'Last time: ' . date('y-m-d H:i:s', $lastTime);
        }
        $content[] = Json::encode($data);
        $content[] = 'Date: ' . date('y-m-d H:i:s');
        $content[] = '';
        $content[] = str_repeat('=', 32);
        $content[] = '';

        $file = Yii::getAlias(CronHelper::$pathAlias) . DIRECTORY_SEPARATOR . 'run.log';
        FileHelper::createDirectory(pathinfo($file, PATHINFO_DIRNAME));
        file_put_contents($file, implode(PHP_EOL, $content), FILE_APPEND);

    }

    public function groupVotesRating($votes)
    {
        $group = [];
        foreach ($votes as $vote) {
            $key = $vote['key'];
            if (!isset($group[$key])) {
                $authorId = null;

                if(isset($vote['comment_author_id'])){
                    $authorId = $vote['comment_author_id'];
                }elseif(isset($vote['author_id'])){
                    $authorId = $vote['author_id'];
                }

                $group[$key] = [
                    'key' => $key,
                    'author_id' => $authorId,
                    'rating' => 0
                ];
            }
            $group[$key]['rating'] += $vote['value'];
        }
        return $group;
    }

    public function updateRating($votes)
    {
        $processed = ['byComments' => 0, 'posts' => 0];
        $groupVotes = $this->groupVotesRating($votes);
        foreach ($groupVotes as $vote) {
            if (strpos($vote['key'], 'comment_') === 0) {
                $author = User::findOne(['id' => $vote['author_id']]);
                $commentRating = $vote['rating'] * 0.5;
                if ($author !== null && $author->updateCounters(['rating' => $commentRating])) {
                    $processed['byComments']++;
                }
            } else  {
                list($model, $type) = CommentsService::getModelByKey($vote['key']);
                if ($model && $model->updateRating($vote['rating'])) {
                    $alias = CommentsService::getAliasByType($type) . "s";
                    if(!isset($processed[$alias])){
                        $processed[$alias] = 0;
                    }
                    $processed[$alias]++;
                }
            }
        }
        return $processed;
    }

    public function updateNotification($comments, $votes)
    {
        $processed = ['commented' => 0, 'upvoted' => 0, 'downvoted' => 0];
        foreach ($comments as $comment) {
            list($model, $parentModelType) = CommentsService::getModelByKey($comment['page_key']);;
            if ($model) {
                if (empty($comment['parent_id'])) {
                    $notification = new Notification([
                        'owner_id' => $model->getOwnerId(),
                        'type' => Notification::TYPE_POST_COMMENTS,
                        'type_id' => $comment['page_key'],
                        'text' => '#comment_' . $comment['id'],
                        'created_at' => new Expression('FROM_UNIXTIME(:created_at)', [':created_at' => $comment['created_at']]),
                        'caller_id' => $comment['comment_author_id'],
                        'parent_model_type' => $parentModelType
                    ]);
                    if ($notification->save()) {
                        $processed['commented']++;
                    } else {
                        $this->log($notification->getErrors());
                    }
                }
                if (isset($comment['at_users_id'])) {
                    foreach ($comment['at_users_id'] as $at_user_id) {
                        $notification = new Notification([
                            'owner_id' => $at_user_id,
                            'type' => Notification::TYPE_COMMENTS_AT,
                            'type_id' => $comment['page_key'],
                            'text' => '#comment_' . $comment['id'],
                            'created_at' => new Expression('FROM_UNIXTIME(:created_at)', [':created_at' => $comment['created_at']]),
                            'caller_id' => $comment['comment_author_id'],
                            'parent_model_type' => CommentsService::MODEL_TYPE_COMMENT
                        ]);
                        if ($notification->save()) {
                            $processed['commented']++;
                        } else {
                            $this->log($notification->getErrors());
                        }
                    }
                }
                if (isset($comment['reply_user_id'])) {
                    $notification = new Notification([
                        'owner_id' => $comment['reply_user_id'],
                        'type' => Notification::TYPE_COMMENTS_REPLY,
                        'type_id' => $comment['page_key'],
                        'text' => '#comment_' . $comment['id'],
                        'created_at' => new Expression('FROM_UNIXTIME(:created_at)', [':created_at' => $comment['created_at']]),
                        'caller_id' => $comment['comment_author_id'],
                        'parent_model_type' => CommentsService::MODEL_TYPE_COMMENT
                    ]);
                    if ($notification->save()) {
                        $processed['commented']++;
                    } else {
                        $this->log($notification->getErrors());
                    }
                }
            }
        }
        foreach ($votes as $vote) {
            if ((int)$vote['value'] !== 0)
            {
                list($model, $parentModelType) = CommentsService::getModelByKey($vote['key']);;
                if($model)
                {

                    $notification = new Notification([
                        'owner_id' => $model->getOwnerId(),
                        'type' => $vote['value'] > 0 ? Notification::TYPE_POST_UPVOTED : Notification::TYPE_POST_DOWNVOTED,
                        'type_id' => $vote['key'],
                        'created_at' => new Expression('FROM_UNIXTIME(:created_at)', [':created_at' => $vote['created_at']]),
                        'caller_id' => isset($vote['author_id']) ? $vote['author_id'] : null,
                        'parent_model_type' => $parentModelType
                    ]);
                    if ($notification->save()) {
                        if ((int)$notification->type === Notification::TYPE_POST_UPVOTED) {
                            $processed['upvoted']++;
                        } else {
                            $processed['downvoted']++;
                        }
                    } else {
                        $this->log($notification->getErrors());
                    }
                }
            }
        }
        return $processed;
    }

    public function actionUpdateNotification()
    {
        if (isset(Yii::$app->params['commentsHost']) && !empty(Yii::$app->params['commentsHost'])) {

            $updateUrl = 'http://' . Yii::$app->params['commentsHost'] . '/v1/vote/notification';

            CronHelper::$fileName = 'notification_time.log';
            $lastTime = CronHelper::getLastTime(strtotime('-7 day'));

            /**
             * @var $collection \yii\authclient\Collection;
             */
            $collection = Yii::$app->authAdminCollection;
            /**
             * @var $comments \app\components\authclients\Comments;
             */
            $comments = $collection->getClient('comments');
            $status = $comments->getFileTokenStatus();

            if (strtolower($status) !== 'ok') {
                die("\n Token is expired, please renew it for uploading!");
//                echo $this->ansiFormat("\n Token is expired, please renew it for uploading!", Console::FG_RED);
            }

            $token = $comments->getTokenFromFile();

            $data = $this->getExternalData($updateUrl, [
                'token' => isset($token['access_token']) ? $token['access_token'] : null,
                'lastTime' => $lastTime
            ]);

            echo $this->ansiFormat("\nGot from '$updateUrl' after ". date("Y-m-d H:i:s",$lastTime) .":" . json_encode($data));

            if (isset($data['status']) && $data['status'] === 'success') {
                echo $this->ansiFormat("\nUpdating rating... from " . json_encode($data['votes']));
                $processedRating = $this->updateRating($data['votes']);
                echo $this->ansiFormat("   results:" . json_encode($processedRating), Console::FG_PURPLE);

//                $processedRating = [];
                echo $this->ansiFormat("\nUpdating notification... from " . json_encode($data['comments']) . json_encode($data['votes']));
                $processedNotification = $this->updateNotification($data['comments'], $data['votes']);
                echo $this->ansiFormat("   results:" . json_encode($processedNotification), Console::FG_PURPLE);

                $this->log($processedRating + $processedNotification, $lastTime);
            } else {

                echo $this->ansiFormat("Wrong data:" . json_encode($data), Console::FG_RED);
                $this->log($data, $lastTime);
            }
        }
        echo "\n";
    }

    public function actionUpdateRating()
    {
        if (isset(Yii::$app->params['commentsHost']) && !empty(Yii::$app->params['commentsHost'])) {

            $updateUrl = 'http://' . Yii::$app->params['commentsHost'] . '/v1/vote/stats';

            $lastTime = CronHelper::getLastTime(strtotime('-7 day'));

            /**
             * @var $collection \yii\authclient\Collection;
             */
            $collection = Yii::$app->authAdminCollection;
            /**
             * @var $comments \app\components\authclients\Comments;
             */
            $comments = $collection->getClient('comments');
            $status = $comments->getFileTokenStatus();

            if (strtolower($status) !== 'ok') {
                die("\n Token is expired, please renew it for uploading!");
                echo $this->ansiFormat("\n Token is expired, please renew it for uploading!", Console::FG_RED);
            }

            $token = $comments->getTokenFromFile();

            $data = $this->getExternalData($updateUrl, [
                'token' => isset($token['access_token']) ? $token['access_token'] : null,
                'lastTime' => $lastTime
            ]);

            $processed = ['byComments' => 0, 'posts' => 0];

            if (isset($data['status']) && $data['status'] === 'success') {
                foreach ($data['votes'] as $vote) {
                    if (strpos($vote['key'], 'comment_') === 0) {

                        $author = User::findOne(['id' => $vote['author_id']]);
                        $commentRating = $vote['rating'] * 0.5;

                        if ($author->updateCounters(['rating' => $commentRating])) {
                            $processed['byComments']++;
                        }

                    } else if (strpos($vote['key'], 'page_') === 0) {

                        $post_id = preg_replace('/^page_/', '', $vote['key']);
                        $post = Post::findOne(['id' => $post_id]);

                        if ($post !== null) {
                            if ($post->updateCounters(['rating' => $vote['rating']])) {
                                $processed['posts']++;
                                if ($post->author) {
                                    $post->author->updateCounters(['rating' => $vote['rating']]);
                                }
                            }
                        }

                    }
                }
            }

            $this->log($processed, $lastTime);
        }
    }

    public function actionUpdateStatistic()
    {
        if (isset(Yii::$app->params['commentsHost']) && !empty(Yii::$app->params['commentsHost'])) {

            $statisticUrl = 'http://' . Yii::$app->params['commentsHost'] . '/v1/widget/stats';

            /**
             * @var $collection \yii\authclient\Collection;
             */
            $collection = Yii::$app->authAdminCollection;
            /**
             * @var $comments \app\components\authclients\Comments;
             */
            $comments = $collection->getClient('comments');
            $status = $comments->getFileTokenStatus();

            if (strtolower($status) !== 'ok') {
                die("\n Token is expired, please renew it for uploading!");
//                echo $this->ansiFormat("\n Token is expired, please renew it for uploading!", Console::FG_RED);
            }

            $token = $comments->getTokenFromFile();

            $data = $this->getExternalData($statisticUrl, [
                'token' => isset($token['access_token']) ? $token['access_token'] : null,
            ]);

            if (isset($data['status']) && $data['status'] === 'success') {

                $commentsStat = Statistic::getCurrentStatistic(Statistic::TYPE_COMMENTS);
                $commentsStat->value = $data['comments'];
                $commentsStat->save();

                $votesUpStat = Statistic::getCurrentStatistic(Statistic::TYPE_VOTES_UP);
                $votesUpStat->value = $data['votes_up'];
                $votesUpStat->save();

                $votesDownStat = Statistic::getCurrentStatistic(Statistic::TYPE_VOTES_DOWN);
                $votesDownStat->value = $data['votes_down'];
                $votesDownStat->save();
            }

            $this->log($data);
        }

        Statistic::updateUserStatistic();
    }

    public function actionUpdateEmptyPosts()
    {
        $posts = Post::find()
            ->where([Post::tableName() . '.status' => Post::STATUS_EMPTY_BLOCK])
            ->joinWith([
                'postBlocks' => function ($query) {
                    $query->andWhere(['type' => PostBlock::BLOCK_TYPE_VIDEO])
                        ->andWhere('video_url != "" AND video_url IS NOT NULL');
                }
            ])
            ->andWhere(['>', 'live_at', new Expression('NOW() - INTERVAL 1 DAY')])
            ->all();

        foreach ($posts as $post) {
            foreach ($post->postBlocks as $postBlock) {
                if (!empty($postBlock->videoImage)) {
                    $postBlock->saveVideoThumbnail();
                    $this->log(['update-empty-preview' => $postBlock->id]);
                }
            }

            $post->refresh();

            if (!$post->isPostPreviewEmpty()) {
                $post->status = Post::STATUS_ACTIVE;
                $post->update(true, ['status']);
                $this->log(['update-empty-post' => $post->id]);
            }
        }
    }


    public function actionUpdateUserComments()
    {
        if (isset(Yii::$app->params['commentsHost']) && !empty(Yii::$app->params['commentsHost'])) {

            $updateUrl = 'http://' . Yii::$app->params['commentsHost'] . '/v1/comments/user-list';

            CronHelper::$fileName = 'user_comment_time.log';
            $lastTime = CronHelper::getLastTime(strtotime('-7 day'));
//            $lastTime = strtotime('-30 day');

            /**
             * @var $collection \yii\authclient\Collection;
             */
            $collection = Yii::$app->authAdminCollection;
            /**
             * @var $comments \app\components\authclients\Comments;
             */
            $comments = $collection->getClient('comments');
            $status = $comments->getFileTokenStatus();

            if (strtolower($status) !== 'ok') {
                die("\n Token is expired, please renew it for uploading!");
//                echo $this->ansiFormat("\n Token is expired, please renew it for uploading!", Console::FG_RED);
            }

            $token = $comments->getTokenFromFile();

            $data = $this->getExternalData($updateUrl, [
                'token' => isset($token['access_token']) ? $token['access_token'] : null,
                'lastTime' => $lastTime
            ]);
            if (isset($data['status']) && $data['status'] === 'ok') {
                $processed = $this->updateComment($data['comments']);
                $this->log($processed, $lastTime);
            } else {
                $this->log($data, $lastTime);
            }
        }

    }



    public function actionUpdateViews()
    {
        $url  = 'http://' . Yii::$app->params['commentsHost'] . '/v1/view/stats-per-page';

        $collection = Yii::$app->authAdminCollection;
        /**
         * @var $comments \app\components\authclients\Comments;
         */
        $comments = $collection->getClient('comments');
        $status = $comments->getFileTokenStatus();

        if (strtolower($status) !== 'ok') {
            die("\n Token is expired, please renew it for uploading!");
        }

        $token = $comments->getTokenFromFile();

        $data = $this->getExternalData($url, [
            'token' => isset($token['access_token']) ? $token['access_token'] : null,
            'day' => date('Y-m-d')
        ]);

        if (isset($data['status']) && $data['status'] === 'ok')
        {
            list($processed, $unprocessed) = CalculatedViewsCount::processData($data['data']);
            echo $this->ansiFormat(count($processed) . " records from request were successfully processed");
            if(!empty($unprocessed)){
                echo $this->ansiFormat("Errors:" . json_encode($unprocessed), Console::FG_RED);
                $this->log($unprocessed);
            }
        }
        else {
            $this->log($data);
            throw new Exception("Got wrong data:" . json_encode($data));
        }
    }

    public function updateComment($data)
    {
        $save = true;
        foreach ($data as $item) {
            $user_comments = new UserComment([
                'author_id' => $item['author_id'],
                'page_id' => $item['page_id'],
                'created_at' => new Expression('FROM_UNIXTIME(:created_at)', [':created_at' => $item['created_at']]),
            ]);
            $save = $user_comments->save() && $save;
        }
        return $save;
    }
}
