function popupUploader(options){

    this.settings = $.extend(true, {}, options);

    var $container = $('#' + this.settings.modalId);

    $container.on('changefpu', '.upload-control-container', function(){
        $(this).parents('.modal').modal('hide');
    });

    this.show = function(){
        $container = $('#' + this.settings.modalId);
        $container.fullPahUploader('reInit');
        $container.modal('show');
    }
};