<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\models\MainSearchForm;

?>

<?php
$model = new MainSearchForm();

$form = ActiveForm::begin([
    'id' =>  "search-form",
    'method' => 'get',
    'successCssClass' => false,
    'action' => Url::to(['site/search', 'cat' => 'ew', 'q' => 'keyword', ]),
    'options' => ['class' => 'search-form navbar-form']
]);

$categories = MainSearchForm::$searchCategories;
if(\Yii::$app->user->isGuest){
    unset($categories['my']);
}
?>

<div class="form-group">
    <div class="input-group">
        <?=
        Html::activeTextInput($model, 'q', [
            'placeholder' => 'Search...',
            'class' => 'form-control',
            'name' => ''
        ]);
        ?>
        <span class="input-group-btn">
            <div class="select-wrapper">
                <?= Html::activeDropDownList($model, 'cat', $categories, [
                    'class' => 'form-control  select-control',
                    'name' => ''
                ]) ?>
                <i id="select-box-arrow" class="fa fa-angle-down"></i>
            </div>
            <button class="btn btn-default btn-search" type="button">
                <span class="glyphicon glyphicon-search"></span>
            </button>
        </span>
    </div>
</div>

<?php ActiveForm::end(); ?>
