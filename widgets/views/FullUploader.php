<?php

use yii\helpers\Url;
use app\helpers\Html;

/**
 * @var $this \yii\web\View;
 *
 * @var $widget FullUploader;
 */

$widget = $this->context;
?>

<div id="<?= $widget->containerId ?>" class="full-uploader-container">
    <div class="upload-control-container text-center">
        <?php  unset($widget->uploaderOptions['js'])?>
        <h3 class="dnd-title">
            Drag & Drop files here to share
        </h3>
        <h3 class="dnd-title-or">
            OR
        </h3>
        <?= \app\widgets\Uploader::widget($widget->uploaderOptions); ?>
        <div class="upload-description">
                    <a href="#" class="show-url-upload disabled-link">or Upload from URL</a>
                    <div class="url-input-group" style="display:none">
                        <textarea name="image-urls" id="image-urls" class="image-urls" rows="5" placeholder="Put urls here..."></textarea>
                        <p class="help-block help-block-error"></p>
                    <div>
                    <button class="btn btn-primary url-upload-btn" data-url="<?= Url::to(['image/upload-by-url']) ?>">Submit</button>
                </div>
            </div>
            <div id="upload-info">
                <div>
                    File types allowed: GIF, JPG, BMP
                </div>
                <div>
                    Maximum upload file size: 5,000KB (MB)
                </div>
            </div>
        </div>
    </div>
</div>



