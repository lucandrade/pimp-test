<?php

namespace app\assets;


use yii\web\AssetBundle;

class TrashBinAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
    ];

    public $js = [
        'js/trash-bin.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
        'app\assets\BootboxAsset',
        'app\assets\ShareImagesModalAsset',
        'app\assets\SelectionBarAsset',
        'app\assets\ImageListAsset',
    ];

    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}