<?php
use yii\widgets\ActiveForm;
use app\helpers\Html;
\app\assets\AlbumAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'id' => 'album-password',
    'options' => ['autocomplete' => 'off']
]);
    ?>
    <div class="top-bar">
        <?= $this->render('_bread_crumbs', ['album' => $album]) ?>
    </div>
    <div class="panel panel-default">

        <div class="panel-body">

            <h4>Album '<?= $album->title ?>' is protected with password</h4>

            <?= $form->field($authForm, 'password')->passwordInput(['autocomplete'=>'off']) ?>

            <div class="form-group text-center">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>