<?php

namespace app\controllers;

use app\models\Album;
use app\models\Image;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\components\filters\JsonResponse;
use app\components\Controller;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class CommonController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['edit-title'],
                        'roles' => ['@']
                    ],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'edit-title' => ['post'],
                ],
            ],
            'jsonResponse' => [
                'class' => JsonResponse::className(),
                'only' => ['edit-title']
            ]
        ];
    }


    public function actionEditTitle()
    {
        $className = \Yii::$app->request->post('className');
        $id = \Yii::$app->request->post('pk');
        $value = \Yii::$app->request->post('value');

        if(!isset($className, $id, $value)){
            throw new BadRequestHttpException("pk, value and className params must be set");
        }

        if(!in_array($className, [Album::className(), Image::className()])){
            throw new BadRequestHttpException("Wrong value for className");
        }

        $model = $className::findOne($id);

        if(!$model){
            throw new NotFoundHttpException("Instance of '$className' with id '$id' not found");
        }

        if(!$model->checkAuthorAccess()){
            throw new ForbiddenHttpException("You haven't access to this item");
        }

        $model->title = $value;
        if(!$model->save()){
            \Yii::$app->response->setStatusCode(501);
            return $model->getErrors('title')[0];
        }

        return "";
    }


}