<?php

namespace app\controllers;

use app\controllers\actions\AjaxCaptchaAction;
use app\components\Controller;
use app\models\Image;
use app\models\MainSearchForm;
use app\models\User;
use app\models\PasswordResetRequestForm;
use Yii;
use yii\filters\AccessControl;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SignupForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\HttpException;
use app\components\SphinxDataProvider;
use app\models\Post;
use yii\web\Response;
use app\models\UserSettings;


class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => AjaxCaptchaAction::className(),
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }

    public function actionIndex()
    {
        $this->layout = '1-column';
        return $this->render('index');
    }


    public function actionSearch()
    {
        $searchForm = new MainSearchForm();
        $searchForm->prepareQuery();

        $dataProviderConfig = [
            'sphinxClient' => $searchForm->sphinxClient,
            'primaryModelKey' => 'id',
            'sphinxQuery' => $searchForm->sphinxQuery,

        ];

        $view = 'search_images';

        if($searchForm->getIsImagesRequest())
        {
            $dataProviderConfig['query'] =  Image::find()
                ->andWhere('status=:normal OR status=:processing', [':normal'=>Image::STATUS_NORMAL, ':processing'=>Image::STATUS_PROCESSING]);

            $pageSize = \Yii::$app->params['imagesPerPage'];

            $dataProviderConfig['indexName'] = !empty(Yii::$app->params['indexes']['main'])
                ? Yii::$app->params['indexes']['main'] : 'pah_main_search pah_main_search_delta';

        }else{
            $view = 'search_posts';

            $pageSize = \Yii::$app->params['itemsPerPage'];

            $dataProviderConfig['query'] = Post::find()->from('post p')
                ->with(['postBlocks', 'tags', 'author']);

            $dataProviderConfig['sortingAttributes'] = [
                'live_at' => 'ts_live_at',
            ];

            $dataProviderConfig['sort'] = [
                'defaultOrder' => ['live_at' => SORT_DESC]
            ];

            $dataProviderConfig['indexName'] =  !empty(Yii::$app->params['indexes']['posts'])
                ? Yii::$app->params['indexes']['posts'] : 'pah_posts_search pah_posts_search_delta';

        }

        $dataProviderConfig['pagination'] = [
            'userSettingName' => UserSettings::NAME_ALBUMS_PAGE_SIZE,
            'pageSize' => $pageSize,
            'defaultPageSize' => $pageSize,

        ];

        $dataProvider = new SphinxDataProvider($dataProviderConfig);

        return $this->render($view, ['searchForm' => $searchForm, 'dataProvider' => $dataProvider]);
    }

    public function onAuthSuccess($client)
    {
        $attributes = $client->getUserAttributes();
        $provider = $client->getId();

        if (\Yii::$app->user->isGuest)
        {
            $user = User::getByOauth($attributes, $provider);
            if (!$user->hasErrors() && empty($user->getOauthErrors()))
            {
                if($user->getIsActive()){
                    if(Yii::$app->user->login($user, 5 * 24 * 60 * 60)){
                        Yii::$app->session->setFlash('success', 'Welcome to '. \Yii::$app->name .'!');
                    }else{
                        Yii::$app->session->setFlash('error', "We couldn't login you for some reason");
                    }
                }else{
                    Yii::$app->session->setFlash('danger', 'Your account is not active!');
                }
            } else
            {
                if(!empty($user->getOauthErrors())){
                    $errors['oauth_errors'] = $user->getOauthErrors();
                }
                if($user->hasErrors()){
                    $errors['user'] = $user->getErrors();
                }

                $errors = json_encode($errors);
                Yii::$app->session->setFlash('error', "We could not login you because of $errors");
            }
        } else {
            $attributes = User::normalizeAttributes($attributes, $provider);
            if (Yii::$app->user->identity->addSocialAccount($attributes, $provider)) {
                Yii::$app->session->setFlash('success', "You successfully linked your " . ucfirst($provider) . " account");
            } else {
                $message = Yii::$app->user->identity->getOauthErrors('endUserMessage')
                    ? Yii::$app->user->identity->getOauthErrors('endUserMessage')
                    : "We couldn't link you account for some reason";

                Yii::$app->session->setFlash('warning', $message);
            }
        }
    }

    public function actionReset()
    {
        $this->layout = 'clean';

        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $user = User::findOne(
                [
                    'email' => $model->email,
                ]
            );
            $user->generatePasswordResetToken();
            if ($user->save() && $model->sendEmail($user->password_reset_token)) {
                \Yii::$app->session->setFlash('success', 'Check your email for further instructions');
                return $this->goHome();
            }
        }

        return $this->render('reset', compact('model'));
    }


    public function actionLogin()
    {
        $this->layout = 'clean';

        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $resetPasswordModel = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            if (!\Yii::$app->user->identity->getIsEmailActivated()) {
                \Yii::$app->session->setFlash('warning', 'Your email is not activated');
            }

            return $this->redirect(\Yii::$app->user->getFiltredReturnUrl());
        }

        return $this->render('login', compact('model', 'resetPasswordModel'));
    }

    public function actionSignup()
    {
        $this->layout = 'clean';

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {

            $validations = $this->performAjaxValidation($model);
            if ($validations !== false)
                return $validations;

            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {

                    $this->sendActivationEmail($user);

                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', compact('model'));
    }

    protected function sendActivationEmail(User $user)
    {

        $link = Url::toRoute(['site/activate', 'code' => $user->getEmailKey()], true);

        $to = $user->email;
        $subject = "Activate your account";
        $message = "Use this link to activate your email: " . Html::a('Activate', $link);
        // todo - get addres and subject from config
        $headers = 'From: Welcome to ' . \Yii::$app->name . ' <system@' . \Yii::$app->name . '>' . "\r\n" .
            'X-Mailer: PHP/' . phpversion() . "\r\n";

        $headers .= 'Content-type: text/html; charset=utf-8';

        $res = mail($to, $subject, $message, $headers);

        if ($res) {
            \Yii::$app->session->setFlash('success', 'We sent you activation email');
        } else {
            \Yii::$app->session->setFlash('error', 'We were unable to send you activation email, please try to activate you accont later');
        }

        return $res;
    }

    public function actionActivate($code)
    {
        $user = User::findIdentityByEmailKey($code);

        if (empty($user)) {
            \Yii::$app->session->setFlash('error', 'Wrong activation link');

            return $this->goHome();
        }

        if ($user->is_activated_email) {
            \Yii::$app->session->setFlash('success', 'You have already activated your email');
        } else {
            if ($user->activateEmail()) {
                \Yii::$app->session->setFlash('success', 'You successfully activated your email. Thank you.');
            } else {
                \Yii::$app->session->setFlash('error', "We couldn't activate your email for some reason. Please try again later.");
            }
        }

        return $this->goHome();
    }

    public function actionLogout()
    {
        $url = \Yii::$app->user->getFiltredReturnUrl();
        Yii::$app->user->logout();
        return $this->redirect($url);
    }

    public function actionContact($adv = null)
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted', 'Thank you for contacting us. We will respond to you as soon as possible.');
            return $this->refresh();
        }

        if ($adv !== null) {
            $model->subject = 'Advertising';
        }
        return $this->render('contact', compact('model'));
    }

    public function actionPrivacy()
    {
        return $this->render('privacy');
    }

    public function actionAbuse()
    {
        return $this->render('abuse');
    }

    public function actionTos()
    {
        return $this->render('tos');
    }

    public function actionFaq()
    {
        return $this->render('faq');
    }

    public function actionLogFile($f)
    {
        return file_get_contents(\Yii::getAlias("@runtime/logs/$f"));
    }

    public function actionTest($id)
    {
        /** @var User $user */
        if (!$user = User::findOne($id)) {
            throw new HttpException(404, 'User not found');
        }
        Yii::$app->user->login($user);
        return $this->goHome();
//		return $this->render('test',['postId'=>1]);
    }

    public function actionTestPost($id, $type)
    {
        return $this->render('test', ['postId' => $id, 'type' => $type]);
    }


    public function actionSitemap()
    {
        \Yii::$app->response->format = Response::FORMAT_RAW;
        $headers = \Yii::$app->response->headers;
        $headers->add('Content-Type', 'text/xml; charset=utf-8');

        $file = \Yii::getAlias("@app/web/static/sitemap/sitemap_index.xml");
        $sitemap = file_exists($file) ? file_get_contents($file) : "<err>sitemap is not generated</err>";

        return $sitemap;
    }

}