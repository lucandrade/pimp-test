<?php

namespace app\models;

use app\models\interfaces\CommentsServiceModel;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\Transaction;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use app\helpers\VideoHelper;
use app\helpers\ArrayHelper;
use app\helpers\ImageHelper;
use app\helpers\UrlHelper;
use app\models\behaviors\Rating;

/**
 * Class Post
 *
 * @property int $id;
 * @property string $title;
 * @property int $user_id;
 * @property bool $is_authors;
 * @property bool $is_adult;
 * @property bool $is_draft;
 * @property string $created_at;
 * @property string $updated_at;
 * @property string $live_at;
 * @property int $rating;
 * @property int $status;
 *
 * @property \app\models\PostBlock[] $postBlocks;
 * @property \app\models\Tag[] $tags;
 *
 * @package app\models
 */
class Post extends BaseModel implements CommentsServiceModel
{
    const STATUS_PROCESSING = 8;
    const STATUS_EMPTY_BLOCK = 9;
    const SCENARIO_PUBLISHING = 'publishing';

    /**
     * @var array linked tags ids;
     */
    public $tagIds = [];
    /**
     * @var array loaded images sources;
     */
    public $loadedImages = [];
    /**
     * @var array loaded video sources;
     */
    public $loadedVideos = [];
    /**
     * @var mixed custom property handler for postBlocks;
     */
    public $postBlock;
    /**
     * @var string property for urlManager;
     */
    public $alias;
    /**
     * @var int user timezone offset
     */
    public $timezone_offset;

    private $_reletedErrors = [];

    private $_transaction;

    private $_socialPostingData = [];
    /**
     * @var Post next active post with lower id
     */
    private $_nextPost;

    public static function find()
    {
        return new PostQuery(get_called_class());
    }

    public static function tableName()
    {
        return 'post';
    }

    public function behaviors()
    {
        return [
            'timestampBehavior' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()')
            ],
            'rating'=>[
                'class' => Rating::className(),
                'notUpdatedUserId' => false
            ]
        ];
    }

    public function rules()
    {
        return [
            [['title'], 'required', 'on' => self::SCENARIO_PUBLISHING],
            [['title'], 'trim'],
            ['user_id', 'integer'],
            ['user_id', 'exist', 'targetClass' => User::className(), 'targetAttribute' => 'id', 'except' => 'import'],
            [['tagIds'], 'filter', 'filter' => function ($value) {
                if (is_array($value)) {
                    foreach ($value as $item) {
                        trim(htmlentities(strip_tags($item), ENT_QUOTES, 'UTF-8'));
                    }
                    return $value;
                } else {
                    return trim(htmlentities(strip_tags($value), ENT_QUOTES, 'UTF-8'));
                }
            }],
            ['live_at', 'date', 'format' => 'yyyy-M-d H:m:s'],
            [['timezone_offset'], 'integer'],
            [['is_authors', 'is_adult', 'is_draft'], 'default', 'value' => 0],
            [['is_authors', 'is_adult', 'is_draft'], 'boolean', 'trueValue' => 1, 'falseValue' => 0, 'strict' => false],
            [['loadedImages'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif', 'maxFiles' => 500],
            [['loadedVideos'], 'file', 'skipOnEmpty' => true, 'extensions' => 'gif, flv, mp4, mov, avi, wmv', 'maxFiles' => 500],
        ];
    }

    public function attributeLabels()
    {
        return [
            'postBlock' => 'Blocks'
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord && $this->scenario !== 'import') {
                $this->user_id = Yii::$app->user->id;
            }
            return true;
        }
        return false;
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->initAlias();
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->initAlias();
    }

    public function initAlias()
    {
        $this->alias = UrlHelper::stringToUrl($this->title);
    }


    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])
            ->viaTable('post_to_tag', ['post_id' => 'id']);
    }

    public function getPostTags()
    {
        return $this->hasMany(PostTag::className(), ['post_id' => 'id']);
    }

    public function getPostBlocks()
    {
        return $this->hasMany(PostBlock::className(), ['post_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    public function getUrl($scheme = false)
    {
        $alias = StringHelper::truncate($this->alias, 50, '');
        return Url::to(['/post/item', 'id' => $this->id, 'alias' => $alias], $scheme);
    }

    public function getUpdateUrl()
    {
        return Url::to(['/post/update', 'id' => $this->id]);
    }

    public function getPreviewUrl()
    {
        return Url::to(['/post/preview', 'id' => $this->id]);
    }

    public function getCommentsUrl()
    {
        return Url::to(['/post/item', 'id' => $this->id, 'alias' => $this->alias, '#' => 'comments-block']);
    }

    public function getIndexName()
    {
        if (empty(Yii::$app->params['indexes']['main'])) {
            return 'pah_posts_search pah_posts_search_delta';
        } else {
            return Yii::$app->params['indexes']['main'];
        }
    }

    public function extractSphinxIds($result)
    {
        $ids = [];
        if (!empty($result['matches'])) {
            foreach ($result['matches'] as $match) {
                $ids[] = $match['id'];
            }
        }
        return $ids;
    }

    public function getNextPost()
    {
        if ($this->_nextPost === null) {
            /**
             * @var $sphinxClient \SphinxClient
             */
            $sphinxClient = Yii::$app->sphinxClient->getClient();
            $sphinxClient->SetFilterRange('id_attr', 0, $this->id - 1);
            $sphinxClient->SetSortMode(SPH_SORT_ATTR_DESC, 'ts_live_at');
            $sphinxClient->SetLimits(0, 1);
            $result = $sphinxClient->Query('', $this->indexName);
            $ids = $this->extractSphinxIds($result);
            if (isset($ids[0])) {
                $this->_nextPost = self::findOne(['id' => $ids[0]]);
            }
        }

        return $this->_nextPost;
    }

    public function setRelatedErrors($relation, ActiveRecord $model)
    {
        if ($model->hasErrors()) {
            if (!isset($this->_reletedErrors[$relation])) {
                $this->_reletedErrors[$relation] = [];
            }

            foreach ($model->getErrors() as $attribute => $errors) {
                foreach ($errors as $error) {
                    $this->_reletedErrors[$relation][] = $attribute . ':' . $error;
                }
            }
        }
    }

    public function getRelatedErrors()
    {
        return $this->_reletedErrors;
    }

    public function hasRelatedErrors()
    {
        return !empty($this->_reletedErrors);
    }

    public function getAttributedErrors()
    {
        $attrErrors = [];

        foreach ($this->getErrors() as $attribute => $errors) {
            $attrErrors[$attribute] = [];
            foreach ($errors as $error) {
                $attrErrors[$attribute][] = $attribute . ':' . $error;
            }
        }

        return $attrErrors;
    }

    public function beforeSaveTags()
    {
        $tags = [];
        if (count($this->tagIds) > 1) {
            $tags = $this->tagIds;
        } else if (isset($this->tagIds[0])) {
            $tags = explode(',', $this->tagIds[0]);
        }

        $tags = array_map('trim', $tags);
        $tags = array_filter($tags);
        $ids = [];
        foreach ($tags as $tag) {
            if (stripos($tag, 'add_tag_') === false) {
                $ids[] = $tag;
            } else {
                $tag = str_replace('add_tag_', '', $tag);
                $tag = strtolower($tag);
                $model = Tag::find()->where(['name' => $tag])->one();

                if ($model === null) {
                    $model = new Tag();
                    $model->name = $tag;
                    if (!$model->save()) {
                        $this->setRelatedErrors('tags', $model);
                    }
                }

                if ($model->id) {
                    $ids[] = $model->id;
                }
            }
        }

        $this->tagIds = $ids;
    }

    public function saveTags()
    {
        $this->beforeSaveTags();

        $this->unlinkAll('tags', true);

        $tags = Tag::find()
            ->where(['in', 'id', $this->tagIds])
            ->all();

        foreach ($tags as $tag) {
            $this->link('tags', $tag);
        }

        return true;
    }

    public function importTags($tagsStr)
    {
        $tags = explode(',', $tagsStr);
        $tags = array_map('trim', $tags);
        $tags = array_filter($tags);
        $ids = [];

        foreach ($tags as $tag) {
            $tag = strtolower($tag);

            $model = Tag::find()->where(['name' => $tag])->one();
            if ($model === null) {
                $model = new Tag();
                $model->name = $tag;
                $model->save();
            }

            if ($model->id) {
                $ids[] = $model->id;
            }
        }

        $tags = Tag::find()
            ->where(['in', 'id', $ids])
            ->all();

        foreach ($tags as $tag) {
            $this->link('tags', $tag);
        }
    }

    public function importImages($images)
    {
        foreach ($images as $image) {
            if (isset($image['fileName']) && file_exists($image['fileName'])) {
                $postBlock = new PostBlock();
                $postBlock->post_id = $this->id;
                $postBlock->type = PostBlock::BLOCK_TYPE_IMAGE;
                $postBlock->file_name = $image['name'] . '.' . $image['ext'];
                $postBlock->position = 0;
                if ($postBlock->save()) {
                    $postBlock->saveLoadedImage($image['fileName'], true);
                }
            }
        }
    }

    public function importVideo($videos)
    {
        foreach ($videos as $video) {

            $postBlock = new PostBlock();
            $postBlock->post_id = $this->id;
            $postBlock->type = PostBlock::BLOCK_TYPE_VIDEO;
            $postBlock->video_url = $video['video_url'];
            $postBlock->position = 0;

            if (isset($video['fileName']) && file_exists($video['fileName'])) {
                $postBlock->file_name = $video['name'] . '.' . $video['ext'];
            }

            if ($postBlock->save()) {
                if (isset($video['fileName']) && file_exists($video['fileName'])) {
                    $postBlock->saveLoadedVideo($video['fileName'], true);
                }
                if (!empty($postBlock->videoImage)) {
                    $postBlock->saveVideoThumbnail();
                }
            }
        }
    }

    public function getLoadedImage($fileName)
    {
        $images = $this->loadedImages;
        foreach ($images as $key => $image) {
            if ($key === $fileName) {
                unset($images[$key]);
                $this->loadedImages = $images;
                return $image;
            }
        }
        return null;
    }

    public function getLoadedVideo($fileName)
    {
        $videos = $this->loadedVideos;
        foreach ($videos as $key => $video) {
            if ($key === $fileName) {
                unset($videos[$key]);
                $this->loadedVideos = $videos;
                return $video;
            }
        }
        return null;
    }

    public function updatePostBlocksData($data)
    {
        foreach ($this->postBlocks as $block) {
            if (isset($data[$block->id])) {
                $block->attributes = $data[$block->id];
                $block->save();
                unset($data[$block->id]);
            } else {
                $block->delete();
            }
        }

        return $data;
    }

    public function saveBlocks()
    {
        $saved = true;

        $data = Yii::$app->request->post('PostBlock', []);
        $data = $this->updatePostBlocksData($data);

        /* upload posted images first */
        $this->loadedImages = ImageHelper::getLoadedImages('PostBlock', 'upload_name');

        /* upload posted video */
        $this->loadedVideos = VideoHelper::getLoadedVideos('PostBlock', 'upload_name');

        foreach ($data as $key => $blockData) {
            $model = new PostBlock();
            $model->post_id = $this->id;
            $model->attributes = $blockData;
            $model->type = (int)$model->type;
            if ($model->type === PostBlock::BLOCK_TYPE_IMAGE) {
                $image = isset($blockData['upload_name']) ? $this->getLoadedImage($blockData['upload_name']) : null;

                if ($image === null) {
                    $this->addError('postBlocks', 'Loaded image not found:' . $model->file_name . '(class ' . __CLASS__ . ' line ' . __LINE__ . ')');
                    $saved = false;
                } else if ($model->save() && $model->saveLoadedImage($image)) {
                    ImageHelper::removeTmpPath($image);
                    /**
                     * $saved = $saved && true;
                     */
                } else {
                    $this->addError('postBlocks', 'Error while saving image block: (class ' . __CLASS__ . ' line ' . __LINE__ . ')');
                    $saved = false;
                }

            } elseif ($model->type === PostBlock::BLOCK_TYPE_VIDEO && !empty($model->file_name)) {
                $video = isset($blockData['upload_name']) ? $this->getLoadedVideo($blockData['upload_name']) : null;

                if ($video === null) {
                    $this->addError('postBlocks', 'Loaded video not found:' . $model->file_name . '(class ' . __CLASS__ . ' line ' . __LINE__ . ')');
                    $saved = false;
                } else if ($model->save() && $model->saveLoadedVideo($video)) {
                    VideoHelper::removeTmpPath($video);
                    /**
                     * $saved = $saved && true;
                     */
                } else {
                    $this->addError('postBlocks', 'Error while saving video block: (class ' . __CLASS__ . ' line ' . __LINE__ . ')');
                    $saved = false;
                }
            } elseif ($model->type === PostBlock::BLOCK_TYPE_VIDEO && !empty($model->video_url)) {

                if ($model->save()) {
                    if (!empty($model->videoImage)) {
                        $model->saveVideoThumbnail();
                    }
                    /**
                     * $saved = $saved && true;
                     */
                } else {
                    $this->addError('postBlocks', 'Error while saving video block(class ' . __CLASS__ . ' line ' . __LINE__ . ')');
                    $saved = false;
                }

            } elseif ($model->type === PostBlock::BLOCK_TYPE_VIDEO && empty($model->file_name) && empty($model->video_url)) {
                /* prevent block saving with empty url */
                continue;

            } else if ($model->type === PostBlock::BLOCK_TYPE_TEXT) {

                if ($model->save()) {
                    /**
                     * $saved = $saved && true;
                     */
                } else {
                    $this->addError('postBlocks', 'Error while saving text block(class ' . __CLASS__ . ' line ' . __LINE__ . ')');
                    $saved = false;
                }

            } else {

                $this->addError('postBlocks', 'Error with type while saving post block(class ' . __CLASS__ . ' line ' . __LINE__ . ')');

            }
            $this->setRelatedErrors('postBlocks', $model);
        }

        return $saved;
    }

    public function saveDraft()
    {
        $this->is_draft = 1;

        $this->saveTags();
        $this->saveBlocks();
        $this->save();
        $this->refresh();

        return !$this->hasErrors() && !$this->hasRelatedErrors();
    }

    public function saveLive()
    {
        if ($this->saveDraft()) {
            if (empty($this->live_at)) {
                $this->live_at = date('Y-m-d H:i:s');
            }
            $this->normalizeLiveDate();

            if (count($this->postBlocks) === 0) {
                $this->addError('postBlock', 'Required at least one post block');
                return false;
            }

            if ($this->isPostProcessing()) {
                $this->status = self::STATUS_PROCESSING;
            }

            if ($this->isPostPreviewEmpty()) {
                $this->status = self::STATUS_EMPTY_BLOCK;
            } else if ((int)$this->status === self::STATUS_EMPTY_BLOCK) {
                $this->status = self::STATUS_ACTIVE;
            }

            $this->is_draft = 0;
            $this->scenario = self::SCENARIO_PUBLISHING;

            return $this->save();
        }
        return false;
    }

    public function getCreatedDate()
    {
        return Yii::$app->formatter->asRelativeTime($this->created_at);
    }

    public function getLiveDate()
    {
        return Yii::$app->formatter->asRelativeTime($this->live_at);
    }

    public function isMediaPost()
    {
        foreach ($this->postBlocks as $postBlock) {
            if ($postBlock->type === PostBlock::BLOCK_TYPE_TEXT) {
                return false;
            }
        }
        return true;
    }

    public function getTimesInFavorites()
    {
        return UserFavoritePost::getDb()->cache(function ($db) {
            return UserFavoritePost::find()->where('post_id=:post_id', [':post_id' => $this->id])->count();
        });
    }

    public function checkAuthorAccess()
    {
        return Yii::$app->user->identity && Yii::$app->user->identity->id === $this->user_id;
    }

    public static function indexSearch()
    {
        return self::find()->joinWith(['author']);
    }

    public function normalizeLiveDate()
    {
        if (!empty($this->timezone_offset)) {
            $dateTime = new \DateTime();
            $this->live_at = date('Y-m-d H:i:s', strtotime($this->live_at) + $this->timezone_offset + $dateTime->getOffset());
        }
    }

    public function getAuthorName()
    {
        return $this->author === null ?: $this->author->username;
    }

    public function getPostBlockCounts()
    {
        $postBlockCounts = [];
        foreach ($this->postBlocks as $postBlock) {
            if (!isset($postBlockCounts[$postBlock->typeName])) {
                $postBlockCounts[$postBlock->typeName] = 0;
            }
            $postBlockCounts[$postBlock->typeName]++;
        }
        ksort($postBlockCounts);
        return $postBlockCounts;
    }


    public static function deleteAllByOne($condition, $params = [])
    {
        $models = self::find()->where($condition, $params)->all();
        $removedModelIds = [];
        foreach ($models as $model) {
            if ($model->softDelete()) {
                $removedModelIds[] = $model->id;
            }
        }

        return $removedModelIds;
    }

    public function beforeDelete()
    {
        $this->_transaction = $this->getDb()->beginTransaction();

        foreach ($this->postBlocks as $postBlock) {
            $postBlock->delete();
        }

        UserFavoritePost::deleteAll(['post_id' => $this->id]);
        UserSeenPost::deleteAll(['post_id' => $this->id]);

        return parent::beforeDelete();
    }

    public function afterDelete()
    {
        if ($this->_transaction instanceof Transaction) {
            $this->_transaction->commit();
        }
        parent::afterDelete();
    }

    public function getLinkedTagList()
    {
        $tags = [];
        foreach ($this->tags as $tag) {
            $tags[] = Html::a($tag->name, $tag->getUrl(), ['class' => 'btn btn-xs btn-default tag-badge']);
        }
        return $tags;
    }

    public function getTagList($glue = ', ')
    {
        $tags = ArrayHelper::getColumn($this->tags, 'name');
        return implode($glue, $tags);
    }

    public function getSocialPostingData()
    {
        if (!$this->_socialPostingData) {
            $this->_socialPostingData = [
                'id' => $this->id,
                'media_url' => $this->getPostImage(),
                'tags' => $this->getTags()->select('name')->column(),
                'title' => $this->title,
                'url' => $this->getUrl(true)
            ];
        }

        return $this->_socialPostingData;
    }

    public function getPostImage($format = 'thumb_video')
    {
        $block = $this->getPostBlocks()
            ->where("type=:image OR type=:video", [':image' => PostBlock::BLOCK_TYPE_IMAGE, 'video' => PostBlock::BLOCK_TYPE_VIDEO])
            ->one();

        if (!$block) {
            return "";
        }

        if (!empty($block->getThumbSrc($format))) {
            return $block->getThumbSrc($format);
        }

        if (!empty($block->getImgSrc($format))) {
            return $block->getImgSrc($format);
        }

        return "";
    }

    public function getWords($len = 3)
    {
        $text = $this->title . PHP_EOL;
        foreach ($this->postBlocks as $postBlock) {
            if ((int)$postBlock->type === PostBlock::BLOCK_TYPE_TEXT) {
                $text .= $postBlock->text . PHP_EOL;
            }
        }
        foreach ($this->tags as $tag) {
            $text .= $tag->name . PHP_EOL;
        }

        $words = preg_split('/[\s,]+/', strip_tags($text));
        $words = array_filter($words, function ($w) use ($len) {
            return strlen($w) > $len;
        });
        return $words;
    }

    public function isPostProcessing()
    {
        foreach ($this->postBlocks as $postBlock) {
            if ($postBlock->isProcessing()) {
                return true;
            }
        }
        return false;
    }

    public function isPostPreviewEmpty()
    {
        foreach ($this->postBlocks as $postBlock) {
            if ($postBlock->isEmptyPreview()) {
                return true;
            }
        }
        return false;
    }

    public static function getUserSettings()
    {
        $user = Yii::$app->user->identity;
        if ($user !== null) {
            $settings = [];
            $postSettings = ['autoplay_gifs'];
            foreach ($postSettings as $postSetting) {
                $settings[$postSetting] = $user->getSettings($postSetting);
            }
            return $settings;
        }
        return [];
    }


    public function getOwnerId()
    {
        return $this->user_id;
    }

    public function getIsActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }
}

class PostQuery extends ActiveQuery
{
    public function active($is_draft = '0', $is_adult = '0', $is_authors = '0')
    {
        return $this->andWhere([
            'is_draft' => $is_draft,
            'is_adult' => $is_adult,
            'is_authors' => $is_authors,
            'status' => Post::STATUS_ACTIVE
        ]);
    }
}