<?php

namespace app\components\filters;


use yii\base\ActionFilter;
use yii\web\Response;

class JsonResponse extends ActionFilter
{
    public function afterAction($action, $result)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return parent::afterAction($action, $result);
    }
}