<?php

namespace app\assets;


use yii\web\AssetBundle;

class ShareCodesAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/jquery.shareCodes.css',
        'css/app.clipboard.css',
    ];

    public $js = [
        'js/jquery.shareCodes.js',
        'js/clipboard.min.js',
        'js/app.clipboard.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
    ];

    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}
