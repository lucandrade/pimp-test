<?php

namespace app\assets;

use yii\web\AssetBundle;

class BootboxAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';

	public $css = [
	];
    public $js = [
		'js/plugins/bootbox.min.js'
    ];
    public $depends = [
        'app\assets\AppAsset'
    ];
}
