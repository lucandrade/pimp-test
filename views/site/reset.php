<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Sign up';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">

			<?php $form = ActiveForm::begin(
				[
//					'action'=>['user/request-password-reset']
				]
			); ?>
			<div class="panel panel-default">
				<div class="panel-body">
					<h3><i class="fa fa-mail-forward"></i> Forgot Password?</h3>

					<p class="hint">
						Enter your e-mail address below to reset your password.
					</p>
					<hr>
					<?= $form->field($model, 'email')
							 ->textInput(['placeholder' => $model->getAttributeLabel('email')])
							 ->label(false);
					?>

					<hr>
					<div class="text-center">
						<div class="btn-group" role="group">
							<?= Html::submitButton(
								'Request',
								[
									'class' => 'btn btn-success uppercase',
								]
							) ?>

							<?= Html::a(
								'Cancel',
								['site/login'],
								[
									'class' => 'btn btn-default',
								]
							) ?>
						</div>
					</div>
				</div>
			</div>

			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
