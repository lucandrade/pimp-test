<?php
namespace app\models\behaviors;

class Rating extends \yii\base\Behavior
{
    public $authorPower = 1;
    public $notUpdatedUserId = \app\models\User::ANONYMOUS_ID;
    public $authorProperty = 'author';


    public function updateRating($rating)
    {
        $tr = \Yii::$app->db->beginTransaction();

        $res = $this->owner->updateCounters(['rating'=>$rating]);

        if(!($this->owner instanceof \app\models\interfaces\CommentsServiceModel)){
            throw new \yii\base\InvalidConfigException("Owner of this behavior must implement \\app\\models\\interfaces\\NotificationParentModel");
        }

        if($this->owner->getOwnerId() !== $this->notUpdatedUserId){
            $author = $this->owner->{$this->authorProperty};
            $author->updateCounters(['rating'=>$rating * $this->authorPower]);
        }

        $tr->commit();

        return $res;
    }
}