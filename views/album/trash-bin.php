<?php

use \yii\helpers\Url;
use app\helpers\Html;

/**
 * @var $dataProvider \yii\data\ActiveDataProvider;
 */
$totalFound  = $dataProvider->totalCount;

\app\assets\TrashBinAsset::register($this);

$this->title = "Trash bin";
?>
<div id="trash-bin-page" data-restore-url="<?= Url::to(['image/restore'])?>" data-purge-url="<?= Url::to(['image/purge']); ?>">
    <div class="author-header">
        <?= $totalFound > 0 ? Html::button("Empty", [
            'class' => 'btn btn-danger pull-right',
            'id'=> 'empty-trash-bin-btn',
            'data-url' => Url::to(['trash-bin/empty']),
            'title' => 'Empty trash bin'
        ]) : "" ?>

        <?= \app\widgets\BreadcrumbsHeader::widget([
            'author' => \Yii::$app->user->getIdentity(),
            'links' => ['trash bin']
        ]) ?>
    </div>

    <div id="trashbin-content">
        <?php if($totalFound > 0): ?>

            <div class="top-post-info post-by-user">
                <?= \app\widgets\AlbumSizeMetrics::widget([
                    'size' => $dataProvider->query->sum('size'),
                    'count' => $totalFound
                ]) ?>
            </div>

            <?= \app\widgets\SelectionBar::widget([
                'show' => $dataProvider->totalCount > 0,
                'icons' => [
                    'move-to' => [
                        'title' => 'Move images to another album',
                        'icon' => 'fa-copy',
                    ],
                    'restore' => [
                        'title' => 'Restore selected images',
                        'icon' => 'fa-undo'
                    ],

                    'purge' => [
                        'title' => 'Remove selected items forever',
                        'icon' => 'fa-remove'
                    ]
                ]
            ]) ?>
            <?php
            $hoverContent = '<i title="Download image" class="fa fa-download"></i>'.
                '<i title="Restore image" class="fa fa-undo"></i>'.
                '<i title="Remove image forever" class="fa fa-remove"></i>';
            ?>
            <?= $this->render("//album/_image_list", compact('dataProvider', 'hoverContent')); ?>


        <?php else: ?>
            <h2>
                Trash bin is empty
            </h2>
        <?php endif; ?>
    </div>
</div>

