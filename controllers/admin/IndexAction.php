<?php

namespace app\controllers\admin;

use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

class IndexAction extends Action
{

    public $viewName = 'index';
    public $modelClass;
    public $filterFields;
    public $redirectSuccessSelected = ['index'];
    public $changeFields;

    public function init()
    {
        if ($this->modelClass === null) {
            throw new InvalidConfigException('Model class must be setted');
        }

        parent::init();
    }

    public function run()
    {
        if (is_callable([$this->modelClass, 'indexSearch'])) {
            $query = call_user_func([$this->modelClass, 'indexSearch']);
        } else {
            $query = call_user_func([$this->modelClass, 'find']);
        }

        $filter = \Yii::$app->request->post('filter', Yii::$app->request->get('filter'));

        // todo: remove this vars
        $changeFields = $this->changeFields;
        $modelClass = $this->modelClass;

        if (\Yii::$app->request->isPost && ($selection = \Yii::$app->request->post('selection'))) {
            // delete selection
            if (\Yii::$app->request->post('delete')) {
                $method = "deleteAll";
                if (\Yii::$app->request->post('deleteByOne')) {
                    $method .= "ByOne";
                }
                call_user_func([$this->modelClass, $method], ['id' => $selection]);

                // redirect after deleted
                $this->controller->redirect($this->redirectSuccessSelected);
            }

            $fieldsForUpdate = [];
            foreach (\Yii::$app->request->post() as $name => $value) {
                if (array_key_exists($name, $this->changeFields) && $value != '') {
                    $fieldsForUpdate[$name] = $value;
                }
            }

            // update model data by post
            if ($fieldsForUpdate) {
                /**
                 * @var $modelClass ActiveRecord
                 */
                $modelClass = new $this->modelClass;
                foreach ($modelClass::findAll(['id' => $selection]) as $model) {
                    $model->setAttributes($fieldsForUpdate, false);
                    $model->update(true, array_keys($fieldsForUpdate));
                }
            }
            // redirect after changes
            $this->controller->redirect($this->redirectSuccessSelected);
        }

        if ($filter && $this->filterFields) {
            foreach ($this->filterFields as $field) {
                $query->orFilterWhere(['LIKE', $field, $filter]);
            }
        }

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query
            ]
        );

        return $this->controller->render($this->viewName, compact(
            'dataProvider',
            'filter',
            'changeFields',
            'modelClass'
        ));
    }

}