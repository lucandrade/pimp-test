<?php

namespace app\components;

use Yii;

class MediaUploadHandler extends UploadHandler
{
    private $_generatedNames = [];

    protected function get_file_name(
        $file_path,
        $name, $size,
        $type, $error,
        $index,
        $content_range
    )
    {
//        $ext = pathinfo($name, PATHINFO_EXTENSION);
//        $name = md5($name);
//        $name = $name . '.' . $ext;

        $name = $this->trim_file_name(
            $file_path
            , $name,
            $size,
            $type,
            $error,
            $index,
            $content_range
        );
        return $this->get_unique_filename(
            $file_path,
            $this->fix_file_extension(
                $file_path,
                $name,
                $size,
                $type,
                $error,
                $index,
                $content_range),
            $size,
            $type,
            $error,
            $index,
            $content_range
        );
    }

    protected function trim_file_name(
        $file_path,
        $name,
        $size,
        $type,
        $error,
        $index,
        $content_range
    )
    {
        $name = trim(basename(stripslashes($name)), ".\x00..\x20");

        $name = preg_replace('/[^A-z\.\d() _-]/', '', $name);
//        $name = preg_replace('/\s+/', '_', $name);

        if (!$name) {
            $name = str_replace('.', '-', microtime(true));
        }
        return $name;
    }

    /* using not unique filename, rewrite existing */
    protected function get_unique_filename(
        $file_path,
        $name,
        $size,
        $type,
        $error,
        $index,
        $content_range
    )
    {
        $fileName = md5($name) . '.' . pathinfo($name, PATHINFO_EXTENSION);

        while (is_dir($this->get_upload_path($fileName))) {
            $fileName = $this->upcount_name($fileName);
        }
        // Keep an existing filename if this is part of a chunked upload:
        $uploaded_bytes = $this->fix_integer_overflow((int)$content_range[1]);
        while (is_file($this->get_upload_path($fileName))) {
            if ($uploaded_bytes === $this->get_file_size(
                    $this->get_upload_path($fileName))
            ) {
                break;
            }
            $fileName = $this->upcount_name($fileName);
        }

        $this->_generatedNames[$fileName] = $name;
        return $fileName;
    }

    protected function addOriginalNames($content)
    {
        foreach ($content as $param => $files) {
            foreach ($files as $file) {
                if (is_object($file) && isset($this->_generatedNames[$file->name])) {
                    $file->incomingName = $this->_generatedNames[$file->name];
                }
            }
        }

        return $content;
    }

    public function generate_response($content, $print_response = true)
    {
        $this->response = $content;

        if (isset($content['loadedImages']) || isset($content['loadedVideos'])) {
            $content = $this->addOriginalNames($content);
        }

        if ($print_response) {
            $json = json_encode($content);
            $redirect = stripslashes($this->get_post_param('redirect'));
            if ($redirect && preg_match($this->options['redirect_allow_target'], $redirect)) {
                $this->header('Location: ' . sprintf($redirect, rawurlencode($json)));
                return;
            }
            $this->head();
            if ($this->get_server_var('HTTP_CONTENT_RANGE')) {
                $files = isset($content[$this->options['param_name']]) ?
                    $content[$this->options['param_name']] : null;
                if ($files && is_array($files) && is_object($files[0]) && $files[0]->size) {
                    $this->header('Range: 0-' . (
                            $this->fix_integer_overflow((int)$files[0]->size) - 1
                        ));
                }
            }
            $this->body($json);
        }
        return $content;
    }

    protected function set_additional_file_properties($file)
    {
        $file->dynamicPath = basename($this->options['upload_dir']);
//        $file->deleteUrl = $this->options['script_url']
//            . $this->get_query_separator($this->options['script_url'])
//            . $this->get_singular_param_name()
//            . '=' . rawurlencode($file->name);
//        $file->deleteType = $this->options['delete_type'];
//        if ($file->deleteType !== 'DELETE') {
//            $file->deleteUrl .= '&_method=DELETE';
//        }
//        if ($this->options['access_control_allow_credentials']) {
//            $file->deleteWithCredentials = true;
//        }
    }

    protected function upcount_name_callback($matches)
    {
        $index = isset($matches[1]) ? ((int)$matches[1]) + 1 : 1;
        $ext = isset($matches[2]) ? $matches[2] : '';
        return '_(' . $index . ')' . $ext;
    }

    protected function upcount_name($name)
    {
        return preg_replace_callback(
            '/(?:(?:_\(([\d]+)\))?(\.[^.]+))?$/',
            array($this, 'upcount_name_callback'),
            $name,
            1
        );
    }

    protected function handle_file_upload(
        $uploaded_file,
        $name,
        $size,
        $type,
        $error,
        $index = null,
        $content_range = null
    )
    {
        $file = new \stdClass();
        $file->name = $this->get_file_name(
            $uploaded_file,
            $name,
            $size,
            $type,
            $error,
            $index,
            $content_range
        );
        $file->size = $this->fix_integer_overflow((int)$size);
        $file->type = $type;
        if ($this->validate($uploaded_file, $file, $error, $index)) {
            $this->handle_form_data($file, $index);
            $upload_dir = $this->get_upload_path();
            if (!is_dir($upload_dir)) {
                mkdir($upload_dir, $this->options['mkdir_mode'], true);
            }
            $file_path = $this->get_upload_path($file->name);
            $append_file = $content_range && is_file($file_path) &&
                $file->size > $this->get_file_size($file_path);
            if ($uploaded_file && is_uploaded_file($uploaded_file)) {
                // multipart/formdata uploads (POST method uploads)
                if ($append_file) {
                    file_put_contents(
                        $file_path,
                        fopen($uploaded_file, 'r'),
                        FILE_APPEND
                    );
                } else {
                    move_uploaded_file($uploaded_file, $file_path);
                }
            } else {
                // Non-multipart uploads (PUT method support)
                file_put_contents(
                    $file_path,
                    fopen('php://input', 'r'),
                    $append_file ? FILE_APPEND : 0
                );
            }
            $file_size = $this->get_file_size($file_path, $append_file);
            $file->is_total_uploaded = false;
            $file->original_name = $name;
            if ($file_size === $file->size) {
//                $file->url = $this->get_download_url($file->name);
                if ($this->is_valid_image_file($file_path)) {
                    $this->handle_image_file($file_path, $file);
                    $file->is_total_uploaded = true;
                }
            } else {
                $file->size = $file_size;
                if (!$content_range && $this->options['discard_aborted_uploads']) {
                    unlink($file_path);
                    $file->error = $this->get_error_message('abort');
                }
            }
            $this->set_additional_file_properties($file);
        }
        return $file;
    }
}
