<?php

namespace app\assets;

use yii\web\AssetBundle;

class InfiniteAjaxScrollAsset extends AssetBundle
{
    public $sourcePath = '@vendor/webcreate/jquery-ias/src';
    public $js = [
        'callbacks.js',
        'jquery-ias.js',
        'extension/history.js',
        'extension/noneleft.js',
        'extension/paging.js',
        'extension/spinner.js',
        'extension/trigger.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];
}