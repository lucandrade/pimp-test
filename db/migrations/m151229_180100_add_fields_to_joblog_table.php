<?php

use yii\db\Schema;
use yii\db\Migration;

class m151229_180100_add_fields_to_joblog_table extends Migration
{
    public $table = "JobLog";


    public function up()
    {
        $this->addColumn($this->table, 'job_id', $this->integer());
        $this->addColumn($this->table, 'execute_time', $this->float());
        $this->addColumn($this->table, 'command_output', $this->text());
    }

    public function down()
    {
        $this->dropColumn($this->table, 'job_id');
        $this->dropColumn($this->table, 'execute_time');
        $this->dropColumn($this->table, 'command_output');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
