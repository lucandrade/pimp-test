<?php
use yii\helpers\Html;
/* @var $oauth app\models\UserOauth */

?>

<div class="col-sm-4 ouath-item">
    <h4><?= ucfirst($oauth->provider) ?></h4>
    <div class="social-image-wrapper">
        <?= Html::img($oauth->getAvatarUrl(), ['class'=>'img-circle']); ?>
    </div>
    <div>
        <?= Html::a("Unlink", ['user/unlink', 'provider'=>$oauth->provider ], ['class'=>'btn btn-danger']); ?>
    </div>
    <?php
    if(!\Yii::$app->user->identity->isDefaultProvider($oauth->provider)){
        echo "<div>" . Html::a('Make default', ['user/set-default', 'provider'=>$oauth->provider], ['class'=>'btn btn-warning']) . "</div>";
    }
    ?>
</div>