<?php

namespace app\helpers;

use Yii;
use yii\helpers\Url;

class WidgetHelper
{

    public static function getScriptInView($view, $data)
    {
        $script = \Yii::$app->controller->renderPartial($view, $data);
        $script = preg_replace('/<script>(.*)?<\/script>/s', '$1', $script);
        $script = trim($script);
        return $script;
    }

    public static function getConfig($type = 1)
    {
        $config = [
            'widget_id' => Yii::$app->params['commentsClientId'],
            'mainUrl' => 'http://' . Yii::$app->params['commentsHost'],
            'loginUrl' => Url::to(Yii::$app->user->loginUrl, true)
        ];

        if ($type == 2) {
            if (Yii::$app->user->isGuest) {
                $user_base64 = '';
            } else {
                $user_attributes = [
                    'username' => Yii::$app->user->identity->username,
                    'avatar' => Yii::$app->user->identity->avatar,
                    'id' => Yii::$app->user->identity->id,
                    'email' => Yii::$app->user->identity->email,
                    'link' => Yii::$app->user->identity->getProfileUrl(true),
                    'badge' => Yii::$app->user->identity->rating
                ];
                $user_base64 = base64_encode(json_encode($user_attributes));
            }
            // Secret key, that you've entered on the website's admin panel.
            $secret = Yii::$app->params['commentsClientSecret'];
            $time = time();
            $sign = md5($secret . $time . $user_base64);
            $config['auth'] = $user_base64 . "_" . $time . "_" . $sign;
        }

        return $config;
    }

}
