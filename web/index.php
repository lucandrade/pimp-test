<?php

if(file_exists('init.php')) include('init.php');

//defined('YII_DEBUG') or define('YII_DEBUG', true);

defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../vendor/autoload.php');

$localConfigFile = __DIR__ . '/../config/local.php';
$localConfig = file_exists($localConfigFile) ? require($localConfigFile) : [];

defined('YII_DEBUG') or define('YII_DEBUG', (isset($localConfig['common']['params']['debug']) && $localConfig['common']['params']['debug']));

require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../config/bootstrap.php');

//if(isset($localConfig['common']['params']['debug']) && $localConfig['common']['params']['debug']){
//	defined('YII_DEBUG') or define('YII_DEBUG', true);
//}else{
//	defined('YII_DEBUG') or define('YII_DEBUG', false);
//}

(new yii\web\Application(yii\helpers\ArrayHelper::merge(
        require(__DIR__ . '/../config/common.php'),
        require(__DIR__ . '/../config/web.php'),
        isset($localConfig['common']) ? $localConfig['common'] : [],
        isset($localConfig['web']) ? $localConfig['web'] : []
)))->run();
