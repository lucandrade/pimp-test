<?php
/**
 * @var $image \app\models\Image
 * @var $imagesDataProvider \yii\data\ActiveDataProvider;
 * @var $this \yii\web\View
 *
 */

use app\components\View;
use app\helpers\WidgetHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\PostAsset;
use app\assets\YTExternalAsset;
use app\models\Image;

\app\assets\ImageAsset::register($this);
PostAsset::register($this);
YTExternalAsset::register($this);


$this->title = Html::encode($image->title) . ' | ' . \Yii::$app->name;

$config = WidgetHelper::getConfig(2);
$ck = WidgetHelper::getScriptInView('//js/widget', ['config' => $config]);
$this->registerJs($ck, View::POS_END);

$this->registerMetaTag(['property' => 'og:image', 'content' => $image->getHttpFtpFileName(Image::SIZE_0_PX)]);
$this->registerMetaTag(['property' => 'og:image:width', 'content' => Image::SIZE_0_PX]);
$this->registerMetaTag(['property' => 'og:image:height', 'content' => Image::SIZE_0_PX]);

$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Pimp and host'
], 'description');

$this->registerMetaTag([
    'name' => 'keywords',
//    'content' => $album->getTagList(',') ?: 'owned,jokes,fun,humour,funny videos,epic,fail,win,funny pics,like a boss,forever alone,meme,gifs,funny gifs,trolling'
], 'keywords');

echo \app\widgets\ShareLinksModal::widget();

$disabledClass = $image->getIsProcessing() ? " icon-disabled" : "";
$disabledTitle = $image->getIsProcessing() ? " (disabled while processing)" : "";

$hoverContent =  '<i title="Download image' . $disabledTitle. '" class="fa fa-download' . $disabledClass. '"></i>';
if($image->checkAuthorAccess()){
    $hoverContent .= '<i title="Remove image" class="fa fa-trash-o"></i><i title="Edit image' . $disabledTitle. '" class="fa fa-edit' . $disabledClass. '"></i>';
}
$this->registerJs("$('.main-image-wrapper').imageSelector({
        enableSelection: false,
        hoverBarContent:'$hoverContent',
        showHover:function(thisObj){
            return $(thisObj).find('#main-image').hasClass('img-responsive');
        }
    });");


$renderNextPrevButtons = true;

$breadcrumbsLinks = [];

$removeRedirectUrl = Url::to(['album/item', 'id'=>$image->albumId]);
$album = $image->album;

/*temp fix*/
if(empty($album)){
    $userId = \Yii::$app->user->isGuest ? \app\models\User::ANONYMOUS_ID : \Yii::$app->user->id;
    $album = \app\models\Album::createUnsorted($userId);
}



if($image->uploadedByGuest()){
    if(!$album->getIsUnsorted()){
        $breadcrumbsLinks = array_merge($breadcrumbsLinks,[['label' => $album->title, 'url'=> $album->getUrl()]]);
    }else{
        $renderNextPrevButtons = false;
    }
    if($album->id == 622 || $album->id == 461) {
        $renderNextPrevButtons = false;
    }
}else
{
    $breadcrumbsLinks[] = [
        'label' => $album->getIsUnsorted()
            ? $album->title
            : \app\widgets\Editable::widget([
                'model' => $album,
                'linkUrl'=> Url::to(['album/item', 'id' => $image->album->id])
                ]
        )];
}

if($album->getIsUnsorted()){
    $renderNextPrevButtons = false;
}

$breadcrumbsLinks[] = ['label' => \app\widgets\Editable::widget(['model' => $image])];
?>

<div id="image-page" class="post item-post image"
     data-widget="page"
     data-page-key="image_<?= $image->id ?>"
     data-key="<?= $image->id ?>"
    >

    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="post-header-main">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="author-header">
                            <div class="top-bar pull-right">
                                <?php if($renderNextPrevButtons):
                                    $nextId = $image->getNextInAlbum('scalar');
                                    $prevId = $image->getPrevInAlbum('scalar');
                                    ?>
                                    <div class="nav-buttons">
                                        <?php if($nextId):
                                            $removeRedirectUrl = Url::to(['image/show', 'id' => $nextId]);
                                            ?>
                                            <a title="View previous image in album" class="btn btn-pah" href="<?= Url::to(['/image/show', 'id' => $prevId]) ?>">
                                                Previous
                                            </a>
                                            <a title="View next image in album" class="btn btn-pah" href="<?= Url::to(['/image/show', 'id' => $nextId]) ?>">
                                                Next
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                                <div class="widget-rating widget-rating-scores">
                                    <div data-rating="page"
                                         data-rating-type="scores"
                                         data-rating-key="image_<?= $image->id ?>"
                                         data-rating-value="<?= $image->rating ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="post-admin">
                               <?= \app\widgets\BreadcrumbsHeader::widget([
                                   'author' => $image->author,
                                   'links' => $breadcrumbsLinks,
                                   'afterLinksContent' => $this->render('_title_icons', [
                                       'image' => $image,
                                       'isMine' => $image->checkAuthorAccess()
                                   ])
                               ]) ?>

                                <?= $this->render("//common/_header_post_info", [
                                    'model' => $image
                                ]) ?>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row collapsing-content">
        <div class="col-xs-12 col-sm-12">
            <div class="collapse in"
                 id="collapsePost-<?= $image->id ?>"
                 aria-expanded="true">
                <div id="ad2" style="margin-bottom: 10px;">
                    <iframe src="/static/html/iframe.html" scrolling="no" frameBorder="0" allowtransparency="true" width="960px" height="252px"></iframe>
                </div>
                <div id="overflow-wrapper" class="post-expand-wrapper">
                    <?= \app\widgets\ZoomImage::widget([
                        'originSrc' => $image->getHttpFtpFileName(),
                        'thumbSrc' => $image->getHttpFtpFileName(Image::SIZE_MEDIUM_PX),
                        'options' => [
                            'class' => 'main-image-wrapper',
                            'data-remove-url' => Url::to(['image/delete']),
                            'data-edit-url' => Url::to(['image/edit', 'id' => $image->id]),
                            'data-redirect-remove-url' => $removeRedirectUrl,
                            'data-isprocessing' => $image->getIsProcessing(),
                            'data-filename' => $image->fileName,
                        ],
                        'imgOptions' => ['class'=>'img-responsive', 'id'=>'main-image']
                    ]) ?>

                </div>
                <center><iframe width="780" scrolling="no" height="210" frameborder="0" marginwidth="0" marginheight="0" src="http://adsbookie.com/y/ad.php?link=aHR0cDovL3YyLmFkc2Jvb2tpZS5jb20vY2RuL3pyODA="></iframe>
                </center>
            </div>
            <div class="post-bottom">
                <div class="pull-right manage-right-icons">
                    <?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->is_admin): ?>
                        <a href="<?= Url::to(["admin/post-to-socials"]); ?>"
                           class="share-to-socials"
                           title="share to socials">
                            <i class="fa fa-2x fa-share"></i>
                        </a>
                    <?php endif; ?>

                </div>
                <span class="widget-rating">
                    <span data-rating="page"
                          data-rating-type="buttons"
                          data-rating-key="page_<?= $image->id ?>"
                          data-rating-value="<?= $image->rating ?>">
                    </span>
                </span>
                <div class="post-btn-group pull-left">
                    <div class="comm">
                        <a class="post-comments" href="<?= Url::to(['/image/show','id'=>$image->id, '#' => 'comments-block']) ?>"
                           title="comments">
                            <i class="fa fa-comment"></i>
                            <span data-page="comments-count"></span>
                        </a>
                    </div>
                    <div class="mys">
                        <div id="rrssb-<?= $image->id ?>-bottom">
                            <span class="btn"
                                  title="share"
                                  data-rrssb="button"
                                  data-rrssb-container="#rrssb-<?= $image->id ?>-bottom"
                                  data-rrssb-url="<?= Url::to(['/image/show', 'id' => $image->id], true); ?>"
                                  data-rrssb-title="<?= $image->title ?>"
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="comments-block"></div>
    <div data-comment="comment-widget" id="comments"></div>
</div>

<script src="http://v2.adsbookie.com/cdn/zp53"></script>

