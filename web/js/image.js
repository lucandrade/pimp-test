$(document).ready(function(){

    if($('#image-page').length == 0){
        return;
    }




    $('.stats-popover').popover();

    var $wrapper = $(".main-image-wrapper");

    $(".post-bottom .share-chain").on('click', function(){
        shareModalWindow.show([$(this).data('share-codes')]);
    });


    $wrapper.on('click', function(){
        var $me = $(this);

        if($me.data('isprocessing')){
            return false;
        }

        if($me.data('loading')){
            return false;
        }
    });

    $('.title-icons .fa-share-alt').click(function(){
        var codes = $(this).data('share-codes');
        shareModalWindow.show([$(this).data('share-codes')]);
        return false;
    });

    $('.title-icons .fa-edit').click(function(){
        var win = window.open($wrapper.data('edit-url'), '_blank');
        win.focus();
        return false;
    });

    $('.title-icons .fa-download').click(function(){
        $wrapper.imageSelector('download');
        return false;
    });

    $('.title-icons .fa-trash').click(function(){
        removeImage();
        return false;
    });

    $wrapper.on('click',' .hover-bar .fa-edit', function(e){
        e.stopPropagation();
        if($(this).hasClass('icon-disabled')){
            return false;
        }
        var win = window.open($wrapper.data('edit-url'), '_blank');
        win.focus();
        return false;
    });



    $wrapper.on('click', '.hover-bar .fa-download', function(e){
        e.stopPropagation();
        if($(this).hasClass('icon-disabled')){
            return false;
        }
        $wrapper.imageSelector('download');
        return false;
    });


    $wrapper.on('click', '.hover-bar .fa-trash-o', function(e){
        removeImage();
        return false;
    });


    var removeImage = function(){
        var url = $wrapper.data('remove-url'),
            id = $('.image').data('key'),
            redirectUrl = $wrapper.data('redirect-remove-url');


        bootbox.confirm("Are you sure?", function(confirmed){
            if(confirmed)
            {
                $.ajax({
                    url:url,
                    data:{ids:id},
                    type:"POST",
                    dataType:"json",
                    success:function(res){
                        if(res.ids && res.ids.length > 0){
                            bootbox.alert("Image was successfully removed!", function(){
                                window.location.href = redirectUrl;
                            });
                        }
                        if(res.errors && res.errors > 0){
                            console.log(res.errors);
                        }
                    }
                });
            }
        });

    }

});
