<?php

namespace app\helpers;


use Yii;
use yii\helpers\Json;

class LogHelper
{
    public static function log($message, $logPathAlias, $fileName = 'run.log')
    {
        if (!is_string($message)) {
            $messageJson = Json::encode($message);
            $messages = Json::decode($messageJson);
            $message = ArrayHelper::recursiveImplode($messages, PHP_EOL);
        }

        $logFile = Yii::getAlias($logPathAlias) . $fileName;
        FileHelper::createDirectory(pathinfo($logFile, PATHINFO_DIRNAME), 0777);

        return file_put_contents(
            $logFile,
            "\n" . date('Y-m-d H:i:s') . str_pad(' ', 40, '=') . "\n" . $message . "\n",
            FILE_APPEND
        );
    }
}
