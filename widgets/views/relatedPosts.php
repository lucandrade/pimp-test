<?php
/**
 * @var $models \app\models\Post[]
 * @var $this \yii\web\View
 */
?>
<div class="related-posts">
    <?php foreach ($models as $model) { ?>
        <div class="related-post">
            <div class="caption">
                <p class="h5">
                    <a href="<?= $model->getUrl() ?>" title="<?= $model->title ?>">
                        <?= $model->title ?>
                    </a>
                </p>
            </div>
            <?php if ($model->postImage) { ?>
                <div class="related-post-img">
                    <a href="<?= $model->getUrl() ?>" title="<?= $model->title ?>">
                        <img src="<?= $model->postImage ?>" alt="<?= $model->postImage ?>" class="img-responsive">
                    </a>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
</div>