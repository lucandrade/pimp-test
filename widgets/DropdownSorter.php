<?php

namespace app\widgets;


use app\helpers\ArrayHelper;
use app\helpers\SortHelper;
use yii\base\Exception;
use yii\base\Widget;
use yii\base\InvalidConfigException;
use yii\bootstrap\Html;
use yii\data\Sort;
use yii\helpers\Inflector;
use yii\widgets\LinkSorter;

class DropdownSorter extends Widget
{
    /**
     * @var Sort;
     */
    public $sort;
    public $attributes = [
        'Album Name' => 'title',
        'Album Size' => 'imageSize',
        'Last Uploaded' => 'createTime',
        'Last Modified' => 'updateTime'
    ];


    public $buttonOptions = [];
    public $ulOptions = [];
    public $linkOptions = [];

    public $options = [];

    public $userSortSetting;

    public $user;





    private $_activeAttribute;


    public function init()
    {
        if ($this->sort === null) {
            throw new InvalidConfigException('The "sort" property must be set.');
        }

        if(empty($this->userSortSetting)){
            throw new InvalidConfigException('The "userSortSetting" property must be set.');
        }

        if(!\Yii::$app->user->isGuest)
        {
            $userSortingValue = \Yii::$app->user->identity->getSettings($this->userSortSetting);
            if($userSortingValue && $this->getIsDefaultSorting())
            {
                $order = SortHelper::valueToArrayOrder($userSortingValue);
                $this->sort->setAttributeOrders($order);

            }else{
                $currentSortingValue = $this->getActiveSortingValue();
                if($userSortingValue !== $currentSortingValue)
                {
                    \Yii::$app->user->identity->setSettings($this->userSortSetting, $currentSortingValue);
                }
            }
        }
    }


    protected function getIsDefaultSorting(){
        return $this->sort->defaultOrder == $this->sort->getAttributeOrders();
    }


    public function getActiveSortingValue()
    {
        list($attribute, $direction) = $this->getActiveAttribute();
        $sign = $direction == SORT_DESC ? "-" : "";
        return $sign . $attribute;
    }

    public function run()
    {
        $options = ArrayHelper::merge([
                'class' => "dropdown dropdown-sorter pull-right"
            ], $this->options);

        return Html::tag('div',
            "<span class='sorting-label'>Sort by:&nbsp;<span>" . $this->renderButton() . $this->renderDropdownUl(),
        $options);
    }



    protected function renderDropdownUl()
    {
        $options = ArrayHelper::merge([
            'class' => 'dropdown-menu',
            'encode' => false
        ], $this->ulOptions);

        $attributes = empty($this->attributes) ? array_keys($this->sort->attributes) : $this->attributes;
        $links = [];
        list($sortingAttribute, $direction) = $this->getActiveAttribute();
        foreach ($attributes as $alias => $name)
        {
            $linkOptions = ArrayHelper::merge([], $this->linkOptions);
            $linkOptions['label'] = is_numeric($alias) ? Inflector::camel2words($name) : $alias;
            if($name == $sortingAttribute){
                $caret =  $direction == SORT_DESC ? "up" : "down";
                $linkOptions['label'] = $linkOptions['label'] .  "<i class='fa fa-caret-$caret'></i>";
            }
            $links[] = $this->sort->link($name, $linkOptions);
        }

        return Html::ul($links, $options);
    }


    protected function getActiveAttribute()
    {
        if(!$this->_activeAttribute)
        {
            $orders = $this->sort->getAttributeOrders();
            $attribute = array_keys($orders)[0];
            $direction = $orders[$attribute];
            $this->_activeAttribute = [$attribute, $direction];
        }
        return $this->_activeAttribute;
    }


    protected function getAttributeLabel($attribute)
    {
        $key = array_search($attribute, $this->attributes);
        if($key === false){
            throw new Exception("Attribute '$attribute' not found in sorting attributes");
        }

        return is_numeric($key) ? Inflector::camel2words($attribute) : $key;
    }

    public function renderButton()
    {
        list($attribute, $direction) = $this->getActiveAttribute();
        $caret = $direction == SORT_DESC ? "down" : "up";
        $attributeLabel = $this->getAttributeLabel($attribute);

        $options = ArrayHelper::merge([
            'data-toggle' => 'dropdown',
            'class' => 'btn btn-default dropdown-toggle'
        ], $this->buttonOptions);

        return Html::button($attributeLabel . "<i class='fa fa-caret-$caret'></i>", $options);

    }

}