<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;
use yii\base\Exception;

class Facebook extends Widget
{
    public $enabled = true;

    public function init()
    {
        if (isset(Yii::$app->params['widgets']['facebook']['enabled'])) {
            $this->enabled = Yii::$app->params['widgets']['facebook']['enabled'];
        }
        parent::init();
    }

    public function run()
    {
        if ($this->enabled) {
            return $this->render('facebook', []);
        }
    }
}