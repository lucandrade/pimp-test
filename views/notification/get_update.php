<?php
/**
 * @var $this \yii\web\View
 * @var $models \app\models\Notification[]
 *
 */
?>

<ul class="list-unstyled list-notification">
    <?php foreach ($models as $model) { ?>
        <?php if ($model->isLinkedPost() || $model->isLinkedComment()) { ?>
            <li class="divider"></li>
            <li class="post-notification bg-default">
                <div class="row">
                    <div class="col-xs-3">
                        <a href="<?= $model->caller->getProfileUrl() ?>">
                            <img src="<?= $model->caller->avatar ?>" class="img-responsive">
                        </a>
                    </div>
                    <div class="col-xs-6">
                        <p><?= $model->notice ?></p>
                        <small class="text-muted">
                            <?= Yii::$app->formatter->asRelativeTime($model->created_at) ?>
                        </small>
                    </div>
                    <div class="col-xs-3">
                        <a href="<?= $model->getUrl() ?>">
                            <img src="<?= $model->post->getPostImage('thumb_50') ?>"
                                 class="img-responsive">
                        </a>
                    </div>
                </div>
            </li>
        <?php } ?>
    <?php } ?>
</ul>


