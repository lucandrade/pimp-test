YII
---
> composer global require "fxp/composer-asset-plugin:~1.1.1"
> composer self-update
> composer update

YII DB
---
> ./yii migrate
> ./yii fixture "*"


Assets compress by Java tools
---
- first, in vagrant you need install jdk-v7
> sudo apt-get install openjdk-7-jre openjdk-7-jdk

- run compress js and css files
> ./yii asset config/minify.php config/asset.php


Assets compress by NodeJS Gulp
---
- first, in vagrant you need install Install NodeJS
> - sudo sh -c "echo 'deb http://ftp.de.debian.org/debian/ jessie main contrib non-free' >> /etc/apt/sources.list"
> - sudo sh -c "echo 'APT::Default-Release \"stable\";' >> /etc/apt/apt.conf.d/80default-release"
> - sudo apt-get update
> - sudo apt-get -t jessie install nodejs npm
> - sudo ln -s /usr/bin/nodejs /usr/local/bin/node

- Install Gulp globaly
> sudo npm install -g gulp

- Make sure that Gulp works fine. Check it with
> gulp --version

- Update npm
> cd tools/gulp
> npm update

- if need, install Gulp local
> npm install gulp

- run compress js and css files
> ./yii asset config/minify.php config/asset.php


Assets compress by MatthiasMullie\Minify
---
- run compress js and css files
> ./yii minify config/assets-config/minify.php config/generated-assets/asset.php


- run compress js and css files for image-editor
> ./yii minify config/assets-config/editor-asset.php config/generated-assets/editor-asset.php


- run compress js and css files for backend
> ./yii minify config/minify-backend.php config/asset-backend.php

- run compress js and css files for forum upload plugin
> ./yii minify config/assets-config/minify-uploader-plugin.php config/generated-assets/uploader-plugin.php

Running Docker containers
---

> Before start, make sure you have php 5.5+ and composer installed, see more at [Installing Composer](https://getcomposer.org/download/)

Install composer dependencies

```
composer global require "fxp/composer-asset-plugin:~1.1.1"
composer self-update
composer update
```

Add the follow entry to your `/ets/hosts` file `127.0.0.1   pah.app`

Execute the command `sh ./run_docker.sh`

Access [http://pah.app:8001](http://pah.app:8001)
