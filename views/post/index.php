<?php

/**
 * @var $this yii\web\View
 */

use app\assets\PostAsset;
use app\assets\YTExternalAsset;
use app\models\Post;
use yii\helpers\Json;

PostAsset::register($this);
YTExternalAsset::register($this);

$this->title = \Yii::$app->name .' - Only the best Owned Pictures & Videos Daily!';

$seenPostIds = [];
if (!Yii::$app->user->isGuest) {
    //switched off make seen posts for until we complete with it
    $this->registerJs("$(document).on('scroll_1', scrollPostsHandler); initProccessSeenPosts($('.item'));");
//    echo Html::hiddenInput('make-seen-url', Url::to(['post/make-seen']));
//    $seenPostIds = Yii::$app->user->identity->getSeenPostIds(['post_id' => $dataProvider->getSphinxIds()]);
    $this->registerJs('var userOptions = ' . Json::encode(Post::getUserSettings()) . ';', $this::POS_HEAD);
}

$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Owned.com - Awesome daily updated Owned Videos, Pictures & Gifs - Visit us to see more!'
], 'description');

$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'owned,jokes,fun,humour,funny videos,epic,fail,win,funny pics,like a boss,forever alone,meme,gifs,funny gifs,trolling'
], 'keywords');
?>

    <div class="well post-filter-panel">
        <?= $this->render('_post_filter', ['filter' => $filter, 'dataProvider' => $dataProvider, 'seenPostsIds' => $seenPostIds]); ?>
    </div>

<?= $this->render("_posts", compact('dataProvider', 'seenPostIds')); ?>