<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use app\widgets\CuteCatIcon;

$this->beginContent('@app/views/layouts/clean.php');
?>
<?= $this->render('includes/_header') ?>

	<div class="container">
		<div class="row">

			<!-- Entries Column -->
			<div class="col-sm-8">
				<?= Alert::widget();?>
				<?= $content ?>
			</div>

			<!-- Sidebar Widgets Column -->
			<div class="col-sm-4 hidden-xs">
				<?= $this->render('includes/_sidebar') ?>
			</div>
		</div>

		<?= $this->render('includes/_footer')?>
	</div>
<?php $this->endContent(); ?>


