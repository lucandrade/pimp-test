<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "abuse_search".
 *
 * @property integer $id
 * @property string $value
 */
class AbuseSearch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'abuse_search';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value'], 'string', 'max' => 255],
            [['value'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value' => 'Value',
        ];
    }



    public static function createFromRequestUrl($url)
    {
        if(preg_match("~.*?/s/(.*)~si", $url, $matches) && isset($matches[1]))
        {
            $matched = $matches[1];
            if(preg_match('~(.*?)/~si', $matched, $results) && $results[1]){
                $matched = $results[1];
            }

            return self::create($matched);
        }
    }


    public static function create($value)
    {
        $model = new self;
        $model->value = $value;
        $model->save();
        return $model;
    }

}
