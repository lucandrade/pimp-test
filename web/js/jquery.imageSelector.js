(function($){
    $.fn.imageSelector = function(method){
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method "' +  method + '" is not found in jquery.imageSelector' );
        }
    };


    var events = {
        onSelected: "onSelected",
        onUnselected: "onUnselected",
        onInited:"onInited"
    };


    var settings = {};
    var defaultSettings = {
        wrapperClass: "im-wr",
        restoreSelection:true,
        commonParentSelector:"body",
        selectIndicatorClass: "select-indicator",
        selectedClass:"selected",
        enableSelection : true,
        showHover:true,
        events:{
            onSelected: "onSelected",
            onUnselected: "onUnselected",
            onInited:"onInited"
        },
        hoverInfoContent:function($wrapper){

        },
        removeSettings:{
            getIds: function ($selectedWrappers){
                var ids = [];
                $selectedWrappers.each(function(){
                    ids.push($(this).data('key'));
                });

                return ids;
            }
        },
        moveOptions: {

        }
    };

    var showOver = function($imageWrapper){
        var $over = $imageWrapper.find('.image-over');
        if($over.length  == 0){
            $over = createOver($imageWrapper);
        }

        $over.show();

        return $over;
    };

    var methods = {
        init:function(options){

            settings = $.extend({}, defaultSettings, options || {});

            this.each(function(){
                var $this =  $(this);

                $this.addClass(settings.wrapperClass);

                var $over = createOver($this);
                createHover($over, $this);
                $this
                    .off('mouseenter')
                    .on('mouseenter', function(){
                    var showHover;

                    if(typeof settings.showHover == 'function'){
                        showHover = settings.showHover(this);
                    }else{
                        showHover = settings.showHover;
                    }

                    if(showHover){
                        var $over = showOver($(this));
                        var $hoverBar = $over.find('.hover-content');
                        $hoverBar.show();
                    }
                });

                if(settings.enableSelection)
                {
                    $this.find("." + settings.selectIndicatorClass)
                        .off('click')
                        .on('click', function(e){
                        e.stopPropagation();
                        var $wrapper = $(this).parents("." + settings.wrapperClass);
                        toggleSelection($wrapper);
                        return false;
                    });

                    $this
                        .off('click')
                        .on('click', function()
                    {
                        if($(this).parents(settings.commonParentSelector)
                                .find("."+settings.wrapperClass+"."+settings.selectedClass).length > 0)
                        {
                            toggleSelection($(this));
                            return false;
                        }
                    });
                }


                $this
                    .off('mouseleave')
                    .on('mouseleave', function(){
                    var showHover;
                    if(typeof settings.showHover == 'function'){
                        showHover = settings.showHover(this);
                    }else{
                        showHover = settings.showHover;
                    }

                    if(showHover){
                        var $hoverBar = $(this).find('.hover-content');
                        $hoverBar.hide();
                        if(!$this.hasClass(settings.selectedClass)){
                            var $over = $(this).find('.image-over');
                            $over.hide();
                        }
                    }
                });
            })
        },


        download:function(){
            this.each(function(){
                var $wrapper = $(this),
                    url = $wrapper.data('src') || $wrapper.find('img').attr('src'),
                    filename  = $wrapper.data('filename') || "image.jpg";

                if(!url){
                    url = $wrapper.find('img').attr('src');
                }

                var link = document.createElement('a');
                link.href = url;
                link.download = filename;
                document.body.appendChild(link);
                link.click();
                setTimeout( function () { link.parentNode.removeChild( link ); },10 );
            });
        },


        removeSelected:function(options){
            var $selectedWrappers = getSelected(this);
            removeImages($selectedWrappers, options);
        },

        remove:function(options){
            removeImages($(this), options);
        },

        selectAll:function() {

            this.each(function() {
               selectImage($(this));
            });
        },

        getSelected:function(){
            return getSelected(this);
        },

        unselectAll:function() {
            this.each(function() {
                unselectImage($(this));
            });
        }
    };

    var getSelected = function(wrappers){
        var $selected = $();
        wrappers.each(function(){
            if($(this).hasClass(settings.selectedClass)){
                $selected = $selected.add($(this).get(0));
            }
        });
        return $selected;
    };

    var removeImages = function($wrappers, options){

        var localSettings = $.extend(settings.removeSettings || defaultSettings.removeSettings,
            options);
        var ids = localSettings.getIds($wrappers);

        if(Array.isArray(ids)){
            ids = ids.join(',');
        }
        bootbox.confirm("Are you sure?", function(confirmed){
            if(confirmed)
            {
                $.ajax({
                    url:localSettings.url,
                    data:{ids:ids},
                    type:"POST",
                    dataType:"json",
                    success:function(res){
                        if(res.ids && res.ids.length > 0){
                            unselectImage($wrappers);
                            if(localSettings.successRemoved && typeof(localSettings.successRemoved) == 'function'){
                                localSettings.successRemoved(res.ids, $wrappers);
                            }
                        }

                        if(res.errors && res.errors > 0){
                            console.log(res.errors);
                        }
                    }
                });
            }
        });
    };



    var toggleSelection = function($wrapper){
        if($wrapper.hasClass(settings.selectedClass)){
            unselectImage($wrapper);
            $wrapper.trigger('mouseenter');
        }else{
            selectImage($wrapper);
        }
    };

    var selectImage = function($wrapper){
        if(!$wrapper.hasClass(settings.selectedClass))
        {
            $wrapper.addClass(settings.selectedClass);
            showOver($wrapper);
            $wrapper.trigger('onSelected');
            //$wrapper.trigger(settings.events.onSelected);
        }
    };

    var unselectImage = function($wrapper)
    {
        if($wrapper.hasClass(settings.selectedClass))
        {
            $wrapper.removeClass(settings.selectedClass);
            $wrapper.find('.image-over').hide();
            //$wrapper.trigger(settings.events.onUnselected);
            $wrapper.trigger("onUnselected");
        }
    };

    var createOver = function($imageWrapper){

        var $over = $imageWrapper.find('.image-over');
        if($over.length == 0)
        {
            $over = $("<div/>").addClass('image-over');
            $over.append($("<div/>").addClass('shadow'));

            if(settings.enableSelection)
            {
                var $selectIndicator = $("<div/>").addClass(settings.selectIndicatorClass);
                $selectIndicator.append("<i class='fa fa-check'></i>");
                $over.append($selectIndicator);
            }

            $imageWrapper.prepend($over);
        }
        $over.hide();
        return $over;
    };

    var createHover = function($over, $wrapper)
    {
        var $hover = $('<div/>').addClass('hover-content');

        var $bar = $('<div/>').addClass('hover-bar');

        if(typeof (settings.hoverBarContent) == 'function'){
            $bar.html(settings.hoverBarContent($wrapper));
        }else{
            $bar.html(settings.hoverBarContent);
        }

        $hover.append($bar);

        if(settings.hoverInfoContent && typeof (settings.hoverInfoContent) == 'function')
        {
            var $info = $('<div/>').addClass('hover-info');
            $info.html(settings.hoverInfoContent($wrapper));
            $hover.append($info);
        }

        $hover.hide();
        $over.prepend($hover);
        return $hover;
    };
})(jQuery);