<?php

use yii\helpers\Html;
use app\helpers\HtmlPurifier;
use yii\helpers\Url;

/**
 * @var $this yii\web\View
 */
$image = $count > 0 ? "recycle_full.png" : "recycle.png";

?>
<div class="col-md-4 col-lg-3 col-xs-4 col-xxs-6 album-thumb-item">
    <div class="ar">
        <a href="<?= Url::to(['/trash-bin/index']) ?>" class="grid-content trash-bin">
            <?= Html::img("@web/static/i/$image", ['class'=>'img-responsive']) ?>
        </a>
    </div>
    <div class="content-title">
        <div class="album-title">
            <?= Html::a("Trash Bin", ['/trash-bin/index']) ?>
        </div>
        <div class="album-info">
            <?= $count ?> image<?= $count !== 1 ? "s" : "" ?>
        </div>
    </div>
</div>
