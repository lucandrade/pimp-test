<?php

namespace app\controllers;

use Yii;
use app\models\UserSeenPost;
use app\components\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use app\components\MediaUploadHandler;
use app\helpers\VideoHelper;
use app\helpers\ImageHelper;
use app\models\Post;
use app\models\PostBlock;
use app\components\SphinxDataProvider;
use yii\web\UnauthorizedHttpException;
use app\models\FilterPost;

class PostController extends Controller
{
    public $layout = '2-columns';

    const CACHE_PREFIX_OLDITEM = 'olditem';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'item', 'old-item'],
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['toggle-favorite']
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'save' => ['post'],
                    'validateVideo' => ['post'],
                    'uploadVideo' => ['post'],
                    'stopUploadVideo' => ['post'],
                    'uploadImage' => ['post'],
                    'stopUploadImage' => ['post'],
                    'make-seen' => ['post']
                ],
            ],
        ];
    }

    public function actions()
    {
        return [

        ];
    }

    public function actionIndex()
    {
        $dataQuery = Post::find()
            ->from('post p')
            ->with([
                'postBlocks',
                'tags',
                'author'
            ]);

        $filter = new FilterPost([
            'tableAlias' => 'p',
            'sphinxClient' => Yii::$app->sphinxClient->getClient()
        ]);

        $filter->applyClientFilters();

        $defaultSort = $filter->type === 'best' ? ['rating' => SORT_DESC, 'live_at' => SORT_DESC]
            : ['live_at' => SORT_DESC];

        $dataProvider = new SphinxDataProvider([
            'cacheDuration' => 60,
            'query' => $dataQuery,
            'sphinxClient' => $filter->getSphinxClient(),
            'indexName' => $filter->indexName,
            'primaryModelKey' => 'p.id',
            'sphinxQuery' => $filter->getSphinxQuery(),
            'useSqlSortMode' => true,
            'sortingAttributes' => [
                'live_at' => 'ts_live_at',
                'rating' => 'rating'
            ],
            'sort' => [
                'defaultOrder' => $defaultSort,
            ],
            'pagination' => [
                'pageSize' => Yii::$app->params['itemsPerPage'],
                'defaultPageSize' => Yii::$app->params['itemsPerPage']
            ]
        ]);

        return $this->render('index', [
            'filter' => $filter,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionUpdate($id = null)
    {

        if ($id === null) {
            $model = Post::find()
                ->with(['postBlocks', 'tags'])
                ->where(['is_draft' => 1, 'user_id' => Yii::$app->user->id])
                ->one();
        } else {
            $postQuery = Post::find()
                ->where(['id' => $id])
                ->with(['postBlocks', 'tags']);

            if (!Yii::$app->user->can('admin')) {
                $postQuery->andWhere(['user_id' => Yii::$app->user->id]);
            }

            $model = $postQuery->one();
            if ($model === null) {
                $this->redirect(['/post/update']);
            }
        }

        if ($model === null) {

            if (!Yii::$app->user->can('admin') && !Yii::$app->user->can('addPosts')) {
                return $this->render('rating_message');
            }

            $model = new Post();
            $model->is_draft = 1;
            $model->save();
            $blockIds = [];

        } else {
            $model->tagIds = array_map(function ($t) {
                return $t->id;
            }, $model->tags);

            $blockIds = array_map(function ($b) {
                return $b->id;
            }, $model->postBlocks);
        }

        $liveAt = Post::find()
            ->select('live_at')
            ->orderBy(['live_at' => SORT_DESC])
            ->limit(1)
            ->scalar();

        $liveAtTime = strtotime($liveAt);
        $liveAtTime += 60 * Yii::$app->params['Post']['liveAtAdditive'];

        $model->scenario = Post::SCENARIO_PUBLISHING;
        return $this->render('update', [
            'model' => $model,
            'blockIds' => $blockIds,
            'liveAt' => date('Y-m-d H:i:s', $liveAtTime),
        ]);
    }

    public function actionSave($id, $draft = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->post('Post')) {
            $postQuery = Post::find()->where(['id' => $id]);
            if (!Yii::$app->user->can('admin')) {
                $postQuery->andWhere(['user_id' => Yii::$app->user->id]);
            }
            /**
             * @var $model Post
             */
            $model = $postQuery->one();
            if ($model === null) {
                throw new NotFoundHttpException('Post not found');
            }

            $model->load(Yii::$app->request->post());
            if ($draft === null && $model->saveLive()) {

                Yii::$app->session->setFlash('success', 'Post saved');
                return [
                    'status' => 'ok',
                    'postUrl' => $model->getUrl(true)
                ];

            } else if ($draft && $model->saveDraft()) {

                return [
                    'status' => 'ok',
                    'post' => $model,
                    'postBlocks' => $model->postBlocks
                ];

            } else {

                return [
                    'status' => 'error',
                    'errors' => $model->getAttributedErrors() + $model->getRelatedErrors()
                ];

            }
        }

        return [
            'status' => 'error',
            'message' => 'Post data not found'
        ];
    }

    public function actionUploadVideo($dynamicPath = null)
    {
        new MediaUploadHandler([
            'param_name' => 'loadedVideos',
            'upload_dir' => VideoHelper::getTmpVideoPath(true, $dynamicPath),
            'accept_file_types' => '/\.(' . implode('|', VideoHelper::$videoExtetsions) . ')$/i',
        ]);
    }

    public function actionStopUploadVideo($dynamicPath, $fileName)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $imageFileName = VideoHelper::getTmpFileName($dynamicPath, $fileName);
        if ($imageFileName !== null) {
            VideoHelper::removeTmpPath($imageFileName);
            return [
                'status' => 'success',
                'message' => 'Video deleted'
            ];
        }

        return [
            'status' => 'error',
            'message' => 'Video not found'
        ];
    }


    public function actionUploadImage($dynamicPath = null)
    {
        new MediaUploadHandler([
            'param_name' => 'loadedImages',
            'upload_dir' => ImageHelper::getTmpImagePath(true, $dynamicPath),
            'accept_file_types' => '/\.(' . implode('|', ImageHelper::$imageExtetsions) . ')$/i',
            'image_versions' => [],
        ]);
    }

    public function actionStopUploadImage($dynamicPath, $fileName)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $imageFileName = ImageHelper::getTmpFileName($dynamicPath, $fileName);
        if ($imageFileName !== null) {
            ImageHelper::removeTmpPath($imageFileName);
            return [
                'status' => 'success',
                'message' => 'Image deleted'
            ];
        }

        return [
            'status' => 'error',
            'message' => 'Image not found'
        ];
    }


    public function actionOldItem($alias)
    {
        $key = self::CACHE_PREFIX_OLDITEM . $alias;
        $url = Yii::$app->cache->get($key);
        if ($url === false) {
            $alias = preg_replace('/\/$/', '', $alias);
            /**
             * removing old increment;
             */
            $alias = preg_replace('/-\d+$/', '', $alias);
            $alias = explode('-', $alias);
            $aliasRegex = implode('.+', $alias);
            $model = Post::find()
                ->where(['is_draft' => 0])
                ->andWhere(['RLIKE', 'TRIM(title)', $aliasRegex])
                ->one();

            $url = $model ? $model->getUrl() : '';

            Yii::$app->cache->set($key, $url);
        }

        if ($url === '') {
            throw new NotFoundHttpException('Post not found');
        }
        return $this->redirect($url);
    }

    public function actionItem($id)
    {
        $postQuery = Post::find()
            ->where(['id' => $id])
            ->with(['postBlocks', 'tags']);

        if (!Yii::$app->user->can('admin')) {
            $postQuery->andWhere('(live_at <= NOW() AND is_draft = 0) OR user_id = :user_id', [
                ':user_id' => Yii::$app->user->id
            ]);
            $postQuery->andWhere(['status' => Post::STATUS_ACTIVE]);
        }

        $model = $postQuery->one();
        if ($model === null) {
            throw new NotFoundHttpException('Post not found');
        }

        Yii::$app->view->registerMetaTag(['property' => 'og:type', 'content' => 'article']);
        Yii::$app->view->registerMetaTag(['property' => 'og:title', 'content' => $model->title]);
        Yii::$app->view->registerMetaTag(['property' => 'og:url', 'content' => $model->getUrl(true)]);
        foreach ($model->postBlocks as $postBlock) {
            if ($postBlock->type === PostBlock::BLOCK_TYPE_IMAGE ||
                $postBlock->type === PostBlock::BLOCK_TYPE_VIDEO
            ) {
                if (!empty($postBlock->thumbSrc)) {
                    Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => $postBlock->getThumbSrc('thumb_video')]);
                    Yii::$app->view->registerMetaTag(['property' => 'og:image:width', 'content' => 640]);
                    Yii::$app->view->registerMetaTag(['property' => 'og:image:height', 'content' => 360]);
                    break;
                } else if (!empty($postBlock->imgSrc)) {
                    Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => $postBlock->getImgSrc('thumb_video')]);
                    Yii::$app->view->registerMetaTag(['property' => 'og:image:width', 'content' => 640]);
                    Yii::$app->view->registerMetaTag(['property' => 'og:image:height', 'content' => 360]);
                    break;
                }
            }
        }
        Yii::$app->view->registerMetaTag(['property' => 'og:site_name', 'content' => Yii::$app->name]);

        return $this->render('item', [
            'model' => $model
        ]);
    }

    public function actionPreview($id)
    {
        $model = Post::find()
            ->where(['id' => $id, 'user_id' => Yii::$app->user->id])
            ->with(['postBlocks', 'tags'])
            ->one();

        if ($model === null) {
            throw new NotFoundHttpException('Post not found');
        }

        return $this->render('item', [
            'model' => $model
        ]);
    }

    public function actionValidateVideo()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [
            'status' => 'error',
            'message' => 'PostBlock undefined',
        ];

        if (Yii::$app->request->post('PostBlock')) {
            $model = new PostBlock();
            $model->type = PostBlock::BLOCK_TYPE_VIDEO;
            $model->attributes = Yii::$app->request->post('PostBlock');
            if ($model->validate('video_url')) {
                $response = [
                    'status' => 'ok',
                    'type' => $model->videoType,
                    'image' => $model->getVideoImage(),
                    'video' => $model->videoUrl
                ];
            } else {
                $errors = $model->getErrors('video_url');
                $response = [
                    'status' => 'error',
                    'message' => isset($errors[0]) ? $errors[0] : 'Unsupported url',
                    'errors' => $errors
                ];
            }
        }

        return $response;
    }


    public function actionMakeSeen()
    {
        $id = Yii::$app->request->post('post_id');
        if (empty($post = Post::find()->where($id)->asArray()->scalar())) {
            throw new NotFoundHttpException("Post with id=$id not found");
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = Yii::$app->user->identity->addPostToSeen($id);

        if ($model->hasErrors()) {
            return [
                'status' => 'error',
                'errors' => $model->getErrors()
            ];
        }
        return [
            'status' => 'ok'
        ];
    }

    public function actionToggleFavorite()
    {
        if (Yii::$app->user->isGuest) {
            if (Yii::$app->request->isAjax) {
                throw new UnauthorizedHttpException;
            } else {
                Yii::$app->session->set('referrerUrl', Yii::$app->request->referrer);
                return Yii::$app->user->loginRequired();
            }
        }

        if (Yii::$app->request->isAjax) {
            $id = Yii::$app->request->post('post_id');
        } else {
            $id = Yii::$app->request->get('post_id');
        }

        if (empty($post = Post::find()->where($id)->asArray()->scalar())) {
            throw new NotFoundHttpException("Post with id=$id not found");
        }

        $res = Yii::$app->user->identity->toggleFavoritePost($id, !Yii::$app->request->isAjax);

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            if (is_array($res)) {
                return [
                    'status' => 'error',
                    'errors' => $res
                ];
            }
            return [
                'status' => 'ok',
                'result' => $res
            ];
        } else {

            if ($res == -1) {
                Yii::$app->session->setFlash('success', 'You have already added this post to favorite');
            }

            $refUrl = Yii::$app->session->get('referrerUrl');
            if (!empty($refUrl)) {
                return $this->redirect($refUrl);
            }
            return $this->goHome();
        }
    }

    public function actionGetSeenIds()
    {
        $ids = UserSeenPost::find()->where('user_id=:user_id', ['user_id' => Yii::$app->user->id])
            ->asArray()->select('id')->column();

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $ids;
    }

}
