<?php
namespace app\controllers\admin;


use yii\base\Action;
use yii\base\Exception;
use yii\helpers\Json;

abstract class PostGridAction extends Action
{
    public $viewName = 'index';
    public $modelClass;
    public $redirectSuccess = ['index'];
    public $appliedActionName = 'processed';

    public function run()
    {
        $ids = explode(',', \Yii::$app->request->post('ids'));

        $processedIds = [];
        $unProcessedIds = [];

        try{
            foreach ($ids as $id){
                $model = call_user_func([$this->modelClass, 'findOne'], $id);
                if($model && $this->processModel($model)){
                    $processedIds[] = $id;
                }else{
                    $unProcessedIds[] = $id;
                }
            }
            if(!empty($processedIds)){
                \Yii::$app->session->setFlash('success', "Models with ids:" . implode($processedIds,',') . " were " . $this->appliedActionName);
            }
            if(!empty($unProcessedIds)){
                \Yii::$app->session->setFlash('error', "Models with ids:" . implode($unProcessedIds,',') . " were NOT " . $this->appliedActionName);
            }
        }catch (Exception $e){
            \Yii::$app->session->setFlash('error', $e->getMessage());
        }

        if(\Yii::$app->request->isAjax){
            return Json::encode(compact('processedIds', 'unprocessedIds'));
        }
        return $this->controller->redirect($this->redirectSuccess);
    }

    abstract protected function processModel($model);
}