<?php
namespace app\controllers\admin;

class PurgeAction extends PostGridAction
{
    public $appliedActionName = 'purged';

    protected function processModel($model)
    {
        return $model->delete();
    }
}