<?php

namespace app\assets;


use yii\web\AssetBundle;

class AlbumAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/album.css',
    ];

    public $js = [
        'js/album.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
        'app\assets\BootboxAsset',
        'app\assets\ShareImagesModalAsset',
        'app\assets\SelectionBarAsset',
        'app\assets\ImageListAsset',
        'app\assets\BlueimpGalleryAsset',
        'app\assets\PostAsset',
        'app\assets\AlbumLockerAsset',
    ];
    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}