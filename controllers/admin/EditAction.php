<?php

namespace app\controllers\admin;

use Yii;
use yii\base\Action;
use yii\base\ActionEvent;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\web\HttpException;

/*
 * todo: add closure param support
 */
class EditAction extends Action {

	const BEFORE_EDIT_SAVE = 'beforeEditSave';
	const AFTER_EDIT_SAVE = 'afterEditSave';
	const BEFORE_EDIT_RENDER = 'beforeEditRender';

	public $viewName = 'edit';
	public $modelClass;
	public $scenario = 'default';
	public $redirectSuccessSave = ['index'];

	public $messageSuccessSave = 'Row update';
	public $messageNotFound = 'Row not found';

	public function init() {
		if ($this->modelClass === null) {
			throw new InvalidConfigException('Model class must be setted');
		}

		parent::init();
	}

	public function run($id) {
		/** @var ActiveRecord $model */
		if ($model = call_user_func([$this->modelClass,'findOne'],$id)) {
			$model->setScenario($this->scenario);

			if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
				if ($this->beforeSave($model) && $model->save()) {
					Yii::$app->session->setFlash('success', $this->messageSuccessSave);

					$this->afterSave($model);
					return $this->controller->redirect($this->redirectSuccessSave);
				}
			}

			if ($model->hasErrors()) {
				Yii::$app->session->setFlash('error', Html::errorSummary($model));
			}

			return $this->controller->render($this->viewName, $this->beforeRender(compact('model')));
		}

		throw new HttpException(404, $this->messageNotFound);
	}

	public function beforeSave($result) {
		$event = new ActionEvent($this);
		$event->result = $result;
		$this->controller->trigger(self::BEFORE_EDIT_SAVE, $event);
		return $event->isValid;
	}

	public function afterSave($result) {
		$event = new ActionEvent($this);
		$event->result = $result;
		$this->controller->trigger(self::AFTER_EDIT_SAVE, $event);
		return $event->result;
	}

	public function beforeRender($result) {
		$event = new ActionEvent($this);
		$event->result = $result;
		$this->controller->trigger(self::BEFORE_EDIT_RENDER, $event);
		return $event->result;
	}

}