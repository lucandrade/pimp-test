<?php

namespace app\assets;

use yii\web\AssetBundle;


class DatetimepickerAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/plugins/bootstrap-datetimepicker.css',
    ];
    public $js = [
        'js/plugins/moment.js',
        'js/plugins/bootstrap-datetimepicker.js',
    ];
    public $depends = [
        'app\assets\AppAsset'
    ];
    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}