<div class="top-post-info <?= $model->uploadedByGuest() ? "" : "post-by-user" ?>">
        <?php if(!$model instanceof \app\models\Album || !$model->getIsUnsorted()): ?>
            <span class="post-date" title="<?= $model->createTime ?>">
                Uploaded
                <?= \Yii::$app->formatter->asRelativeTime($model->createTime) ?>
            </span>
            <span class="post-date">
                Views :
                <?php
                $stats = $model->getViewsStatistic();
                echo \app\widgets\PopoverViewsStats::widget([
                    'stats' => $stats,
                    'content'=> $stats['unique']['total']
                ]);
                ?>
            </span>
        <?php endif; ?>
        <?php if($model instanceof \app\models\Album): ?>
            <?= \app\widgets\AlbumSizeMetrics::widget([
                'size' => $model->size,
                'count' => $totalCount
            ]) ?>
        <?php endif; ?>

</div>