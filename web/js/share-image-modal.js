var shareModalWindow = (function($){

    var $modal;
    var sourceRows, sourceHiTaRows, addLinkSource, isInited = false;
    var init = function(){
        $modal = $('#share-modal');
        $modal.on('shown.bs.modal', function(){
            $modal.shareCodes(sourceRows, sourceHiTaRows, addLinkSource);
            $modal.css('opacity', 1);
        });

        $modal.on('show.bs.modal', function(){
            $modal.css('opacity', 0);
        });

        $modal.on('hide.bs.modal', function(){
            $modal.shareCodes('reset');
        });
        isInited = true;
    };

    init();

    return {
        loadAndShow:function(ids, type, $elem){
            var that = this, data;
            var url = $modal.data(type + '-codes-url');
            if(!url){
                throw "Wrong type must be 'album' or 'image'";
            }

            if(Object.prototype.toString.call(ids) === '[object Array]'){
                ids = ids.join(',');
            }

            if($elem && (data = $elem.data('codes'))){
                that.show(data.codes, data.hiTaRows, data.addLink);
            }else{
                $.ajax({
                    url:url,
                    data:{ids:ids},
                    dataType:'json',
                    success:function(res){
                        if(res.status && res.status == 'ok')
                        {
                            var hiTaRows = res.hiTaRows ? res.hiTaRows : undefined,
                                addLink = res.addLink ? res.addLink : undefined;

                            if($elem){
                                $elem.data('codes', res);
                            }

                            that.show(res.codes, hiTaRows, addLink);
                        }
                        else{
                            alert('Something went wrong, please try again later');
                            console.log(res);
                        }
                    },

                    error:function(){
                        alert('error');
                    }
                });
            }

        },

        show:function(rows, hiTaRows, addLink){
            if(!isInited){
                init();
            }
            sourceRows = rows;
            sourceHiTaRows = hiTaRows;
            addLinkSource = addLink;
            $modal.modal('show');
        },
        reInit:function(){
            init();
        }
    };
})(jQuery);