<?php

namespace app\commands;


use app\helpers\ArrayHelper;
use app\models\Album;
use app\models\Image;
use app\models\PahUser;
use app\models\User;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\console\Controller;
use yii\console\controllers\MigrateController;
use yii\db\ActiveQuery;
use yii\db\Connection;
use yii\db\Query;
use yii\db\Transaction;
use yii\helpers\Console;
use yii\helpers\Json;


class ImportController extends Controller
{

    const PAGE_SIZE = 50;


    /**
     * @var Connection
     */
    public $sourceDb;

    /**
     * @var Connection
     */
    public $distDb;

    /**
     * @var Transaction
     */
    protected $transaction;

    protected $usersImported = 0;
    protected $imagesImported = 0;
    protected $albumsImported = 0;


    protected $totalUsers;
    protected $totalImages;
    protected $totalAlbums;

    protected $lastIds = [];

    public $debug;

    public function options($actionID)
    {
        return ['debug'];
    }

    public function init(){

        $this->sourceDb = \Yii::$app->pahDb;
        $this->distDb = \Yii::$app->db;
    }

    public function actionImportPaging()
    {
        $offset = 0;

        $this->prepareTables();
        echo "Starting import...\n";
        echo "Counting data to be imported...\n";

        $this->totalUsers = $this->sourceDb->createCommand("select count(id) FROM User")->queryScalar();
        $this->totalAlbums = $this->sourceDb->createCommand("select count(id) FROM Album")->queryScalar();
        $this->totalImages = $this->sourceDb->createCommand("select count(id) FROM Image")->queryScalar();

        echo "Users/Albums/Images will be imported: $this->totalUsers/$this->totalAlbums/$this->totalImages";
        echo ". Importing data...\n";

        do{
            $oldUsers = PahUser::find()->offset($offset)->asArray()->limit(self::PAGE_SIZE)->all();

            if($this->debug){
                echo $this->ansiFormat(count($oldUsers) . " old users were quired for import\n");
            }

            foreach($oldUsers as $oldUser)
            {
                if($user = User::find()->select('id')
                    ->where(['email'=>$oldUser['email']])->asArray()->scalar()){
                    continue;
                }

                $this->transaction = $this->distDb->beginTransaction();
                $oldUserId = $oldUser['id'];
                unset($oldUser['password'], $oldUser['password_md5']);
                try{
                    $userSaved = true;
                    if($oldUser['id'] != User::ANONYMOUS_ID)
                    {
                        $user = new User();
                        $user->setAttributes($oldUser, false);
                        $user->generateAuthKey();
                        $userSaved = $user->save(false);
                    }
                    if($userSaved)
                    {
                        $this->importAlbums($oldUser['id']);
                        $this->transaction->commit();
                        $this->usersImported++;
                    }
                }catch (Exception $e){
                    $this->transaction->rollBack();
                    echo $this->ansiFormat("\nException while importing user $oldUserId: {$e->getMessage()}" . PHP_EOL, Console::FG_RED);
                }
            }

            $offset += self::PAGE_SIZE;
        }while(count($oldUsers) == self::PAGE_SIZE);

        echo $this->ansiFormat("\n\nImport is completed!\n", Console::FG_GREEN);
        echo $this->ansiFormat("Total imported users/albums/images $this->usersImported($this->totalUsers)/$this->albumsImported($this->totalAlbums)/$this->imagesImported($this->totalImages)\n");
    }


    protected function importAlbums($oldUserId)
    {
        $offset = 0;
        $pageSize = self::PAGE_SIZE;

        do {
            $oldAlbums = $this->sourceDb
                ->createCommand("SELECT * FROM Album WHERE authorId=:id LIMIT $offset,$pageSize",[':id'=>$oldUserId])->queryAll();

            if($this->debug){
                echo $this->ansiFormat(count($oldAlbums) . " old albums were queried for import\n");
            }


            foreach($oldAlbums as $oldAlbum)
            {
                $oldAlbumId = $oldAlbum['id'];
                $album = new Album();
                $album->setAttributes($oldAlbum, false);

                if($oldAlbum['status'] == 2 && !empty($oldAlbum['privateKey'])){
                    $album->setPassword($oldAlbum['privateKey']);
                }

                $album->createTime = date('Y-m-d H:i:s', $oldAlbum['createTime']);
                $album->updateTime = date('Y-m-d H:i:s', $oldAlbum['updateTime']);
                if($album->save(false))
                {
                    $this->importImages($oldAlbumId);
                    $this->albumsImported++;
                }else{
                    throw new \yii\db\Exception("Can't create album record from old album #$oldAlbumId");
                }
            }

            $offset += self::PAGE_SIZE;
        }while(count($oldAlbums) == self::PAGE_SIZE);

    }

    protected function importImages($oldAlbumId)
    {
        $offset = 0;
        $pageSize = self::PAGE_SIZE;

        do{
            $oldImages = $this->sourceDb
                ->createCommand("SELECT * FROM Image WHERE albumId=:albumId LIMIT $offset,$pageSize", [':albumId' => $oldAlbumId])
                ->queryAll();

            if($this->debug){
                echo $this->ansiFormat(count($oldImages) . " old images were queried for import\n");
            }

            foreach($oldImages as $oldImage)
            {
                $image = new Image();
                $oldImageId = $oldImage['id'];

                $image->setAttributes($oldImage, false);

                $image->createTime = date('Y-m-d H:i:s', $oldImage['createTime']);
                $image->updateTime = date('Y-m-d H:i:s', $oldImage['updateTime']);

                if($image->save(false))
                {
                    $this->imagesImported++;
                    $usersImported = $this->usersImported + 1;
                    $albumsImported = $this->albumsImported + 1;
                    echo $this->ansiFormat("\rTotal imported users/albums/images $usersImported($this->totalUsers)/$albumsImported($this->totalAlbums)/$this->imagesImported($this->totalImages)");
                }
                else{
                    throw new \yii\db\Exception("Can't import image #$oldImageId");
                }
            }
            $offset += self::PAGE_SIZE;
        }while(count($oldImages) == self::PAGE_SIZE);

    }

    protected function prepareTables()
    {
        $tables = ['User', 'Album', 'Image'];
        foreach($tables as $table)
        {
           $this->setAutoIncrementId($table);
        }
    }

    protected function setAutoIncrementId($table)
    {
        $maxId = $this->sourceDb->createCommand("select max(id) FROM $table")->queryScalar();
        $maxId++;
        $table = strtolower($table);
        $this->distDb->createCommand("ALTER TABLE " . strtolower($table) . " AUTO_INCREMENT = $maxId")->execute();

    }


    protected function copyTable($table)
    {
        echo $this->ansiFormat("Copying table '$table'...");
        $distTable = $this->getDbName($this->distDb) . "." . strtolower($table);
        $sourceTable = $this->getDbName($this->sourceDb) . ".$table";

        $timeConverting = $table == 'User' ? "": ",FROM_UNIXTIME(createTime) as ct_buf, FROM_UNIXTIME(updateTime) ut_buf";

        $lastId = $this->lastIds[$table];
        $this->distDb->createCommand("CREATE TABLE $distTable SELECT *$timeConverting FROM $sourceTable WHERE id <= $lastId")
            ->execute();

        echo $this->ansiFormat("\nTable '$table' was successfully copied\n", Console::FG_GREEN);
    }

    protected function getDbName($db)
    {
        if(preg_match("/dbname=(.*)/", $db->dsn, $matches) && isset($matches[1])){
            return $matches[1];
        }else{
            throw new InvalidConfigException("Wrong dsn format for dist db");
        }
    }

    protected function purgeDistDb()
    {
        $dbName = $this->getDbName($this->distDb);
        echo $this->ansiFormat("\nPurging db $dbName...");
        $this->distDb->createCommand("DROP DATABASE $dbName; CREATE DATABASE $dbName CHARACTER SET utf8 COLLATE utf8_general_ci; USE $dbName;")->execute();
        echo $this->ansiFormat("ok\n", Console::FG_GREEN);
    }

    public function actionIndex()
    {
        $this->purgeDistDb();

        $tables = ['User', 'Album', 'Image'];

        foreach($tables as $table){
            $this->lastIds[$table] = (new Query())->select('id')->from($table)->max('id', $this->sourceDb);
        }

        foreach($tables as $table){
            $this->copyTable($table);
        }

        $this->modifyMainTables();
        $this->applyOtherMigrations();
        $this->modifyData();
        echo $this->ansiFormat("\nImport completed\n", Console::FG_GREEN);
    }

    protected function modifyMainTables()
    {
        echo $this->ansiFormat("\nModifying table `user`...");
        $this->distDb->createCommand("ALTER TABLE `user`
            MODIFY COLUMN `id` int(11) primary KEY NOT NULL AUTO_INCREMENT,
            DROP COLUMN password_md5,
            ADD COLUMN `rating` float NOT NULL DEFAULT '0',
            ADD COLUMN `activkey` varchar(128) DEFAULT NULL,
            ADD COLUMN `status` int(1) DEFAULT NULL,
            ADD COLUMN `is_activated_email` int(1) DEFAULT NULL,
            ADD COLUMN `is_admin` int(1) DEFAULT NULL,
            ADD COLUMN `email_activation_key` varchar(100) DEFAULT NULL,
            ADD COLUMN `current_oauth_provider` varchar(255) DEFAULT NULL,
            ADD COLUMN `password_reset_token` varchar(255) DEFAULT NULL,
            ADD COLUMN `created_at` DATETIME DEFAULT NULL ,
            ADD COLUMN `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            ADD INDEX (`email` , `status`),
            ADD INDEX (`username`, `status`),
            ADD INDEX (`money_make`),
            ADD COLUMN `lastvisit_at` DATETIME  DEFAULT NULL;"
        )->execute();

        $this->distDb->createCommand("UPDATE `user` set `password`='', `is_activated_email`=1, `status`=" . User::STATUS_ACTIVE)->execute();

        echo $this->ansiFormat("ok");

        echo $this->ansiFormat("\nModifying table `album`...");
        $this->distDb->createCommand("ALTER TABLE `album`
            MODIFY COLUMN `id` int(11) primary KEY NOT NULL AUTO_INCREMENT,
            DROP COLUMN `tags`,
            DROP COLUMN `updateTime`,
            DROP COLUMN `createTime`,
            ADD COLUMN `rating` float NOT NULL DEFAULT '0',
            ADD COLUMN `totalViews` int(11) NOT NULL DEFAULT '0',
            ADD COLUMN `todayViews` int(11) NOT NULL DEFAULT '0',
            ADD INDEX (`authorId`),
            ADD COLUMN `password` varchar(255) DEFAULT NULL,
            MODIFY  `imageCount` int(11) DEFAULT NULL,
            MODIFY  `imageSize` int(11) DEFAULT NULL,
            CHANGE  `ut_buf` `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            CHANGE  `ct_buf` `createTime` datetime DEFAULT NULL;"
        )->execute();
        echo $this->ansiFormat("ok");

        echo $this->ansiFormat("\nModifying table `image`...");
        $this->distDb->createCommand("ALTER TABLE `image`
            MODIFY COLUMN `id` int(11) primary KEY NOT NULL AUTO_INCREMENT,
            DROP COLUMN `tags`,
            DROP COLUMN `updateTime`,
            DROP COLUMN `createTime`,
            ADD COLUMN `rating` float NOT NULL DEFAULT '0',
            ADD COLUMN `totalViews` int(11) NOT NULL DEFAULT '0',
            ADD COLUMN `todayViews` int(11) NOT NULL DEFAULT '0',
            ADD COLUMN `purgedDate` datetime DEFAULT NULL,
            ADD INDEX `hash`(`hash`),
            ADD INDEX `authorId` (`albumId`, `authorId`, `status`, `createTime`),
            ADD INDEX (`authorId`, `status`),
            ADD INDEX (`albumId`, `status`),
            MODIFY COLUMN `albumId` int(11),
            CHANGE  `ut_buf` `updateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            CHANGE  `ct_buf` `createTime` datetime DEFAULT NULL;"
        )->execute();
        echo $this->ansiFormat("ok");
    }




    protected function applyOtherMigrations()
    {
        echo $this->ansiFormat("\nApplying other migrations after copying ...");
        $sql = file_get_contents(\Yii::getAlias("@app/db/scripts/after_copy_main_tables.sql"));
        $res = $this->distDb->createCommand($sql)->execute();

        $sql = file_get_contents(\Yii::getAlias("@app/db/scripts/populate_migrations.sql"));
        $res = $this->distDb->createCommand($sql)->execute();

        echo $this->ansiFormat("ok\n", Console::FG_GREEN);
    }



    protected function modifyData()
    {
        $tables  = [
            'user' =>[
                'query' => User::find(),
                'callback' =>
                    function(User $model){
                        $model->generateAuthKey();
                        $model->generateEmailKey();
                        return $model->save(false);
                    },
            ],

//            'album' => [
//                'query' => Album::find()->where("privateKey!=''"),
//                'callback'=> function(Album $model){
//                    $model->setPassword($model->privateKey);
//                    return $model->save(false);
//                }
//            ]
            'album_wo_create_date' => [
                'query' => Album::find()->where("createTime IS NULL")->with('author'),
                'callback'=> function(Album $model){

                    if(($author = $model->author)
                        && ($forumUser = $author->getForumUser(false))
                        && !empty($forumUser->joindate))
                    {
                        $model->createTime = date('Y-m-d H:i:s');
                    }
                    else{
                        $model->createTime = strtotime("2010-01-01");
                    }

                    return $model->save(false);
                }
            ],

            'unsorted' => [
                'query' => Album::find()->where(['type' => Album::TYPE_UNSORTED]),
                'callback' => function(Album $album){
                    $albumId = $album->id;
                    $count = \Yii::$app->db->createCommand()
                        ->update('image',[
                            'albumId' => NULL
                        ],['albumId' => $albumId])
                        ->execute();

                    echo $this->ansiFormat("\n $count images of unsorted album $albumId were unbound from album\n");
                    try{
                        if($album->delete()){
                            echo $this->ansiFormat("unsorted album $albumId was removed\n");
                            return true;
                        }else{
                            echo $this->ansiFormat("unsorted album $albumId WAS NOT removed because: ".json_encode($album->getErrors()) . "\n", Console::FG_RED);
                            return false;
                        }
                    }catch (Exception $e){
                        echo $this->ansiFormat("unsorted album $albumId WAS NOT removed because:Exception '".$e->getMessage() . "'\n", Console::FG_RED);
                    }
                }
            ],

            'trashes' => [
                'query' => Album::find()->where(['type' => Album::TYPE_TRASH]),
                'callback' => function(Album $album){

                    try{
                        $count = \Yii::$app->db->createCommand()
                            ->update('image',[
                                'status' => Image::STATUS_DELETED,
                                'albumId' => null
                            ],['albumId' => $album->id])
                            ->execute();
                    }catch (Exception $e){
                        echo $this->ansiFormat("\n Exception while reformating trash: {$e->getMessage()} \n");
                        return false;
                    }

                    echo $this->ansiFormat("\n $count trash images of user $album->authorId were reformated\n");

                    return true;
                }
            ]
        ];

        foreach($tables as $dataCategory => $object){
            $this->modifyTableData($object['query'], $object['callback'], $dataCategory);
        }
    }





    protected function modifyTableData(ActiveQuery $query, $modifyCallback, $dataCategory)
    {
        $offset = 0;
        $totalItems = $query->count('id');
        $totalModified = 0;
        echo $this->ansiFormat("Modifying $totalItems items of data category '" . $dataCategory. "''...\n");
        do{
            $items = $query->limit(self::PAGE_SIZE)->offset($offset)->all();

            foreach($items as $item){
                if(call_user_func($modifyCallback, $item)){
                    $totalModified++;
                    echo $this->ansiFormat("\rProcessed $totalModified of $totalItems (" .$totalModified*100/$totalItems. "%)");
                }else{
                    echo $this->ansiFormat("\n Can't modify item #$item->id of $dataCategory because of "
                        . Json::encode($item->getErrors()));
                }
            }

            $offset += self::PAGE_SIZE;

        }while(count($items) === self::PAGE_SIZE);

        echo $this->ansiFormat("\nProccessing data for category '" . $dataCategory ."' completed. $totalModified of $totalItems were modified\n", Console::FG_GREEN);
    }



    public function actionImportUnsorted()
    {
        $offset = 0;
        $pageSizeAlbum = 50;
        $pageSizeImage = 100;

        do {

            $oldAlbums = (new Query())
                ->from('Album')
                ->where(['type'=>Album::TYPE_UNSORTED])
                ->indexBy('id')
                ->limit($pageSizeAlbum)
                ->offset($offset)
                ->all($this->sourceDb);

            if($this->debug){
                echo $this->ansiFormat(count($oldAlbums) . " unsorted albums were queried for import\n");
            }

            $oldIds = array_keys($oldAlbums);

            $currentIds = Album::find()->where(['id'=>$oldIds])->select('id')->asArray()->column();

            $notFoundIds = array_diff($oldIds, $currentIds);
            

            foreach($notFoundIds as $id){
            		if($oldAlbums[$id]['authorId'] == 1) continue;
            
                $album = new Album();
                
                $tr = $this->distDb->beginTransaction();

                $album->setAttributes([
                   'id' => $id,
                    'title' => $oldAlbums[$id]['title'],
                    'createTime' => date('Y-m-d H:i:s', $oldAlbums[$id]['createTime']),
                    'updateTime' => date('Y-m-d H:i:s', $oldAlbums[$id]['updateTime']),
                    'authorId' => $oldAlbums[$id]['authorId'],
                    'privateKey' => $oldAlbums[$id]['privateKey'],
                    'status' => $oldAlbums[$id]['status'],
                    'type' => Album::TYPE_UNSORTED
                ], false);

                try{
                    $album->save(false);
                    $imageQuery = Image::find()
                        ->where(['authorId'=>$album->authorId, 'albumId' => null])
                        ->select('id')
                        ->asArray()
				                ;

                    $totalMoved = 0;

                    do{
                        $imageIds = $imageQuery
                            ->limit($pageSizeImage)
                            ->column($this->distDb);

                        $res = 0;

                        if(!empty($imageIds)){
                            $res = Image::updateAll([
                                'albumId' => $album->id
                            ],['id'=>$imageIds]);
                        }

                        $totalMoved += $res;

                    }while(count($imageIds) == $pageSizeImage);
                    $tr->commit();
                    echo $this->ansiFormat("\nAlbum #$album->id was created. $totalMoved images were moved to it");

                }catch (Exception $e){
                    $tr->rollBack();
                    echo $this->ansiFormat("\nCan't process album $id. Error:" . $e->getMessage());
                }
            }

            $offset += $pageSizeAlbum;
        }while(count($oldAlbums) == $pageSizeAlbum);
    }

    public function actionFixPrivate()
    {
        $offset = 0;
        $pageSizeAlbum = 50;
        $pageSizeImage = 100;

        do {
            $albums = Album::find()
													->where(['or', ['status' => Album::STATUS_PRIVATE],
																				 'privateKey!=\'\'',
																				 ['and', ['not', ['password' => null]], 'password!=\'\''],
																				 ])
													->limit($pageSizeAlbum)
													->offset($offset)
            							->all()
            							;

            foreach($albums as $album){
            		
                $tr = $this->distDb->beginTransaction();
                try{
                    if(!$album->privateKey && !$album->password) {
                    	$album->privateKey = 'changed due to system does not support passwordless private albums anymore';
                    	$album->save(false);
	                    echo $this->ansiFormat("\nAlbum #$album->id password changed");
                    }
                    
                    $imageQuery = Image::find()
                        ->where(['albumId' => $album->id, 'status' => Image::STATUS_NORMAL])
                        ->select('id')
                        ->asArray()
				                ;

                    $totalMoved = 0;

                    do{
                        $imageIds = $imageQuery
                            ->limit($pageSizeImage)
                            ->column($this->distDb);

                        $res = 0;

                        if(!empty($imageIds)){

                            $res = Image::updateAll([
                                'status' => Image::STATUS_PRIVATE
                            ],['id'=>$imageIds]);
                        }
                        $totalMoved += $res;

                    }while(count($imageIds) == $pageSizeImage);


                    $tr->commit();

                    echo $this->ansiFormat("\nAlbum #$album->id was processed. $totalMoved images were converted");

                }catch (Exception $e){
                    $tr->rollBack();
                    echo $this->ansiFormat("\nCan't process album $id. Error:" . $e->getMessage());
                }
            }

            $offset += $pageSizeAlbum;
        }while(count($albums) == $pageSizeAlbum);
    }

}