<?php

use yii\db\Migration;

class m160812_084834_add_old_fields_to_new_user_table extends Migration
{
    public  $table = "user";

    public function up()
    {
        $this->addColumn($this->table, 'profile', $this->text());
        $this->addColumn($this->table, 'settings', $this->text());

        $this->addColumn($this->table, 'gender', $this->boolean());
        $this->addColumn($this->table, 'money_make', $this->boolean());

        $this->addColumn($this->table, 'payout_type', $this->string(20));
        $this->addColumn($this->table, 'wallet', $this->string());
        $this->addColumn($this->table, 'full_name', $this->string());
        $this->addColumn($this->table, 'icq', $this->string(15));
        $this->addColumn($this->table, 'payment_address', $this->string());
        $this->addColumn($this->table, 'payment_city', $this->string(100));
        $this->addColumn($this->table, 'payment_postcode', $this->string(10));
        $this->addColumn($this->table, 'payment_country', $this->string(100));
        $this->addColumn($this->table, 'payment_phone', $this->string(20));
    }

    public function down()
    {
        $names = ['profile', 'settings', 'gender', 'money_make',
            'payout_type', 'wallet', 'full_name', 'icq', 'payment_address', 'payment_city', 'payment_postcode','payment_country','payment_phone'];

        foreach($names as $name){
            $this->dropColumn($this->table, $name);
        }


    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
