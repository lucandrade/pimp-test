<?php

namespace app\helpers;

use Yii;
use yii\base\Exception;

class ImageHelper
{
    public static $randomPath;
    public static $imageExtetsions = ['png', 'jpg', 'jpeg', 'gif'];
    public static $imageMimeTypes = ['image/png', 'image/jpeg', 'image/gif'];
    public static $imagePathAlias = '@webroot/../media_protected/tmp/image/';
    public static $logPathAlias = '@app/runtime/logs/imagehelper/';
    public static $defaultSrcFormat = 'default';
    public static $availableFormats = [
        'default',
        'original',
        'adjusted',
        'thumb_50',
        'thumb_video'
    ];

    public static function filterFileName($name)
    {
        $name = filter_var($name, FILTER_SANITIZE_STRING);
        return preg_replace('/\s+/', '', $name);
    }

    public static function getTmpImagePath($useRandomPath = true, $defaultPath = null)
    {
        if ($useRandomPath) {
            if ($defaultPath === null) {

                if (self::$randomPath === null) {
                    self::$randomPath = Yii::$app->security->generateRandomString(10);
                }

            } else {
                self::$randomPath = self::filterFileName($defaultPath);
            }

            $tmpPath = Yii::getAlias(self::$imagePathAlias) . self::$randomPath . DIRECTORY_SEPARATOR;

        } else {
            $tmpPath = Yii::getAlias(self::$imagePathAlias);
        }
        FileHelper::createDirectory(pathinfo($tmpPath, PATHINFO_DIRNAME));
        return $tmpPath;
    }

    public static function getTmpFileName($path, $name)
    {
        $tmpPath = Yii::getAlias(self::$imagePathAlias) . self::filterFileName($path) . DIRECTORY_SEPARATOR;
        $tmpFileName = $tmpPath . self::filterFileName($name);
        return file_exists($tmpFileName) ? $tmpFileName : null;
    }

    public static function getLoadedImages($className, $attribute)
    {
        $path = self::getTmpImagePath(false);
        $images = [];
        if (Yii::$app->request->post($className) && file_exists($path)) {
            $data = Yii::$app->request->post($className);
            foreach ($data as $item) {
                if (
                    isset($item[$attribute]) &&
                    file_exists($path . $item[$attribute]) &&
                    self::isImageExt($item[$attribute])
                ) {
                    $images[$item[$attribute]] = $path . $item[$attribute];
                }
            }
        }
        return $images;
    }

    public static function isImageExt($fileName)
    {
        $ext = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
        return in_array($ext, self::$imageExtetsions);
    }

    public static function getPathPrefix($fileName)
    {
        $fileName = preg_replace('/[^\d\w-]/', '', $fileName);
        $arr = str_split($fileName);
        $result = '';
        $result .= (isset($arr[0]) ? $arr[0] : '_') . '/';
        $result .= (isset($arr[1]) ? $arr[1] : '_') . '/';
        $result .= (isset($arr[2]) ? $arr[2] : '_') . '/';
        $result .= (isset($arr[3]) ? $arr[3] : '_') . '/';
        return $result;
    }

    public static function removeTmpPath($image)
    {
        FileHelper::removeDirectory(pathinfo($image, PATHINFO_DIRNAME));
    }

    public static function convertGifToWebm($sourceFileName, $destFileName, $crf = 4, $maximumBitrate = "2500K")
    {
        if (!$img = @imagecreatefromgif($sourceFileName)) {
            throw new Exception("$sourceFileName is not a gif image!");
        }

        $command = "ffmpeg -i $sourceFileName -c:v libvpx -crf $crf -b:v $maximumBitrate $destFileName 2>&1";
        exec($command, $output);
//        exec("avconv -i $sourceFileName -c:v libvpx -crf $crf -b:v $maximumBitrate $destFileName 2>&1", $output);

        self::log([
            $command,
            $output
        ], 'convertGifToWebm.log');

        return $output;
    }

    public static function convertGifToM4v($sourceFileName, $destFileName)
    {
        if (!$img = @imagecreatefromgif($sourceFileName)) {
            throw new Exception("$sourceFileName is not a gif image!");
        }

        $command = "ffmpeg -i $sourceFileName -movflags +faststart -codec:v libx264 -profile:v baseline -pix_fmt yuv420p -vf \"scale=trunc(iw/2)*2:trunc(ih/2)*2\" $destFileName 2>&1";
        exec($command, $output);

        self::log([
            $command,
            $output
        ], 'convertGifToM4v.log');

        return $output;
    }

    public static function createGifThumbnail($sourceFileName, $destFileName)
    {
        if (!$img = @imagecreatefromgif($sourceFileName)) {
            throw new Exception("$sourceFileName is not a gif image!");
        }

        $commandFrame = "ffmpeg -nostats -i \"$sourceFileName\" -vcodec copy -f rawvideo -y /dev/null 2>&1 | grep frame | awk '{split($0,a,\"fps\")}END{print a[1]}' | sed 's/.*= *//' 2>&1";
        exec($commandFrame, $output);
        self::log([
            'commandFrame' => $commandFrame,
            'output' => $output
        ], 'createGifThumbnail.log');

        if (isset($output[0])) {
            $frame = (int)($output[0] / 2);
            $commandCreate = "ffmpeg -i $sourceFileName -filter:v select=\"eq(n\,$frame)\" -vframes 1 $destFileName 2>&1";
            exec($commandCreate, $outputCreate);

            $output = array_merge($output, $outputCreate);
            self::log([
                $commandCreate,
                $outputCreate
            ], 'createGifThumbnail.log');
        }

        return $output;
    }

    public static function urlExist($url)
    {
        $headers = @get_headers($url);
        if (isset($headers[0]) && strpos($headers[0], '200') === false && strpos($headers[0], '302') === false) {
            return false;
        }
        return true;
    }

    public static function log($message, $fileName = 'image.log')
    {
        LogHelper::log($message, self::$logPathAlias, $fileName);
    }


    public static function compressPng($filename)
    {
        if (!file_exists($filename)) {
            throw new Exception("File does not exist: $filename");
        }
        // escapeshellarg() makes this safe to use with any path
        return ConsoleHelper::exec("pngquant --ext=.png --force " . escapeshellarg($filename));
    }

    public static function sqThm($src, $dest, $width, $height){
        $cmd = \Yii::$app->params['convertCmdPrefix'] .  " convert '".$src."[0]' -resize ".$width."x".$height." -quality 70 -unsharp 0x.5 '".$dest."' ";
        $out = ConsoleHelper::exec($cmd);
        return $out;
    }


    public static function isAnimatedGif( $filename )
    {
        $raw = file_get_contents( $filename );
        $offset = 0;
        $frames = 0;
        while ($frames < 2)
        {
            $where1 = strpos($raw, "\x00\x21\xF9\x04", $offset);
            if ( $where1 === false )
            {
                break;
            }
            else
            {
                $offset = $where1 + 1;
                $where2 = strpos( $raw, "\x00\x2C", $offset );
                if ( $where2 === false )
                {
                    break;
                }
                else
                {
                    if ( $where1 + 8 == $where2 )
                    {
                        $frames ++;
                    }
                    $offset = $where2 + 1;
                }
            }
        }

        return $frames > 1;
    }

}
