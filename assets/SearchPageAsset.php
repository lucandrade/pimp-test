<?php

namespace app\assets;


use yii\web\AssetBundle;

class SearchPageAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
    ];

    public $js = [
        'js/search-page.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
        'app\assets\BootboxAsset',
        'app\assets\ShareImagesModalAsset',
        'app\assets\SelectionBarAsset',
        'app\assets\ImageListAsset',
    ];

    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}