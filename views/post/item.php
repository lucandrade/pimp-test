<?php
/**
 * @var $model app\models\Post
 * @var $tag app\models\Tag
 * @var $this \yii\web\View
 *
 */

use app\components\View;
use app\helpers\WidgetHelper;
use app\models\Post;
use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use yii\helpers\Json;
use yii\widgets\ListView;
use yii\helpers\Url;
use app\assets\PostAsset;
use app\assets\YTExternalAsset;

PostAsset::register($this);
YTExternalAsset::register($this);

$this->registerJs("$('.post-expand-wrapper').lightGallery({
        selector : '.post-block > a'
    });
     pahIniter.widgets.lightGallery($('.post-expand-wrapper'));");

$this->title = Html::encode($model->title) . ' | ' . \Yii::$app->name;

$config = WidgetHelper::getConfig(2);
$ck = WidgetHelper::getScriptInView('//js/widget', ['config' => $config]);
$this->registerJs($ck, View::POS_END);

if (!\Yii::$app->user->isGuest) {
    echo Html::hiddenInput('add-to-favorite-url', Url::to(['post/toggle-favorite']));
    $this->registerJs("$('body').on('click', '.add-to-favorite', addToFavoriteHandler)");
    $this->registerJs('var userOptions = ' . Json::encode(Post::getUserSettings()) . ';', $this::POS_HEAD);
}

$this->registerMetaTag([
    'name' => 'description',
    'content' => \Yii::$app->name . ' - Awesome daily updated Owned Videos, Pictures & Gifs - Visit us to see more!'
], 'description');

$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $model->getTagList(',') ?: 'owned,jokes,fun,humour,funny videos,epic,fail,win,funny pics,like a boss,forever alone,meme,gifs,funny gifs,trolling'
], 'keywords');

$this->params['postModel'] = $model;
?>
<div class="post item-post"
     data-widget="page"
     data-page-key="post_<?= $model->id ?>">
    <?php if ($model->nextPost) { ?>
        <div class="pull-right next-post">
            <a href="<?= $model->nextPost->getUrl() ?>"
               class="btn btn-next"
               title="<?= $model->nextPost->title ?>">
                Next post <i class="fa fa-chevron-right"></i>
            </a>
        </div>
    <?php } ?>
    <div class="clearfix"></div>
    <br>
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="post-header-main">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="widget-rating widget-rating-scores">
                            <div data-rating="page"
                                 data-rating-type="scores"
                                 data-rating-key="post_<?= $model->id ?>"
                                 data-rating-value="<?= $model->rating ?>">
                            </div>
                        </div>
                        <h1 class="p-title">
                            <a href="<?= $model->getUrl(); ?>" class="post-title save-history">
                                <?= Html::encode($model->title) ?>
                            </a>
                        </h1>
                        <?php if (!empty($model->tags)) { ?>
                            <div class="top-tag">
                                <span class="post-tags">
                                    <?= implode(' ', $model->linkedTagList) ?>
                                </span>
                            </div>
                        <?php } ?>
                        <div class="post-admin">
                            <?php if (!empty($model->author)) { ?>
                                <a class="post-author" href="<?= $model->author->profileUrl ?>">
                                    <i>
                                        <img src="<?= $model->author->avatar ?>"
                                             alt="<?= $model->author->avatar ?>"
                                             width="25"
                                             height="25">
                                    </i>
                                    <?= $model->author->username ?>
                                </a>
                            <?php } ?>
                            <span class="post-date" title="<?= $model->live_at ?>">
                                <?= $model->getLiveDate() ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row collapsing-content">
        <div class="col-xs-12 col-sm-12">
            <div class="collapse in post-content-wrapper"
                 id="collapsePost-<?= $model->id ?>"
                 aria-expanded="true">
                <div class="post-expand-wrapper">
                    <?php
                    $blockDataProvider = new ArrayDataProvider([
                        'allModels' => $model->getPostBlocks()->all(),
                        'pagination' => false
                    ]);
                    ?>
                    <?php
                    echo ListView::widget([
                        'dataProvider' => $blockDataProvider,
                        'itemView' => '_post_block',
                        'viewParams' => [
                            'postUrl' => false
                        ],
                        'summary' => false,
                        'emptyText' => false,
                    ]);
                    ?>
                </div>
            </div>
            <div class="post-bottom">
                <div class="pull-right manage-right-icons">
                    <?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->is_admin): ?>
                        <a href="<?= Url::to(["admin/post-to-socials"]); ?>"
                           class="share-to-socials"
                           title="share to socials">
                            <i class="fa fa-2x fa-share"></i>
                        </a>
                    <?php endif; ?>

                    <?php if (Yii::$app->user->can('admin') || $model->checkAuthorAccess()) { ?>
                        <a href="<?= $model->getUpdateUrl(); ?>" class="btn-update-post" title="update post">
                            <i class="fa fa-2x fa-edit"></i>&nbsp
                        </a>
                    <?php } ?>
                </div>
                <span class="widget-rating">
                    <span data-rating="page"
                          data-rating-type="buttons"
                          data-rating-key="post_<?= $model->id ?>"
                          data-rating-value="<?= $model->rating ?>">
                    </span>
                </span>
                <div class="post-btn-group pull-left">
                    <div class="favo">
                        <?php
                        $iconClass = "fa-heart active";
                        if (Yii::$app->user->isGuest || !Yii::$app->user->identity->isFavoritePost($model->id)) {
                            $iconClass = "fa-heart";
                        }
                        ?>
                        <a class="add-to-favorite"
                           title="add to favorites"
                           href="<?= Yii::$app->user->isGuest ? Url::to(['post/toggle-favorite', 'post_id' => $model->id]) : '#' ?>">
                            <i class="fa <?= $iconClass ?>"></i>
                        </a>
                    </div>
                    <div class="comm">
                        <a class="post-comments" href="<?= $model->commentsUrl ?>" title="comments">
                            <i class="fa fa-comment"></i>
                            <span data-page="comments-count"></span>
                        </a>
                    </div>
                    <div class="mys">
                        <div id="rrssb-<?= $model->id ?>-bottom">
                            <span class="btn"
                                  title="share"
                                  data-rrssb="button"
                                  data-rrssb-container="#rrssb-<?= $model->id ?>-bottom"
                                  data-rrssb-url="<?= $model->getUrl(true) ?>"
                                  data-rrssb-title="<?= $model->title ?>"
                                  data-rrssb-image="<?= $model->postImage ?>">
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="comments-block"></div>
    <div data-comment="comment-widget" id="comments"></div>
</div>
