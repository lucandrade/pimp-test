<?php

use yii\db\Migration;
use yii\db\Schema;

class m160225_180000_add_statistic_table extends Migration
{
    public $table = 'statistic';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => Schema::TYPE_PK,
            'type' => $this->string()->notNull(),
            'value' => $this->integer()->notNull()->defaultValue(0),
            'day' => $this->date()->notNull()
        ], $tableOptions);

        $this->createIndex('ix_Type', $this->table, ['type']);
    }

    public function down()
    {
        $this->dropTable($this->table);
    }
}
