<?php

namespace app\helpers;


use yii\helpers\BaseHtmlPurifier;

class HtmlPurifier extends BaseHtmlPurifier
{
    public static function process($content, $config = null)
    {
        return parent::process($content, function($config){
            $config->set('Attr.AllowedFrameTargets', array('_blank', '_self', '_parent', '_top'));
        });
    }
}