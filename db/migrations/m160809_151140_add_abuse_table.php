<?php

use yii\db\Migration;

class m160809_151140_add_abuse_table extends Migration
{
    public $table = 'abuse';
    public $imageRef = 'imageId';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'reporterIp' => $this->string(128),
            'reporterName' => $this->string(128),
            'reporterText' => $this->string(),
            $this->imageRef => $this->integer(11),
        ], $tableOptions);

        $this->createIndex($this->imageRef . "_index", $this->table, $this->imageRef);
        $this->addForeignKey($this->imageRef . "_{$this->table}_fk", $this->table, $this->imageRef,'image', 'id');
    }

    public function down()
    {
        $this->dropForeignKey($this->imageRef . "_{$this->table}_fk", $this->table);
        $this->dropTable($this->table);
    }
}
