<?php

namespace app\assets;


use yii\web\AssetBundle;

class UploaderAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
    ];

    public $js = [
        'js/jquery.pahUploader.js',
    ];

    public $depends = [
        'app\assets\FileuploadAsset'
    ];
    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}