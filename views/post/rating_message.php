<?php
/**
 * @var $this \yii\web\View
 */

$rating = Yii::$app->user->identity->rating;
?>
<div class="alert alert-info">

    <p>Posts per day limited by <strong>your rating: <?= $rating ?></strong></p>
    <br>
    <table class="table table-bordered table-condensed">
        <thead>
        <tr>
            <th>Rating</th>
            <th>Posts limit</th>
        </tr>
        </thead>
        <tbody>
        <tr class="text-center <?= $rating < 200 ? 'success' : '' ?>">
            <td> < 200</td>
            <td>1</td>
        </tr>
        <tr class="text-center <?= $rating >= 200 && $rating < 500 ? 'success' : '' ?>">
            <td>200 - 500</td>
            <td>2</td>
        </tr>
        <tr class="text-center <?= $rating >= 500 && $rating < 1000 ? 'success' : '' ?>">
            <td>500 - 1000</td>
            <td>3</td>
        </tr>
        <tr class="text-center <?= $rating >= 1000 && $rating < 3000 ? 'success' : '' ?>">
            <td>1000 - 3000</td>
            <td>4</td>
        </tr>
        <tr class="text-center <?= $rating >= 3000 ? 'success' : '' ?>">
            <td> > 3000</td>
            <td>more</td>
        </tr>
        </tbody>
    </table>

</div>