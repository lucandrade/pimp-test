<?php

use yii\db\Migration;

class m160905_125935_add_server_field_to_post_block_table extends Migration
{
    public $table = 'post_block';
    public $col = 'server';
    public $status = 'status';


    public function up()
    {
        $this->addColumn($this->table, $this->col, $this->integer()->unsigned());
        $this->addColumn($this->table, $this->status, $this->smallInteger()->unsigned());
    }

    public function down()
    {
        $this->dropColumn($this->table, $this->col);
        $this->dropColumn($this->table, $this->status);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
