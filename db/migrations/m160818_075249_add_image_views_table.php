<?php

use yii\db\Migration;

class m160818_075249_add_image_views_table extends Migration
{
    public $table = 'image_views';
    public $imageRef = 'image_id';


    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            $this->imageRef => $this->integer(),
            'ip' => $this->bigInteger(20),
            'counted_at' => $this->timestamp()
        ], $tableOptions);

        $this->createIndex($this->imageRef . "_index", $this->table, $this->imageRef);
        $this->createIndex('common_index', $this->table, [$this->imageRef, 'ip', 'counted_at']);
        $this->addForeignKey($this->imageRef . "_{$this->table}_fk", $this->table, $this->imageRef,'image', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey($this->imageRef . "_{$this->table}_fk", $this->table);
        $this->dropTable($this->table);
    }
}
