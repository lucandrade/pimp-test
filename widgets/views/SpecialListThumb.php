<?php

use yii\helpers\Html;
use app\helpers\HtmlPurifier;
use yii\helpers\Url;

/**
 * @var $this yii\web\View
 *
 * @var $widget \app\widgets\SpecialListThumb
*/

$widget = $this->context;
$image = $widget->thumb;
if(!empty($widget->emptyThumb) && $count == 0){
    $image = $widget->emptyThumb;
}

?>
<div class="col-md-4 col-lg-3 col-xs-4 col-xxs-6 album-thumb-item special-list-wrapper pull-right">
    <div class="sl-thumb">
        <a href="<?= Url::to($widget->url) ?>" class="special-list-item">
            <?= Html::img("@web/static/i/$image", ['class'=>'img-responsive']) ?>
        </a>
    </div>
    <div class="content-title">
        <div class="album-info">
            <?= $count ?> image<?= $count != 1 ? "s" : "" ?>
        </div>
    </div>
</div>
