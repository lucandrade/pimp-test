<?php

namespace app\widgets;


use app\assets\FullUploaderAsset;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;

class FullUploader extends Widget
{

    public $albumId;
    public $containerId = 'uploader-container';
    public $assetConfig = 'popup-uploader';
    public $modalTitle = 'Upload images';
    public $initAlbumPage = true;

    public $dndShowZone = '.full-uploader-container';
    public $dndShowZoneClass = 'dnd-area-show';


    public $loadAlbumPage = true;

    public $uploaderOptions = [];

    public $defaultUploaderOptions = [
        'registerAsset' => false,
        'buttonOptions' => [
            'id' => 'fileinput-button'
        ],
        'js' => [
            'previewsContainer' => '#album-images'
        ],
    ];


    public function init(){

        $this->uploaderOptions = ArrayHelper::merge($this->defaultUploaderOptions, $this->uploaderOptions);

        if(empty($this->uploaderOptions['uploadUrl'])){
            $this->uploaderOptions['uploadUrl'] =  Url::to(['image/upload-file'], true);
        }
    }


    public function getJsPluginOptions()
    {
        $uploaderOptions = ArrayHelper::merge([
            'albumId' => $this->albumId,
        ], $this->uploaderOptions['js']);

        $pluginOptions = [
            'albumId' => $this->albumId,
            'containerId' => $this->containerId,
            'uploaderOptions' => $uploaderOptions,
            'loadAlbumPage' => $this->loadAlbumPage,
            'initAlbumPage' => $this->initAlbumPage,
            'albumCreateUrl' => Url::to(['album/create-by-uploading']),
            'dndShowZone' => $this->dndShowZone,
            'dndShowZoneClass' => $this->dndShowZoneClass
        ];

        return  Json::encode($pluginOptions);
    }


    public function run()
    {
        $pluginOptions = $this->getJsPluginOptions();

        FullUploaderAsset::register($this->getView());
        $this->getView()->registerJs("$('#$this->containerId').fullPahUploader($pluginOptions)");

        return $this->render('FullUploader');
    }

    protected function getSourceFile($type)
    {
        $configFile = \Yii::getAlias("@app/config/generated-assets/$this->assetConfig.php");
        if(!file_exists($configFile)){
            throw new InvalidConfigException("Config file /$this->assetConfig.php not found");

        }
        $config = require $configFile;
        if(empty($config['all'])
            || empty($config['all']['baseUrl'])
            || empty($config['all'][$type])){
            throw new InvalidConfigException("Wrong format of config file /$this->assetConfig.php not found");
        }

        $config = $config['all'];

        $file = \Yii::getAlias($config['baseUrl'] . "/" . $config[$type][0]);
        return $file;
    }
}