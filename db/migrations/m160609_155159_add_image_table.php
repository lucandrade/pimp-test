<?php

use yii\db\Migration;

class m160609_155159_add_image_table extends Migration
{
    public $table = 'image';
    public $userRef = 'authorId';
    public $albumRef = 'albumId';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'fileName' => $this->string()->notNull(),
            'hash' => $this->string()->notNull(),
            'status' => "tinyint(1) NOT NULL DEFAULT '0'",
            'updateTime' => $this->timestamp(),
            'createTime' => $this->dateTime(),
            'commentCount' => $this->integer()->notNull()->defaultValue(0),
            'size' => $this->integer()->notNull()->defaultValue(0),
            'biggestSize' => $this->boolean(),
            'isGif' => $this->boolean()->defaultValue(0),
            'isAdult' => $this->boolean()->defaultValue(0),
            'removeText' => $this->string(100)->notNull(),
            'server' => $this->integer()->notNull(),
            'ip' => $this->bigInteger(20)->unsigned()->notNull(),
            'country' => $this->string(2),
            $this->userRef => $this->integer(11),
            $this->albumRef => $this->integer(11),
        ], $tableOptions);

        $this->createIndex($this->userRef . "_index", $this->table, $this->userRef);
        $this->addForeignKey($this->userRef . "_{$this->table}_fk", $this->table, $this->userRef,'user', 'id');

        $this->createIndex($this->albumRef . "_index", $this->table, $this->albumRef);
        $this->addForeignKey($this->albumRef . "_{$this->table}_fk", $this->table, $this->albumRef,'album', 'id');
    }

    public function down()
    {
        $this->dropForeignKey($this->userRef . "_{$this->table}_fk", $this->table);
        $this->dropForeignKey($this->albumRef . "_{$this->table}_fk", $this->table);
        $this->dropTable($this->table);
    }
}
