<?php

namespace app\assets;

use yii\web\AssetBundle;

class GCExternalAsset extends AssetBundle
{
    public $css = [
    ];
    public $js = [
        'https://www.gstatic.com/charts/loader.js',
    ];
    public $depends = [
    ];
    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}