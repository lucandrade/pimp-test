<?php

namespace app\helpers;

use Yii;

class CronHelper extends \yii\helpers\FileHelper
{
    public static $pathAlias = '@runtime/cron';
    public static $fileName = 'time.log';


    public static function getLastTime($default, $refresh = true)
    {
        $file = Yii::getAlias(self::$pathAlias) . DIRECTORY_SEPARATOR . self::$fileName;

        if (file_exists($file)) {
            $time = file_get_contents($file);
        } else {
            FileHelper::createDirectory(pathinfo($file, PATHINFO_DIRNAME));
            $time = $default;
        }

        if ($refresh) {
            file_put_contents($file, time());
        }

        return (int)$time;
    }
}