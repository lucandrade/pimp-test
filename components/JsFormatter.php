<?php

namespace app\components;

use yii\web\ResponseFormatterInterface;

class JsFormatter implements ResponseFormatterInterface
{
    public function format($response)
    {
        $response->getHeaders()->set('Content-Type', 'text/javascript; charset=UTF-8');
        if ($response->data !== null) {
            $response->content = $response->data;
        }
    }
}