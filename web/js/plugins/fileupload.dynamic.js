$(function () {
    $.widget('blueimp.fileupload', $.blueimp.fileupload, {
        // Uploads a file in multiple, sequential requests
        // by splitting the file up in multiple blob chunks.
        // If the second parameter is true, only tests if the file
        // should be uploaded in chunks, but does not invoke any
        // upload requests:
        _chunkedUpload: function (options, testOnly) {
            options.uploadedBytes = options.uploadedBytes || 0;
            var that = this,
                file = options.files[0],
                fs = file.size,
                ub = options.uploadedBytes,
                mcs = options.maxChunkSize || fs,
                slice = this._blobSlice,
                dfd = $.Deferred(),
                promise = dfd.promise(),
                jqXHR,
                upload;

            if (!(this._isXHRUpload(options) && slice && (ub || mcs < fs)) ||
                options.data) {
                return false;
            }
            if (testOnly) {
                return true;
            }
            if (ub >= fs) {
                file.error = options.i18n('uploadedBytes');
                return this._getXHRPromise(
                    false,
                    options.context,
                    [null, 'error', file.error]
                );
            }
            /**
             * Use dynamic url from response
             * and cancel loading on list item delete
             */
            var dynamicUrl = options.url;
            var dynamicPath, fileName;
            var liDeleted = false;
            var $li = $('[data-list-item="' + file['list_item'] + '"]');
            if ($li.find('.js-remove').length > 0) {
                el = $li.find('.js-remove').get(0);
                el.removableCall = function () {
                    liDeleted = true;
                    if (
                        (typeof stopUploadVideoUrl !== 'undefined' ||
                        typeof stopUploadImageUrl !== 'undefined') &&
                        typeof  dynamicPath !== 'undefined'
                    ) {
                        var stopUrl = file.type.indexOf('image') === -1 ? stopUploadVideoUrl : stopUploadImageUrl;
                        var params = {
                            'dynamicPath': dynamicPath,
                            'fileName': fileName,
                        };
                        $.ajax({
                            url: stopUrl + '?' + $.param(params),
                            method: 'POST',
                            dataType: 'JSON'
                        });
                    }
                }
            }
            // The chunk upload method:
            upload = function () {
                // Clone the options object for each chunk upload:
                var o = $.extend({}, options),
                    currentLoaded = o._progress.loaded;
                o.url = dynamicUrl;
                o.blob = slice.call(
                    file,
                    ub,
                    ub + mcs,
                    file.type
                );
                // Store the current chunk size, as the blob itself
                // will be dereferenced after data processing:
                o.chunkSize = o.blob.size;
                // Expose the chunk bytes position range:
                o.contentRange = 'bytes ' + ub + '-' +
                    (ub + o.chunkSize - 1) + '/' + fs;
                // Process the upload data (the blob and potential form data):
                that._initXHRData(o);
                // Add progress listeners for this chunk upload:
                that._initProgressListener(o);
                jqXHR = ((that._trigger('chunksend', null, o) !== false && $.ajax(o)) ||
                that._getXHRPromise(false, o.context))
                    .done(function (result, textStatus, jqXHR) {
                        ub = that._getUploadedBytes(jqXHR) ||
                            (ub + o.chunkSize);
                        // Create a progress event if no final progress event
                        // with loaded equaling total has been triggered
                        // for this chunk:
                        if (currentLoaded + o.chunkSize - o._progress.loaded) {
                            that._onProgress($.Event('progress', {
                                lengthComputable: true,
                                loaded: ub - o.uploadedBytes,
                                total: ub - o.uploadedBytes
                            }), o);
                        }
                        options.uploadedBytes = o.uploadedBytes = ub;
                        o.result = result;
                        if (typeof  o.paramName[0] !== 'undefined' &&
                            typeof result[o.paramName[0]][0]['dynamicPath'] !== 'undefined') {
                            fileName = result[o.paramName[0]][0]['name']
                            dynamicPath = result[o.paramName[0]][0]['dynamicPath'];
                            dynamicUrl = options.url + '?dynamicPath=' + dynamicPath;
                        }
                        o.textStatus = textStatus;
                        o.jqXHR = jqXHR;
                        that._trigger('chunkdone', null, o);
                        that._trigger('chunkalways', null, o);
                        if (ub < fs) {
                            // File upload not yet complete,
                            // continue with the next chunk:
                            if (!liDeleted) {
                                upload();
                            }
                        } else {
                            dfd.resolveWith(
                                o.context,
                                [result, textStatus, jqXHR]
                            );
                        }
                    })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        o.jqXHR = jqXHR;
                        o.textStatus = textStatus;
                        o.errorThrown = errorThrown;
                        that._trigger('chunkfail', null, o);
                        that._trigger('chunkalways', null, o);
                        dfd.rejectWith(
                            o.context,
                            [jqXHR, textStatus, errorThrown]
                        );
                    });
            };
            this._enhancePromise(promise);
            promise.abort = function () {
                return jqXHR.abort();
            };
            upload();
            return promise;
        },
    });
});

