<?php

namespace app\commands\actions;


use yii\base\Action;
use yii\base\InvalidConfigException;
use app\helpers\LogHelper;
use yii\helpers\Console;

class Process extends Action
{
    
    public $modelClass;
    public $entityName;
    public $processMethodName = 'ftpProcess';
    
    public function init()
    {
        parent::init();
        if(empty($this->modelClass)){
            throw new InvalidConfigException("'modelClass' for processing model must be set");
        }
    }

    public function run($id)
    {
        $class = $this->modelClass;
        $model = $class::findOne(['id' => $id]);
        
        if($model){
            LogHelper::log("$this->entityName #$id found, uploading to server...",'@runtime/logs/ftp_uploading/', 'uploading.log');
            echo $this->controller->ansiFormat("$this->entityName #$id found, uploading to server..." . PHP_EOL);
            if(call_user_func([$model, $this->processMethodName])){
                echo $this->controller->ansiFormat("$this->entityName with id '$id' was successfully saved to FTP", Console::FG_GREEN);
                LogHelper::log("$this->entityName with id '$id' was successfully saved to FTP...",'@runtime/logs/ftp_uploading/', 'uploading.log');
            }else{
                LogHelper::log("Couldn't process $this->entityName with id '$id' because of:" . \json_encode($model->getErrors()),'@runtime/logs/ftp_uploading/', 'uploading.log');
                echo $this->controller->ansiFormat("Couldn't process $this->entityName with id '$id' because of:" . \json_encode($model->getErrors()), Console::FG_RED);
            }

        }else{
            LogHelper::log("$this->entityName with id '$id' not found", '@runtime/logs/ftp_uploading/', 'uploading.log');
            echo $this->controller->ansiFormat("$this->entityName with id '$id' not found", Console::FG_RED);
        }

        echo PHP_EOL;
    }
}