<?php

namespace app\assets;

use yii\web\AssetBundle;

class NotificationAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/notification.css',
    ];
    public $js = [
        'js/notification.js'
    ];
    public $depends = [
        'app\assets\SiteAsset'
    ];
    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}