<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;


class FilterUserPost extends Model
{
    const DEFAULT_TYPE_VALUE = 'overview';

    public $type;
    public $user;

    /**
     * @var \SphinxClient
     */
    private $_sphinxClient;
    private $_sphinxQuery = '';

    /**
     * @return string
     */
    public function getSphinxQuery()
    {
        return $this->_sphinxQuery;
    }

    /**
     * @return \SphinxClient
     */
    public function getSphinxClient()
    {
        return $this->_sphinxClient;
    }

    /**
     * @param \SphinxClient $sphinxClient
     */
    public function setSphinxClient(\SphinxClient $sphinxClient)
    {
        $this->_sphinxClient = clone $sphinxClient;
    }

    public function filterAttributeParams()
    {
        return [
            'type',
        ];
    }

    public function rules()
    {
        return [
            [['type'], 'in', 'range' => ['commented', 'overview', 'my', 'favorites'], 'skipOnEmpty' => true],
        ];
    }

    public function init()
    {
        $this->loadFromGet();
        $this->unsetInvalidAttributes();

        if (empty($this->type)) {
            $this->type = self::DEFAULT_TYPE_VALUE;
        }

        parent::init();
    }

    protected function loadFromGet()
    {
        foreach ($this->filterAttributeParams() as $attribute => $param) {
            $attribute = is_numeric($attribute) ? $param : $attribute;
            $this->{$attribute} = Yii::$app->request->get($param);
        }
    }

    public function unsetInvalidAttributes()
    {
        $this->validate();
        if ($this->hasErrors()) {
            foreach ($this->getErrors() as $attr => $message) {
                $this->{$attr} = NULL;
            }
        }
    }

    protected function addUserFilter()
    {
        if (Yii::$app->user->identity && !Yii::$app->user->identity->isShowAdult()) {
            $this->_sphinxClient->SetFilter('is_adult_attr', [0]);
        }

        if ($this->type === 'my' && !Yii::$app->user->isGuest) {
            $this->_sphinxClient->SetFilter('user_id_attr', [$this->user->id]);
            
        } else if ($this->type === 'favorites' && !Yii::$app->user->isGuest) {
            $ids = $this->user->getFavoritePostIds();
            $this->_sphinxClient->SetFilter('id_attr', $ids ?: [0]);
            
        } else if ($this->type === 'overview' && !Yii::$app->user->isGuest) {
            $commented_ids = $this->user->getCommentedPost();
            $favorite_ids = $this->user->getFavoritePostIds();
            $own_ids = $this->user->getOwnedPostIds();
            $ids = array_merge($commented_ids, $favorite_ids, $own_ids);
            $ids = array_unique($ids);
            $this->_sphinxClient->SetFilter('id_attr', $ids ?: [0]);
        } else if ($this->type === 'commented' && !Yii::$app->user->isGuest) {
            $ids = $this->user->getCommentedPost();
            $this->_sphinxClient->SetFilter('id_attr', $ids ?: [0]);
        }
    }

    public function applyClientFilters()
    {
        if ($this->_sphinxClient !== null) {
            $this->addUserFilter();
        }
    }

    public function getIndexName()
    {
        if ($this->type === 'my') {

            return 'pah_posts_search pah_posts_search pah_posts_search_future';

        } else if (empty(Yii::$app->params['indexes']['main'])) {

            return 'pah_posts_search pah_posts_search_delta';

        } else {
            return Yii::$app->params['indexes']['main'];
        }
    }

}