<?php

namespace app\widgets;


use yii\base\Widget;

class AlbumSizeMetrics extends Widget
{
    public $size = 0;
    public $count = 0;

    public function run()
    {
        return $this->render("AlbumSizeMetrics");
    }

}