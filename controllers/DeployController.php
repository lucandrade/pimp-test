<?php

namespace app\controllers;

use app\components\Controller;
use app\helpers\ConsoleHelper;
use app\helpers\DeployHelper;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use Yii;
class DeployController extends Controller
{

    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return ArrayHelper::merge([
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'auto' => ['post'],
                ],
            ],

        ], parent::behaviors());
    }

    public function actionAuto()
    {
        $branch = !empty(\Yii::$app->params['autoDeployBranch']) ? \Yii::$app->params['autoDeployBranch'] : "master";

        if(!$this->validateBitbucketSender()){
            throw new NotFoundHttpException("Request not found");
        }

        $json = file_get_contents('php://input', true);
        $payback = Json::decode($json);

        $logFile = \Yii::getAlias("@runtime") . "/logs/deploy.log";
		ConsoleHelper::log($json, $logFile);

        if(empty($payback['push']['changes'][0])){
		    ConsoleHelper::log("Empty changes", $logFile);
            return;
        }
        $change = $payback['push']['changes'][0];
        $update = isset($change['new']['type']) && $change['new']['type'] == 'branch'
                    && isset($change['new']['name']) && $change['new']['name'] == $branch;

        if($update){
            DeployHelper::runPull();
            DeployHelper::runAfterPullScripts();
        }
    }


    protected function validateBitbucketSender()
    {
        $validIP = isset ($_SERVER['REMOTE_ADDR']) && preg_match("/^(131\.103\.20\..+|165\.254\.145.+|104\.192\.143\..+)/",
            $_SERVER['REMOTE_ADDR']);

        return $validIP;
    }

}