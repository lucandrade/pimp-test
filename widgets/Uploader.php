<?php

namespace app\widgets;


use app\assets\FullUploaderAsset;
use app\helpers\ArrayHelper;
use app\helpers\Html;
use app\models\User;
use yii\base\Widget;
use app\models\Album;
use yii\helpers\Url;

class Uploader extends Widget
{
    public $isEnabled;

    public $albumId = false;

    public $albumItems = [];

    public $filesParamName = 'files';

    public $registerAsset = true;

    public $fileInputInsideContainer = true;

    public $uploadUrl = ['image/upload-file'];

    public $disabledBeforeLoad = false;

    public $buttonAlbumSelectorConnector = "&nbsp; to album: &nbsp;";

    protected $defaultButtonOptions = [
        'id' => 'fileinput-button',
        'label' => '<i class="fa fa-cloud-upload"></i> Upload files',
        'class' => 'btn btn-primary btn-lg fileinput-button',
        'title' => 'Select files to upload'
    ];

    public $options = [
        'class' => 'uploader-container'
    ];

    public $buttonOptions = [];


    public function init()
    {
        if($this->isEnabled === null){
            $this->isEnabled = isset(\Yii::$app->params['uploadingEnabled']) ? \Yii::$app->params['uploadingEnabled'] : true;
        }
    }


    public function run()
    {
        if($this->isEnabled)
        {
            if($this->registerAsset){
                FullUploaderAsset::register($this->getView());
            }

            $content = $this->renderBrowseButton();

            if($this->fileInputInsideContainer){
                $content .= $this->renderFileInput();
            }
        }
        else
        {
            $content = Html::tag("div", "Uploading is disabled", ['class'=>'alert alert-warning']);
        }

        return Html::tag('div', $content, $this->options);
    }

    protected function renderBrowseButton()
    {
        $options = ArrayHelper::merge($this->defaultButtonOptions, $this->buttonOptions);

        if($this->disabledBeforeLoad){
            $options['disabled'] = 'disabled';
            $this->getView()->registerJs("$('#".$options['id'] ."').removeAttr('disabled');");
        }
        $label = $options['label'];
        unset($options['label']);

        return Html::button($label, $options);
    }

    protected function renderFileInput()
    {
        return Html::fileInput($this->filesParamName, null, [
            'class' => 'file-input',
            'accept' => 'image/*',
            'multiple' => 'multiple',
            'data-url' => Url::to($this->uploadUrl),
            'style' => 'display:none;'
        ]);
    }


}
