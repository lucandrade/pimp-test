<?php

namespace app\assets;

use yii\web\AssetBundle;

class BootstrapDatePickerAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';

	public $css = [
		'css/plugins/bootstrap-datepicker3.min.css',
	];
    public $js = [
		'js/plugins/bootstrap-datepicker.min.js'
    ];
    public $depends = [
        'app\assets\AppAsset'
    ];
}
