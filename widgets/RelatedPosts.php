<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;
use yii\base\Exception;
use app\models\Post;
use app\components\SphinxDataProvider;

class RelatedPosts extends Widget
{
    public static $offset = 0;
    private static $_hasModels = true;

    public $postModel;
    public $numberOfPosts = 3;
    public $cacheDuration = 3600;
    public $cachePrefix = 'RelatedPosts';
    /**
     * @var \SphinxClient
     */
    private $_sphinxClient;

    public function initSphinxClient()
    {
        $this->_sphinxClient = clone Yii::$app->sphinxClient->getClient();
        $this->_sphinxClient->SetFilter('id_attr', [$this->postModel->id], true);
    }

    public function init()
    {
        parent::init();
        if (!($this->postModel instanceof Post)) {
            throw new Exception(self::className() . '::postModel must be instance of Post');
        }

        $this->initSphinxClient();
    }

    public function getCacheKey()
    {
        return $this->cachePrefix . '_' . $this->postModel->id . '_' . self::$offset;
    }

    public function getIndexName()
    {
        if (empty(Yii::$app->params['indexes']['main'])) {
            return 'pah_posts_search pah_posts_search_delta';
        } else {
            return Yii::$app->params['indexes']['main'];
        }
    }

    public function run()
    {
        $models = Yii::$app->cache->get($this->cacheKey);
        if ($models === false) {
            $dataQuery = Post::find()
                ->from('post p')
                ->with([
                    'postBlocks'
                ]);

            $words = array_map('strtolower', $this->postModel->words);
            $sphinxQuery = implode(' | ', array_unique($words));

            $dataProvider = new SphinxDataProvider([
                'cacheDuration' => 60,
                'query' => $dataQuery,
                'sphinxClient' => $this->_sphinxClient,
                'indexName' => $this->indexName,
                'primaryModelKey' => 'p.id',
                'sphinxQuery' => $sphinxQuery,
                'useSqlSortMode' => true,
                'sortingAttributes' => [
                    'live_at' => 'ts_live_at',
                    'rating' => 'rating'
                ],
                'sort' => [
                    'defaultOrder' => ['live_at' => SORT_DESC],
                ],
                'pagination' => [
                    'page' => self::$offset,
                    'pageSize' => $this->numberOfPosts
                ]
            ]);
            $models = $dataProvider->getModels();
            Yii::$app->cache->set($this->cacheKey, $models, $this->cacheDuration);
        }

        if (empty($models)) {
            self::$_hasModels = false;
        } else {
            self::$offset++;
            return $this->render('relatedPosts', ['models' => $models]);
        }
    }

    public static function hasModels()
    {
        return self::$_hasModels;
    }
}