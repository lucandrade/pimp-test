<?php
use yii\helpers\Url;
?>
<span class="title-icons">
    <?php if(!$album->getIsUnsorted()):?>
        <a class="share-chain"
           href="#"
           title="Get share codes for this album">
            <i class="fa fa-share-alt pah-color"></i>
        </a>
    <?php endif; ?>

    <a href="<?= \Yii::$app->user->isGuest ? Url::to('/login') : "#" ?>" class="download-link" title="Download this album">
        <i class="pah-color fa  fa-download"></i>
    </a>

    <?php if($isMine): ?>
        <?php if(!$album->getIsUnsorted()): ?>
            <a id="btn-lock-album"
               href="#" class="lock-album"

               title="<?= $lockStatus[1] . " this album" ?>"
               data-locked="<?= (int) $album->getIsLocked(); ?>"
               data-id="<?= $album->id ?>"
               data-url = <?= Url::to(['album/lock']) ?>
            >
                <i class="pah-color fa fa-<?= $lockStatus[0] ?>"></i>
            </a>
            <a href="#" title="Remove this album">
                <i class="pah-color fa fa-trash"></i>
            </a>
        <?php endif; ?>
    <?php endif; ?>
</span>