function popupUploaderLoader(options) {

    var scriptIdPrefix = 'popup-uploader-src-';
    var that = this;

    this.settings = options;

    var srcFileIsLoaded = function(type){
        return $('#' + scriptIdPrefix + type).length > 0;
    };

    var srcIsLoaded = function(){
        return srcFileIsLoaded('js') && srcFileIsLoaded('css');
    };

    var loadScript = function (url, type, callback) {

        var fileRef;
        if (type == "js") { //if filename is a external JavaScript file
            fileRef = document.createElement('script');
            fileRef.setAttribute("type", "text/javascript");
            var srcExist = document.querySelectorAll('script[src]')[0];
            fileRef.setAttribute("src", url);
            srcExist.parentNode.insertBefore(fileRef, srcExist.nextSibling);
        }
        else if (type == "css") { //if filename is an external CSS file
            fileRef = document.createElement("link");
            fileRef.setAttribute("rel", "stylesheet");
            fileRef.setAttribute("type", "text/css");
            document.getElementsByTagName("head")[0].appendChild(fileRef);
            fileRef.setAttribute("href", url)
        }


        fileRef.setAttribute('id', scriptIdPrefix + type);

        fileRef.onload = function () {
            callback();
        };


        if (typeof fileRef != "undefined"){
            document.getElementsByTagName("head")[0].appendChild(fileRef)
        }
    };



    this.load = function(){
        if(!srcIsLoaded()){
            loadScript(that.settings.jsSrc, 'js', function () {
                    loadScript(that.settings.cssSrc, 'css', function () {
                        that.popupUploader = new popupUploader(that.settings);
                        that.popupUploader.show();
                    });
                }
            );
        }else{
            if(!that.popupUploader){
                that.popupUploader = new popupUploader(that.settings);
            }
            that.popupUploader.show();
        }
    }

}