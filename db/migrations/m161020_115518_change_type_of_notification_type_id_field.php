<?php

use yii\db\Migration;

class m161020_115518_change_type_of_notification_type_id_field extends Migration
{
    public $table = 'notification';
    public $col = 'type_id';

    public function up()
    {
        $this->alterColumn($this->table, $this->col, $this->string());
    }

    public function down()
    {
        $this->alterColumn($this->table, $this->col, $this->integer());
    }
}
