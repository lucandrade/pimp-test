<?php
use yii\helpers\Url;
?>
<span class="title-icons">
    <a class="share-chain"
        href="#"
        title="Get share codes for this album"
    >
        <i data-share-codes='<?= json_encode($image->getShareCodes()) ?>' class="fa fa-share-alt pah-color"></i>
    </a>

    <a href="#" title="Download this image">
        <i class="pah-color fa  fa-download"></i>
    </a>

    <?php if($isMine): ?>
        <a href="#" title="Remove this image">
            <i class="pah-color fa fa-trash"></i>
        </a>
        <a href="#" title="Edit this image">
            <i class="pah-color fa fa-edit"></i>
        </a>

    <?php endif; ?>
</span>