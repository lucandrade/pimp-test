<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/**
 * @var $this yii\web\View
 * @var $model app\models\PostBlock
 *
 */

?>

<?php if ($model->isGIF() && !empty($model->webmSrc)) { ?>

    <div class="gif-player-wrapper">
        <span class="i-gif-play fa fa-play-circle-o"></span>
        <div id="gifPlayer-<?= $model->id ?>"
             class="gif-player"
             data-src-orig="<?= $model->webmSrc ?>"
             data-src-alt="<?= $model->m4vSrc ?>"
             data-src-poster="<?= $model->getThumbSrc('original') ?>"
             data-width="<?= $model->adjustedWidth ?>"
             data-height="<?= $model->adjustedHeight ?>">
        </div>
    </div>

<?php } else if (empty($model->videoUrl)) { ?>

    <div class="jumbotron">
        <p class="text-muted">
            <small>Processing...</small>
        </p>
        <p class="h3 video-info">
            <?= empty($model->text) ? $model->file_name : $model->text ?>
        </p>
    </div>

<?php } else { ?>

    <?php if ($model->videoType === $model::VIDEO_TYPE_FACEBOOK) { ?>
        <div class="fb-video-wrapper">
            <div class="fb-video"
                 data-allowfullscreen="1"
                 data-href="<?= $model->videoUrl ?>">
            </div>
        </div>
    <?php } else if ($model->videoType === $model::VIDEO_TYPE_YOUTUBE) { ?>

        <div id="ytThumb-<?= $model->videoId ?>"
             hidden="hidden"
             data-thumb-src="<?= $model->videoImage ?: $model->getThumbSrc('adjusted') ?>">
            <div class="yt-play-shadow"></div>
            <div class="yt-replay">
                <i class="fa fa-repeat"></i>
            </div>
        </div>
        <div id="ytPlayer-<?= $model->id ?>"
             data-videoid="<?= $model->videoId ?>"
             data-width="100%"
             data-height="315px">
        </div>

    <?php } else { ?>
        <iframe width="100%" height="315" src="<?= $model->videoUrl ?>" frameborder="0"
                allowfullscreen></iframe>
    <?php } ?>

<?php } ?>

