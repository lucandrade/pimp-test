<?php

namespace app\helpers;

class DateHelper
{
    public static function getWeekIntervalDates($year, $week, $format = 'Y-m-d')
    {
        $startDate = date($format, strtotime($year . 'W' . $week . '0'));
        $endDate = date($format, strtotime($year . 'W' . $week . '6'));
        return [$startDate, $endDate];
    }

    public static function getMonthIntervalDates($year, $month, $format = 'Y-m-d')
    {
        $startDate = date($format, strtotime($year . '-' . $month . '-01'));
        $t = date('t', strtotime($year . '-' . $month . '-01'));
        $endDate = date($format, strtotime($year . '-' . $month . '-' . $t));
        return [$startDate, $endDate];
    }
}
