<?php

namespace app\widgets;


use app\helpers\ArrayHelper;
use app\helpers\Html;

class LinkPager extends \yii\widgets\LinkPager
{

    public $pageSizeList = [16, 64, 100, 'all'];

    public $defaultPageSize = 16;

    public $pageSizeDropdownOptions = [];

    public $allLabel = 'all';

    public $summary = true;

    public $summaryOptions = ['class'=>'summary'];

    public $countOnPage;


    public function init()
    {
        parent::init();
    }

    public function run()
    {
        if ($this->registerLinkTags) {
            $this->registerLinkTags();
        }
        return $this->renderPageSizeDropdown() . $this->renderSummary() . $this->renderPageButtons();
    }


    public function renderSummary(){
        if(!$this->summary){
            return '';
        }
        $count = $this->countOnPage;
        if ($count <= 0) {
            return '';
        }
        $summaryOptions = $this->summaryOptions;
        $tag = ArrayHelper::remove($summaryOptions, 'tag', 'div');
        if (($pagination = $this->pagination) !== false) {
            $totalCount = $this->pagination->totalCount;
            $begin = $pagination->getPage() * $pagination->pageSize + 1;
            $end = $begin + $count - 1;
            if ($begin > $end) {
                $begin = $end;
            }
            return Html::tag($tag, "<b>$begin-$end</b> of <b>$totalCount</b>", $summaryOptions);
        }
    }


    protected function renderPageSizeDropdown()
    {
        $this->getView()->registerJs("$('select.per-page').on('change', function(){window.location.href=$(this).val()})");

        $options = ArrayHelper::merge([
            'class' => 'per-page form-control pagination'
        ], $this->pageSizeDropdownOptions);

        $items = [];
        $page = $this->pagination->getPage();

        $buttonLabel = false;
        $activePageSize = $this->pagination->getPageSize();

        if($activePageSize == $this->pagination->maxItems){
            $buttonLabel = "View all";
        }

        foreach($this->pageSizeList as $size){
            $label = $size == 'all' ? 'View all' : $size . " per page";
            if(!$buttonLabel && $size == $activePageSize){
                $buttonLabel = $label;
            }
            $items[] = Html::a($label, $this->pagination->createUrl($page, $size));
        }

        if(!$buttonLabel){
            $buttonLabel = $activePageSize . ' per page';
        }

        return Html::bootstarpDropdown([
            'label' => $buttonLabel . "<i class='fa fa-caret-down'></i>"
            ],
            $items,
            ['class' => 'pagination']
        );
    }
}