<?php

use yii\db\Migration;
use app\models\PostBlock;
use yii\db\Schema;

class m160210_130000_modify_type_in_post_block extends Migration
{
    public function up()
    {
        $this->update(PostBlock::tableName(), ['type' => PostBlock::BLOCK_TYPE_TEXT], 'type = ""');
        $this->update(PostBlock::tableName(), ['type' => PostBlock::BLOCK_TYPE_TEXT], 'type = "text"');
        $this->update(PostBlock::tableName(), ['type' => PostBlock::BLOCK_TYPE_IMAGE], 'type = "image"');
        $this->update(PostBlock::tableName(), ['type' => PostBlock::BLOCK_TYPE_VIDEO], 'type = "video"');

        $this->alterColumn(PostBlock::tableName(), 'type', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        $this->alterColumn(PostBlock::tableName(), 'type', Schema::TYPE_STRING);

        $this->update(PostBlock::tableName(), ['type' => 'text'], 'type = :type', [':type' => PostBlock::BLOCK_TYPE_TEXT]);
        $this->update(PostBlock::tableName(), ['type' => 'image'], 'type = :type', [':type' => PostBlock::BLOCK_TYPE_IMAGE]);
        $this->update(PostBlock::tableName(), ['type' => 'video'], 'type = :type', [':type' => PostBlock::BLOCK_TYPE_VIDEO]);
    }
}
