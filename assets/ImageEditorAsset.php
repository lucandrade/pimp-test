<?php

namespace app\assets;


use yii\web\AssetBundle;

class ImageEditorAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/jquery.imageEditor.css',
    ];

    public $js = [
        'js/jquery.imageEditor.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
    ];
}