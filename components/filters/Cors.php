<?php

namespace app\components\filters;


class Cors extends \yii\filters\Cors
{
    public function beforeAction($action)
    {
        if(!parent::beforeAction($action) || $_SERVER['REQUEST_METHOD'] == 'OPTIONS' ){
            \Yii::$app->response->statusCode = 200;
            \Yii::$app->response->data = 1;
            return false;
        }

        return true;
    }
}