<?php

namespace app\helpers;

use Yii;
use yii\base\Exception;
use yii\helpers\Json;

class VideoHelper
{
    public static $randomPath;
    public static $videoExtetsions = ['flv', 'mp4', 'mov', 'avi', 'wmv', 'webm'];
    public static $videoPathAlias = '@webroot/../media_protected/tmp/video/';
    public static $logPathAlias = '@app/runtime/logs/videohelper/';

    public static function filterFileName($name)
    {
        $name = filter_var($name, FILTER_SANITIZE_STRING);
        return preg_replace('/\s+/', '', $name);
    }

    public static function getTmpVideoPath($useRandomPath = true, $defaultPath = null)
    {
        if ($useRandomPath) {
            if ($defaultPath === null) {

                if (self::$randomPath === null) {
                    self::$randomPath = Yii::$app->security->generateRandomString(10);
                }

            } else {
                self::$randomPath = self::filterFileName($defaultPath);
            }

            $tmpPath = Yii::getAlias(self::$videoPathAlias) . self::$randomPath . DIRECTORY_SEPARATOR;

        } else {
            $tmpPath = Yii::getAlias(self::$videoPathAlias);
        }

        FileHelper::createDirectory(pathinfo($tmpPath, PATHINFO_DIRNAME));
        return $tmpPath;
    }

    public static function getTmpFileName($path, $name)
    {
        $tmpPath = Yii::getAlias(self::$videoPathAlias) . self::filterFileName($path) . DIRECTORY_SEPARATOR;
        $tmpFileName = $tmpPath . self::filterFileName($name);
        return file_exists($tmpFileName) ? $tmpFileName : null;
    }

    public static function getLoadedVideos($className, $attribute)
    {
        $path = self::getTmpVideoPath(false);
        $videos = [];
        if (Yii::$app->request->post($className) && file_exists($path)) {
            $data = Yii::$app->request->post($className);
            foreach ($data as $item) {
                if (
                    isset($item[$attribute]) &&
                    file_exists($path . $item[$attribute]) &&
                    self::isVideoExt($item[$attribute])
                ) {
                    $videos[$item[$attribute]] = $path . $item[$attribute];
                }
            }
        }
        return $videos;
    }

    public static function isVideoExt($fileName)
    {
        $ext = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
        return in_array($ext, self::$videoExtetsions);
    }

    public static function removeTmpPath($video)
    {
        FileHelper::removeDirectory(pathinfo($video, PATHINFO_DIRNAME));
    }

    public static function createVideoThumbnail($sourceFileName, $destFileName)
    {
        if (!self::isVideoExt($sourceFileName)) {
            throw new Exception("$sourceFileName is not a video!");
        }

        $commandFrame = "ffmpeg -nostats -i \"$sourceFileName\" -vcodec copy -f rawvideo -y /dev/null 2>&1 | grep frame | awk '{split($0,a,\"fps\")}END{print a[1]}' | sed 's/.*= *//' 2>&1";
        exec($commandFrame, $output);

        self::log([
            $commandFrame,
            $output
        ], 'createVideoThumbnail.log');

        if (isset($output[0]) && is_numeric($output[0])) {
            $frame = (int)($output[0] / 2);
            $commandCreate = "ffmpeg -i $sourceFileName -filter:v select=\"eq(n\,$frame)\" -vframes 1 $destFileName 2>&1";
            exec($commandCreate, $outputCreate);

            $output = array_merge($output, $outputCreate);
            self::log([
                $commandCreate,
                $outputCreate
            ], 'createVideoThumbnail.log');
        }

        return $output;
    }

    public static function log($message, $fileName = 'video.log')
    {
        LogHelper::log($message, self::$logPathAlias, $fileName);
    }

    public static function getVideoInfo($fileName, $fields = ['bit_rate', 'duration', 'width', 'height'])
    {
        $info = [];
        if (file_exists($fileName) && self::isVideoExt($fileName)) {
            $commandInfo = 'ffprobe -v quiet -show_streams -select_streams v:0 -i ' . $fileName . ' -print_format json 2>&1';
            exec($commandInfo, $outputInfo);
            self::log([
                $commandInfo,
                $outputInfo
            ], 'videoInfo.log');

            if (is_array($outputInfo) && $outputInfo[0] === '{') {
                $outputData = Json::decode(implode(PHP_EOL, $outputInfo));
                if (isset($outputData['streams'][0])) {
                    foreach ($fields as $field) {
                        if (isset($outputData['streams'][0][$field])) {
                            $info[$field] = $outputData['streams'][0][$field];
                        }
                    }
                }
            }
        }
        return $info;
    }

    public static function convertToM4v($sourceFileName, $destFileName)
    {
        if (!self::isVideoExt($sourceFileName)) {
            throw new Exception("$sourceFileName is not a video!");
        }

        $command = "ffmpeg -i $sourceFileName -vcodec libx264 -strict -2 $destFileName 2>&1";
        exec($command, $output);

        self::log([
            $command,
            $output
        ], 'convertToM4v.log');

        return $output;
    }

    public static function convertToWebm($sourceFileName, $destFileName)
    {
        if (!self::isVideoExt($sourceFileName)) {
            throw new Exception("$sourceFileName is not a video!");
        }

        $command = "ffmpeg -i $sourceFileName $destFileName 2>&1";
        exec($command, $output);

        self::log([
            $command,
            $output
        ], 'convertToWebm.log');

        return $output;
    }

    public static function createVideoPreview($webmFileName, $duration)
    {
        if (!self::isVideoExt($webmFileName)) {
            throw new Exception("$webmFileName is not a video!");
        }

        $fps = empty($duration) || $duration > 300 ? '1/60' : '1';
        $path = dirname($webmFileName) . '/preview';
        FileHelper::createDirectory($path);
        $command = "ffmpeg -i $webmFileName -vf fps=$fps $path/out%04d.png 2>&1";
        exec($command, $output);

        self::log([
            $command,
            $output
        ], 'createVideoPreview.log');

        return $output;
    }

}
