<?php

namespace app\helpers;


use app\models\Album;
use app\models\Image;
use app\models\Post;
use yii\base\InvalidParamException;
use yii\base\Exception;

class CommentsService
{
    const MODEL_TYPE_POST = 1;
    const MODEL_TYPE_ALBUM = 2;
    const MODEL_TYPE_IMAGE = 3;
    const MODEL_TYPE_COMMENT = 4;

    const ALIAS_ALBUM = 'album';
    const ALIAS_IMAGE = 'image';
    const ALIAS_POST = 'post';
    const ALIAS_COMMENT = 'comment';

    public static function aliasesModels()
    {
        return [
            self::ALIAS_ALBUM => Album::className(),
            self::ALIAS_POST => Post::className(),
            self::ALIAS_IMAGE => Image::className()
        ];
    }

    public static function aliasesTypes()
    {
        return [
            self::ALIAS_ALBUM => self::MODEL_TYPE_ALBUM,
            self::ALIAS_POST => self::MODEL_TYPE_POST,
            self::ALIAS_IMAGE => self::MODEL_TYPE_IMAGE,
            self::ALIAS_COMMENT => self::MODEL_TYPE_COMMENT
        ];
    }

    public static function resolveKey($key)
    {
        $parts = explode('_', $key);
        if(count($parts) < 2 || !in_array($parts[0], array_keys(self::aliasesModels()))){
            throw new InvalidParamException("Key $key has a wrong format");
        }

        $id = (int)$parts[1];
        if($id == 0){
            throw new InvalidParamException("Id part of key must be integer");
        }

        return [$id, self::aliasesModels()[$parts[0]], self::aliasesTypes()[$parts[0]]];
    }


    public static function getModelByKey($key)
    {
        try{
            list($id, $modelClass, $modelType) = self::resolveKey($key);
        }catch (InvalidParamException $e){
            return null;
        }

        $model = $modelClass::findOne(['id' => $id]);
        return [$model, $modelType];
    }

    public static function getAliasByType($type){
        return array_search($type, self::aliasesTypes());
    }




}