<?php use app\helpers\Html;
    \app\assets\PluginsPageAsset::register($this);
?>
<div id="plugins-page" class="row">
    <h2 class="text-center">PLUG-INS</h2>
    <div class="text-center">
        <?php foreach($plugins as $plugin): ?>
            <div class="plugin-header text-center" data-alias="<?= $plugin['alias']; ?>">
                <h4>
                    <a href="javascript:;" class="title-link text-center">
                        <?= $plugin['fullName'] ?>
                    </a>
                </h4>
                <?=  Html::img("@web/static/i/forum-plugins/" . $plugin['alias'] . "logo.png") ?>
                <div class="help-links">
                    <?= Html::a("Download", "@web/media/plugins-assets/pah_vBulletin.zip", ['target' => "_blank",
                       'download'=>'pah_vBulletin.zip']) ?>&nbsp;|&nbsp;
                    <?= Html::a('Instructions', 'javascript:;', ['class'=>'show-instructions'])?>&nbsp;|&nbsp;
                    <?= Html::a('Screenshot', "@web/static/i/forum-plugins/" . $plugin['alias'] . "-screen.gif") ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

    <div class="clearfix"></div>

    <?php foreach($plugins as $plugin): ?>
        <div id="instructions-<?= $plugin['alias'] ?>" class="instructions" style="display: none">
            <h3 class="text-center"><?= $plugin['shortName'] . " installation:" ?> </h3>
            <?= Html::img("@web/static/i/forum-plugins/" . $plugin['alias'] . "install.png"); ?>
            <ul class="steps">
            <?php foreach($plugin['steps'] as $step){
                echo "<li>$step</li>";
            } ?>
            </ul>
        </div>
    <?php endforeach; ?>

    <div class="clearfix"></div>
    <br>
    <br>
    <p class="text-center">Forum Plugin - will add a small upload box to your forum, that will upload unlimited images with AJAX,
        <br>will add a code with thumbnail and link to full image, to the thread field and host it on our servers
        <br>Save bandwidth and make your forum user friendly with fast and easy image upload
        <br><br>
        For any bugs/problems/feedbacks visit our <a href="<?= \yii\helpers\Url::to("/forum") ?>" target="_blank">forums</a>
    </p>



</div>


