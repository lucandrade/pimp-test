<?php

namespace app\commands;


use app\commands\actions\Process;
use app\models\Image;
use app\models\PostBlock;
use yii\console\Controller;
use yii\helpers\Console;
use app\helpers\LogHelper;

class PostBlockController extends Controller
{

    public function actions()
    {
        return [
            'process' => [
                'class' => Process::className(),
                'modelClass' => PostBlock::className(),
                'entityName' => 'post block'
            ]
        ];
    }
}