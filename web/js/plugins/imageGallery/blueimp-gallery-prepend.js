/*
 * blueimp Gallery Indicator JS
 * https://github.com/blueimp/Gallery
 *
 * Copyright 2013, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global define, window, document */

;(function (factory) {
    'use strict'
    if (typeof define === 'function' && define.amd) {
        // Register as an anonymous AMD module:
        define([
            './blueimp-helper',
            './blueimp-gallery'
        ], factory)
    } else {
        // Browser globals:
        factory(
            window.blueimp.helper || window.jQuery,
            window.blueimp.Gallery
        )
    }
}(function ($, Gallery) {
    'use strict'

    $.extend(Gallery.prototype.options, {
        onnext:undefined,
        onprev:undefined,
        settingsProperty: "settings"
    });


    var next = Gallery.prototype.next;
    var prev = Gallery.prototype.prev;

    $.extend(Gallery.prototype, {

        add: function (list, toBeginning) {
            var i
            if (!list.concat) {
                // Make a real array out of the list to add:
                list = Array.prototype.slice.call(list);
            }
            if (!this.list.concat) {
                // Make a real array out of the Gallery list:
                this.list = Array.prototype.slice.call(this.list);
            }
            if(toBeginning){
                this.list = list.concat(this.list);
            }else{
                this.list = this.list.concat(list);
            }
            this.num = this.list.length;
            if (this.num > 2 && this.options.continuous === null) {
                this.options.continuous = true;
                this.container.removeClass(this.options.leftEdgeClass);
            }
            this.container
                .removeClass(this.options.rightEdgeClass)
                .removeClass(this.options.singleClass);

            if(toBeginning){
                for (i = list.length - 1; i >= 0; i -= 1) {
                    this.prependSlide(i);
                    this.positionSlide(list.length - 1 - i);
                }
                for(var j = 0; j < this.slides.length; j++){
                    this.slides[j].setAttribute('data-index', j);
                    if(this.indicators){
                        this.indicators[j].setAttribute('data-index', j);
                    }
                }

                this.shiftElements(list.length);
                this.index += list.length;
            }else{
                for (i = this.num - list.length; i < this.num; i += 1) {
                    this.addSlide(i);
                    this.positionSlide(i);
                }
            }
            this.positions.length = this.num;
            this.initSlides(true)
        },

        shiftElements:function(shiftNumber){
            var buffer ={};
            for(var attr in this.elements){
                var newAttr = parseInt(attr) + parseInt(shiftNumber);
                buffer[newAttr] = this.elements[attr];
            }
            this.elements = buffer;
        },

        prependSlide: function(index){
            var slide = this.slidePrototype.cloneNode(false);
            this.slidesContainer[0].insertBefore(slide, this.slidesContainer[0].firstChild);
            this.slides.unshift(slide);
            this.prependIndicator(index);
        },

        next:function(ignoreCallback){
            if(!ignoreCallback && this.options.onnext){
                if(this.options.onnext.call(this, this.index)){
                    next.call(this);
                };
            }else{
                next.call(this);
            }
        },

        prev:function(ignoreCallback){
            if(!ignoreCallback && this.options.onprev){
                if(this.options.onprev.call(this, this.index)){
                    prev.call(this);
                }

            }else{
                prev.call(this);
            }
        },

        prependIndicator:function(index){
            if (this.indicatorContainer && this.indicatorContainer.length) {
                var indicator = this.createIndicator(this.list[index]);
                this.indicatorContainer[0].insertBefore(indicator, this.indicatorContainer[0].firstChild);
                this.indicators.unshift(indicator);
            }
        },

        imageFactory: function (obj, callback) {
            var that = this;
            var img = this.imagePrototype.cloneNode(false);
            var url = obj;
            var backgroundSize = this.options.stretchImages;
            var called;
            var element;
            var title;
            var settings;
            function callbackWrapper (event) {
                if (!called) {
                    event = {
                        type: event.type,
                        target: element
                    }
                    if (!element.parentNode) {
                        // Fix for IE7 firing the load event for
                        // cached images before the element could
                        // be added to the DOM:
                        return that.setTimeout(callbackWrapper, [event]);
                    }
                    called = true
                    $(img).off('load error', callbackWrapper)
                    if (backgroundSize) {
                        if (event.type === 'load') {
                            element.style.background = 'url("' + url +
                                '") center no-repeat';
                            element.style.backgroundSize = backgroundSize;
                        }
                    }
                    callback(event);
                }
            }
            if (typeof url !== 'string') {
                url = this.getItemProperty(obj, this.options.urlProperty);
                title = this.getItemProperty(obj, this.options.titleProperty);
                settings = this.getItemProperty(obj, this.options.settingsProperty);
            }
            if (backgroundSize === true) {
                backgroundSize = 'contain';
            }
            backgroundSize = this.support.backgroundSize &&
                this.support.backgroundSize[backgroundSize] && backgroundSize
            if (backgroundSize) {
                element = this.elementPrototype.cloneNode(false);
            } else {
                element = img;
                img.draggable = false;
            }

            if(settings){
                element.settings = settings;
            }

            if (title) {
                element.title = title;
            }
            $(img).on('load error', callbackWrapper);
            img.src = url;
            return element;
        }

    });

    return Gallery;
}));
