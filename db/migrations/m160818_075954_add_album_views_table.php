<?php

use yii\db\Migration;

class m160818_075954_add_album_views_table extends Migration
{
    public $table = 'album_views';
    public $albumRef = 'album_id';


    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            $this->albumRef => $this->integer(),
            'ip' => $this->bigInteger(20),
            'counted_at' => $this->timestamp()
        ], $tableOptions);

        $this->createIndex($this->albumRef . "_index", $this->table, $this->albumRef);
        $this->createIndex('common_index', $this->table, [$this->albumRef, 'ip', 'counted_at']);

        $this->addForeignKey($this->albumRef . "_{$this->table}_fk", $this->table, $this->albumRef, 'album', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey($this->albumRef . "_{$this->table}_fk", $this->table);
        $this->dropTable($this->table);
    }
}
