<?php

namespace app\commands;


use yii\console\Controller;
use yii\helpers\FileHelper;
use app\models\Post;
use yii\helpers\Url;

class SitemapController extends Controller
{
    const PER_SITE_MAP = 50000;

    public $startTime;


    private function __interval($str)
    {
        echo "\n";
        echo "###(".number_format((microtime(true) - $this->startTime),2)." $str)\n";
        $this->startTime = microtime(true);
    }


    public function actionIndex()
    {
        ini_set('memory_limit', '1000M');

        $query = $items = Post::find()
            ->where('status=:status AND live_at < NOW()', [':status'=>Post::STATUS_ACTIVE])
            ->select('id,title');

        $count = $query->count('id');
        $webDir = \Yii::getAlias("@app/web/");

        $dir = 'static/sitemap/';
        $dir_new = 'static/sitemap_new/';

        $this->__interval('Begin sitemap generation.');
        $this->__interval('Cleanup.');
        $this->__interval('Count.');

		// sitemap
		FileHelper::createDirectory($webDir . $dir);
		// sitemap_new
		FileHelper::removeDirectory($webDir . $dir_new);
		FileHelper::createDirectory($webDir . $dir_new);

//        mkdir($webDir . $dir, 0777, true);
//        exec('rm -rf '.$webDir . $dir_new);
//        mkdir($webDir . $dir_new, 0777, true);

        $sitemapCount = ceil($count/self::PER_SITE_MAP);

        $this->__interval('Total '.$count.' posts. Will generate '. $sitemapCount. ' sitemaps.');

        $sitemap_index = '<?xml version="1.0" encoding="UTF-8" ?>'."\n";
        $sitemap_index .= '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";

        $baseUrl = Url::base(true);

        for($i = 1; $i <= $sitemapCount; $i++)
        {
            $this->__interval('Generating sitemap #'.$i);
            $query->offset(($i - 1) * self::PER_SITE_MAP);
            $query->limit(self::PER_SITE_MAP);
            $posts = $query->all();
            $sitemap_txt = '';

            foreach($posts as $post){
                $sitemap_txt .= $post->getUrl(true) ."\n";
            }
            $fileNameTxt = 'sitemap_'.$i.'.txt';

            file_put_contents($webDir . $dir_new . $fileNameTxt, $sitemap_txt);
            $sitemap_index .=  "<sitemap><loc>" . $baseUrl . $dir . $fileNameTxt . "</loc></sitemap>";
        }

        $sitemap_index .= '</sitemapindex>'."\n";

        file_put_contents($webDir . $dir_new . "sitemap_index.xml", $sitemap_index);
//        exec('rm -rf ' . $webDir.$dir.' && mv '.$webDir.$dir_new.' '.$webDir.$dir);

		// remove sitemap
		FileHelper::removeDirectory($webDir.$dir);
		// copy sitemap_new to sitemap
		FileHelper::copyDirectory($webDir.$dir_new,$webDir.$dir);
		// remove sitemap_new
		FileHelper::removeDirectory($webDir.$dir_new);
    }
}