<?php

namespace app\widgets;


use app\assets\EditableAsset;
use app\helpers\ArrayHelper;
use app\helpers\Html;
use app\models\Album;
use app\models\Image;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\Json;
use yii\helpers\Url;

class Editable extends Widget
{
    public $text;

    public $model;

    public $url;
    public $type = 'text';
    public $modelClass = 'image';
    public $options = [];
    public $linkUrl = false;

    public $containerOptions = [];

    public $checkAuthorAccess = true;



    public $iconOptions = [
        'class' => "fa fa-cog editable-trigger pah-color"
    ];

    public $requiredClass = 'editable-content';


    public function init(){
        if(empty($this->url)){
            $this->url = Url::to(['common/edit-title']);
        }

        if(!($this->model instanceof Album) && !($this->model instanceof Image)){
            throw new InvalidConfigException("'model' must be instance of either 'app\\models\\Album' or 'app\\models\\Image' classes");
        }
    }

    public function run()
    {

        if($this->modelCanBeEdited())
        {

            $className = get_class($this->model);
            $parts =  explode('\\', $className);
            $defaults = [
                'data-type' => $this->type,
                'data-pk' => $this->model->id,
                'data-url' => $this->url,
                'data-title' => "Change " . end($parts) . " Name",
                'name' => 'title',
                'class' => 'editable-link',
                'data-toggle' => "manual",
                'data-animation' => 0,
                'data-placement' => 'right',
                'data-params' => Json::encode(['className' => $className])
            ];

            $options = ArrayHelper::merge($defaults, $this->options);

            if(isset($options['class'])){
                $options['class'] .= ' ' . $this->requiredClass;
            }else{
                $options['class'] = $this->requiredClass;
            }

            $content = empty($this->linkUrl)
                ? Html::tag('span', $this->model->title, $options)
                : Html::a($this->model->title, $this->linkUrl, $options);


            EditableAsset::register($this->getView());
            $this->getView()->registerJs("$('.editable-wrapper').popover()");

            if(isset($this->containerOptions['class'])){
                $this->containerOptions['class'] .= ' editable-wrapper';
            }else{
                $this->containerOptions['class'] = 'editable-wrapper';
            }

            $this->containerOptions['data-has-access'] = 1;
            $this->containerOptions['data-content'] = Html::tag('i','', $this->iconOptions);
            $this->containerOptions['data-trigger'] = 'manual';
            $this->containerOptions['data-html'] = 1;
            $this->containerOptions['data-animation'] = 0;

            return  Html::tag('div', $content, $this->containerOptions);

        }else{
            return  empty($this->linkUrl)
                ? Html::tag('span', $this->model->title, $this->options)
                : Html::a($this->model->title, $this->linkUrl, $this->options);
        }

    }
    
    
    
    protected function modelCanBeEdited()
    {
        $authorAccess = !$this->checkAuthorAccess || $this->model->checkAuthorAccess();
        $albumAccess = !($this->model instanceof Album)  || !$this->model->getIsUnsorted();
        
        return $authorAccess && $albumAccess;
    }
    
}