<?php

namespace app\controllers\admin;

use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;

class DeleteAction extends Action {

	public $viewName = 'index';
	public $modelClass;
	public $redirectSuccess = ['index'];

	public $messageSuccess = 'Row deleted';

	public function init() {
		if ($this->modelClass === null) {
			throw new InvalidConfigException('Model class must be setted');
		}

		parent::init();
	}

	public function run($id){
		if (call_user_func([$this->modelClass,'deleteAll'],['id'=>$id])){
			Yii::$app->session->setFlash('success', $this->messageSuccess);
		}
		return $this->controller->redirect($this->redirectSuccess);
	}

}