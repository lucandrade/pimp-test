<?php

use yii\db\Schema;
use yii\db\Migration;

class m160111_100000_add_rating_fields_to_user_and_post extends Migration
{

    public function up()
    {
        $this->addColumn('user', 'rating', 'float NOT NULL DEFAULT 0 AFTER activkey');
        $this->addColumn('post', 'rating', 'float NOT NULL DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('user', 'rating');
        $this->dropColumn('post', 'rating');
    }
}
