<?php
/**
 * @var \app\models\User $user
 * @var $dataProvider \app\components\SphinxDataProvider
 */

use app\assets\UserAsset;
use app\components\ListView;
use app\models\Post;
use yii\helpers\Json;
use yii\helpers\Url;
use app\widgets\ScrollPager;
use app\helpers\WidgetHelper;
use yii\bootstrap\Html;
use yii\web\View;

UserAsset::register($this);

$seenPostIds = [];
if (!Yii::$app->user->isGuest) {
    //switched off make seen posts for until we complete with it
    $this->registerJs("$(document).on('scroll_1', scrollPostsHandler); initProccessSeenPosts($('.item'));");
//    echo Html::hiddenInput('make-seen-url', Url::to(['post/make-seen']));
//    $seenPostIds = Yii::$app->user->identity->getSeenPostIds(['post_id' => $dataProvider->getSphinxIds()]);
    $this->registerJs('var userOptions = ' . Json::encode(Post::getUserSettings()) . ';', $this::POS_HEAD);
}

$this->registerMetaTag([
    'name' => 'description',
    'content' =>  \Yii::$app->name . ' - Awesome daily updated Owned Videos, Pictures & Gifs - Visit us to see more!'
], 'description');

$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'owned,jokes,fun,humour,funny videos,epic,fail,win,funny pics,like a boss,forever alone,meme,gifs,funny gifs,trolling'
], 'keywords');

$config = WidgetHelper::getConfig(2);
$jsonConfig = Json::encode($config);
$ck = WidgetHelper::getScriptInView('//js/widget', ['config' => $config]);
$this->registerJs($ck, View::POS_END);

$pageParam =  $dataProvider->pagination->pageParam;
$pageNumber = !empty($_GET[$pageParam]) ? $_GET[$pageParam] : 1;
$link = '';

if (!\Yii::$app->user->isGuest) {
    echo Html::hiddenInput('add-to-favorite-url', Url::to(['post/toggle-favorite']));
    $this->registerJs("$('body').on('click', '.add-to-favorite', addToFavoriteHandler)");
}

if($postId = \Yii::$app->request->get('p')){
    $this->registerJs("scrollToElement($('[data-key=\"$postId\"]'))", View::POS_READY);
    if($pageNumber>1){
        $postsNumber = ($pageNumber-1) * $dataProvider->pagination->pageSize;
        $link = Html::a("Show previous $postsNumber posts", parse_url(\Yii::$app->request->getUrl(),PHP_URL_PATH));
    }
}

$collapsePosts = \Yii::$app->user->isGuest || !(bool) \Yii::$app->user->identity->getSettings('expand_posts');
$jsAjaxListEvent = $collapsePosts ?
    "if(typeof $.fn.expandPostAjaxListEvent === \"function\"){
        $(this).expandPostAjaxListEvent();
     }" : "";


?>

<div class="row">
    <div class="col-md-12">
        <!-- Page Header -->
        <div class="page-header position-relative">
            <div class="header-title">
                <h1>
                    <?= $user->username ?> profile
                </h1>
            </div>
        </div>
        <!-- /Page Header -->
        <div class="profile-container">
            <div class="profile-header well row">
                <div class="col-lg-2 col-md-4 col-sm-12 text-center">
                    <?= $user->publicProfileAvatar ?>
                </div>
                <div class="col-lg-4 col-md-8 col-sm-12 profile-info text-center">
                    <div class="header-fullname"><?= $user->username ?></div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 stats-col">
                    <div class="stats-value pink"><?= $user->rating ?></div>
                    <div class="stats-title">Rating</div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 stats-col">
                    <div class="stats-value pink">
                        <a href="<?= $user->getAuthorUrl() ?>">
                            <?= $user->getPosts()->count(); ?>
                        </a>
                    </div>
                    <div class="stats-title">Number of posts</div>
                </div>

                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12 stats-col">
                    <div class="stats-value pink">
                        <a href="<?= Url::to(['album/user', 'id' => $user->id]) ?>">
                            <?= $user->getImages()->count(); ?>
                        </a>
                    </div>
                    <div class="stats-title">Number of images</div>
                </div>
            </div>
            <div class="profile-body">
                <div class="col-lg-12">
                    <div class="tabbable">
                        <ul class="nav nav-tabs tabs-flat  nav-justified" id="myTab11">
                            <li class="<?= $filter->type === 'overview' ? 'active' : '' ?>">
                                <a href="<?= Url::to(['user/profile', 'id' => $user->id, 'type' => 'overview']) ?>" class="rlinks">
                                    Overview
                                </a>
                            </li>
                            <li class="<?= $filter->type === 'commented' ? 'active' : '' ?>">
                                <a href="<?= Url::to(['user/profile', 'id' => $user->id, 'type' => 'commented']) ?>" class="rlinks">
                                    Commented
                                </a>
                            </li>
                            <li class="<?= $filter->type === 'favorites' ? 'active' : '' ?>">
                                <a href="<?= Url::to(['user/profile', 'id' => $user->id, 'type' => 'favorites']) ?>" class="rlinks">
                                    Liked
                                </a>
                            </li>
                            <li class="<?= $filter->type === 'my' ? 'active' : '' ?>">
                                <a href="<?= Url::to(['user/profile', 'id' => $user->id, 'type' => 'my']) ?>" class="rlinks">
                                    Own
                                </a>
                            </li>
                        </ul>
                        <div class="col-md-12">
                            <?php
                            echo ListView::widget([
                                'id'=>'main-list-view',
                                'summary' => false,
                                'dataProvider' => $dataProvider,
                                'itemView' => '/post/_post',

                                'itemOptions' => [
                                    'class' => ''
                                ],

                                'viewParams' => [
                                    'seenIds' => isset($seenPostIds) ? $seenPostIds : [],
                                    'page' => $pageNumber
                                ],

                                'pager' => [
                                    'class' => ScrollPager::className(),
                                    'item' => '.item',
                                    'enabledExtensions' => [
                                        ScrollPager::EXTENSION_PAGING,
                                        ScrollPager::EXTENSION_SPINNER
                                    ],
                                    'eventOnRender' => $collapsePosts ? 'function(items){
                                         $(items).each(function() {
                                            if(typeof $.fn.expandPostAjaxList === "function"){
                                                $(this).expandPostAjaxList();
                                            }
                                        });
                                    }' : null,

                                    'eventOnRendered' => "function(items) {
                                        var \$items = $(items);
                                        initProccessSeenPosts(\$items);
                                        \$items.each(function() {
                                             if (typeof $.fn.initGifPlayer === 'function') {
                                                 $('[id^=\"gifPlayer-\"]', this).initGifPlayer();
                                             }
                                             $jsAjaxListEvent
                                        });
                                    }"
                                ]
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>








