<?php
namespace app\models;

use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $email;
    public $password;
    public $password2;
    public $tnc;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],

            [['password', 'password2'], 'required'],
            [['password', 'password2'], 'string', 'min' => 6],
            ['password2', 'compare', 'compareAttribute' => 'password',
                'operator' => '===', 'message' =>"Passwords don't match"],

            ['tnc', 'required', 'message' => 'Please accept TNC first', 'requiredValue' => 1],
            ['tnc', 'boolean']
        ];
    }


    public function attributeLabels()
    {
        return [
            'password2' =>'Repeated password',
            'tnc' => "I agree to the <a href='javascript:;'>
                            Terms of Service </a>
                            & <a href='javascript:;'>
                            Privacy Policy </a>"
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate())
        {
            $user = new User();
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->generateEmailKey();
            $user->registerForumUser($this->password);
            $user->save();
            return $user;
        }

        return null;
    }
}
