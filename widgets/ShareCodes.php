<?php
/**
 * Created by PhpStorm.
 * User: mactra
 * Date: 31.08.16
 * Time: 17:35
 */

namespace app\widgets;


use app\assets\ShareCodesAsset;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class ShareCodes extends Widget
{
    public $options = [];

    public $defaultOptions = [
        'id'=>'share-codes-widget'
    ];

    public function run()
    {
        $options = ArrayHelper::merge($this->defaultOptions, $this->options);
        $this->getView()->registerAssetBundle(ShareCodesAsset::className());
        return Html::tag('div', $this->render('ShareCodes'), $options);
    }
}