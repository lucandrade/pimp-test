<?php
namespace app\db\fixtures;

use yii\test\ActiveFixture;

class ArchiveFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Archive';
    public $depends = [
        'app\db\fixtures\ImageFixture',
    ];
}