(function(){
'use strict';
var Resize = Darkroom.Transformation.extend({
    applyTransformation:function(canvas, image, next){

        image.width = this.options.width;
        image.height = this.options.height;


        this.options.widthInput.setValue(this.options.width);
        this.options.heightInput.setValue(this.options.height);

        canvas.setWidth(this.options.width);
        canvas.setHeight(this.options.height);

        canvas.centerObject(image);
        image.setCoords();
        canvas.renderAll();

        next();
    }
});

var icons = [
    {
        id:"transform",
        code:'<path d="M0 0h24v24H0z" fill="none"/>'
        +'<path d="M22 18v-2H8V4h2L7 1 4 4h2v2H2v2h4v8c0 1.1.9 2 2 2h8v2h-2l3 3 3-3h-2v-2h4zM10 8h6v6h2V8c0-1.1-.9-2-2-2h-6v2z"/>'
    }
];


Darkroom.plugins['resize'] = Darkroom.Plugin.extend({
    initialize:function InitDarkroomResizePlugin(){

        injectIcons(icons);

        var buttonGroup = this.darkroom.toolbar.createButtonGroup();

        this.resizeButton = buttonGroup.createButton({
            image: 'transform'
        });

        this.widthInput = createInput(buttonGroup, {
            label:'Width',
            inputId:'width-input',
            value: this.darkroom.sourceImage.width,
            hide: true
        });

        this.heightInput = createInput(buttonGroup, {
            label:'Height',
            inputId:'height-input',
            value: this.darkroom.sourceImage.height,
            hide: true
        });

        this.okButton = buttonGroup.createButton({
            image: 'done',
            type: 'success',
            hide: true
        });


        this.cancelButton = buttonGroup.createButton({
            image: 'close',
            type: 'danger',
            hide: true
        });

        addTitlesToButtons.call(this);

        this.newWidth = this.darkroom.sourceImage.width;
        this.newHeight = this.darkroom.sourceImage.height;

        this.resizeButton.addEventListener('click', this.toggleResize.bind(this));
        this.okButton.addEventListener('click', this.resizeImage.bind(this));
        this.cancelButton.addEventListener('click', this.releaseResizeFocus.bind(this));

        this.widthInput.addEventListener("blur", this.setWidthByInput.bind(this));
        this.heightInput.addEventListener("blur", this.setHeightByInput.bind(this));


        this.darkroom.canvas.on('object:scaling', this.onObjectScaling.bind(this));
    },

    heightInput:null,
    widthInput:null,

    ratioX:null,
    ratioY:null,

    initialImage:null,

    resizeImage:function(){

        this.disableToolbar(true);

        this.darkroom.applyTransformation(
            new Resize({width:this.newWidth, height:this.newHeight, widthInput:this.widthInput, heightInput:this.heightInput})
        );

        this.activateToolbar(false);
    },


    onObjectScaling:function(event){
        if (!this.hasResizeFocus()) {
            return;
        }

        var preventScaling = false;
        var currentObject = event.target;
        if (currentObject !== this.darkroom.image){
            return;
        }

        var currentW = currentObject.getWidth(),
            currentH = currentObject.getHeight(),
            canvasResized = false;


        if(currentW > this.darkroom.options.maxWidth && currentW < this.options.maxResizeCanvasWidth){
            this.darkroom.canvas.setWidth(currentW+15);
            canvasResized = true;
        }
        if(currentH > this.darkroom.options.maxHeight && currentH < this.options.maxResizeCanvasHeight){
            this.darkroom.canvas.setHeight(currentH+15);
            canvasResized = true;
        }

        if(canvasResized){
            this.darkroom.canvas.centerObject(currentObject);
            this.darkroom.canvas.renderAll();
        }

        this.newWidth = Math.round(currentW * this.ratioX);
        this.newHeight = Math.round(currentH * this.ratioX);

        this.heightInput.setValue(this.newHeight);
        this.widthInput.setValue(this.newWidth);
    },

    newWidth:null,
    newHeight:null,

    toggleResize:function(){
        if (!this.hasResizeFocus())
            this.requireResizeFocus();
        else
            this.releaseResizeFocus();
    },

    requireResizeFocus:function(){

        if(this.darkroom.plugins.crop.hasFocus()){
            alert("Resizing is not enable while cropping mode is on");
            return;
        }

        this.ratioX = Math.round(this.darkroom.sourceImage.width / this.darkroom.image.getWidth());
        this.ratioY = Math.round(this.darkroom.sourceImage.height / this.darkroom.image.getHeight());

        this.initialImage = fabric.util.object.clone(this.darkroom.image);

        this.activateToolbar(true);
        this.disableToolbar(false);

        this.makeImageResizable(true);

    },
    releaseResizeFocus:function(){

        if(this.initialImage !== null){
            this.darkroom.image.remove();
            this.darkroom.canvas.add(this.initialImage);
            this.darkroom.image = this.initialImage
        }

        this.activateToolbar(false);

        this.makeImageResizable(false);
    },

    makeImageResizable:function(resizable){

        this.darkroom.image.hasRotatingPoint = false;
        this.darkroom.image.lockMovementX = true;
        this.darkroom.image.lockMovementY = true;
        this.darkroom.image.lockRotation = true;

        this.darkroom.image.lockScalingX = !resizable;
        this.darkroom.image.lockScalingY = !resizable;
        this.darkroom.image.selectable = resizable;
        this.darkroom.image.hasControls = resizable;
        this.darkroom.image.hasBorders = resizable;
        this.darkroom.canvas.selection = resizable;
        if(resizable){
            this.darkroom.canvas.setWidth(this.darkroom.canvas.getWidth() + 15);
            this.darkroom.canvas.setHeight(this.darkroom.canvas.getHeight() + 15);
            this.darkroom.canvas.centerObject(this.darkroom.image);
            this.darkroom.canvas.renderAll();

            this.darkroom.canvas.setActiveObject(this.darkroom.image);
        }
    },

    activateToolbar: function(activate){
        this.resizeButton.active(activate);
        this.okButton.hide(!activate);
        this.cancelButton.hide(!activate);
        this.heightInput.hide(!activate);
        this.widthInput.hide(!activate);
    },


    hasResizeFocus:function(){
        return this.darkroom.image.selectable;
    },


    setWidthByInput:function(){
        var val = parseInt(this.widthInput.getValue());
        if(val >= 0){
            this.setSize(val, "Width");
        }
    },

    setHeightByInput:function(){
        var val = parseInt(this.heightInput.getValue());
        if(val >= 0){
            this.setSize(val, "Height");
        }
    },

    setSize:function(size, side){
        this['new'+side] = size;
        var ratio = side == 'Width' ? this.ratioX : this.ratioY,
            imageSize = size/ratio;

        this.darkroom.image['set'+side](imageSize);

        if(imageSize > this.darkroom.canvas['get'+side]){
            this.darkroom.canvas['set'+side](imageSize);
        }

        this.darkroom.image.setCoords();
        this.darkroom.canvas.renderAll();
    },

    disableToolbar:function(disable){
        this.okButton.disable(disable);
        this.widthInput.disable(disable);
        this.heightInput.disable(disable);
    }

});

var createInput = function(buttonGroup, options){

    var defaults = {
        label: 'Input',
        value:"",
        hide: false,
        disabled: false
    };

    options = Darkroom.Utils.extend(options, defaults);

    var inputWrapper = document.createElement('div');
    inputWrapper.className = "darkroom-inputwrapper";
    inputWrapper.innerHTML = '<label for="'+options.inputId+'">'+options.label+'</label><input type="number" id="'+options.inputId+'" type="text">px';
    buttonGroup.element.appendChild(inputWrapper);

    var input = new Input(inputWrapper);
    input.setValue(options.value);
    input.hide(options.hide);

    return input;
};


var injectIcons = function(icons){
    var iconsWrapper = document.getElementById('darkroom-icons').getElementsByTagName('svg')[0];
    for (var i = 0; i < icons.length; i++){
        var code = '<symbol id="'+icons[i].id+'" viewBox="0 0 24 24">'+icons[i].code+'</symbol>';
        iconsWrapper.innerHTML += code;
    }
};

var addTitlesToButtons = function(){
    var titles = {
        undo : "Undo action",
        crop : "Crop image",
        done: "Apply changes",
        close: "Cancel changes",
        transform : "Resize image",
        'rotate-right': "Rotate right",
        'rotate-left' : "Rotate left"
    };
    var buttons = this.darkroom.toolbar.element.getElementsByTagName("button");
    for(var i = 0; i < buttons.length; i++){
        var use = buttons[i].getElementsByTagName('use')[0];
        var key = use.getAttribute('xlink:href').replace("#", '');
        if(titles[key] !== undefined){
            buttons[i].setAttribute('title', titles[key]);
        }
    }
};



function Input(element){
    this.element = element;
}

Input.prototype = {
    addEventListener: function(eventName, listener) {
        if (this.element.getElementsByTagName('input')[0].addEventListener){
            this.element.getElementsByTagName('input')[0].addEventListener(eventName, listener);
        } else if (this.element.getElementsByTagName('input')[0].attachEvent) {
            this.element.getElementsByTagName('input')[0].attachEvent('on' + eventName, listener);
        }
    },

    setValue:function(value){
        this.element.getElementsByTagName('input')[0].value = value;
    },

    getValue:function(){
        return this.element.getElementsByTagName('input')[0].value;
    },



    removeEventListener: function(eventName, listener) {
        if (this.element.getElementsByTagName('input')[0].removeEventListener){
            this.element.getElementsByTagName('input')[0].removeEventListener(eventName, listener);
        }
    },
    active: function(value) {
        if (value)
            this.element.classList.add('darkroom-button-active');
        else
            this.element.classList.remove('darkroom-button-active');
    },
    hide: function(value) {
        if (value)
            this.element.classList.add('darkroom-button-hidden');
        else
            this.element.classList.remove('darkroom-button-hidden');
    },
    disable: function(value) {
        this.element.getElementsByTagName('input')[0].disabled = (value) ? true : false;
    }
};


})();


