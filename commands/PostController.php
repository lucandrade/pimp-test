<?php

namespace app\commands;


use app\helpers\PostHelper;
use app\models\Post;
use app\models\SocialPosterLog;
use yii\console\Controller;
use yii\helpers\Console;

class PostController extends Controller
{

    public function actionShareToSocials()
    {
        $post = [
            'id' => 11321,
            'media_url' => 'https://cs7066.vk.me/c540109/v540109215/ffec/oX-oHiIEic8.jpg',
            'tags' => ['tag1', 'tag2', 'tag3'],
            'title' => 'Title post sample111qdaasdweqweq31231',
            'url' => 'http://pikabu.ru/story/ya_vsyo_bolshe_boyus_svoikh_snov_3914258'
        ];

        $this->postToSocials($post);
    }

    public function actionPostBest()
    {
        $publishPostsIds = [];
        $continueSearching = true;

        do{
            $postQuery = Post::find()
                ->where('live_at > DATE_SUB(NOW(), INTERVAL 24 HOUR) AND live_at < NOW() AND status=:status',
                    [':status'=>Post::STATUS_ACTIVE])
                ->orderBy("rating DESC");

            if(!empty($publishPostsIds)){
                $postQuery->andWhere(['not in', 'id', $publishPostsIds]);
            }

            $post = $postQuery->one();

            if(!$post)
            {
                echo $this->ansiFormat("There is no not-posted posts for today." . PHP_EOL, Console::FG_YELLOW);
                return;
            }

            if(SocialPosterLog::wasPostedLastDay($post->getUrl(true))){
                $publishPostsIds[] = $post->id;
            }else{
                $continueSearching = false;
            }

        }while($continueSearching);

        $this->postToSocials($post);
    }


    protected function postToSocials($post)
    {
        $results = PostHelper::postToSocialPlatforms($post);
        foreach($results as $network => $result)
        {
            if($result == 'ok'){
                $this->stdout(PHP_EOL . "Was successfully posted to " . strtoupper($network), Console::FG_GREEN);
            }
            else{
                if(is_array($result))
                {
                    echo "Stumbleupon output:" . isset($result['output']) ?:"output is not provided";
                    $result = isset($result['res']) ?: "No result for stableupon";
                }

                if($result instanceof \Exception){
                    $this->stdout(PHP_EOL . "Exception while posting to $network:" . $result->getMessage(), Console::FG_RED);
                }else{
                    $this->stdout(PHP_EOL . "Errors while posting to $network:" . $result, Console::FG_RED);
                }
            }
        }

        echo PHP_EOL;
    }

    private function getLastPosted()
    {
        $filename = \Yii::getAlias("@runtime/fanposter");
        if(!file_exists($filename)) {
            $this->setFirst($filename);
        }
        $contents = file_get_contents($filename);
        return $contents;
    }

    private function setFirst($fileName)
    {
        return file_put_contents($fileName, json_encode([
            'date'=>'0000-00-00 00:00:00',
            'count'=>1
        ]));
    }
}