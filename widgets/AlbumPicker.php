<?php

namespace app\widgets;


use app\assets\AlbumPickerAsset;
use app\models\Album;
use app\models\User;
use yii\base\Widget;
use app\helpers\ArrayHelper;

class AlbumPicker extends Widget
{

    public $albumIds = [];
    public $defaultAlbumId = '';
    public $url = ['album/move-images-to'];

    public $excludeAlbumIds = [];


    public function init()
    {
        if(empty($this->albumIds))
        {
            if(!\Yii::$app->user->isGuest)
            {
                $this->albumIds = ArrayHelper::map( \Yii::$app->user->identity->getAlbums()
                    ->select(['id', 'title'])
                    ->asArray()
                    ->all(), 'id', 'title');

                foreach($this->excludeAlbumIds as $id){
                    unset($this->albumIds[$id]);
                }
            }
        }
    }


    public function run()
    {
        AlbumPickerAsset::register($this->getView());
        return $this->render('AlbumPicker', ['albumIds' => $this->albumIds]);
    }
}