<?php
error_reporting(E_ALL);
ini_set('display_errors', true);

date_default_timezone_set('America/Los_Angeles');
define('ABSPATH', dirname(__FILE__).'/');
define('WPINC', 'wp-admin');
define('WP_PLUGIN_DIR', 'wp-admin');
define('WPMU_PLUGIN_DIR', 'wp-admin');

define('qtrans_split', '|');


class wpdb {
	public static function get_row($data = null){call_user_func_array('base_function', func_get_args());}
	public static function prepare($data = null){call_user_func_array('base_function', func_get_args());}
	public static function insert($data = null){call_user_func_array('base_function1', func_get_args());}
	public static function update($data = null){call_user_func_array('base_function', func_get_args());}
	public static function suppress_errors($data = null){call_user_func_array('base_function', func_get_args());}
	public static function get_results($data = null){call_user_func_array('base_function', func_get_args());}

}

global $wpdb;
$wpdb = new wpdb;
$wpdb->prefix = 'wp_';
$wpdb->insert_id = 0;
$wpdb->options = 'wp_options';
$lng = 'en';

global $plgn_NS_SNAutoPoster;
$plgn_NS_SNAutoPoster = new stdclass;
$plgn_NS_SNAutoPoster->nxs_options = array();

function is_multisite(){call_user_func_array('base_function', func_get_args());}
function get_bloginfo(){call_user_func_array('base_function', func_get_args());}
function wp_cache_get(){
	call_user_func_array('base_function', func_get_args());
	$args = func_get_args();
	if($args[0]=='NS_SNAutoPoster' && $args[1]=='options') {
		return array(
			'twAccTokenSec' => null,
			'bgBlogID' => null,
			'nsOpenGraph' => 0,
		);
	}
}
function wp_cache_add(){call_user_func_array('base_function', func_get_args());}
function wp_cache_set(){call_user_func_array('base_function', func_get_args());}
function admin_url(){call_user_func_array('base_function', func_get_args());}
function dbDelta(){call_user_func_array('base_function', func_get_args());}
function load_plugin_textdomain(){call_user_func_array('base_function', func_get_args());}
function get_post_meta($data = null){call_user_func_array('base_function', func_get_args());}
function delete_post_meta($data = null){call_user_func_array('base_function', func_get_args());}
function add_post_meta($data = null){call_user_func_array('base_function', func_get_args());}
//function is_wp_error($data = null){call_user_func_array('base_function', func_get_args());}
function plugins_url($data = null){call_user_func_array('base_function', func_get_args());}

// function wp_remote_get($url, $args){call_user_func_array('base_function', func_get_args());}
// function wp_remote_post($url, $args){call_user_func_array('base_function', func_get_args());}
require dirname(__FILE__)."/wp-admin/formatting.php";
require dirname(__FILE__)."/wp-admin/functions.php";
require dirname(__FILE__)."/wp-admin/class-http.php";
require dirname(__FILE__)."/wp-admin/class-wp-error.php";
require dirname(__FILE__)."/wp-admin/http.php";
require dirname(__FILE__)."/wp-admin/plugin.php";
require dirname(__FILE__)."/wp-admin/includes/shortcodes.php";


function base_function($var = null) {
 	echo get_callee().":\n";
 	if($args = func_get_args()) {
 		return $args[0];
 	}
}
function base_function1($var = null) {
	echo get_callee().":\n";
	if($args = func_get_args()) {
		return $args[0];
	}
}
function get_callee() {
 $backtrace = debug_backtrace();
//  foreach($backtrace as $line) {
// 	echo (isset($line['file'])?($line['file']):'').':'.(isset($line['line'])?($line['line']):'')."\n";
//  }
 return $backtrace[3]['function'];
}

function get_posts(){
	call_user_func_array('base_function', func_get_args());
	return array();
}

function home_url($postID = null){
	global $ss_post;
	echo "home_url:\n";
	echo "\n\n";
	return $ss_post->url;	
}

function get_permalink($postID){
	global $ss_post;
	echo "get_permalink:\n";
	echo "\n\n";
	return $ss_post->url;	
}

function get_post($postID){
	global $ss_post;
	echo "get_post:\n";
	echo "\n\n";
	$result = new stdclass;
	$result->post_title = $ss_post->title;
	$result->post_excerpt = $ss_post->excerpt;
	$result->post_content = $ss_post->content;
	return $result;	
}

function wp_get_post_tags($postID){
	global $ss_post;
	echo "wp_get_post_tags:\n";
	echo "\n\n";
	$result = array();
	foreach($ss_post->tags as $v) {
		$tag = new stdclass;
		$tag->name = $v;
		$result[] = $tag;
	}
	return $result;
	//return $post->tags;	
}


// $post = new stdclass;
// $post->id = 666;
// $post->title = 'title';
// $post->excerpt = 'excerpt';
// $post->content = 'content';
// $post->url = 'http://ya.ru/';
// $post->tags = array('tag1','tag2');
// 
// $options = array(
// 	'ii' => 777,
// 	'nName' => '=== nName ===',
// 	'reset' => false,
// 
// 	'suSvC' => 1,
// 	'suCat' => 1,
// 	'suUName' => 'alex.kirs@gmail.com',
// 	'suPass' => '1234554321',
// 	'suMsgFormat' => "%TITLE% - %EXCERPT%",
// 	'suInclTags' => 1,
// 
// 	'pnBoard' => 92042454823384320,
// 	'pnUName' => 'alex.kirs@gmail.com',
// 	'pnPass' => '1234554321',
// 	'pnMsgFormat' => "%TITLE% - %EXCERPT%",
// 	'pnDefImg' => 'http://img2.owned.com/media/images/1/4/9/6/14969/upsexy_540.jpg',
// 
// 	'trUName' => 'media@owned.com',
// 	'trPass' => 'h7!LiB6@bR)9',
// 	'trMsgFormat' => "%TITLE% - %EXCERPT%",
// 	'trDefImg' => 'http://img2.owned.com/media/images/1/4/9/6/14969/upsexy_540.jpg',
// );
//isPosted
//suInclTags
//nsOpenGraph

//nxs_doPublishToSU($post->id, $options);
//var_dump(doConnectToPinterest($options['pnUName'], $options['pnPass']));
//var_dump(doGetBoardsFromPinterest());
//nxs_doPublishToPN($post->id, $options);
//nxs_doPublishToTR($post->id, $options);

//echo "\n";


if (!function_exists('nxs_getPostImage')){ function nxs_getPostImage($postID, $size='large', $def='') { $imgURL = '';  global $plgn_NS_SNAutoPoster;  if (!isset($plgn_NS_SNAutoPoster)) return; $options = $plgn_NS_SNAutoPoster->nxs_options;
  $imgURL = "https://www.google.com/images/srpr/logo3w.png"; $res = nxs_chckRmImage($imgURL); $imgURL = ''; if (!$res) $options['imgNoCheck'] = '1';
  //## Featured Image from Specified Location
  if ((int)$postID>0 && isset($options['featImgLoc']) && $options['featImgLoc']!=='') {  $afiLoc= get_post_meta($postID, $options['featImgLoc'], true); 
    if (is_array($afiLoc) && $options['featImgLocArrPath']!='') { $cPath = $options['featImgLocArrPath'];
      while (strpos($cPath, '[')!==false){ $arrIt = CutFromTo($cPath, '[', ']'); $arrIt = str_replace("'", "", str_replace('"', '', $arrIt)); $afiLoc = $afiLoc[$arrIt]; $cPath = substr($cPath, strpos($cPath, ']'));}    
    } $imgURL = trim($options['featImgLocPrefix']).trim($afiLoc); if ($imgURL!='' && stripos($imgURL, 'http')===false) $imgURL =  home_url().$imgURL;
  }
  if ($imgURL!='' && $options['imgNoCheck']!='1' && nxs_chckRmImage($imgURL)==false) $imgURL = '';  if ($imgURL!='') return $imgURL;
  //## Featured Image
  if ($imgURL=='') { if ((int)$postID>0 && function_exists("get_post_thumbnail_id") ){ $imgURL = wp_get_attachment_image_src(get_post_thumbnail_id($postID), $size); $imgURL = $imgURL[0]; } } 
  if ($imgURL!='' && $options['imgNoCheck']!='1' && nxs_chckRmImage($imgURL)==false) $imgURL = ''; if ($imgURL!='') return $imgURL;  
  //## YAPB
  if ((int)$postID>0 && class_exists("YapbImage")) { $imgURLObj = YapbImage::getInstanceFromDb($postID); if (is_object($imgURLObj)) $imgURL = $imgURLObj->uri; 
    $stURL = site_url(); if (substr($stURL, -1)=='/') $stURL = substr($stURL, 0, -1);  if ($imgURL!='') $imgURL = $stURL.$imgURL; 
  }
  if ($imgURL!='' && $options['imgNoCheck']!='1' && nxs_chckRmImage($imgURL)==false) $imgURL = ''; if ($imgURL!='') return $imgURL;
  //## Find Images in Post
  if ((int)$postID>0 && $imgURL=='') {$post = get_post($postID); $imgsFromPost = nsFindImgsInPost($post, $options['useUnProc'] == '1'); if (is_array($imgsFromPost) && count($imgsFromPost)>0) $imgURL = $imgsFromPost[0]; } //echo "##".count($imgsFromPost); prr($imgsFromPost);
  if ($imgURL!='' && $options['imgNoCheck']!='1' && nxs_chckRmImage($imgURL)==false) $imgURL = ''; if ($imgURL!='') return $imgURL;
  //## Attachements
  if ((int)$postID>0 && $imgURL=='') { $attachments = get_posts(array('post_type' => 'attachment', 'posts_per_page' => -1, 'post_parent' => $postID)); 
      if (is_array($attachments) && isset($attachments[0]) && is_object($attachments[0])) $imgURL = wp_get_attachment_image_src($attachments[0]->ID, $size);
      if(isset($imgURL[0]))
	      $imgURL = $imgURL[0];      
  }
  if ($imgURL!='' && $options['imgNoCheck']!='1' && nxs_chckRmImage($imgURL)==false) $imgURL = ''; if ($imgURL!='') return $imgURL;    
  //## Default
  if (trim($imgURL)=='' && trim($def)=='') $imgURL = $options['ogImgDef']; 
  if (trim($imgURL)=='' && trim($def)!='') $imgURL = $def; 

  return $imgURL;
}}
if (!function_exists("nxs_metaMarkAsPosted")) { function nxs_metaMarkAsPosted($postID, $nt, $did, $args=''){ $mpo =  get_post_meta($postID, 'snap'.$nt, true); $mpo =  maybe_unserialize($mpo); 
  if (!is_array($mpo)) $mpo = array(); if (!isset($mpo[$did]) || !is_array($mpo[$did])) $mpo[$did] = array();
  if ($args=='' || (isset($args['isPosted']) && $args['isPosted']=='1')) $mpo[$did]['isPosted'] = '1';  
  if (is_array($args) && isset($args['isPrePosted']) && $args['isPrePosted']==1) $mpo[$did]['isPrePosted'] = '1';  
  if (is_array($args) && isset($args['pgID'])) $mpo[$did]['pgID'] = $args['pgID'];  
  if (is_array($args) && isset($args['postURL'])) $mpo[$did]['postURL'] = $args['postURL'];  
  if (is_array($args) && isset($args['pDate'])) $mpo[$did]['pDate'] = $args['pDate'];  
  /*$mpo = mysql_real_escape_string(serialize($mpo)); */ delete_post_meta($postID, 'snap'.$nt); add_post_meta($postID, 'snap'.$nt, serialize($mpo));
}}

//## Format Message
if (!function_exists("nsFormatMessage")) { function nsFormatMessage($msg, $postID, $addURLParams=''){ global $ShownAds, $plgn_NS_SNAutoPoster, $nxs_urlLen; $post = get_post($postID); $options = $plgn_NS_SNAutoPoster->nxs_options; 
	$lng = 'en';
  if ($addURLParams=='' && $options['addURLParams']!='') $addURLParams = $options['addURLParams'];
  $msg = stripcslashes($msg); if (isset($ShownAds)) $ShownAdsL = $ShownAds; // $msg = htmlspecialchars(stripcslashes($msg)); 
  $msg = nxs_doSpin($msg);
  if (preg_match('%URL%', $msg)) { $url = get_permalink($postID); if($addURLParams!='') $url .= (strpos($url,'?')!==false?'&':'?').$addURLParams;  $nxs_urlLen = nxs_strLen($url); $msg = str_ireplace("%URL%", $url, $msg);}
  if (preg_match('%SURL%', $msg)) { $url = get_permalink($postID); if($addURLParams!='') $url .= (strpos($url,'?')!==false?'&':'?').$addURLParams; 
    $url = nxs_mkShortURL($url, $postID); $nxs_urlLen = nxs_strLen($url); $msg = str_ireplace("%SURL%", $url, $msg);
  }
  if (preg_match('%IMG%', $msg)) { $imgURL = nxs_getPostImage($postID); $msg = str_ireplace("%IMG%", $imgURL, $msg); } 
  if (preg_match('%TITLE%', $msg)) { $title = nxs_doQTrans($post->post_title, $lng);  $msg = str_ireplace("%TITLE%", $title, $msg); }                    
  if (preg_match('%STITLE%', $msg)) { $title = nxs_doQTrans($post->post_title, $lng);   $title = substr($title, 0, 115); $msg = str_ireplace("%STITLE%", $title, $msg); }                    
  if (preg_match('%AUTHORNAME%', $msg)) { $aun = $post->post_author;  $aun = get_the_author_meta('display_name', $aun );  $msg = str_ireplace("%AUTHORNAME%", $aun, $msg);}                    
  if (preg_match('%ANNOUNCE%', $msg)) { $postContent = nxs_doQTrans($post->post_content, $lng);     
    $postContent = strip_tags(strip_shortcodes(str_ireplace('<!--more-->', '#####!--more--!#####', str_ireplace("&lt;!--more--&gt;", '<!--more-->', $postContent))));
    if (stripos($postContent, '#####!--more--!#####')!==false) { $postContentEx = explode('#####!--more--!#####',$postContent); $postContent = $postContentEx[0]; }    
      else $postContent = nsTrnc($postContent, $options['anounTagLimit']);  $msg = str_ireplace("%ANNOUNCE%", $postContent, $msg);
  }
  if (preg_match('%TEXT%', $msg)) {      
    if ($post->post_excerpt!="") $excerpt = apply_filters('the_content', nxs_doQTrans($post->post_excerpt, $lng)); else $excerpt= apply_filters('the_content', nxs_doQTrans($post->post_content, $lng)); 
      $excerpt = nsTrnc(strip_tags(strip_shortcodes($excerpt)), 300, " ", "..."); $msg = str_ireplace("%TEXT%", $excerpt, $msg);
  }
  if (preg_match('%EXCERPT%', $msg)) {      
    if ($post->post_excerpt!="") $excerpt = apply_filters('the_content', nxs_doQTrans($post->post_excerpt, $lng)); else $excerpt= apply_filters('the_content', nxs_doQTrans($post->post_content, $lng)); 
      $excerpt = nsTrnc(strip_tags(strip_shortcodes($excerpt)), 300, " ", "..."); $msg = str_ireplace("%EXCERPT%", $excerpt, $msg);
  }
  if (preg_match('%RAWEXTEXT%', $msg)) {      
    if ($post->post_excerpt!="") $excerpt = nxs_doQTrans($post->post_excerpt, $lng); else $excerpt= nxs_doQTrans($post->post_content, $lng); 
      $excerpt = nsTrnc(strip_tags(strip_shortcodes($excerpt)), 300, " ", "..."); $msg = str_ireplace("%RAWEXTEXT%", $excerpt, $msg);
  }
  if (preg_match('%RAWEXCERPT%', $msg)) {      
    if ($post->post_excerpt!="") $excerpt = nxs_doQTrans($post->post_excerpt, $lng); else $excerpt= nxs_doQTrans($post->post_content, $lng); 
      $excerpt = nsTrnc(strip_tags(strip_shortcodes($excerpt)), 300, " ", "..."); $msg = str_ireplace("%RAWEXCERPT%", $excerpt, $msg);
  }
  if (preg_match('%TAGS%', $msg)) { $t = wp_get_object_terms($postID, 'product_tag'); if ( empty($t) || is_wp_error($pt) || !is_array($t) ) $t = wp_get_post_tags($postID);
    $tggs = array(); foreach ($t as $tagA) {$tggs[] = $tagA->name;} $tags = implode(', ',$tggs); $msg = str_ireplace("%TAGS%", $tags, $msg);
  }
  if (preg_match('%CATS%', $msg)) { $t = wp_get_post_categories($postID); $cats = array();  foreach($t as $c){ $cat = get_category($c); $cats[] = str_ireplace('&','&amp;',$cat->name); } 
          $ctts = implode(', ',$cats); $msg = str_ireplace("%CATS%", $ctts, $msg);
  }
  if (preg_match('%HCATS%', $msg)) { $t = wp_get_post_categories($postID); $cats = array();  
    foreach($t as $c){ $cat = get_category($c);  $cats[] = "#".trim(str_replace(' ','', str_replace('  ', '', trim(str_ireplace('&','',str_ireplace('&amp;','',$cat->name)))))); } 
    $ctts = implode(', ',$cats); $msg = str_ireplace("%HCATS%", $ctts, $msg);
  }  
  if (preg_match('%HTAGS%', $msg)) { $t = wp_get_object_terms($postID, 'product_tag'); if ( empty($t) || is_wp_error($pt) || !is_array($t) ) $t = wp_get_post_tags($postID);
    $tggs = array(); foreach ($t as $tagA) {$tggs[] = "#".trim(str_replace(' ','',preg_replace('/[^a-zA-Z0-9\p{L}\p{N}\s]/u', '', trim(ucwords(str_ireplace('&','',str_ireplace('&amp;','',$tagA->name)))))));  } 
    $tags = implode(', ',$tggs); $msg = str_ireplace("%HTAGS%", $tags, $msg);
  }   
  if (preg_match('%CF-[a-zA-Z0-9]%', $msg)) { $msgA = explode('%CF', $msg); $mout = '';
    foreach ($msgA as $mms) { 
        if (substr($mms, 0, 1)=='-' && stripos($mms, '%')!==false) { $mGr = CutFromTo($mms, '-', '%'); $cfItem =  get_post_meta($postID, $mGr, true); $mms = str_ireplace("-".$mGr."%", $cfItem, $mms); } $mout .= $mms; 
    } $msg = $mout; 
  }  
  if (preg_match('%FULLTEXT%', $msg)) { $postContent = apply_filters('the_content', nxs_doQTrans($post->post_content, $lng)); $msg = str_ireplace("%FULLTEXT%", $postContent, $msg);}                    
  if (preg_match('%RAWTEXT%', $msg)) { $postContent = nxs_doQTrans($post->post_content, $lng); $msg = str_ireplace("%RAWTEXT%", $postContent, $msg);}
  if (preg_match('%SITENAME%', $msg)) { $siteTitle = htmlspecialchars_decode(get_bloginfo('name'), ENT_QUOTES); $msg = str_ireplace("%SITENAME%", $siteTitle, $msg);}      
  if (isset($ShownAds)) $ShownAds = $ShownAdsL; // FIX for the quick-adsense plugin
  return trim($msg);
}}


require dirname(__FILE__)."/social-networks-auto-poster-facebook-twitter-g/NextScripts_SNAP.php";
require dirname(__FILE__)."/social-networks-auto-poster-facebook-twitter-g/inc-cl/apis/base_facebook.php";
// require dirname(__FILE__)."/social-networks-auto-poster-facebook-twitter-g/inc-cl/pn.php";
// require dirname(__FILE__)."/social-networks-auto-poster-facebook-twitter-g/inc-cl/tr.php";
//require dirname(__FILE__)."/__plugins_cache_242.php_";
