<?php

namespace app\models;


use Yii;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\helpers\Url;

class FilterStatistic extends Model
{
    const DAY_TIME = 86400;

    const DEFAULT_PERIOD_VALUE = 'today';

    public $period;
    public $date_from;
    public $date_to;
    public $timeFrom;
    public $timeTo;
    public $tableAlias = 'p';
    public $formActionUrl;
    public $presets = [
        'today',
        'week',
        'month',
        'all',
    ];

    public function getTime24()
    {
        return time() - self::DAY_TIME;
    }

    public function getAttributeTime($attribute, $defaultDate = null)
    {
        if ($this->{$attribute} === null || !strtotime($this->{$attribute})) {
            return $defaultDate === null ? $this->time24 : strtotime($defaultDate);
        }

        return strtotime($this->{$attribute});
    }

    public function filterAttributeParams()
    {
        return [
            'period',
            'date_from',
            'date_to',
        ];
    }

    public function rules()
    {
        return [
            [['period'], 'in', 'range' => $this->presets, 'skipOnEmpty' => true],
            [['date_from', 'date_to'], 'default', 'value' => null],
            [['date_from', 'date_to'], 'date', 'format' => 'yyyy-MM-dd'],
        ];
    }

    public function initFormActionUrl()
    {
        $urlParams = [Yii::$app->controller->getRoute()];
        $this->formActionUrl = Url::to($urlParams, true);
    }

    public function unsetInvalidAttributes()
    {
        $this->validate();
        if ($this->hasErrors()) {
            foreach ($this->getErrors() as $attr => $message) {
                $this->{$attr} = NULL;
            }
        }
    }

    public function init()
    {
        parent::init();

        $this->loadFromGet();
        $this->unsetInvalidAttributes();

        $this->date_to = $this->date_to ?: $this->date_from;

        $this->timeFrom = $this->getAttributeTime('date_from');
        $this->timeTo = $this->getAttributeTime('date_to');
        if ($this->timeFrom > $this->timeTo) {
            list($this->timeFrom, $this->timeTo) = array($this->timeTo, $this->timeFrom);
        }

        $this->initFormActionUrl();
    }


    protected function loadFromGet()
    {
        foreach ($this->filterAttributeParams() as $attribute => $param) {
            $attribute = is_numeric($attribute) ? $param : $attribute;
            $this->{$attribute} = Yii::$app->request->get($param);
        }
    }

    public function initPeriodTime()
    {
        switch ($this->period) {
            case 'today':
                $this->timeFrom = $this->time24;
                $this->timeTo = $this->time24;
                break;

            case 'week':
                $this->timeFrom = strtotime('-7 DAY');
                $this->timeTo = strtotime('today');
                break;

            case 'month':
                $this->timeFrom = strtotime('-30 DAY');
                $this->timeTo = strtotime('today');
                break;

            case 'all':
                $this->timeFrom = null;
                $this->timeTo = null;
                break;

            default:
                $this->timeFrom = $this->timeFrom ?: $this->time24;
                $this->timeTo = $this->timeTo ?: $this->time24;
        }
    }

    protected function reInitPeriodTime()
    {
        if ($this->timeFrom === $this->time24 && $this->timeTo === $this->time24) {
            $this->timeFrom = strtotime('today');
            $this->timeTo = strtotime('today');
        }
    }

    public function addPeriodFilter(ActiveQuery $query, $dateField = 'date')
    {
        $this->initPeriodTime();
        if ($this->timeFrom && $this->timeTo) {

            if ($this->timeFrom === $this->timeTo) {

                if ($this->timeFrom === $this->time24) {
                    $from = new Expression('NOW() - INTERVAL 24 HOUR');
                    $to = new Expression('NOW()');
                } else {
                    $from = date('Y-m-d H:i:s', $this->timeFrom);
                    $to = date('Y-m-d H:i:s', $this->timeFrom + self::DAY_TIME);
                }

            } else {
                $from = date('Y-m-d H:i:s', $this->timeFrom);
                $to = date('Y-m-d H:i:s', $this->timeTo + self::DAY_TIME);
            }

            $query->andWhere(['between', $dateField, $from, $to]);
        }
        $this->reInitPeriodTime();
        return $query;
    }

    public function getDateFrom($format = 'Y-m-d', $defaultTime = null)
    {
        if ($this->timeFrom === null) {
            return $defaultTime ? date($format, $defaultTime) : null;
        }

        return date($format, $this->timeFrom);
    }

    public function getDateTo($format = 'Y-m-d', $defaultTime = null)
    {
        if ($this->timeTo === null) {
            return $defaultTime ? date($format, $defaultTime) : null;
        }
        return date($format, $this->timeTo);
    }

    public function getPeriodString($format = 'F d Y')
    {
        if ($this->period !== null) {
            return $this->period;
        } else if ($this->timeFrom === $this->timeTo) {
            return $this->timeFrom === strtotime('TODAY') ? 'today' : date($format, $this->timeFrom);
        } else {
            return date($format, $this->timeFrom) . ' - ' . date($format, $this->timeTo);
        }
    }

    public function getPresetsLabels()
    {
        $labels = [];
        foreach ($this->presets as $preset) {
            $labels[$preset] = [
                'url' => Url::to([Yii::$app->controller->getRoute(), 'period' => $preset]),
                'title' => $preset,
            ];
        }
        return $labels;
    }

    public function getUserStatisticUrl($type)
    {
        if ($this->dateFrom === $this->dateTo) {
            return Url::to([
                'admin/users-statistic',
                'type' => $type,
                'date_from' => $this->dateFrom,
            ]);
        } else {
            return Url::to([
                'admin/users-statistic',
                'type' => $type,
                'date_from' => $this->dateFrom,
                'date_to' => $this->dateTo,
            ]);
        }
    }

    public function getExternalCommentsUrl()
    {
        $host = 'http://' . Yii::$app->params['commentsHost'];
        return $host . Url::to([
            '/widgets/manage-comments',
            'widget_id' => Yii::$app->params['commentsClientId'],
            'date_from' => $this->dateFrom,
            'date_to' => $this->dateTo
        ]);
    }

}
