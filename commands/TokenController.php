<?php

namespace app\commands;


use app\helpers\TokenHelper;
use yii\console\Controller;

class TokenController extends Controller
{
    public function actionUpdate()
    {
//        try {
//            echo TokenHelper::updateYoutubeToken() ? "Youtube token was successfully updated" : "Token was not updated";
//        } catch (\yii\console\Exception $e) {
//            echo  $e->getMessage();
//        }
//        echo PHP_EOL;

        try {
            TokenHelper::$tokenFilePathAlias = '@runtime/token_comments';
            echo TokenHelper::updateCommentsToken() ? "Comments token was successfully updated" : "Token was not updated";
        } catch (\yii\console\Exception $e) {
            echo  $e->getMessage();
        }
        echo PHP_EOL;
    }
}