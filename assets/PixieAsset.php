<?php

namespace app\assets;

use app\components\AssetBundle;

class PixieAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/edit-image.js',
    ];

    public $css = [
        'css/pixie/css/main.css',
        'css/edit-image.css'
    ];

    public $jsFolders = [
        'js/pixie/vendor',
        'js/pixie/editor/basics',
        'js/pixie/editor/crop',
        'js/pixie/editor/directives',
        'js/pixie/editor/drawing',
        'js/pixie/editor/resources',
        'js/pixie/editor/shapes',
        'js/pixie/editor/text',
        'js/pixie/editor/filters',
        'js/pixie/editor',
        'js/pixie/editor/objects',
    ];

    public $depends = [
        'app\assets\FullUploaderAsset',
        'app\assets\AngularjsAsset',
    ];


}