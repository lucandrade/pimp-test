<?php

namespace app\assets;


use yii\web\AssetBundle;

class UploaderPopupLoaderAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
    ];

    public $js = [
        'js/popup-uploader-loader.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];

    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}