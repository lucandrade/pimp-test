<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use app\widgets\AuthChoice;
use yii\widgets\Breadcrumbs;
use app\widgets\Alert;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;

?>
<?= Alert::widget();?>
<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<?= Breadcrumbs::widget(
				[
					'encodeLabels'=>false,
					'homeLink'=>[
						'label'=>'<i class="fa fa-home"></i>',
						'url'=>['post/index']
					],
					'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
				]
			) ?>

			<?php $form = ActiveForm::begin([]); ?>
			<div class="panel panel-default">
				<div class="panel-body">
					<h3><i class="fa fa-sign-in"></i> Sign-in</h3>
					<hr>
					<?= $form->field($model, 'loginId')->label(false)->textInput(['placeholder' => 'Email or Username']) ?>
					<?= $form->field($model, 'password')->label(false)->passwordInput(['placeholder' => 'Password']) ?>
					<?= Html::a('Forgot Password?', ['site/reset']) ?>
					<?= $form->field($model, 'rememberMe')->checkbox(['class']) ?>

					<div class="pull-left">Or login with:</div>
					<?php
					$authAuthChoice = AuthChoice::begin(
						[
							'baseAuthUrl' => ['site/auth']
						]
					);

					echo Html::ul(
						array_map(
							function ($client) use ($authAuthChoice) {
								return Html::a('<i class="fa fa-' . $client->getName() . ' fa-2x fa-fw"></i>', $authAuthChoice->createClientUrl($client));
							},
							$authAuthChoice->getClients()
						),
						[
							'encode' => false,
							'class' => 'list-inline pull-right'
						]
					);
					AuthChoice::end();
					?>
					<div class="clearfix"></div>
					<hr>
					<div class="text-center">
						<div class="btn-group" role="group">
							<?= Html::submitButton('<i class="fa fa-check"></i> Login', ['class' => 'btn btn-primary']) ?>
							<?= Html::a('<i class="fa fa-user-plus"></i> Create an account', ['site/signup'], ['class' => 'btn btn-default']) ?>
						</div>
					</div>
				</div>
				<?php $form::end(); ?>
			</div>
		</div>
	</div>
</div>