<?php

/**
 * @var $this yii\web\View
 * @var $form yii\widgets\ActiveForm
 * @var $model app\models\PostBlock
 */

use yii\helpers\Html;

?>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="post-block-manage">
            <button type="button" class="js-remove btn btn-sm btn-danger pull-right">
                <i class="fa fa-trash"></i>
            </button>
            <span class="drag-handle text-muted h4">&#9776;</span>
            &nbsp;
            <button type="button" class="btn btn-sm btn-default add-post-item">
                <i class="fa fa-plus"></i>
            </button>
        </div>
        <div class="form-group">
            <?= Html::activeHiddenInput($model, '[' . $model->id . ']id', ['class' => 'form-control']); ?>
            <?= Html::activeHiddenInput($model, '[' . $model->id . ']position', ['class' => 'form-control']); ?>
            <div class="help-block">
                <img class="img-responsive" src="<?= $model->getImgSrc('adjusted'); ?>">
            </div>
        </div>
    </div>
</div>