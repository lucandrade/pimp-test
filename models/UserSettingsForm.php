<?php

namespace app\models;

use yii\base\Model;
use app\helpers\ImageHelper;

class UserSettingsForm extends Model
{
    public $avatar;
    public $profileAvatar;
    public $file_name;
    public $username;
    public $show_adult;
    public $autoplay_gifs;
    public $expand_posts;
    public $currentPassword;
    public $newPassword;
    public $newPassword2;

    /**
     * @var User
     */
    private $_user;
    private $_passwordIsSet;


    public function rules()
    {
        return [
            [['username', 'file_name'], 'filter', 'filter' => 'trim'],
            [['username'], 'string', 'min' => 2, 'max' => 255],
            [['show_adult', 'expand_posts', 'autoplay_gifs'], 'boolean'],

            ['currentPassword', 'validateCurrentPassword', 'skipOnEmpty' => false, 'on' => 'change_password'],
            [['currentPassword', 'newPassword', 'newPassword2'], 'string', 'min' => 6],

            ['newPassword2', 'required', 'on' => 'change_password'],
            ['newPassword2', 'compare', 'compareAttribute' => 'newPassword',
                'operator' => '===', 'message' => "Passwords don't match", 'skipOnEmpty' => false],

        ];
    }

    public function attributeLabels()
    {
        return [
            'show_adult' => 'Show adult',
            'autoplay_gifs' => 'On/off autoplay on gifs'
        ];
    }


    public function validateCurrentPassword($attribute, $params)
    {
        if (!empty($this->newPassword) && $this->getPasswordIsSet()) {
            if (empty($this->currentPassword)) {
                $this->addError($attribute, "Please input current password to set new one");
            } else {
                if (!$this->_user->validatePassword($this->currentPassword)) {
                    $this->addError('currentPassword', 'Current password is wrong');
                }
            }
        }
    }


    public function __construct(array $config = [])
    {
        $this->_user = \Yii::$app->user->identity;

        $this->avatar = $this->_user->avatar;
        $this->profileAvatar = $this->_user->profileAvatar;
        $this->username = $this->_user->username;
        $this->show_adult = $this->_user->getSettings('show_adult');
        $this->autoplay_gifs = $this->_user->getSettings('autoplay_gifs');
        $this->expand_posts = $this->_user->getSettings('expand_posts');
        $this->_passwordIsSet = !empty($this->_user->password);

        $this->resetPasswords();
        parent::__construct($config);
    }


    public function applySettings()
    {
        if ($this->validate()) {
            $user = $this->_user;
            $user->scenario = User::SCENARIO_SETTINGS;


            //disabled changing username
//            $user->username = $this->username;
//            $user->getForumUser()->username = $this->username;

            $user->setSettings('show_adult', $this->show_adult);
            $user->setSettings('expand_posts', $this->expand_posts);
            $user->setSettings('autoplay_gifs', $this->autoplay_gifs);

            if (!empty($this->newPassword))
            {
                $user->setPassword($this->newPassword);
                $user->getForumUser()->setPassword($this->newPassword);
                $this->_passwordIsSet = true;
            }

            if(!$user->getForumUser()->save()){
                $user->transferForumErrors();
            }

            if ($user->save()) {
                return true;
            }
        }

        return false;
    }

    public function applyAvatar()
    {
        $user = $this->_user;
        $loadedImages = ImageHelper::getLoadedImages('UserSettingsForm', 'upload_name');
        $avatarImage = reset($loadedImages);

        if (!$avatarImage) {
            $this->addErrors(['file_name' => 'Avatar image not found']);
            return false;
        }

        if ($user->saveLoadedAvatar($avatarImage)) {
            ImageHelper::removeTmpPath($avatarImage);
            $user->current_oauth_provider = null;
            $user->update(true, ['current_oauth_provider']);
            return true;
        }

        $this->addErrors(['file_name' => $user->getErrors('file_name')]);
        return false;
    }


    public function getUser()
    {
        return $this->_user;
    }


    public function resetPasswords()
    {
        $this->newPassword = '';
        $this->newPassword2 = '';
        $this->currentPassword = '';
    }


    public function getPasswordIsSet()
    {
        return $this->_passwordIsSet;
    }
}