<?php

use app\models\User;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\grid\CheckboxColumn;
use yii\bootstrap\Html;
use yii\grid\ActionColumn;


/**
 * @var $dataProvider ActiveDataProvider
 * @var $filter string
 * @var $changeFields array
 * @var $modelClass string
 */

$this->title = $this->title ?: 'Users';
$this->params['path'] = isset($this->params['path']) && !empty($this->params['path']) ? $this->params['path'] : [['label' => 'Users']];

// todo: create link - take from action link(?)
// todo: reset link - take from current route
// todo: delete selected button - move on top

echo GridView::widget(
    [
        'layout' => $this->render('_index_grid_layout', [
            'filter' => $filter,
            'changeFields' => $changeFields,
            'modelClass' => $modelClass
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover responsive'
        ],
        'dataProvider' => $dataProvider,
        'rowOptions' => function ($model, $key, $index, $grid) {
            if ($model->isSoftDeleted) {
                return ['class' => 'soft-deleted'];
            }
            return [];
        },
        'columns' => [
            [
                'class' => CheckboxColumn::className(),
                'headerOptions' => [
                    'class' => 'col-static',
                ],
                'contentOptions' => [
                    'class' => 'col-static',
                ]
            ],
            [
                'attribute' => 'id',
                'headerOptions' => [
                    'class' => 'col-static',
                ],
                'contentOptions' => [
                    'class' => 'col-static',
                ]
            ],
            [
                'attribute' => 'username',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->username, ['admin/users-edit', 'id' => $model->id]);
                }
            ],
            'email',
            [
                'attribute' => 'is_activated_email',
                'format' => 'raw',
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function ($model) {
                    return '<i class="fa fa-' . ($model->is_activated_email ? 'check text-success' : 'times text-danger') . '"></i>';
                }
            ],
            [
                'attribute' => 'is_admin',
                'format' => 'raw',
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function ($model) {
                    return '<i class="fa fa-' . ($model->is_admin ? 'check text-success' : 'times text-danger') . '"></i>';
                }
            ],
            [
                'attribute' => 'lastvisit_at',
                'format' => 'raw',
                'contentOptions' => [
                    'style' => 'width: 200px'
                ],
                'value' => function (User $model) {
                    if (!$model->lastvisit_at || $model->lastvisit_at === '0000-00-00 00:00:00') {
                        return ' - ';
                    }

                    $visitDate = $model->getLastVisitDate();
                    $visitDate = '<span class="small text-muted">'
                        . Yii::$app->formatter->asDatetime($model->lastvisit_at, 'php:Y-m-d H:i')
                        . '</span>'
                        . '<br>'
                        . $visitDate;

                    return $visitDate;
                }
            ],
            [
                'label' => 'Banned',
                'attribute' => 'status',
                'format' => 'raw',
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function ($model) {
                    return '<i class="fa fa-' . ($model->status === User::STATUS_BANNED ? 'check text-success' : 'times text-danger') . '"></i>';
                }
            ],
            [
                'label' => 'Linked',
                'attribute' => 'current_oauth_provider',
                'format' => 'raw',
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'value' => function ($model) {
                    return $model->linkedProviderIcons;
                }
            ],
            [
                'class' => ActionColumn::className(),
                'template' => '{update} {delete}',
                'contentOptions' => [
                    'class' => 'text-right nowrap'
                ],
                'buttons' => [
                    'update' => function ($url, $model, $key) {

                        $html = $model->isSoftDeleted
                            ? Html::a('<i class="fa fa-reply"></i> Restore',
                                ['admin/users-restore', 'ids' => $model->id],
                                [
                                    'class' => 'btn btn-xs btn-info action-button',
                                    'data-id' => $model->id
                                ]) . " "
                            : "";
                        $html .= Html::a('<i class="fa fa-edit"></i> Edit',
                            [
                                'admin/users-edit',
                                'id' => $model->id
                            ],
                            [
                                'class' => 'btn btn-xs btn-success'
                            ]
                        );

                        return $html;
                    },
                    'delete' => function ($url, $model, $key) {
                        return $model->isSoftDeleted ?
                            Html::a('<i class="fa fa-remove"></i> Purge', [
                                'admin/users-purge',
                            ], [
                                    'class' => 'btn btn-xs btn-danger delete-button action-button',
                                    'data-id' => $model->id,
                                    'data-need-confirmation' => 1
                                ]
                            )
                            : Html::a('<i class="fa fa-trash"></i> Delete', [
                                'admin/users-delete',
                            ], [
                                    'class' => 'btn btn-xs btn-danger delete-button action-button',
                                    'data-id' => $model->id,
                                    'data-need-confirmation' => 1
                                ]
                            );
                    }
                ]
            ]
        ]
    ]
);