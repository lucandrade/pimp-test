<?php
namespace app\components\authclients;

use Google_Client;
use Google_Exception;
use Google_Service_YouTube;
use app\components\behaviors\ConsoleAuthClientBehavior;
use yii\authclient\clients\GoogleOAuth;

class GoogleYoutubeAdmin extends GoogleOAuth
{
    public $tokenFileDir = "@runtime/token";

    public $tokenUrl = 'https://www.googleapis.com/oauth2/v4/token';

    public $deleteVideoUrl = "https://www.googleapis.com/youtube/v3/videos";

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = ConsoleAuthClientBehavior::className();
        return $behaviors;
    }

    public function buildAuthUrl(array $params = [])
    {
        return parent::buildAuthUrl(['approval_prompt' => 'force', 'access_type' => 'offline']);
    }

    public function getYoutubeToken()
    {
        $token = $this->getTokenFromFile();
        if (strtolower($this->getFileTokenStatus()) === 'ok') {
            return $token;
        } else {
            throw new Google_Exception('Youtube admin token is wrong or expired, please update it');
        }
    }

    public function removeVideo($id)
    {
        if (empty($id)) {
            throw new Google_Exception("Please provide youtube video id to delete");
        }

        $googleCLient = new Google_Client();
        $googleCLient->setAccessToken($this->getYoutubeToken());
        $service = new Google_Service_YouTube($googleCLient);
        /**
         * @var $response \GuzzleHttp\Psr7\Response;
         */
        $response = $service->videos->delete($id);
        return $response instanceof \GuzzleHttp\Psr7\Response && $response->getStatusCode() === 204;
    }

    public function renameVideo($id, $title)
    {
        if (empty($id)) {
            throw new Google_Exception("Please provide youtube video id to rename");
        }

        if (empty($title)) {
            throw new Google_Exception("Please provide title for video to rename");
        }

        $googleCLient = new Google_Client();
        $googleCLient->setAccessToken($this->getYoutubeToken());
        $service = new Google_Service_YouTube($googleCLient);

        $listResponse = $service->videos->listVideos('snippet', ['id' => $id]);
        if (isset($listResponse[0])) {
            $listResponse[0]['snippet']['title'] = $title;
            /**
             * @var $video \Google_Service_YouTube_Video
             */
            $video = $service->videos->update('snippet', $listResponse[0]);
            return $video instanceof \Google_Service_YouTube_Video && $video->snippet['title'] === $title;
        }

        throw new Google_Exception('<h3>Can\'t find a video with video id: ' . $id . '</h3>');
    }
}