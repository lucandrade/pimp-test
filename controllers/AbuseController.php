<?php

namespace app\controllers;


use app\components\Controller;
use app\helpers\ArrayHelper;
use app\helpers\LogHelper;
use app\models\Abuse;
use app\models\AbuseSearch;
use app\models\Image;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\components\filters\JsonResponse;

class AbuseController extends Controller
{

    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['bot-batch-report']
                    ],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'cancel' => ['post'],
                    'purge-all' => ['post'],
                ],
            ],
            'jsonResponse' => [
                'class' => JsonResponse::className(),
                'only' => ['ajax-batch-report', 'ajax-report', 'cancel', 'purge-all']
            ]
        ];
    }


    public function actionAjaxBatchReport()
    {
        $urls = $this->processAbuseRequest()[0];
        return ['urls'=>$urls, 'time' => date('Y-m-d H:i:s')];
    }


    public function actionBotBatchReport()
    {
        ini_set('memory_limit', '999M');
        $reportRes = $this->actionAjaxBatchReport();
        Abuse::purgeAll();
        echo json_encode($reportRes);
        echo json_encode(['time'=>date('Y-m-d H:i:s')]);
    }

    public function actionPurgeAll()
    {
        return [
            'count' => Abuse::purgeAll()
        ];
    }

    public function actionCancel()
    {
        $id = \Yii::$app->request->post('id');
        if(!$abuse  = Abuse::findOne(['id' => $id])){
            throw new NotFoundHttpException("Abuse record with such id not found");
        }

        $res = [];
        if($abuse->delete()){
            $res['status'] = 'ok';
        }else{
            $res['error'] = $abuse->hasErrors() ? $abuse->getErrors() : "Can't cancel abuse for some reason";
        }

        return $res;
    }

    public function actionList()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Abuse::find()->with('image')
        ]);

        return $this->render('list', ['dataProvider' => $dataProvider]);
    }


    public function actionAjaxReport()
    {
        $request = Json::decode($_GET['req'], false);
        if(empty($request->url)){
            throw new BadRequestHttpException("Json object must contain 'url' property");
        }

        $result = [];
        $abuses = Abuse::createByUrl($request->url);
        if(!empty($abuses)) {
            $result['code'] = 0;
        }else{
            $result['code'] = 'error';
        }

        $result['time'] = date('Y-m-d H:i:s');
        return $result;
    }

    public function actionReport($url = null)
    {
        return $this->render('report', [
            'url' => urldecode($url),
        ]);
    }


    protected function processAbuseRequest()
    {
        $request = Json::decode(@$_REQUEST['req'], false);
        $result = [];
        $string = str_replace("\r", "\n", $request->text);
        $string = str_replace("\n\n", "\n", $string);
        $urls = explode("\n",$string);
        $result['strings'] = [];

        $abuses = [];

        foreach($urls as $url)
        {
            AbuseSearch::createFromRequestUrl($url);
            $abuses = array_merge($abuses, Abuse::createByUrl($url));
        }

        LogHelper::log(json_encode([
            'req'=>$request,
            'urls' => $urls,
            'abuses_count' => count($abuses)
        ]), '@runtime/logs/', 'abuses.log');

        return [$urls, $abuses];
    }
}