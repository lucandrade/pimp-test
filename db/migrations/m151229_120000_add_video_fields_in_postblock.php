<?php

use yii\db\Schema;
use yii\db\Migration;

class m151229_120000_add_video_fields_in_postblock extends Migration
{
    public function up()
    {
        $this->addColumn('{{%post_block}}', 'video_url', 'varchar(255) NOT NULL');
    }

    public function down()
    {
        $this->dropColumn('{{%post_block}}', 'video_url');
    }
}
