<?php

use yii\db\Migration;

class m160825_100222_rename_album_image_views_tables extends Migration
{
    public function up()
    {
        $this->renameTable('image_views', 'unique_image_views');
        $this->renameTable('album_views', 'unique_album_views');

    }

    public function down()
    {
        $this->renameTable('unique_image_views', 'image_views');
        $this->renameTable('unique_album_views', 'album_views');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
