(function($){

    var defaultSettings = {
        controlSelector: "#destination-album-id-list",
        submitBtn:"#btn-move-album",
        createAlbumBtn: '#btn-create-album',
        dataKey:"key",
        modalSelector:"#album-picker-modal",
        createAlbumFrom:"#create-album-form",
        newTitleInput:"#new-title-input",
        successMoving:function(ids, album){
            var s = ids.length == 1 ? "was " : "s were";
            bootbox.dialog({
                message:"Image"+s+" moved to album '" + album.title+"'",
                buttons:{
                    viewAlbum:{
                        label:"View album",
                        className:"btn-default",
                        callback:function(){
                            window.location.href = album.url;
                        }
                    },
                    ok:{
                        label:"Ok",
                        className:"btn-primary"
                    }
                }
            });
        }
    };

    var settings, url, ids = [], inited = false;

    var $albumDropdown,
        $btnCreateAlbum,
        $btnMove,
        $createAlbumForm,
        $modal,
        $newTitleInput;

    $.fn.moveToAlbum = function(method){
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method "' +  method + '" is not found in jquery.imageSelector' );
        }
    };



    var methods = {
        init: function(options){

            settings = $.extend(defaultSettings, options || {});

            ids = [];
            this.each(function(){
                ids.push($(this).data(settings.dataKey));
            });

            initPluginData(options);
            $modal.modal('show');
        }
    };


    var initPluginData = function(options){

        if(!inited){
            initElems();
            initCreateForm(ids);
            initMoveForm();

            url = settings.url || $createAlbumForm.attr('action');
            inited = true;
        }

    };

    var initElems = function(){
        $albumDropdown = $(settings.controlSelector);
        $btnCreateAlbum = $(settings.createAlbumBtn);
        $createAlbumForm =  $(settings.createAlbumFrom);
        $btnMove = $(settings.submitBtn);
        $modal = $(settings.modalSelector);
        $newTitleInput = $(settings.newTitleInput);
    };


    var initMoveForm = function(){
        $btnMove.on('click', function(){
            var $this = $(this);
            $this.attr('disabled', 'disabled');

            $.ajax({
                url:url,
                data:{ids:ids.join(','), albumId:$albumDropdown.val()},
                type:"POST",
                dataType:"JSON",
                success:function(res){
                    if(res.ids && res.ids.length > 0)
                    {
                        $this.removeAttr("disabled");
                        $modal.modal('hide');
                        if(settings.successMoving && typeof(settings.successMoving) === "function"){
                            settings.successMoving(res.ids, res.album);
                        }
                    }else{
                        alert('Some errors were occured while moving, please try again later');
                    }
                },
                fail:function(){
                    $this.removeAttr("disabled");
                    alert('Some errors were occured while moving, please try again later');
                }
            });
        });

        $albumDropdown.on('change', function(){

            if($(this).val() != ''){
                $btnMove.removeAttr('disabled');
            }else{
                $btnMove.attr('disabled', 'disabled');
            }
        });
    };

    var initCreateForm = function(){

        $btnCreateAlbum.on('click', function(){
            $createAlbumForm.data('yiiActiveForm').submitting = true;
            $createAlbumForm.yiiActiveForm('validate');
            $(this).attr('disabled','disabled');
            return false;
        });

        $createAlbumForm.on('submit', function(){
            return false;
        });

        $createAlbumForm.on('afterValidate', function(event, messages, errorAttributes){

            $createAlbumForm.data('yiiActiveForm').submitting = false;
            if(errorAttributes.length == 0){
                $.ajax({
                    url:url,
                    data:{ids:ids.join(','), albumTitle: $newTitleInput.val()},
                    type:"POST",
                    dataType:"json",
                    success:function(res){
                         if(res.ids && res.ids.length > 0 && res.album
                             && res.album.id && res.album.title)
                        {
                            $albumDropdown.append("<option selected='selected' value='"+res.album.id+"'>"+res.album.title+"</option>");
                            $albumDropdown.val(res.album.id);
                            $modal.modal('hide');
                            $createAlbumForm.find('input').val('');
                            $createAlbumForm.yiiActiveForm('resetForm');
                            if(settings.successMoving && typeof(settings.successMoving) === "function"){
                                settings.successMoving(res.ids, res.album);
                            }
                        }
                         else
                         {
                             if(res.album.errors && res.album.errors.length > 0){
                                 $createAlbumForm.yiiActiveForm('updateMessages',res.album.errors);
                             }else{
                                console.log(res);
                                alert("Something was going wrong while album creating");
                             }
                        }
                    },
                    complete:function(){
                        $btnCreateAlbum.removeAttr('disabled');
                    }
                });
            }else{
                $btnCreateAlbum.removeAttr('disabled');
            }

            return false;
        });
    }
})(jQuery);