<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;

class Statistic extends ActiveRecord
{
    const TYPE_COMMENTS = 'comments';
    const TYPE_VOTES_UP = 'votes_up';
    const TYPE_VOTES_DOWN = 'votes_down';
    const TYPE_USER_REGISTRATION = 'user_registration';
    const TYPE_USER_VISIT = 'user_visit';
    const TYPE_USER_LOGIN = 'user_login';

    public static function tableName()
    {
        return 'statistic';
    }

    public function rules()
    {
        return [
            ['type', 'trim'],
            ['value', 'integer'],
        ];
    }

    public static function getCurrentStatistic($type)
    {
        $statistic = self::find()
            ->where([
                'type' => $type,
                'DATE(date)' => new Expression('CURDATE()'),
                'HOUR(date)' => new Expression('HOUR(NOW())')
            ])
            ->one();

        if ($statistic === null) {
            $statistic = new Statistic([
                'type' => $type,
                'date' => new Expression('DATE_FORMAT(NOW(),"%Y-%m-%d %H:00:00")')
            ]);
        }

        return $statistic;
    }

    public static function updateUserStatistic()
    {
        $registrStat = self::getCurrentStatistic(self::TYPE_USER_REGISTRATION);
        $registrStat->value = User::countCurrentRegistred();
        $registrStat->save();

        $visitStat = self::getCurrentStatistic(self::TYPE_USER_VISIT);
        $visitStat->value = self::find()
            ->where([
                'type' => self::TYPE_USER_LOGIN,
                'DATE(date)' => new Expression('CURDATE()'),
                'HOUR(date)' => new Expression('HOUR(NOW())')
            ])
            ->count();
        $visitStat->save();
    }

    public static function updateUserLogin($user_id)
    {
        $exists = self::find()
            ->where([
                'type' => self::TYPE_USER_LOGIN,
                'value' => $user_id,
                'DATE(date)' => new Expression('CURDATE()')
            ])
            ->exists();

        if (!$exists) {
            $model = new Statistic([
                'type' => self::TYPE_USER_LOGIN,
                'value' => $user_id,
                'date' => new Expression('DATE_FORMAT(NOW(),"%Y-%m-%d %H:00:00")')
            ]);

            return $model->save();
        }

        return true;
    }

    public static function getUserVisitIds($dateFrom = null, $dateTo = null)
    {
        $query = self::find()
            ->select('value')
            ->distinct(true)
            ->indexBy('value')
            ->where(['type' => self::TYPE_USER_LOGIN]);

        if ($dateFrom !== null && $dateTo !== null) {
            $datetimeFrom = strlen($dateFrom) === 10 ? $dateFrom . ' 00:00:00' : $dateFrom;
            $datetimeTo = strlen($dateTo) === 10 ? $dateFrom . ' 23:59:59' : $dateTo;
            $query->andWhere(['between', 'date', $datetimeFrom, $datetimeTo]);
        }

        $models = $query->all();

        return array_keys($models);
    }

    public static function getDateDiff($from, $to)
    {
        $dateDiff = strtotime($to) - strtotime($from);
        return floor($dateDiff / (60 * 60 * 24));
    }

    public static function resolveDataPeriod($from, $to)
    {
        if (!$from) {
            $from = '2015-01-01';
        }
        if (!$to) {
            $to = date('Y-m-d');
        }
        if ($from === $to && $from === date('Y-m-d')) {
            $from = date('Y-m-d', strtotime('-30 DAY'));
        }
        return [$from, $to];
    }

    public static function resolveDataQueryDateType($from, $to)
    {
        $dateDiff = self::getDateDiff($from, $to);
        if ($dateDiff > 730) {
            return 'Year';
        } else if ($dateDiff > 90) {
            return 'Year/month';
        } else if ($dateDiff > 30) {
            return 'Year/month/week';
        }
        return 'Day';
    }

    public static function resolveDataQuerySelect($from, $to)
    {
        $dateDiff = self::getDateDiff($from, $to);
        if ($dateDiff > 730) {
            return new Expression('YEAR(date) AS date');
        } else if ($dateDiff > 90) {
            return new Expression('CONCAT(DATE_FORMAT(date, "%Y"), "/", DATE_FORMAT(date, "%m")) AS date');
        } else if ($dateDiff > 30) {
            return new Expression('CONCAT(DATE_FORMAT(date, "%Y"), "/", DATE_FORMAT(date, "%m"), " - ", DATE_FORMAT(date, "%v")) AS date');
        }
        return new Expression('DATE(date) AS date');
    }

    public static function resolveDataQueryGroup($from, $to)
    {
        $dateDiff = self::getDateDiff($from, $to);
        if ($dateDiff > 730) {
            return new Expression('YEAR(date)');
        } else if ($dateDiff > 90) {
            return new Expression('YEAR(date), MONTH(date)');
        } else if ($dateDiff > 30) {
            return new Expression('YEAR(date), WEEKOFYEAR(date)');
        }
        return new Expression('DATE(date)');
    }

    public static function findAllData($type, $from, $to)
    {
        return self::find()
            ->indexBy('date')
            ->select(['id', 'type', 'value' => new Expression('SUM(value)')])
            ->addSelect(self::resolveDataQuerySelect($from, $to))
            ->where(['type' => $type])
            ->andWhere(['between', 'date', $from, $to])
            ->groupBy(self::resolveDataQueryGroup($from, $to))
            ->orderBy(['date' => SORT_ASC])
            ->asArray()
            ->all();
    }

    public static function getLoggedData($dateFrom, $dateTo, $listOrigDate = false)
    {
        list($dateFrom, $dateTo) = self::resolveDataPeriod($dateFrom, $dateTo);

        $logged = self::findAllData(self::TYPE_USER_VISIT, $dateFrom, $dateTo);
        $registred = self::findAllData(self::TYPE_USER_REGISTRATION, $dateFrom, $dateTo);

        $data = [];
        $dateList = [];
        $data[] = [self::resolveDataQueryDateType($dateFrom, $dateTo), 'Logged', 'Registred'];
        foreach ($logged as $l) {
            $registredValue = isset($registred[$l['date']]) ? $registred[$l['date']]['value'] : 0;

            $dateFormatted = $l['date'];
            if (strlen($dateFormatted) === 10) {
                $dateFormatted = substr($dateFormatted, 8);
            }

            $data[] = [$dateFormatted, (int)$l['value'], (int)$registredValue];
            $dateList[] = $l['date'];
        }

        if ($listOrigDate) {
            return [$dateList, $data];
        }

        return $data;
    }

    public static function getCommentsData($dateFrom, $dateTo, $listOrigDate = false)
    {
        list($dateFrom, $dateTo) = self::resolveDataPeriod($dateFrom, $dateTo);

        $comments = self::findAllData(self::TYPE_COMMENTS, $dateFrom, $dateTo);
        $votesUp = self::findAllData(self::TYPE_VOTES_UP, $dateFrom, $dateTo);

        $data = [];
        $dateList = [];
        $data[] = [self::resolveDataQueryDateType($dateFrom, $dateTo), 'Commented', 'Upvoted'];
        foreach ($comments as $c) {
            $votesUpValue = isset($votesUp[$c['date']]) ? $votesUp[$c['date']]['value'] : 0;

            $dateFormatted = $c['date'];
            if (strlen($dateFormatted) === 10) {
                $dateFormatted = substr($dateFormatted, 8);
            }

            $data[] = [$dateFormatted, (int)$c['value'], (int)$votesUpValue];
            $dateList[] = $c['date'];
        }

        if ($listOrigDate) {
            return [$dateList, $data];
        }

        return $data;
    }

}