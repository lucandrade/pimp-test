<?php

namespace app\assets;


use yii\web\AssetBundle;

class MainPageAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/main-page.css',
    ];

    public $js = [
    ];

    public $depends = [
        'app\assets\AppAsset',
    ];
    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}