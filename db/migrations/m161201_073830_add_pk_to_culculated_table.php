<?php

use yii\db\Migration;

class m161201_073830_add_pk_to_culculated_table extends Migration
{
    public $table = 'calculated_views_count';


    public function up()
    {
        $this->addPrimaryKey('cvl_pk', $this->table, ['model_id', 'model_type', 'day']);
    }

    public function down()
    {
    }

}
