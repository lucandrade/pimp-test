<?php

namespace app\assets;


use yii\web\AssetBundle;

class AlbumPickerAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/jquery.moveToAlbum.css'
    ];

    public $js = [
        'js/jquery.moveToAlbum.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
    ];
    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}