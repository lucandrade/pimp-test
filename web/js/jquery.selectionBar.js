(function($){
    $.fn.selectionBar = function(method){
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method "' +  method + '" is not found in jquery.selectionBar' );
        }
    };
    
    var defaultSettings = {
        selectAllBtn : '#select-all',
        imagesSelector:'#image-list-container .image-wrapper',
        selectEvents:{
            onSelected:"onSelected", 
            onUnselected:"onUnselected"
        },
        parentWidgetSelector:'body'
    };

    var settings = {}, container = {};


    var methods = {
        init:function(options){
            this.each(function(){
                
                
                settings =  $.extend(true, defaultSettings, options || {});
                $(this).attr('data-settings', JSON.stringify(settings));
                $(this).attr('data-selected-images-count', 0);

                container = this;
                var $parentWidget = settings.parentWidgetSelector instanceof jQuery ? settings.parentWidgetSelector : $(settings.parentWidgetSelector);
                

                container.$selectAllBtn = $(this).find((settings.selectAllBtn));

                $parentWidget
                    .off("onSelected", settings.imagesSelector)
                    .on("onSelected", settings.imagesSelector , function(){
                        updateSelectedImagesCount.call(container, true);
                    });

                $parentWidget
                    .off("onUnselected", settings.imagesSelector)
                    .on("onUnselected", settings.imagesSelector, function(){
                        updateSelectedImagesCount.call(container, false);
                    });
                
                container.$selectAllBtn.on('click', function(){
                    $(this).toggleClass('checked');

                    if($(this).hasClass('checked')){
                        $(settings.imagesSelector).imageSelector('selectAll');
                    }else{
                        $(settings.imagesSelector).imageSelector('unselectAll');
                    }
                });

                $(container).find('#select-all-label').on('click', function(){
                    container.$selectAllBtn.click();
                });

                $(container).find('.cancel-icon').on('click', function(){
                    if($(container).hasClass('active')){
                        $(settings.imagesSelector).imageSelector('unselectAll');
                        container.$selectAllBtn.removeClass('checked');
                    }
                });
                
                
                
            });
        },

        reset:function(){
            settings = this.data('settings');
            container = this[0];
            reset.call(this);
        },

        reInit:function(){
            container = this[0];
            settings = this.data('settings');
        },

        updateIcon:function(){
            var selectedImagesCount = $(this).data('selected-images-count') || 0;
            var $btn = $(this).find(settings.selectAllBtn);
            updateIcon(selectedImagesCount, $btn)
        }
    };


    var reset = function(){
        this.removeClass('active');

        if(settings && settings.selectAllBtn){
            var $selectAllBtn = this.find(settings.selectAllBtn);

            if($selectAllBtn && $selectAllBtn.length > 0){
                $selectAllBtn.removeClass('checked');
            }
        }
        if(settings && settings.imageSelector){
            if($(settings.imagesSelector).length == 0){
                this.hide();
            }
        }

        this.data('selected-images-count', 0)
    };

    var updateSelectedImagesCount = function(increment){
        var $this = $(this);
        var settings = $(this).data('settings');
        var selectedImagesCount = $this.data('selected-images-count') || 0;

        var $selectAllBtn = $this.find(settings.selectAllBtn);

        if(increment){
            selectedImagesCount++;
        }else{
            selectedImagesCount--;
        }

        
        if(selectedImagesCount <= 0){
            $this.removeClass('active');

        }else{
            if($this.is(':hidden')){
                $this.show();
                $selectAllBtn.show();
            }
            if(!$this.hasClass('active')){
                $this.addClass('active');
            }
        }

        updateIcon(selectedImagesCount, $selectAllBtn);

        $this.find('.status-text .count').text(selectedImagesCount);
        $this.find('.status-text .s').text(selectedImagesCount == 1 ? "" : "s");
        $this.data('selected-images-count', selectedImagesCount);
    };


    var updateIcon = function(selectedImagesCount, $selectAllBtn){
        if(selectedImagesCount <=0){
            $selectAllBtn.removeClass('checked')
        }else{
            if(!$selectAllBtn.hasClass('checked')){
                $selectAllBtn.addClass('checked');
            }
            var $icon = $selectAllBtn.children('i');


            if(selectedImagesCount == $(settings.imagesSelector).length){
                $icon.removeClass("fa-minus").addClass('fa-check');
            }else{
                $icon.removeClass('fa-check').addClass('fa-minus');
            }
        }
    }
    
    
})(jQuery);