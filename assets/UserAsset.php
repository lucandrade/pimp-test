<?php

namespace app\assets;

use yii\web\AssetBundle;

class UserAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/user_profile.css',
    ];
    public $js = [
        'js/plugins/jquery.ui.widget.js',
        'js/plugins/jquery.iframe-transport.js',
        'js/plugins/jquery.fileupload.js',
        'js/plugins/fileupload.dynamic.js',
        'js/user.js'
    ];
    public $depends = [
        'app\assets\SiteAsset',
        'app\assets\PostAsset',
        'app\assets\YTExternalAsset'
    ];
    public $jsOptions = [
    ];
    public $cssOptions = [
    ];

}