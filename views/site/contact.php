<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')) { ?>

        <div class="alert alert-success">
            <?= (Yii::$app->session->getFlash('contactFormSubmitted')) ?>
        </div>

    <?php } else { ?>

        <p>
            If you have business inquiries or other questions, please fill out the following form to contact us.
            Thank you.
        </p>

        <div class="row">
            <div class="col-lg-8">
                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'name') ?>
                    </div>

                    <div class="col-md-6">
                        <?= $form->field($model, 'email') ?>
                    </div>
                </div>


                <?=
                $form->field($model, 'subject')
                    ->dropDownList([
                        'Advertising' => 'Advertising',
                        'General Question' => 'General Question',
                        'Suggestion' => 'Suggestion',
                        'Other' => 'Other'
                    ], [
                        'prompt' => '(Select a subject)'
                    ])
                ?>

                <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>

                <?=
                $form->field($model, 'verifyCode')
                    ->widget(Captcha::className(), [
                        'template' => '<div class="row"><div class="col-xs-6">{input}</div><div class="col-xs-6">{image}</div></div>',
                    ])
                ?>

                <div class="form-group">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    <?php } ?>
</div>
