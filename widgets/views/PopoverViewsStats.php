<?php $keys = array_keys($stats);
    $heads = array_keys($stats[$keys[0]]);
?>
<table class="table table-striped">
    <thead>
        <tr>
            <?php foreach($heads as $head){
                echo "<th>" . ucfirst($head) . "</th>";
            }
            ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach($stats as $head => $stat): ?>
            <tr>
                <?php foreach($stat as $value){
                    echo "<td>$value</td>";
                } ?>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
