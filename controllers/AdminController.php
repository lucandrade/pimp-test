<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\filters\AccessControl;
use app\components\authclients\Comments;
use app\controllers\admin\CreateAction;
use app\controllers\admin\EditAction;
use app\controllers\admin\IndexAction;
use app\controllers\admin\PurgeAction;
use app\controllers\admin\RestoreAction;
use app\controllers\admin\SoftDeleteAction;
use app\components\Controller;
use app\helpers\TokenHelper;
use app\helpers\PostHelper;
use app\models\User;
use app\models\Post;
use app\models\FilterStatistic;
use app\models\Statistic;
use app\helpers\DateHelper;
use yii\web\NotFoundHttpException;

class AdminController extends Controller
{

    public $layout = 'admin';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
//                'only' => ['logout'],
                'rules' => [
                    [
//                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function () {
                            return Yii::$app->user->identity->is_admin;
                        }
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'users' => [
                'class' => IndexAction::className(),
                'modelClass' => User::className(),
                'filterFields' => ['id', 'username', 'email'],
                'redirectSuccessSelected' => ['users'],
                'viewName' => 'users/index',
                'changeFields' => [
                    'is_activated_email' => [
                        User::EMAIL_NOT_ACTIVATED => 'Deactivate email',
                        User::EMAIL_ACTIVATED => 'Activate email',
                    ],
                    'status' => [
                        User::STATUS_DELETED => 'Deactivate',
                        User::STATUS_ACTIVE => 'Activate',
                        User::STATUS_BANNED => 'Ban'
                    ],
                    'is_admin' => [
                        User::TYPE_USER => 'User',
                        User::TYPE_ADMINISTRATOR => 'Admin',
                    ]
                ]
            ],
            'users-edit' => [
                'class' => EditAction::className(),
                'modelClass' => User::className(),
                'redirectSuccessSave' => ['users'],
                'viewName' => 'users/form'
            ],
            'users-create' => [
                'class' => CreateAction::className(),
                'modelClass' => User::className(),
                'redirectSuccessSave' => ['users'],
                'viewName' => 'users/form'
            ],
            'users-delete' => [
                'class' => SoftDeleteAction::className(),
                'modelClass' => User::className(),
                'redirectSuccess' => ['users'],
            ],
            'users-restore' => [
                'class' => RestoreAction::className(),
                'modelClass' => User::className(),
                'redirectSuccess' => ['users'],
            ],
            'users-purge' => [
                'class' => PurgeAction::className(),
                'modelClass' => User::className(),
                'redirectSuccess' => ['users'],
            ],
            'posts' => [
                'class' => IndexAction::className(),
                'modelClass' => Post::className(),
                'filterFields' => ['post.id', 'post.title'],
                'redirectSuccessSelected' => ['posts'],
                'viewName' => 'posts/index',
                'changeFields' => [
                    'is_authors' => [
                        0 => 'No author',
                        1 => 'Author',
                    ],
                    'is_adult' => [
                        0 => 'No adult',
                        1 => 'Adult',
                    ],
                    'is_draft' => [
                        0 => 'Published',
                        1 => 'Draft',
                    ]
                ]
            ],
            'posts-delete' => [
                'class' => SoftDeleteAction::className(),
                'modelClass' => Post::className(),
                'redirectSuccess' => ['posts'],
            ],
            'posts-restore' => [
                'class' => RestoreAction::className(),
                'modelClass' => Post::className(),
                'redirectSuccess' => ['posts'],
            ],
            'posts-purge' => [
                'class' => PurgeAction::className(),
                'modelClass' => Post::className(),
                'redirectSuccess' => ['posts'],
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
                'clientCollection' => 'authAdminCollection'
            ],
//			'report-job' => [
//				'class' => ReportJobAction::className()
//			],
        ];
    }

    public function actionIndex()
    {
        $statisticQuery = Statistic::find()
            ->indexBy('type')
            ->select(['id', 'type', 'value' => new Expression('SUM(value)')])
            ->where(['<>', 'type', Statistic::TYPE_USER_LOGIN])
            ->groupBy('type');

        $filter = new FilterStatistic();
        $statisticQuery = $filter->addPeriodFilter($statisticQuery);

        return $this->render('index', [
            'statistic' => $statisticQuery->all(),
            'filter' => $filter,
        ]);
    }

    public function actionUsersStatistic($type, $date_from = null, $date_to = null, $date = null)
    {
        if ($date !== null) {
            if (strlen($date) === 10) {
                $date_to = $date_from = $date;
            } else if (strlen($date) === 12 && preg_match('/(\d{4})\/(\d{2}) - (\d{2})/', $date, $m)) {
                list($date_to, $date_from) = DateHelper::getWeekIntervalDates($m[1], $m[3]);
            } else if (strlen($date) === 7 && preg_match('/(\d{4})\/(\d{2})/', $date, $m)) {
                list($date_to, $date_from) = DateHelper::getMonthIntervalDates($m[1], $m[2]);
            } else if (strlen($date) === 4) {
                $date_from = $date . '-01-01';
                $date_to = $date . '-12-31';
            }
        }

        if ($date_to === null) {
            $date_to = $date_from;
        }

        if ($date_from === $date_to && $date_from === date('Y-m-d')) {
            $date_from = new Expression('NOW() - INTERVAL 24 HOUR');
            $date_to = new Expression('NOW()');
        }

        if ($type === Statistic::TYPE_USER_REGISTRATION) {
            $ids = User::findAllRegistrationId($date_from, $date_to);
        } else if ($type === Statistic::TYPE_USER_VISIT) {
            $ids = Statistic::getUserVisitIds($date_from, $date_to);
        } else {
            throw new NotFoundHttpException('Statistic type not found');
        }

        $filter = Yii::$app->request->post('filter', Yii::$app->request->get('filter'));
        $dataProvider = new ActiveDataProvider([
            'query' => User::find()->where(['id' => $ids])
        ]);

        $this->view->title = 'Users statistic';
        $this->view->params['path'] = [['label' => 'Users statistic']];

        return $this->render('users/index', [
            'modelClass' => User::className(),
            'dataProvider' => $dataProvider,
            'filter' => $filter,
            'changeFields' => [
                'is_activated_email' => [
                    0 => 'Deactivate email',
                    1 => 'Activate email',
                ],
                'status' => [
                    0 => 'Deactivate',
                    1 => 'Activate',
                ],
                'is_admin' => [
                    0 => 'User',
                    1 => 'Admin',
                ]
            ]
        ]);
    }

    public function actionTokenSettings()
    {
        return $this->render('token-settings');
    }

    public function onAuthSuccess($client)
    {
        $token = $client->getAccessToken();
        $tokenParams = $token->getParams();
        $tokenParams['created'] = $token->createTimestamp;

        if (!empty($client->tokenFileDir)) {
            TokenHelper::$tokenFilePathAlias = $client->tokenFileDir;
        }

        if (TokenHelper::saveTokenToFile($tokenParams, true)) {
            Yii::$app->session->setFlash('success', 'Token is obtained for ' . $client->id . "token :" . json_encode($tokenParams));
        } else {
            Yii::$app->session->setFlash('warning', 'Token was not saved to file');
        }
    }


    public function actionAuthComments()
    {
        $oauthClient = new Comments();
        $url = $oauthClient->buildAuthUrl();
        return Yii::$app->getResponse()->redirect($url);
    }

    public function actionAuthCommentsSuccess()
    {
        $oauthClient = new Comments();

        if (Yii::$app->request->getQueryParam('code')) {
            $token = $oauthClient->fetchAccessToken(Yii::$app->request->getQueryParam('code'));
            $tokenParams = $token->getParams();
            $tokenParams['created'] = $token->createTimestamp;
            TokenHelper::$tokenFilePathAlias = '@runtime/token_comments';
            if (TokenHelper::saveTokenToFile($tokenParams, true)) {
                Yii::$app->session->setFlash('success', 'Comments token is obtained');
                return $this->redirect(['admin/token-settings']);
            }
        }

        Yii::$app->session->setFlash('warning', 'Token was not saved to file');
        return $this->redirect(['admin/token-settings']);
    }


    public function actionPostToSocials()
    {
        $id = \Yii::$app->request->post('id');
        $forcePosting = \Yii::$app->request->post('forcePosting', false);
        $platforms = \Yii::$app->request->post('platforms','');
        $platforms = empty($platforms) ? [] : explode(',',$platforms);
        $platforms = array_map(function($value){
            return ucfirst($value);
        }, $platforms);


        $post = Post::findOne($id);
        if ($post) {
            $results = PostHelper::postToSocialPlatforms($post, $forcePosting, $platforms);
        } else {
            throw new NotFoundHttpException("Post not found");
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $results;
    }

}
