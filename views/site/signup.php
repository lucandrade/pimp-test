<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Breadcrumbs;

$this->title = 'Sign up';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <?=
            Breadcrumbs::widget(
                [
                    'encodeLabels' => false,
                    'homeLink' => [
                        'label' => '<i class="fa fa-home"></i>',
                        'url' => ['post/index']
                    ],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]
            )
            ?>
            <?php $form = ActiveForm::begin([]); ?>
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3><i class="fa fa-user-plus"></i> Sign Up</h3>
                    <p class="hint">Enter your account details below:</p>
                    <hr>
                    <?=
                    $form->field($model, 'email')
                        ->textInput([
                            'class' => 'form-control',
                            'placeholder' => $model->getAttributeLabel('email')
                        ])
                        ->label(false);
                    ?>

                    <?=
                    $form->field($model, 'password')
                        ->passwordInput(['placeholder' => $model->getAttributeLabel('password')])
                        ->label(false);
                    ?>

                    <?=
                    $form->field($model, 'password2')
                        ->passwordInput(['placeholder' => 'Re-type your password'])
                        ->label(false);
                    ?>

                    <?= $form->field($model, 'tnc', [])->checkbox()->label($model->getAttributeLabel('tnc')); ?>

                    <hr>
                    <div class="text-center">
                        <div class="btn-group" role="group">
                            <?=
                            Html::submitButton('Signup', [
                                'class' => 'btn btn-success uppercase'
                            ])
                            ?>
                            <?=
                            Html::a(
                                'Have account?',
                                ['site/login'],
                                ['class' => 'btn btn-default']
                            )
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
