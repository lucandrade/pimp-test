<?php
namespace app\db\fixtures;

use yii\test\ActiveFixture;

class UserFixture extends ActiveFixture {

	public $modelClass = 'app\models\User';
	public $depends = [

	];
}