<?php

namespace app\components;

use Yii;
use yii\base\InvalidConfigException;

class View extends \yii\web\View
{
    public $minifiedAsset = "asset";

    public $generatedAssetsFolder = "@app/config/generated-assets/";

    public function init()
    {
        // load compressed asset
        if (!YII_DEBUG) {
            $route = \Yii::$app->controller->getRoute();
            $this->minifiedAsset = $route == "image/edit" ? "editor-asset" : "asset";
            $file = Yii::getAlias($this->generatedAssetsFolder . $this->minifiedAsset.".php");
            if (file_exists($file)) {
                Yii::$app->assetManager->bundles = require $file;
            }else{
                throw new InvalidConfigException("Config file for minifying assets $file not found");
            }

        }
        parent::init();
    }

}