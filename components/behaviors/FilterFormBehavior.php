<?php

namespace app\components\behaviors;

/**
 * Created by PhpStorm.
 * User: mactra
 * Date: 19.11.15
 * Time: 14:09
 */
class FilterFormBehavior extends \yii\base\Behavior
{

    public $tagsAttr = 'tags';
    public $queryAttr = 'domain';


    public function prepareSphinxQuery()
    {
        $attr = $this->tagsAttr;
        $owner = $this->owner;

        $queryString = $owner->$attr;
        $includeTags = $this->getIncludeTags($queryString);
        $query = implode(' | ', $includeTags);
        return " @{$this->queryAttr} (" . $query . ')';
    }

    protected function getIncludeTags($queryString)
    {
        $tags = explode(',', $queryString);

        $include_tags = [];

        foreach($tags as $tag)
        {
            $tag = '*' . trim(strtolower($tag)) . '*';
            if(strlen($tag) >=2 && $tag[0] == '-'){
                $exclude_tags[] = substr($tag,1);
            }
            elseif(strlen($tag) >=2 && $tag[0] == '+'){
                $include_tags[] = substr($tag,1);
            }
            elseif($tag){
                $include_tags[] = $tag;
            }
        }


        return $include_tags;
    }
}