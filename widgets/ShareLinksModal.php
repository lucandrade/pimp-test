<?php

namespace app\widgets;


use app\assets\ShareImagesModalAsset;
use yii\base\Widget;

class ShareLinksModal extends Widget
{
    public function run()
    {
        ShareImagesModalAsset::register($this->getView());
        return $this->render("ShareLinksModal");
    }
}