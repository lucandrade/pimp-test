<?php

$disabledClass = $model->getIsProcessing() ? " icon-disabled" : "";
$disabledTitle = $model->getIsProcessing() ? " (disabled while processing)" : "";

if(empty($hoverContent)){
    $hoverContent = $model->checkAuthorAccess()
        ?
        '<i title="Download image' . $disabledTitle. '" class="fa fa-download' . $disabledClass . '"></i>'.
        '<i title="Remove image" class="fa fa-trash-o"></i>' .
        '<i title="Edit image' . $disabledTitle . '" data-url="'. Url::to(['image/edit', 'id'=>$model->id]).'" class="fa fa-edit' . $disabledClass . '"></i>' .
        '<i title="Share image" class="fa fa-share-alt"></i>'.
        '<i title="See full image" class="fa fa-picture-o"></i>'
        :
        '<i title="Download image" class="fa fa-download"></i>'.
        '<i title="Share image" class="fa fa-share-alt"></i>'.
        '<i title="See full image" class="fa fa-picture-o"></i>';
}
?>

<div class="hover-donor" style="display: none">
    <div class="hover-bar-content"><?= $hoverContent ?></div>

    <div class="hover">
        <div class="content-header" title="<?= $model->getOriginalName() ?>"><?= $model->getOriginalName() ?></div>
        <div class="content-info">Size <?= $model->getSizeInKb() ?> kB <?php
            $stats = $model->getViewsStatistic();
            echo \app\widgets\PopoverViewsStats::widget([
                'stats' => $stats,
                'options' => [
                    'data-placement' => 'top'
                ],
                'content'=> $stats['raw']['all']
            ])
            ?>  view<?= $stats['raw']['all'] !== 1 ? "s" : "" ?></div>
    </div>

</div>

