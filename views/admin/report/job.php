<?php
use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;

/**
 * @var $this \app\components\View
 * @var $data array
 * @var $start string
 * @var $end string
 */

$this->title = 'Statistics';
$this->params['path'] = [
	['label' => 'Statistics']
];

$this->registerJs('updateStat($("#stat-graph"),' . json_encode($data) . ')');
?>
	<div class="panel panel-default">
		<div class="panel-body">
			<form class="form-inline" method="get">
				<div class="input-group">
					<div class="input-daterange input-group">
						<?= Html::textInput('start', $start, ['class' => 'form-control']) ?>
						<span class="input-group-addon">to</span>
						<?= Html::textInput('end', $end, ['class' => 'form-control']) ?>
					</div>
				</div>

				<?=
				Html::a('<i class="fa fa-times"></i> Reset',['report-job'],['class'=>'btn btn-default'])."\n",
				Html::button(
					'<i class="fa fa-line-chart"></i> Get report',
					[
						'class' => 'btn btn-default',
						'type' => 'submit'
					]
				);
				?>
			</form>
		</div>
	</div>
	<div id="stat-graph" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

	<hr>
<?php

echo GridView::widget(
	[
		'dataProvider' => new ArrayDataProvider(
			[
				'allModels' => array_map(
					function ($value, $key) use ($data) {
						return [
							'date' => $value,
							'job_count' => $data['job_count'][$key],
							'command_output' => $data['command_output'][$key],
							'execute_time' => $data['execute_time'][$key],
						];
					},
					$data['categories'],
					array_keys($data['categories']
					)
				),
				'pagination' => false
			]
		)
	]
);


