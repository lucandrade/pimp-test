<?php
namespace app\controllers;

use app\components\Controller;
use app\components\MediaUploadHandler;
use app\helpers\FileHelper;
use app\helpers\ImageHelper;
use app\models\User;
use app\models\Album;
use app\models\UserSettings;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\Url;
use app\models\Image;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use app\components\filters\Cors;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use app\components\filters\JsonResponse;
use app\models\Post;
use app\helpers\SortHelper;


class ImageController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['jsonResonse'] = [
            'class' => JsonResponse::className(),
            'only' => ['upload-by-data', 'delete', 'restore', 'purge', 'get-share-codes']
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => false,
                    'matchCallback' => function(){
                        $uploadingEnabled = isset(\Yii::$app->params['uploadingEnabled']) ? \Yii::$app->params['uploadingEnabled'] : true;
                        return !$uploadingEnabled;
                    },

                    'actions' => ['upload-file','upload-by-url'],
                    'denyCallback' => function(){
                        throw new ForbiddenHttpException("Uploading is disabled");
                    }
                ],
                [
                    'allow' => true,
                    'actions' => ['delete','restore', 'purge','edit'],
                    'roles' => ['@']
                ],
                [
                    'allow' => true,
                ],
            ],
        ];

        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['POST', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
            ]
        ];

        return $behaviors;
    }

    public function beforeAction($action)
    {
        $csrfOpened = ['upload-file'];

        if(in_array($action->id, $csrfOpened)){
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }


    public function actionUpload()
    {
        $this->layout = '1-column';
        $data = [];

        if(!\Yii::$app->user->isGuest)
        {
            $album = \Yii::$app->user->indentity->getUnsortedAlbum();

            if($album->checkAuthorAccess())
            {
                $albumsForMoving = \Yii::$app->user->identity->getPreparedAlbums();

                unset($albumsForMoving[$album->id]);
                $data['albumsForMoving'] = $albumsForMoving;
            }
        }else{
            $data['albumsForMoving'] = [];
        }

        $data['imagesDataProvider'] = new ActiveDataProvider([
            'query' => Image::find()
                ->where(['status' => Image::STATUS_NORMAL])
                ->andWhere(['>', 'createTime', (new Expression("(NOW() - INTERVAL 24 HOUR)"))])
                ->limit(5),
            'sort' => [
                'defaultOrder' => [
                    'rating' => SORT_DESC,
                    'viewsCount' => SORT_DESC
                ]
            ],
           'pagination' => false,

        ]);

        $data['postsDataProvider'] = new ActiveDataProvider([
            'query' =>  Post::find()
                ->with([
                    'postBlocks',
                    'tags',
                    'author'
                ])
                ->where(['status' => Post::STATUS_ACTIVE])
                ->andWhere(['>', 'created_at', (new Expression("(NOW() - INTERVAL 24 HOUR)"))])
                ->limit(5)
            ,

            'sort' => [
                'defaultOrder' => [
                    'rating' => SORT_DESC,
                ]
            ],
           'pagination' => false,
        ]);

        $data['album'] = $album;

        return $this->render("upload", $data);
    }


    public function actionDelete()
    {
        return $this->processImagesFromPost(function(Image $image){
            return  $image->softDelete();
        });
    }

    protected function processImagesFromPost($conditionCallback, $defaultError = "Couldn't process images for some reason")
    {
        $processedIds = [];
        $errors = [];

        $images = $this->loadImagesFromPost();

        foreach($images as $image){
            if($image->checkAuthorAccess() && call_user_func($conditionCallback, $image)){
                $processedIds[] = $image->id;
            }else{
                $errors[$image->id] = $image->hasErrors() ? $image->getErrors() :
                    $image->checkAuthorAccess() ? $defaultError : "You haven't access to this image";
            }
        }

        return [
            'ids' => $processedIds,
            'errors' => $errors
        ];
    }


    public function actionRestore()
    {
        return $this->processImagesFromPost(function(Image $image){
            return $image->restore();
        }, "Can't restore that image for some reason");
    }

    public function actionPurge()
    {
        return $this->processImagesFromPost(function(Image $image){
            return (bool) $image->putToQueForPurging();
        }, "Can't purge that image for some reason");
    }



    public function actionShow($id)
    {
        $image = $this->loadImage($id, false, false);
        if(!$image){
            return $this->render('empty_image', ['label'=>'Image not found']);
        }

        if($image->getIsPrivate() && !$image->checkAuthorAccess()){
            $savedPasswordHash = \Yii::$app->session->get(AlbumController::SESSION_PASSWORD_KEY);

            if(!$savedPasswordHash || !$image->album->validatePasswordHash($savedPasswordHash)){
                return $this->redirect(['album/item', 'id'=>$image->albumId]);
            }
        }

        if($image->getIsDeleted()){
            return $this->render('empty_image', ['label'=>'Image removed']);
        }

        $html = $this->render('show', compact('image'));

        return $html;
    }

    public function actionUploadByUrl()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $url = trim(\Yii::$app->request->post('url'));
        $ext = pathinfo($url, PATHINFO_EXTENSION);

        if(!in_array($ext, ImageHelper::$imageExtetsions)){
            return $this->processError("File by ulr '$url' has wrong type",
                "Please upload one of the following formats:" . implode(",", ImageHelper::$imageExtetsions));
        }

        $remoteSize = FileHelper::getRemoteFileSize($url);
        if($remoteSize === 'unknown' || $remoteSize == 0){
            return $this->processError("Couldn't download file by url '$url'", "Please make sure it's accessible");
        }
        if(FileHelper::getRemoteFileSize($url) > Image::MAX_FILE_SIZE){
            $inkB = floor($remoteSize/1000);
            return $this->processError("File by ulr '$url' is too big",
                "Size $inkB kB . Maximum allowed " . Image::MAX_FILE_SIZE/1000 . "kB", $url);
        }

        $dir =  \Yii::getAlias("@runtime/tmp/");
        FileHelper::createDirectory($dir);
        $tmpName = $dir . md5($url.time() . rand(1, 100000)).".".$ext;
        if(file_put_contents($tmpName, file_get_contents($url)))
        {
            $size = getimagesize($tmpName);
            if(!isset($size['mime']) || !in_array($size['mime'], ImageHelper::$imageMimeTypes)){
                unlink($tmpName);
                return $this->processError("File is not an image", "Please upload one of the following formats:" . implode(",", ImageHelper::$imageExtetsions));
            }

            $file = new \stdClass();

            $file->original_name = pathinfo($url, PATHINFO_BASENAME);
            $file->size = filesize($tmpName);
            $file->name = md5($url.time()).".".$ext;
            $this->processFile($file, $tmpName);

            $file->fileId = \Yii::$app->request->post('fileId');

            $res = ['status'=>'ok', 'file' => $file];
        }else{
            $res = ['status'=>'error', 'messages'=>"Can't upload file by url $url"];
        }

        return $res;
    }


    protected function processError($title, $description, $url = null){

        if(!isset($url)){
            $url = \Yii::getAlias("@web/static/i/wrong.png");
        }
        return ['status'=>'ok',
            'file'=>[
                'fileId' => \Yii::$app->request->post('fileId'),
                'error'=> [
                    'title' => $title,
                    'description' =>  $description,
                    'imageUrl' => $url

                ],
            ]
        ];
    }



    public function actionUploadByData()
    {
        $data = \Yii::$app->request->post('data');
        if(empty($data)){
            throw new BadRequestHttpException("Base encoded data can't be empty");
        }
        $originalName = \Yii::$app->request->post('originalName');

        list($name, $ext) = explode('.', $originalName);
        if(!isset($name, $ext)){
            throw new BadRequestHttpException("Wrong original file name passed");
        }
        $name .= "_edited";

        list($type, $data) = explode(';', $data);
        if($type !== 'data:image/png'){
            throw new BadRequestHttpException("Wrong data type");
        }

        list(, $data)      = explode(',', $data);
        $data = base64_decode($data);

        $dir =  \Yii::getAlias("@runtime/tmp/");
        FileHelper::createDirectory($dir);

        $tmpFileName = md5($data . time()) . ".$ext";
        $tmpName = $dir.$tmpFileName;

        if(file_put_contents($tmpName, $data))
        {
            $file = new \stdClass();
            $file->original_name = $name . '.' . $ext;
            $file->size = filesize($tmpName);
            $file->name = $tmpFileName;
            $this->processFile($file, $tmpName);
            $res = ['status'=>'ok', 'file' => $file];
        }

        else{
            throw new Exception("Couldn't save file by this data");
        }

        return $res;
    }

    public function actionUploadFile($dynamicPath = null)
    {
        $handler = new MediaUploadHandler([
            'param_name' => 'files',
            'upload_dir' => ImageHelper::getTmpImagePath(true, $dynamicPath),
            'accept_file_types' => '/\.(' . implode('|', ImageHelper::$imageExtetsions) . ')$/i',
            'image_versions' => [],
            'print_response' => false
        ]);

        if(!empty($handler->response['files']))
        {
            foreach($handler->response['files'] as &$file)
            {
                if(!empty($file->is_total_uploaded)){
                    $this->processFile($file);
                }

                $file->fileId =  \Yii::$app->request->getBodyParam('fileId');
            }
        }

        $handler->generate_response($handler->response);
    }


    public function actionGetShareCodes($ids)
    {
        if(empty($ids)){
            throw new BadRequestHttpException("Ids can't be empty");
        }

        $ids = explode(',', $ids);
        $query = Image::find()->where(['id' => $ids]);

        if(!\Yii::$app->user->isGuest)
        {
            $value = \Yii::$app->user->identity->getSettings(UserSettings::NAME_IMAGES_SORTING);
            if(!empty($value)){
                $query->orderBy(SortHelper::valueToArrayOrder($value));
            }
        }

        $images = $query->all();

        if(empty($images)){
            throw new NotFoundHttpException('Images by ids ' . json_encode($ids) . " was not found");
        }

        $codes = Image::extractShareCodes($images);
        return [
            'status' => 'ok',
            'codes' => $codes
        ];
    }



    public function actionEdit($id)
    {
        $image = $this->loadImage($id, true);
        $this->layout = 'pixie';

        if($image->getIsDeleted()){
            return $this->render("removed_image");
        }


        $this->getView()->minifiedAsset = 'editor-asset';
        return $this->render('edit', ['image' => $image]);
    }


    protected function processFile(&$file, $tempFileName = null)
    {
        $fileArr = json_decode(json_encode($file), true);

        $userId = \Yii::$app->user->isGuest ? User::ANONYMOUS_ID : \Yii::$app->user->id;

        $requestAttributes = [
            'authorId' => $userId,
            'ip' => \Yii::$app->request->getUserIP()
        ];

        $albumId = \Yii::$app->request->getBodyParam('albumId');

        if(!($albumId
            && ($album = Album::findOne(['id'=>$albumId]))
            && $album->belongsToUser($userId)))
        {
            $album = Album::createUnsorted($userId);
        }

        $requestAttributes['albumId'] = $album->id;
        $image = Image::createdFromUploadedFile($fileArr, $requestAttributes, $tempFileName);

        if(!$image->hasErrors())
        {
            $file->image['id'] =$image->id;
            $file->image['pageUrl'] = Url::to(['image/show', 'id' => $image->id], true);
            $file->image['editUrl'] = Url::to(['image/edit', 'id' => $image->id], true);
            $file->image['title'] = $image->title;
            $file->image['fileUrl'] = $image->getHttpFtpFileName(Image::SIZE_0_PX, false);
            return true;
        }
        else{
            $file->image['errors'] = $image->getErrors();
            return false;
        }
    }

    protected function loadImagesFromPost($name = 'ids')
    {
        $ids = \Yii::$app->request->post($name);
        return $this->loadImages($ids);
    }

    protected function loadImages($ids)
    {
        if(empty($ids)){
            throw new BadRequestHttpException("Ids can't be empty");
        }

        $ids = explode(',', $ids);
        return Image::findAll(['id' => $ids]);
    }

    /**
     * @param $id
     * @param bool|false $checkAuthorAccess
     * @return Image
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */

    protected function loadImage($id, $checkAuthorAccess  = false, $throwNotFoundException = true)
    {
        $image = Image::findOne(['id'=>$id, 'status' => [
            Image::STATUS_NORMAL,
            Image::STATUS_DELETED,
            Image::STATUS_PROCESSING,
            Image::STATUS_PRIVATE
        ]
        ]);
        if(!$image){
            if($throwNotFoundException){
                throw new NotFoundHttpException("Image not found");
            }else{
                return null;
            }
        }

        if($checkAuthorAccess && !$image->checkAuthorAccess()){
            throw new ForbiddenHttpException("You haven't access to this image");
        }

        return $image;
    }
}