<?php
namespace app\components\behaviors;


use yii\base\Behavior;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\db\Query;
use yii\db\Expression;
use app\models\User;

class AlbumImageCommon extends Behavior
{
    public function uploadedByGuest(){
        return $this->owner->authorId == User::ANONYMOUS_ID;
    }

    public function getViewsStatistic()
    {
        $owner = $this->owner;
        return [
            'unique' => [
                'today' => $owner->todayViews,
                'total' => $owner->totalViews
            ]
        ];
    }


    public function updateViewsStats($dayRawViews)
    {
        $owner = $this->owner;
        $owner->todayViews = $dayRawViews;
        $total = $owner->totalViews + $dayRawViews;
        $owner->totalViews = $total;
        return $owner->save();
    }
}