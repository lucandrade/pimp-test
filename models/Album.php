<?php

namespace app\models;

use app\components\ActiveRecord;
use app\helpers\SortHelper;
use app\models\behaviors\Rating;
use app\models\interfaces\CommentsServiceModel;
use Yii;
use yii\db\Exception;
use yii\helpers\Json;
use yii\helpers\Url;
use app\components\behaviors\AlbumImageCommon;
use app\components\behaviors\HasOwner;

/**
 * This is the model class for table "album".
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property string $password
 * @property string $contentDisplay
 * @property integer $status
 * @property integer $imageCount
 * @property integer $viewsCount
 * @property integer $imageSize
 * @property integer $commentCount
 * @property integer $type
 * @property integer $defaultImageId
 * @property string $commentKey
 * @property string $privateKey
 * @property string $guestKey
 * @property integer $authorId
 * @property string $updateTime
 * @property string $createTime
 *
 * @property User $author
 * @property Image[] $images
 */
class Album extends ActiveRecord implements CommentsServiceModel
{
    const UNSORTED_TITLE = 'Unsorted';

    const SCENARIO_AUTO_CREATING = 'auto_creating';
    const SCENARIO_LOCKING = 'locking';
    const SCENARIO_CREATING_BY_USER = 'creating_by_user';

    const SESSION_PASSWORD_KEY = 'alpa';


    const TYPE_USER        = 0;
    const TYPE_UNSORTED    = 1;
    const TYPE_TRASH       = 2;
    const TYPE_SCREENSHOTS = 3;
    const TYPE_PIMPED      = 4;
    const TYPE_WATERMARKS  = 5;
    const TYPE_GUEST       = 10;
    const TYPE_SAMPLE      = 11;



    const STATUS_NORMAL = 0;
    const STATUS_DELETED = 1;
    //@todo do we need it?
    const STATUS_PRIVATE = 2;

    const TITLE_UNTITLED = 'Untitled';


    private $_isUnsorted = false;

    const MAX_SHARE_CODES = 100;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'album';
    }



    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['ownerBehavior'] = [
            'class' => HasOwner::className()
        ];

        $behaviors['albumImageCommon'] = [
            'class' => AlbumImageCommon::className(),
        ];

        $behaviors['rating'] =[
            'class' => Rating::className()
        ];

        return $behaviors;
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'required'],
            ['password', 'default', 'value' => ''],
            ['password', 'required', 'on' => self::SCENARIO_LOCKING],

            [['content', 'contentDisplay','password'], 'string'],
            [['status', 'imageCount', 'imageSize', 'commentCount', 'defaultImageId', 'authorId','totalViews', 'todayViews'], 'integer'],
            [['updateTime', 'createTime'], 'safe'],
            ['type', 'default', 'value' => self::TYPE_USER],
            ['type', 'in', 'range' => [
                 self::TYPE_USER, self::TYPE_UNSORTED, self::TYPE_TRASH, self::TYPE_SCREENSHOTS,
                    self::TYPE_PIMPED,self::TYPE_WATERMARKS, self::TYPE_GUEST, self::TYPE_SAMPLE
            ]],
            [['title', 'commentKey', 'privateKey', 'guestKey'], 'string', 'max' => 255],
            [['authorId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['authorId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'content' => 'Content',
            'contentDisplay' => 'Content Display',
            'status' => 'Status',
            'imageCount' => 'Image Count',
            'imageSize' => 'Image Size',
            'commentCount' => 'Comment Count',
            'defaultImageId' => 'Default Image ID',
            'commentKey' => 'Comment Key',
            'privateKey' => 'Private Key',
            'guestKey' => 'Guest Key',
            'authorId' => 'Author ID',
            'updateTime' => 'Update Time',
            'createTime' => 'Create Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'authorId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages($imageStatus = [Image::STATUS_NORMAL, Image::STATUS_PROCESSING])
    {
        if($this->getIsUnsorted()){
            return Image::find()->where(['status' => $imageStatus, 'authorId' => $this->authorId, 'albumId' => null]);
        }else{
            return $this->hasMany(Image::className(), ['albumId' => 'id'])->where(['status' => $imageStatus]);
        }
    }


    public function beforeSave($insert)
    {
        if($insert && $this->isNewRecord && empty($this->createTime)){
            $this->createTime = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }


    public function belongsToUser($user)
    {
        if($user instanceof User){
            return $this->authorId == $user->id;
        }

        return $this->authorId == $user;
    }

    public static function createUnsorted($userId)
    {
        $album = new self;
        $album->authorId = $userId;
        $album->makeUnsorted();
        $album->title = self::UNSORTED_TITLE;
        return $album;
    }


    public static function create($attributes, $scenario = null)
    {
        $album = new self;

        if($scenario){
            $album->setScenario($scenario);
        }
        $album->setAttributes($attributes);
        $album->save();
        return $album;
    }

    public function getCommentsUrl()
    {
        return Url::to(['/album/item', 'id' => $this->id, '#' => 'comments-block']);
    }

    public function getUrl($scheme = false)
    {
        $route = $this->getIsUnsorted() ? ['album/unsorted'] : ['/album/item', 'id' => $this->id];
        return Url::to($route, $scheme);
    }

    public function getIsLocked()
    {
        if(empty($this->password) && !empty($this->privateKey))
        {
            $this->setPassword($this->privateKey);
            $this->privateKey = null;
            if(!$this->save()){
                throw new Exception("Can't convert password : " . json_encode($this->getErrors()));
            }
        }
        return !empty($this->password);
    }

    public function unlock()
    {
        $tr = $this->getDb()->beginTransaction();
        if($this->setImagesStatus(Image::STATUS_NORMAL) !== false)
        {
            $this->password = '';
            $this->status = self::STATUS_NORMAL;
            if($this->save())
            {
                $tr->commit();
                return true;
            }
        }
        $tr->rollBack();
        return false;
    }

    public function lock($password)
    {
        $tr = $this->getDb()->beginTransaction();
        if($this->setImagesStatus(Image::STATUS_PRIVATE) !== false)
        {
            $this->setScenario(Album::SCENARIO_LOCKING);
            $this->setPassword($password);
            $this->status = self::STATUS_PRIVATE;
            if($this->save())
            {
                $tr->commit();
                return true;
            }
        }

        $tr->rollBack();
        return false;
    }


    public function setImagesStatus($status, $updatedStatuses = [Image::STATUS_NORMAL, Image::STATUS_PROCESSING, Image::STATUS_PRIVATE])
    {
        if(!in_array($status, [
            Image::STATUS_PRIVATE,
            Image::STATUS_NORMAL,
            Image::STATUS_DELETED,
            Image::STATUS_PROCESSING,
            Image::STATUS_DELETED_FOREVER,
        ])){
            $this->addError("id","Wrong status to be set for album images");
            return false;
        }

        return $this->getDb()
            ->createCommand()
            ->update(Image::tableName(), ['status' => $status], ['albumId' => $this->id, 'status' => $updatedStatuses])
            ->execute();
    }

    public function setPassword($password)
    {
        $this->password = \Yii::$app->security->generatePasswordHash($password);
    }


    public function validatePasswordHash($passwordHash)
    {
        return $this->password === $passwordHash;
    }


    public function save($runValidation = true, $attributeNames = null)
    {
        if($this->getIsUnsorted()){
            throw new Exception("This album is Unsorted, it's impossible to save");
        }
        return parent::save($runValidation, $attributeNames);
    }

    public function softDelete()
    {
        $tr = $this->getDb()->beginTransaction();
        $removeImages = true;
        if(!$this->getIsUnsorted())
        {
            $this->status = self::STATUS_DELETED;
            $removeImages = $this->save();
        }
        if($removeImages){
            list($removed, $unremoved) = $this->softDeleteImages();
            if(count($unremoved) == 0){
                $tr->commit();
                return true;
            }else{
                $this->addError("albumId", "Cound't soft delete images:" . Json::encode($unremoved));
            }
        }

        return false;
    }

    public function softDeleteImages()
    {
        $images = $this->getImages([Image::STATUS_NORMAL, Image::STATUS_PROCESSING, Image::STATUS_PRIVATE])->all();
        $removed = [];
        $unremoved = [];
        foreach($images as $image){
            if($image->softDelete()){
                $removed[] = $image->id;
            }else{
                $unremoved[$image->id] = $image->getErrors();
            }
        }

        return [$removed, $unremoved];
    }


    public function addArchiveToQueue($userId)
    {
        $archive = new Archive();
        $archive->authorId = $userId;
        $archive->setScenario(Archive::SCENARIO_ADDING_TO_QUEUE);
        $archive->setTitleForAlbum($this->title);
        $request = $this->getIsUnsorted() ? ['imagesAuthorId' => $this->authorId] : ['album_id'=>$this->id];
        $archive->request = Json::encode($request);
        return $archive->save() && $archive->addToQueue();
    }


    public function getIsUnsorted()
    {
        return $this->_isUnsorted;
    }

    public function makeUnsorted()
    {
        $this->_isUnsorted = true;
    }


    public function getShareCodes($maxShareCodes = self::MAX_SHARE_CODES)
    {
        $imagesMoreThanMax = false;
        $query = $this->getImages()->select(['id', 'authorId', 'server', 'fileName', 'biggestSize', 'size', 'createTime', 'updateTime'])
            ->limit($maxShareCodes + 1);

        if(!\Yii::$app->user->isGuest)
        {
            $value = \Yii::$app->user->identity->getSettings(UserSettings::NAME_IMAGES_SORTING);

            if(!empty($value)){
                $query->orderBy(SortHelper::valueToArrayOrder($value));
            }
        }

        $images = $query->all();

        if(isset($images[$maxShareCodes])){
        $imagesMoreThanMax = true;
        unset($images[$maxShareCodes]);
        }
        return [Image::extractShareCodes($images), $imagesMoreThanMax];
        }



    public function getOwnerId()
    {
        return $this->authorId;
    }

    public function getNotificationType()
    {
        return Notification::PARENT_MODEL_ALBUM;
    }

    public function getIsActive()
    {
        return $this->status == self::STATUS_NORMAL;
    }


    public function getIsSoftDeleted()
    {
        return $this->status == self::STATUS_DELETED;
    }


    public function recalculateSize($save = true)
    {
        $this->imageSize = $this->calculateSize();
        if(!$this->getIsUnsorted() && $save){
            return $this->save();
        }else{
            return true;
        }
    }

    public function recalculateStats()
    {
        $this->recalculateSize(false);
        $this->recalculateImagesCount(false);
        return $this->save();
    }


    public function recalculateImagesCount($save = true)
    {
        $this->imageCount = $this->getImages([Image::STATUS_NORMAL, Image::STATUS_PROCESSING, Image::STATUS_PRIVATE])->count('id');
        if(!$this->getIsUnsorted() && $save){
            return $this->save();
        }else{
            return true;
        }

    }

    public function getSize()
    {
       $size = $this->getAttribute('imageSize');
       if(is_null($size))
       {
           $this->recalculateSize();
           $size = $this->getAttribute('imageSize');
       }
       return $size;

    }

    public function getImageCount()
    {
       $count = $this->getAttribute('imageCount');
       if(is_null($count))
       {
           $this->recalculateImagesCount();
           $count = $this->getAttribute('imageCount');
       }
       return $count;

    }


    public function calculateSize()
    {
        return $this->getImages([Image::STATUS_NORMAL, Image::STATUS_PROCESSING, Image::STATUS_PRIVATE])->asArray()->sum('size');
    }
}
