<?php

use yii\helpers\Url;
use app\helpers\Html;

/**
 * @var $this \yii\web\View;
 *
 * @var $widget \app\widgets\UploaderPopup;
 */

$widget = $this->context;
?>

<div id="popup-uploader-container" class="popup-uploader-container">

    <?php
    if(!$widget->clicker)
    {
        $label = $widget->buttonOptions['label'];
        unset($widget->buttonOptions['label']);
        echo Html::button($label, $widget->buttonOptions);
    }
    ?>

    <div class="modal uploader-modal fade" id="<?= $widget->modalId ?>" tabindex="-1" role="dialog" aria-labelledby="uploader-modal">
    <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-dnd dnd-area-show">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><?= $widget->modalTitle ?></h4>
                    </div>
                    <?= $widget->getUploader()->run(); ?>
                </div>
            </div>
        </div>
    </div>

</div>



