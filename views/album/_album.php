<?php

use app\helpers\Html;
use yii\helpers\Url;
use app\models\Image;

/**
 * @var $this yii\web\View
 * @var $model \app\models\Album;
 *
 */
$images = $model->getImages([Image::STATUS_NORMAL, Image::STATUS_PROCESSING, Image::STATUS_PRIVATE])->limit(4)->all();
$imagesCount = $model->getImageCount();

?>
<div class="ar">
    <a href="<?= Url::to(['album/item', 'id'=>$model->id], true) ?>"
       class="grid-content album-thumb"
   >
        <?php foreach($images as $i => $image){
            echo Html::inlineImageStyle($image->id, $image->getHttpFtpFileName(\app\models\Image::SIZE_0_PX));
        }?>

        <i class='fa fa-lock thumb-lock-icon' <?= !$model->getIsLocked() ? "style='display:none'" : "" ?>></i>

        <ul class="album-cover">
            <?php foreach($images as $i => $image):
                ?>
                <li class="center-cropped image_<?= $image->id ?>">
                </li>
            <?php
                if($i == 3){
                    break;
                }
                endforeach;
            ?>
        </ul>
    </a>
</div>
<div class="content-title">
    <div class="album-title">
    <?= Html::a($model->title, ['album/item', 'id' => $model->id], ['title' => $model->title]) ?>
    </div>
    <div class="album-info">
        <?= $imagesCount ?> image<?= $imagesCount !== 1 ? "s" : "" ?>
        <?= date("F j, Y  ", strtotime($model->createTime)) ?>
          <?php
                $stats = $model->getViewsStatistic();
                echo \app\widgets\PopoverViewsStats::widget([
                    'stats' => $stats,
                    'content'=> $stats['unique']['total']
                ])
            ?>  view<?= $stats['unique']['total'] !== 1 ? "s" : "" ?>
    </div>
</div>

<div class="hover-donor" style="display: none">
    <?php
    $lockStatus = $model->getIsLocked() ? ["lock", "Unlock"] : ["unlock", "Lock"];
    if($model->checkAuthorAccess()){

        $hoverContent = '<i class="fa fa-download" title="Download album"></i>'.
        '<i class="fa fa-trash-o" title="Remove album"></i>' .
        '<i class="fa fa-share-alt" title="Get share codes for this album"></i>' .
        '<i class="fa fa-picture-o" title="Go to album page"></i>'.
        "<i class='unlock-icon fa fa-" . $lockStatus[0] . "' title='" . $lockStatus[1] .
        " this album' data-locked='" . (int) $model->getIsLocked() . "'" .
            " data-id='".$model->id."'></i>";
    }else{
        $downloadIcon = !\Yii::$app->user->isGuest ? '<i class="fa fa-download" title="Download album"></i>': "";
        $hoverContent = $downloadIcon .
        '<i title="Share image" class="fa fa-share-alt"></i>'.
        '<i title="See full image" class="fa fa-picture-o"></i>';

    }

    ?>
    <div class="hover-bar-content"><?= $hoverContent ?></div>
</div>