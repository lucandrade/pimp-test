<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Url;

/**
 * Class Tag
 *
 * @property string $name;
 *
 * @package app\models
 */
class UserSeenPost extends ActiveRecord
{

    public static function tableName()
    {
        return 'user_seen_post';
    }

    public function rules()
    {
        return [
            [['user_id', 'post_id'], 'required'],
            [['user_id', 'post_id'], 'integer'],
            ['post_id', 'exist', 'targetClass' => Post::className(), 'targetAttribute' => 'id'],
            ['user_id', 'exist', 'targetClass' => User::className(), 'targetAttribute' => 'id'],
        ];
    }


    public function behaviors()
    {
        return [
        ];
    }


    public static function create($userId, $postId)
    {
        $model = new self;
        $model->user_id = $userId;
        $model->post_id = $postId;
        $model->save();
        return $model;
    }

}