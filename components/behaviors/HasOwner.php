<?php
namespace app\components\behaviors;


use app\models\Image;
use yii\base\Behavior;
use app\models\User;

class HasOwner extends Behavior
{
    public $ownerProperty = 'authorId';

    public function checkAuthorAccess($allowForAnons = false)
    {
        $ownerAuthorId = $this->owner->{$this->ownerProperty};
        if($ownerAuthorId == User::ANONYMOUS_ID){
            return $allowForAnons;
        }
        return (\Yii::$app->user->can('admin')) ||
        (\Yii::$app->user->identity && \Yii::$app->user->identity->id == $ownerAuthorId);
    }

}