var uploadedImages = [];

var insertAll = function() {
    for (var i = 0; i < uploadedImages.length; i++) {
        insertImg(uploadedImages[i].fileUrl, uploadedImages[i].pageUrl);
    }
    return false;
};


var insertImg = function(src1, src2) {
    var str = '[URL=' + src2 + '][IMG]' + src1 + '[/IMG][/URL] ';
    var myObj= vB_Editor['vB_Editor_001'] ? vB_Editor['vB_Editor_001'] : vB_Editor['vB_Editor_QR'];
    var tmp = myObj.get_editor_contents();
    if (tmp){
        myObj.write_editor_contents('');
        myObj.insert_text(tmp+str,tmp.length+str.length,0);
        myObj.editwin.onmouseover = function(){ myObj.check_focus(); }
    } else {
        myObj.insert_text(tmp+str,tmp.length+str.length,0);
    }
};


$(document).ready(function(){

    var cssNode = document.createElement('link');
    cssNode.type = 'text/css';
    cssNode.rel = 'stylesheet';
    cssNode.href = '<?= $stylesLink ?>';
    cssNode.media = 'screen';
    document.getElementById('up_script').appendChild(cssNode);

    var fieldsetNode = document.createElement("fieldset");
    fieldsetNode.innerHTML = '<?= str_replace("\n",'',$this->render("_uploader_fieldset")); ?>';
    fieldsetNode.setAttribute ("class", "fieldset");
    document.getElementById ("up_script").appendChild (fieldsetNode);

    $('#up_script').on('click', '.preview-container', function(){
        var image = $(this).data('image');
        if(image){
            insertImg(image.fileUrl, image.pageUrl);
        }
        return false;
    });

    $("#pah-upload-container").pahUploader({
        'clicker' : "#pah-upload-container #upload_pickfiles",
        'url' : '<?= $url ?>',
        'fileInput' : '.file-input',
        'showAfterUploadingInfo' : false,
        createPreviewContainer:function(file, settings){
            var  $previewContainer = $("<div></div>").addClass(settings.previewContainerClass);
            $previewContainer.attr('id', 'preview_' + file.fileId);

            var $progress = $(
            '<div class="absolute-progress">' +
                '<div class="progress">' +
                    '<div class="progress-text">' +
                        '<span class="progress-percent">0%</span> - ' +
                        '<span class="progress-loaded">0kb</span>' +
                        ' (<span class="progress-bitrate">0</span>' +
                        '<span class="progress-bitrate-units">kb</span>)'+
                        '</div>' +
                    '<div class="bar"></div>' +
                    '   </div>' +
                '</div>');

            $previewContainer.append($progress);
            $previewContainer.append(
                "<i class='fa fa-2x fa-spin fa-spinner loading-indincator'></i>"
                );

            var $img = $('<img/>').addClass("img-responsive").hide();
            $previewContainer.append($img);

            if(!file.error)
            {
                if(settings.renderPreviewImage){
                    var reader = new FileReader();
                    reader.onload = function (event) {
                        $img.attr('src', event.target.result);
                        $img.show();
                    };
                    reader.readAsDataURL(file);
                }
            }else{
                $img.attr('src', '<?= $wrongImage ?>')
                $img.show();

            }

            if(file.error){
                $("#pah-upload-container").pahUploader('addError', $previewContainer, file.error);
            }

            return $previewContainer;
        },


        'successUploaded':function(file, $previewContainer){
            var image = file.image;

            $previewContainer.data('image', image);
            uploadedImages.push(image);

            if(uploadedImages.length > 1){
                $('#up_script #msg').show();
            }
            var $info = $("<a href='javascript:;' class='preview-title'>" + image.title + "</a>").hide();
            $previewContainer.append($info);
            $previewContainer.find('.absolute-progress').fadeOut("slow");
            $info.fadeIn('slow');
        }
    });
});
