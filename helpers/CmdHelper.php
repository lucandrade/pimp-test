<?php

namespace app\helpers;


class CmdHelper
{
    public static function exec($cmd, $logPathAlias = "@runtime/logs/zip/", $logFile = null)
    {
        LogHelper::log($cmd, $logPathAlias, $logFile);
        exec($cmd, $output);
        LogHelper::log($output, $logPathAlias, $logFile);
        return $output;
    }
}