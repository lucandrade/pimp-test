<?php

namespace app\assets;


use yii\web\AssetBundle;

class ImageAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/image.css',
    ];

    public $js = [
        'js/image.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
        'app\assets\ShareImagesModalAsset',
        'app\assets\ImageSelectorAsset',
        'app\assets\PostAsset'
    ];
    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}