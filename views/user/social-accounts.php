<?php
use yii\helpers\Html;
use app\models\UserOauth;
use yii\helpers\Url;
use app\helpers\WidgetHelper;
use yii\web\View;

/* @var $user \app\models\User */
$user = \Yii::$app->user->identity;
$config = WidgetHelper::getConfig(2);
$config['avatarUploader']=[
    'clicker' => '[data-uploader="avatar-image"]',
    'progressContainer' => '[data-uploader="progress-container"]',
    'isMultiple' => false,
];
$ck = WidgetHelper::getScriptInView('//js/widget', ['config' => $config]);
$this->registerJs($ck, View::POS_END);

?>

<h3 class="col-sm-12">Social accounts</h3>
<div class="row">
    <div class="col-xs-12">
        <table id="social-accounts-table" class="table">
            <?php foreach (UserOauth::possibleSocialAccounts() as $provider) { ?>
                <tr>
                    <?php if ($user->isLinkedAccount($provider)) { ?>
                        <?php
                        /* @var $oauth app\models\UserOauth */
                        $oauth = $user->getLinkedAccount($provider)
                        ?>
                        <td>
                            <h4><?= ucfirst($oauth->provider) ?></h4>
                        </td>
                        <td>
                            <?= Html::a("Unlink", ['user/unlink', 'provider' => $oauth->provider], ['class' => 'btn btn-danger']); ?>
                        </td>
                        <td>
                            <h4><?= ucfirst($oauth->display_name) ?></h4>
                        </td>
                        <td>
                            <div class="social-image-wrapper text-center">
                                <?= Html::img($oauth->getAvatarUrl(), ['class' => 'img-thumbnail']); ?>
                            </div>
                        </td>
                        <td class="text-center">
                            <?php if ($user->current_oauth_provider !== $provider) { ?>
                                <?=
                                Html::a('Set avatar', [
                                    'user/set-default',
                                    'provider' => $oauth->provider
                                ], ['class' => 'btn btn-warning'])
                                ?>
                            <?php } ?>

                        </td>
                    <?php } else { ?>
                        <td>
                            <h4><?= ucfirst($provider) ?></h4>
                        </td>
                        <td>
                            <a class="btn btn-primary"
                               href="<?= Url::toRoute(['site/auth', 'authclient' => $provider]) ?>">
                                Link
                            </a>
                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                    <?php } ?>
                </tr>
            <?php } ?>
            <tr class="custom-avatar-row">
                <td><h4>Upload custom avatar</h4></td>
                <td></td>
                <td></td>
                <td>
                    <div class="user-avatar social-image-wrapper text-center">
                        <label>
                            <input id="user-avatar-input" type="file" name="loadedImages" hidden="hidden">
                            <div class="img-thumbnail img-avatar"
                                 style="background-image: url(<?= $model->profileAvatar ?>)">
                                <img src="<?= $model->profileAvatar ?>" alt="<?= $model->profileAvatar ?>">
                            </div>
                            <div class="absolute-progress">
                                <div class="progress" hidden="hidden">
                                    <div class="bar" style="width: 0%;"></div>
                                </div>
                            </div>
                        </label>

                        <input type="hidden" name="<?= $model->formName() ?>[0][file_name]">
                        <input type="hidden" name="<?= $model->formName() ?>[0][upload_name]">
                    </div>
                </td>
                <td class="text-center">
                    <?php if ($model->profileAvatar !== $model->avatar) { ?>
                        <?= Html::a('Set avatar', ['user/set-default-avatar'], ['class' => 'btn btn-warning']) ?>
                    <?php } ?>
                </td>
            </tr>
        </table>
    </div>
</div>