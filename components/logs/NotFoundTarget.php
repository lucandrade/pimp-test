<?php

namespace app\components\logs;


class NotFoundTarget extends \yii\log\FileTarget
{
    public function formatMessage($message)
    {
        $request = \Yii::$app->getRequest();
        if(!$request instanceof \yii\web\Request){
            return null;
        }

        $referrer = empty($request->getReferrer()) ? "no referrer" : $request->getReferrer();

        return "[" . date("Y-m-d H:i:s") . "] "
            . $request->getUrl() . "; "
            . $referrer . "; "
            . $request->getUserIP() . "; "
            . $request->getUserAgent() . ";";
    }

}