<?php

use yii\helpers\Html;
use app\helpers\HtmlPurifier;
use yii\helpers\Url;

/**
 * @var $this yii\web\View
 * @var $model app\models\PostBlock
 *
 */

?>
<div class="post-block" data-type="<?= $model->isGIF() ? 'video' : $model->typeName ?>">
    <?php

    switch ($model->type) {

        case $model::BLOCK_TYPE_IMAGE:
            if ($model->isGIF()) {
                echo $this->render('_post_block_video', ['model' => $model]);
            } else {
                $postUrl = $postUrl === false ? $model->getImgSrc() : $postUrl;
                echo Html::a(
                	Html::img(Url::to('@web/media/spacer.gif'), [
                    'class' => 'img-responsive save-history lazyload',
                    'data-src' => $model->getImgSrc('adjusted'),
                    'data-width' => $model->media_width,
                    'data-height' => $model->media_height,
                    'data-origin-url' => $model->imgSrc,
                    'width' => $model->adjustedWidth,
                    'height' => $model->adjustedHeight,
                    'alt' => $model->file_name
                ]), $postUrl);
            }
            break;

        case $model::BLOCK_TYPE_VIDEO:
            echo $this->render('_post_block_video', ['model' => $model]);
            break;

        default:
            echo HtmlPurifier::process($model->text);
            break;
    }
    ?>
</div>
