<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AdminAsset;
use app\widgets\Alert;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\helpers\ArrayHelper;
use yii\widgets\Breadcrumbs;

AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title?:$this->context->title) ?></title>
	<?php $this->head() ?>
</head>
<body class="">
<?php $this->beginBody() ?>

<?php
NavBar::begin(['brandLabel' => 'Owned']);
echo Nav::widget(
	[
		'items' => [
			['label' => 'Home', 'url' => ['/']],
			['label' => 'Dashboard', 'url' => ['admin/index']],
			['label' => 'Users', 'url' => ['admin/users']],
			['label' => 'Posts', 'url' => ['admin/posts']],
			['label' => 'Token settings', 'url' => ['admin/token-settings']],
		],
		'options' => ['class' => 'navbar-nav'],
	]
);
echo Nav::widget(
	[
		'items' => [
			[
				'label' => Yii::$app->user->identity->displayName,
				'url' => 'javascript: return false;',
				'dropDownOptions'=>[
					'class'=>'pull-right'
				],
				'items'=>[
					['label'=>'Logout','url'=>['site/logout']]
				]
			],
		],
		'options' => ['class' => 'navbar-nav pull-right'],
	]
);

NavBar::end();
?>

<div class="container">
	<?= Breadcrumbs::widget(
		[
			'encodeLabels'=>false,
			'homeLink'=>[
				'label' => '<i class="fa fa-home"></i>',
				'url'=>['admin/index']
			],
			'links'=>ArrayHelper::getValue($this->params,'path')
		]
	)
	?>
	<?= Alert::widget(); ?>
	<?= $content ?>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
