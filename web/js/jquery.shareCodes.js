(function($){
    $.fn.shareCodes = function(method){
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method "' +  method + '" is not found in jquery.shareCodes' );
        }
    };

    var sourceRows,
        sourceLinks = [],
        sourceLinksContent,
        endPhrase;


    var getEndPhrase = function(albumLink){
        return "View more on <a href='"+albumLink+"'>pimpandhost.com</a>";
    };


    var initContainer = function(){
        var $this = $(this);
        var container = {};

        container.$this = $this;
        container.$hiContent = $this.find('#high-content');
        container.$thisWord = $this.find('#this');
        container.$mainBtns = $this.find('#main-btns');
        container.$btnHtml = $this.find('#btn-html');
        container.$btnLinks = $this.find('#btn-links');
        container.$btnBB = $this.find("#btn-bb");
        container.$ta =  $this.find('#site-codes');
        container.$sizesBlock = $this.find('#sizes-block');

        container.$btnLinks.on('click', function(){
            if($(this).hasClass("active")){
                return false;
            }
            container.$mainBtns.find('button').removeClass('active');
            $(this).addClass('active');
            container.$sizesBlock.hide();

            setTextAndAllign(container.$ta, sourceLinksContent);
        });

        container.$btnBB.on('click', function(){
            if($(this).hasClass("active")){
                return false;
            }

            container.$mainBtns.find('button').removeClass('active');
            $(this).addClass('active');


            container.$sizesBlock.fadeIn();
            setTextAndAllign(container.$ta, container.prepareContent(sourceRows));
        });

        container.$btnHtml.on('click', function(){
            if($(this).hasClass("active")){
                return false;
            }
            container.$mainBtns.find('button').removeClass('active');
            container.$sizesBlock.find('button').show();

            $(this).addClass('active');
            container.$sizesBlock.fadeIn();
            setTextAndAllign(container.$ta, container.prepareContent(sourceRows));
        });

        container.$sizesBlock.find('button').on('click', function(){
            var $me = $(this);
            container.$sizesBlock.find('button').removeClass('active');
            $me.addClass('active');
            setTextAndAllign(container.$ta, container.prepareContent(sourceRows));
        });


        container.prepareContentByType = function(rows){
            var codes = [];
            var delimetr = "\n";
            var size = this.$sizesBlock.find('.active').data('size');
            if(this.$btnBB.hasClass('active')){
                delimetr = "";
                $.each(rows, function(){
                    codes.push('[URL=' + this.url + '][IMG]' + this.src[size] + '[/IMG][/URL] ');
                });
            }else{
                $.each(rows, function(){
                    codes.push('<a  href="'+this.url+'"><img width="'+size+'"  src="' + this.src[size] + '"></a>');
                });
            }

            if(endPhrase){
                codes.push(endPhrase)
            }

            return codes.join(delimetr);
        };

        container.prepareContent = function(rows){
            return this.prepareContentByType(rows);
        };


        container.reset = function (){
            this.$hiContent.hide();
        };

        container.allignTa = function(){
            allignHeight(this.$ta[0]);
        };

        container.renderCodes = function(rows, albumLink, addLinkToEnd){

            sourceRows = rows;
            sourceLinks = [];
            addLinkToEndSource = addLinkToEnd;
            albumLinkSource = albumLink;
            $.each(rows, function(){
                sourceLinks.push(this.url);
            });

            var codesContent = this.prepareContent(rows);

            if(albumLink)
            {
                this.$hiContent.show();
                this.$this.find('#album-link').val(albumLinkSource)
                // setTextAndAllign(this.$this.find('#album-link'), albumLink);
            }else{
                this.$hiContent.hide();
            }

            if(albumLink && addLinkToEnd)
            {
                endPhrase = getEndPhrase(albumLink);
            }else{
                endPhrase = false;
            }

            sourceLinksContent = sourceLinks.join('\n');


            if(container.$btnLinks.hasClass('active')){
                setTextAndAllign(this.$ta, sourceLinksContent);
                container.$sizesBlock.hide();
            }else{
                setTextAndAllign(this.$ta, this.prepareContent(sourceRows));
                container.$sizesBlock.show();
            }
        };


        return container;
    };

    var allignHeight = function (ta){
        ta.style.height = '1px';
        ta.style.height = ta.scrollHeight+'px';
    };

    var setTextAndAllign = function($ta, text){
        if($ta.length > 0){
            $ta.text(text);
            allignHeight($ta[0]);
        }
    };


    var methods ={
        init:function(rows, albumLink, addToLinkToEnd){
            if(!this.shareCodesContainer){
                this.shareCodesContainer = initContainer.call(this);
            }

            this.shareCodesContainer.renderCodes(rows, albumLink, addToLinkToEnd);
        },
        reset:function(){
            if(this.shareCodesContainer){
                this.shareCodesContainer.reset();
            }
        },
        allignTa:function(){
            if(this.shareCodesContainer){
                this.shareCodesContainer.allignTa();
            }
        }
    };

})(jQuery);
