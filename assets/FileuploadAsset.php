<?php

namespace app\assets;

use yii\web\AssetBundle;

class FileuploadAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
    ];

    public $js = [
        'js/plugins/jquery.ui.widget.js',
        'js/plugins/jquery.iframe-transport.js',
        'js/plugins/jquery.fileupload.js',
        'js/plugins/fileupload.dynamic.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];
    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}