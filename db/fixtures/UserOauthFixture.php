<?php
namespace app\db\fixtures;

use yii\test\ActiveFixture;

class UserOauthFixture extends ActiveFixture {

	public $modelClass = 'app\models\UserOauth';
	public $depends = [
	];
}