<?php
namespace app\commands;


use app\helpers\FileHelper;
use app\helpers\LogHelper;
use app\helpers\StringHelper;
use app\helpers\CmdHelper;
use app\models\Album;
use app\models\Archive;
use app\models\Image;
use app\models\User;
use yii\base\Exception;
use yii\console\Controller;
use yii\helpers\Console;
use yii\db\Expression;

class ArchiveController extends Controller
{
    const PAGE_SIZE = 50;

    public function actionIndex($id)
    {
        /**
         * @var Archive
         */
        $archive = Archive::findOne(['id'=>$id]);
        if(!$archive){
            echo $this->ansiFormat("Archive with id '$id' not found", Console::FG_RED);
            return;
        }

        $request = $archive->getRequest();
        $images = Image::find();

        if(!empty($request['album_zip_name']))
        {
            if (empty($request['images'])) {
                throw new Exception("No images");
            }

            $archive->setFileName($request['album_zip_name']);
            $archive->title = $request['album_zip_name'];
            $images->where(['id' => $request['images']]);
            $images->with('album');
        }
        else
        {
            if (!empty($request['album_id'])) {
                $album = Album::findOne(['id' => $request['album_id']]);
            }
            elseif(!empty($request['imagesAuthorId'])){
                $userId = User::find()->where(['id'=>$request['imagesAuthorId']])->select('id')->scalar();
                if(empty($userId)){
                    throw new Exception("Can't find user #$userId for archiving images");
                }
                $album = Album::createUnsorted($request['imagesAuthorId']);
            }
            else{
                throw new Exception("Ablum id or images author id must be provided for archiving");
            }

            if(empty($archive->title)){
                $archive->setTitleForAlbum($album->title);
            }
            if($album)
            {
                $archive->setFileName(StringHelper::filenameToUrl($album->title));
                $images = $album->getImages([Image::STATUS_NORMAL, Image::STATUS_PRIVATE]);
            }
            else{
                throw new Exception("Album with id '" . $request['album_id'].  "' not found");
            }
        }

        $images = $images
            ->limit(\Yii::$app->params['maxImagesInArchive'])
            ->all();

        if(!empty($images)){
            try{
                $this->archiveImages($archive, $images);
            }catch (\Exception $e){
                $archive->makeErrored();
                $this->ansiFormat("Can't archive images");
            }
        }else{
            $archive->makeErrored();
            $this->ansiFormat("Archive #$archive->id was not created because there is not files to archive");
        }

        echo PHP_EOL;
    }

    protected function archiveImages($archive, $images)
    {
        $zip = new \ZipArchive();

        $fullArchiveFileName = $archive->getFullFileName();
        $archiveFileName = $archive->filename;

        LogHelper::log("Creating archive  #{$archive->id}",'@runtime/logs/zip/', 'console_archiving.log');

        if ($zip->open($fullArchiveFileName, \ZipArchive::CREATE) !== true) {
            throw new Exception("Coudn't  create file " . $fullArchiveFileName);
        }

        echo $this->ansiFormat("Creating archive $fullArchiveFileName...\n");

        $i = 1;

        $tmpDir = \Yii::getAlias("@runtime/archive/tmp/" . mt_rand() . "/");
        FileHelper::createDirectory($tmpDir);

        $savedImages = [];
        $archivedImageIds = [];


        foreach ($images as $image)
        {
            $album = $image->album;
            if ($image->authorId == $archive->authorId ||
                ($album && $album->authorId == $archive->authorId))
            {
                $localDestination = $tmpDir . $image->id .".". pathinfo($image->fileName, PATHINFO_EXTENSION);

                try {
                    if (!$image->saveFileFromServer($localDestination)) {
                        continue;
                    }
                } catch (\Exception $e) {
                    LogHelper::log($e, '@runtime/logs/zip/', 'console_downloading_file.log');
                    continue;
                }

                if (in_array(pathinfo($image->title, PATHINFO_FILENAME), $savedImages))
                {
                    $filename = $archiveFileName . '/' . pathinfo($image->title, PATHINFO_FILENAME) . '_' . $i . '.' . pathinfo($image->fileName, PATHINFO_EXTENSION);
                    $filename = StringHelper::toFilename($filename);
                    if (!$zip->addFile($localDestination, $filename)) {
                        continue;
                    }
                    $savedImages[] = pathinfo($image->title, PATHINFO_FILENAME) . '_' . $i++;
                }
                else
                {
                    $filename = $archiveFileName . '/' . pathinfo($image->title, PATHINFO_FILENAME) . '.' . pathinfo($image->fileName, PATHINFO_EXTENSION);
                    $filename = StringHelper::toFilename($filename);
                    if (!$zip->addFile($localDestination, $filename)) {
                        continue;
                    }

                    $savedImages[] = pathinfo($image->title, PATHINFO_FILENAME);
                }

                $archivedImageIds[] = $image->id;
                echo $this->ansiFormat("File $localDestination was added to archive" . PHP_EOL);
            }
        }

        if($zip->close() &&  file_exists($fullArchiveFileName))
        {
            CmdHelper::exec('rm -rf ' . $tmpDir, "@runtime/logs/zip/", 'archiving.log');
            @chmod($fullArchiveFileName, 0777);

            echo $this->ansiFormat("$fullArchiveFileName was successfully created\n", Console::FG_GREEN);
            $archive->imageCount = count($savedImages);
            $archive->size = filesize($fullArchiveFileName);
            $archive->status = Archive::STATUS_ARCHIVED;
            $archive->createTime = date('Y-m-d H:i:s');
            if($archive->save()){
                $emailStatus = $this->sendNotificationEmail($archive) ? " Email was sent." : "Email was NOT sent!";
                echo $this->ansiFormat("Data for archive #$archive->id was updated." . $emailStatus, Console::FG_GREEN);
                LogHelper::log("Archive  #{$archive->id} was updated.$emailStatus\n"
                    . count($savedImages) . " images(ids:".implode(",", $archivedImageIds).") were added to $fullArchiveFileName",
                    '@runtime/logs/zip/', 'console_archiving.log');

            }else{
                echo $this->ansiFormat("Archive #$archive->id was NOT saved because of ", json_encode($archive->getErrors()));
            }
        }
        else{
            throw new Exception("Can't create close archive!");
        }
    }


    protected function sendNotificationEmail($archive)
    {
        $user = $archive->user;

        $from_email = 'no-reply@' . \Yii::$app->name;
        $from_name = \Yii::$app->name;
        $recipients = $user->email;
        $subject = 'Your archive is ready';

        $name = '=?UTF-8?B?' . base64_encode($from_name) . '?=';
        $subject = '=?UTF-8?B?' . base64_encode($subject) . '?=';
        $headers = "From: $name <{$from_email}>\r\n"
            . "Reply-To: {$from_email}\r\n"
            . "MIME-Version: 1.0\r\n"
            . "Content-type: text/html; charset=UTF-8";

        $html = $this->renderPartial('//email_templates/archive', array(
            'user' => $user,
            'link' => $archive->getDownloadLink(),
        ), true);

        return mail($recipients, $subject, $html, $headers);
    }

    public function actionAddAllToQueue()
    {
        $archives = Archive::find()->select('id')->column();
        foreach($archives as $id){
            $cmd = "archive $id";
            $res = \Yii::$app->pheanstalk->addJob($cmd);
            if($res){
                echo $this->ansiFormat("Archiving $id was added to queue...\n, position #$res");
            }else{
                echo $this->ansiFormat("Archiving $id is already in queue...\n, position #$res");
            }
        }

        echo PHP_EOL;
    }


    public function actionRemoveExpired()
    {
        $query = Archive::find()->where(new Expression("DATE_ADD(createTime, INTERVAL " . Archive::EXPIRE_PERIOD . " SECOND) < NOW()"));
        echo "\nStarting removing...\n";
        $total = $query->count('id');
        echo $total . " expired archives found to be deleted...";

        $offset = $totalDeleted = 0;

        do{
            $archives = $query->limit(self::PAGE_SIZE)->offset($offset)->all();
            foreach($archives as $archive)
            {
                if($archive->delete()){
                    $totalDeleted++;
                }else
                {
                    $error = "Can't delete archive#$archive->id because: " . json_encode($this->getErrors());
                    echo $this->ansiFormat("\n" . $error . "\n", Console::FG_RED);
                    LogHelper::log($error, '@runtime/logs/zip/', 'console_removing_expired.log');
                }
            }

            echo $this->ansiFormat("\r $totalDeleted of $total(" .floor($totalDeleted*100/$total). "%) archives were deleted                   ");


        }while(count($archives) == self::PAGE_SIZE);

        echo $this->ansiFormat("\nRemoving complete!",Console::FG_GREEN);
    }



}
