<?php

use yii\db\Migration;

class m160609_153823_add_album_table extends Migration
{
    public $table = 'album';
    public $userRef = 'authorId';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'content' => $this->text(),
            'type' => $this->smallInteger(1)->defaultValue(0),
            'contentDisplay' => $this->text(),
            'status' => "tinyint(1) NOT NULL DEFAULT '0'",
            'imageCount' => $this->integer()->notNull()->defaultValue(0),
            'imageSize' => $this->integer()->notNull()->defaultValue(0),
            'commentCount' => $this->integer()->notNull()->defaultValue(0),
            'defaultImageId' => $this->integer(),
            'commentKey' => $this->string()->notNull()->defaultValue(''),
            'privateKey' => $this->string()->notNull()->defaultValue(''),
            'guestKey' => $this->string()->notNull()->defaultValue(''),
            'rating' => $this->integer()->notNull()->defaultValue(0),
            'viewsCount' => $this->integer()->notNull()->defaultValue(0),
            $this->userRef => $this->integer(11),
            'updateTime' => $this->timestamp(),
            'createTime' => $this->dateTime(),
        ], $tableOptions);

        $this->createIndex($this->userRef . "_index", $this->table, $this->userRef);
        $this->addForeignKey($this->userRef . "_{$this->table}_fk", $this->table, $this->userRef,'user', 'id');
    }

    public function down()
    {
        $this->dropForeignKey($this->userRef . "_{$this->table}_fk", $this->table);
        $this->dropTable($this->table);
    }

}
