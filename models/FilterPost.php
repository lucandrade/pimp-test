<?php

namespace app\models;


use Yii;
use yii\base\Model;
use yii\helpers\Url;

class FilterPost extends Model
{
    const DAY_TIME = 86400;

    const DEFAULT_TYPE_VALUE = 'new';
    const DEFAULT_PERIOD_VALUE = 'all';

    public $type;
    public $period;
    public $date_from;
    public $date_to;
    public $timeFrom;
    public $timeTo;
    public $tableAlias = 'p';
    public $formActionUrl;
    public $presets = [
        'today',
        'week',
        'month',
        'all',
    ];


    /**
     * @var \SphinxClient
     */
    private $_sphinxClient;
    private $_sphinxQuery = '';
    private $_time24;

    /**
     * @return string
     */
    public function getSphinxQuery()
    {
        return $this->_sphinxQuery;
    }

    /**
     * @return \SphinxClient
     */
    public function getSphinxClient()
    {
        return $this->_sphinxClient;
    }

    /**
     * @param \SphinxClient $sphinxClient
     */
    public function setSphinxClient(\SphinxClient $sphinxClient)
    {
        $this->_sphinxClient = clone $sphinxClient;
    }

    public function getTime24()
    {
//        if ($this->_time24 === null) {
//            $this->_time24 = time() - self::DAY_TIME;
//        }
//        return $this->_time24;
        return time() - self::DAY_TIME;
    }

    public function getAttributeTime($attribute, $defaultDate = null)
    {
        if ($this->{$attribute} === null || !strtotime($this->{$attribute})) {
            return $defaultDate === null ? $this->time24 : strtotime($defaultDate);
        }

        return strtotime($this->{$attribute});
    }

    public function filterAttributeParams()
    {
        return [
            'period',
            'type',
            'date_from',
            'date_to',
        ];
    }

    public function rules()
    {
        return [
            [['period'], 'in', 'range' => $this->presets, 'skipOnEmpty' => true],
            [['type'], 'in', 'range' => ['best', 'new', 'my', 'favorites'], 'skipOnEmpty' => true],

            [['date_from', 'date_to'], 'default', 'value' => null],
            [['date_from', 'date_to'], 'date', 'format' => 'yyyy-MM-dd'],
        ];
    }

    public function initFormActionUrl()
    {
        $urlParams = [Yii::$app->controller->getRoute()];
        $urlParams['type'] = $this->type;
        $this->formActionUrl = Url::to($urlParams, true);
    }

    public function init()
    {
        parent::init();

        $this->loadFromGet();
        $this->initMainAttributes();
        $this->unsetInvalidAttributes();

        $this->date_to = $this->date_to ?: $this->date_from;

        $this->timeFrom = $this->getAttributeTime('date_from');
        $this->timeTo = $this->getAttributeTime('date_to');
        if ($this->timeFrom > $this->timeTo) {
            list($this->timeFrom, $this->timeTo) = array($this->timeTo, $this->timeFrom);
        }

        $this->initFormActionUrl();
    }


    protected function loadFromGet()
    {
        foreach ($this->filterAttributeParams() as $attribute => $param) {
            $attribute = is_numeric($attribute) ? $param : $attribute;
            $this->{$attribute} = Yii::$app->request->get($param);
        }
    }

    public function initPeriodTime()
    {
        switch ($this->period) {
            case 'today':
                $this->timeFrom = $this->time24;
                $this->timeTo = $this->time24;
                break;

            case 'week':
                $this->timeFrom = strtotime('-7 DAY');
                $this->timeTo = strtotime('today');
                break;

            case 'month':
                $this->timeFrom = strtotime('-30 DAY');
                $this->timeTo = strtotime('today');
                break;

            case 'all':
                $this->timeFrom = null;
                $this->timeTo = null;
                break;

            default:
                $this->timeFrom = $this->timeFrom ?: $this->time24;
                $this->timeTo = $this->timeTo ?: $this->time24;
        }
    }

    public function applyClientFilters()
    {
        if ($this->_sphinxClient !== null) {
            $this->addPeriodFilter();
            $this->addUserFilter();

        }
    }

    protected function reInitPeriodTime()
    {
        if ($this->timeFrom === $this->time24 && $this->timeTo === $this->time24) {
            $this->timeFrom = strtotime('today');
            $this->timeTo = strtotime('today');
        }
    }

    protected function addPeriodFilter()
    {
        $this->initPeriodTime();
        if ($this->timeFrom && $this->timeTo) {
            if ($this->timeFrom === $this->timeTo) {
                $this->_sphinxClient->SetFilterRange('ts_live_at', $this->timeFrom, $this->timeFrom + self::DAY_TIME);
            } else {
                $this->_sphinxClient->SetFilterRange('ts_live_at', $this->timeFrom, $this->timeTo + self::DAY_TIME);
            }
        }
        $this->reInitPeriodTime();
    }

    protected function addUserFilter()
    {
        if (Yii::$app->user->identity && !Yii::$app->user->identity->isShowAdult()) {
            $this->_sphinxClient->SetFilter('is_adult_attr', [0]);
        }

        if ($this->type === 'my' && !Yii::$app->user->isGuest) {
            $this->_sphinxClient->SetFilter('user_id_attr', [Yii::$app->user->identity->id]);
        } else if ($this->type === 'favorites' && !Yii::$app->user->isGuest) {
            $ids = Yii::$app->user->identity->getFavoritePostIds();
            $this->_sphinxClient->SetFilter('id_attr', $ids ?: [0]);
        }
    }

    public function getDateFrom($format = 'Y-m-d', $defaultTime = null)
    {
        if ($this->timeFrom === null) {
            return $defaultTime ? date($format, $defaultTime) : null;
        }

        return date($format, $this->timeFrom);
    }

    public function getDateTo($format = 'Y-m-d', $defaultTime = null)
    {
        if ($this->timeTo === null) {
            return $defaultTime ? date($format, $defaultTime) : null;
        }
        return date($format, $this->timeTo);
    }

    public function getPeriodString($format = 'F d Y')
    {
        if ($this->period !== null) {
            return $this->period;
        } else if ($this->timeFrom === $this->timeTo) {
            return $this->timeFrom === strtotime('TODAY') ? 'today' : date($format, $this->timeFrom);
        } else {
            return date($format, $this->timeFrom) . ' - ' . date($format, $this->timeTo);
        }
    }

    public function getPresetsLabels()
    {
        $labels = [];
        foreach ($this->presets as $preset) {
            $labels[$preset] = [
                'url' => Url::to([Yii::$app->controller->getRoute(), 'type' => $this->type, 'period' => $preset]),
                'title' => $preset,
            ];
        }
        return $labels;
    }

    public function getIndexName()
    {
        if ($this->type === 'my') {

            return 'pah_posts_search pah_posts_search_delta pah_posts_search_future';

        } else if (empty(Yii::$app->params['indexes']['main'])) {

            return 'pah_posts_search pah_posts_search_delta';

        } else {
            return Yii::$app->params['indexes']['main'];
        }
    }

    public function initMainAttributes()
    {
        if (empty($this->period)) {
            if (is_null($this->type) || in_array($this->type, ['my', 'favorites'])) {
                $this->period = self::DEFAULT_PERIOD_VALUE;
            }
        }
        if (empty($this->type)) {
            $this->type = self::DEFAULT_TYPE_VALUE;
        }
    }

    public function unsetInvalidAttributes()
    {
        $this->validate();
        if ($this->hasErrors()) {
            foreach ($this->getErrors() as $attr => $message) {
                $this->{$attr} = NULL;
            }
        }
    }

}
