<?php

namespace app\components;

use Yii;
use yii\rbac\Rule;
use app\models\Post;

class RatingRule extends Rule
{

    public $name = 'ratingRule';

    public function execute($user, $item, $params)
    {
        if (Yii::$app->user->isGuest || Yii::$app->user->identity === null) {
            return false;
        }

        switch ($item->name) {
            case 'addPosts':
                return $this->executeAddPosts($user, $item, $params);

            case 'addComments':
                $ratingLimit = -50;
                break;

            case 'addCommentImages':
                $ratingLimit = 10;
                break;

            case 'addVideo':
                $ratingLimit = 150;
                break;

            case 'combineTags':
                $ratingLimit = 10000;
                break;

            default:
                $ratingLimit = PHP_INT_MAX;
        }

        return Yii::$app->user->identity->rating > $ratingLimit;
    }

    public function executeAddPosts($user, $item, $params)
    {
        $postCount = (int)Post::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->andWhere('DATE(live_at) >= CURDATE()')
            ->count();

        return $postCount < $this->getPostLimit(Yii::$app->user->identity->rating);
    }

    public function getPostLimit($rating)
    {
        $ratingLimits = [
            200 => 1,
            500 => 2,
            1000 => 3,
            3000 => 4
        ];

        foreach ($ratingLimits as $limit => $postLimitCount) {
            if ($rating < $limit) {
                return $postLimitCount;
            }
        }

        return PHP_INT_MAX;

    }
}
