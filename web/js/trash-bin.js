var trashBinPage = (function(){
    var processImages = function($wrappers, type){
        var ids = imageListUtilities.getIdsFromWrappers($wrappers, true);
        $.ajax({
            url:$('#trash-bin-page').data(type + '-url'),
            data: {ids:ids},
            dataType:"json",
            type:"POST",
            success:function(){
                selectedIds = imageListUtilities.getIdsFromWrappers($('.image-wrapper').imageSelector('getSelected'));
                imageListUtilities.updateListView();
                pahIniter.widgets.albumMetrics.removeWrappers($wrappers);

            }
        })
    };

    var purgeImages = function ($wrappers){
        bootbox.confirm("Are you sure?", function(confirmed){
            if(confirmed){
                processImages($wrappers, 'purge');
            }
        });
    };

    return {
        init:function(){
            $('#trash-bin-page').on('click','.hover-bar .fa-undo', function(e){
                e.stopPropagation();
                processImages($(this).parents(' .image-wrapper'), 'restore');
                return false;
            });

            $('#trash-bin-page').on('click',' .hover-bar .fa-remove', function(e){
                e.stopPropagation();
                purgeImages($(this).parents(' .image-wrapper'));
                return false;
            });
        },
        restoreImages:function($wrappers){
            return processImages($wrappers, 'restore');
        },
        purgeImages: function($wrappers){
            purgeImages($wrappers);
        }
    };
})();

$(document).ready(function(){

    if($('#trash-bin-page').length == 0){
        return;
    }

    $('.selection-bar .restore').on('click', function(){
        trashBinPage.restoreImages( $('.image-wrapper').imageSelector('getSelected'));
    });

    $('.selection-bar .purge').on('click', function(){
        trashBinPage.purgeImages( $('.image-wrapper').imageSelector('getSelected'));
    });

    $('#empty-trash-bin-btn').on('click', function(e){
        e.preventDefault();
        var $button = $(this);
        bootbox.confirm("Are you sure?", function(confirmed){
            if(confirmed){
                $button.attr('disabled', 'disabled');
                $button.html("Deleting...");
                $.ajax({
                    url:$button.data('url'),
                    type:"POST",
                    success:function(){
                        $('#trashbin-content').fadeOut('slow', function(){
                            $(this).html('<h2>Trash bin is empty</h2>').show();
                        });
                    },
                    error:function(){
                        alert("Something was going wrong while erasing trashbin");
                        $button.removeAttr('disabled');
                        $button.html("Empty");
                    }
                })
            }
        });

    });

    trashBinPage.init();
});
