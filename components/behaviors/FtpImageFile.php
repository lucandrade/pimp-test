<?php
namespace app\components\behaviors;
use app\helpers\ImageHelper;
use app\models\Image;
use yii\base\InvalidConfigException;
use app\helpers\StringHelper;
use app\helpers\FileHelper;
use app\helpers\LogHelper;
use yii\helpers\Json;


class FtpImageFile extends ImageFileBehavior
{
    public $serverAttr = 'server';
    public $idAttr = 'id';
    public $filenameProperty = 'fileName';
    public $statusAttr = 'status';

    public $processRoute;

    public $normalStatus = Image::STATUS_NORMAL;
    public $processingStatus = Image::STATUS_PROCESSING;

    public $folderHashAttr = 'authorId';

    public $serversForUpload = [55];

    public $oldServerThumbExtension = 'jpg';

    const SERVER_TYPE_VER2 = 'ver2';


    public function init(){
        parent::init();
        if(empty($this->processRoute)){
            throw new InvalidConfigException("Process route must be set to console command will upload file to ftp");
        }

    }

    public function getServerConfig()
    {
        $servers = \Yii::$app->params['servers'];
        $serverNumber = $this->owner->{$this->serverAttr};
        if (isset($servers[$serverNumber]))
        {
            $server = $servers[$serverNumber];
            if (isset($server['server'], $server['username'], $server['password'])) {
                return $server;
            } else {
                throw new InvalidConfigException("Properties 'server', 'username', 'password' must be set");
            }
        } else {
            throw new InvalidConfigException("Config for server $serverNumber not found");
        }

    }

    public function getFtpFolder()
    {
        if($this->owner->{$this->serverAttr} < 10){
            return 'image/' . FileHelper::hashDir($this->owner->{$this->folderHashAttr})
                . $this->owner->{$this->folderHashAttr} . "/" . FileHelper::hashDir($this->owner->hash);
        }
        else{
            return FileHelper::hashDir($this->owner->{$this->folderHashAttr}) . $this->owner->{$this->folderHashAttr} . '/'
            . FileHelper::hashDir(StringHelper::dec2any($this->owner->{$this->idAttr}))
            . StringHelper::dec2any($this->owner->{$this->idAttr}) . '/';
        }
    }


    public function createThumb($size)
    {
        if($this->getIsThumbRequired($size)){
            return parent::createThumb($size);
        }

        return true;
    }

//    public function createThumbs()
//    {
//        if($this->createZeroThumb()){
//            return parent::createThumbs();
//        }else{
//            return false;
//        }
//    }


    protected function getIsThumbRequired($size)
    {
        if(empty($size) || $size === Image::SIZE_0_PX){
            return true;
        }

        $biggestSize = empty($this->owner->biggestSize) ? 0 : $this->owner->biggestSize;

        $suffixData = $this->oldSuffixes[$size];

        return $biggestSize >= $suffixData[1];
    }


    public function getFtpPath()
    {
        $server = $this->getServerConfig();
        $dir = isset($server['nfs']) ? $server['nfs'] : $server['path'];
        return $dir . $this->getFtpFolder();
    }

    public function connectToFtpServer()
    {
        $server = $this->getServerConfig();
        $connId = ftp_connect($server['server']);

        if(!$connId){
            $error = error_get_last();
            throw new InvalidConfigException("Can't connect to ftp server:" . $server['server'] . "Error:" . json_encode($error));
        }

        if (!ftp_login($connId, $server['username'], $server['password'])) {
            $error = error_get_last();
            throw new InvalidConfigException("Can't login to ftp server:" . $server['server'] . "Error:" . json_encode($error));
        }

        return $connId;
    }

    public function prepareFtpFolder($connId)
    {
        $server = $this->getServerConfig();

        $ftpRootDir = "ftp://" . $server['username'] . ":" . $server['password'] . "@" . $server['server'] . "/";
        $paths = explode('/', $this->getFtpPath());

        foreach ($paths as $k => $v) {
            $arrayTmp = array_slice($paths, 0, $k + 1);
            $pathTmp = implode('/', $arrayTmp);
            if ($pathTmp) {
                $ftpDir = $ftpRootDir . $pathTmp;
                if (!is_dir($ftpDir)) {
                    ftp_mkdir($connId, $pathTmp);
                }
                //@ftp_chmod($conn_id, 0777, $path_tmp);
            }
        }
    }

    public function saveToFptServer($connId, $distFile, $localFile)
    {
        $fileSaved = true;
        $retry = 0;
        while (!ftp_put($connId, $distFile, $localFile, FTP_BINARY)) {
            $retry++;
            if ($retry >= 3) {
                $fileSaved = false;
                break;
            }
        }
        if (!$fileSaved) {
            $this->owner->addError($this->filenameProperty, "Coudn't put file $localFile to ftp $distFile. Error:" . Json::encode(error_get_last()));
        }

        return $fileSaved;
    }

    public function getHttpFtpPath()
    {
        $server = $this->getServerConfig();
        return $server['url'] . $this->getFtpFolder();
    }

    public function getFtpFileName($thumbSize = false)
    {
        return $this->getFtpPath() . $this->getFilename($thumbSize);
    }


    public function getHttpFtpFileName($thumbSize = false, $displayProcessing = true, $urlencodeFilename = true)
    {
//        return $this->getImgSrc('adjusted');
        if($displayProcessing && $this->getIsProcessing()){
            return \Yii::getAlias("@web/static/i/processing.gif");
        }


        $filename = $urlencodeFilename ? rawurlencode($this->getFilename($thumbSize)) : $this->getFilename($thumbSize);
        return $this->getHttpFtpPath() . $filename;
    }

    protected function remoteFileExists($fileUrl)
    {
        return !empty(@getimagesize($fileUrl));
    }

    public function putToFtpServer()
    {
        $connId = $this->connectToFtpServer();
        $this->prepareFtpFolder($connId);

        ftp_pasv($connId, true);

        foreach($this->allSizes() as $size)
        {
            if($this->getIsThumbRequired($size)){
                if(!$this->saveToFptServer($connId, $this->getFtpFileName($size), $this->getFullFileName($size))){
                    return false;
                }
            }
        }

        if(!empty($this->statusAttr)){
            $this->owner->{$this->statusAttr} = $this->normalStatus;
            return $this->owner->save();
        }

        return true;

    }


    protected function getSizesForRemoving()
    {
        $sizes = $this->allSizes();
        $sizes[] = self::OLD_GIF_THUMB_SIZE;
        return $sizes;
    }

    public function removeFromLocal()
    {
        foreach($this->getSizesForRemoving() as $size)
        {
            $removed = FileHelper::removeFile($this->getFullFileName($size));
            if(!$removed)
            {
                $this->owner->addError($this->filenameProperty, "Coudn't delete local file : " . $this->getFullFileName($size));
                return false;
            }
        }

        return true;
    }


    public function removeFromFtp()
    {
        $connId = $this->connectToFtpServer();

        $cdnUrls = [];

        foreach($this->getSizesForRemoving() as $size)
        {
            $cdnUrls[] = $this->getHttpFtpFileName($size);
            $name = $this->getFtpFileName($size);
            if(ftp_size($connId, $name) > 0){
                if(!ftp_delete($connId, $name))
                {
                    $this->owner->addError($this->filenameProperty, "Coudn't  {$name} delete from ftp because of " . error_get_last());

                    if(!empty($cdnUrls)){
                        \Yii::$app->cdnApi->reflected_clear_cache($cdnUrls);
                        \Yii::$app->cdnApi->inxy_clear_cache($cdnUrls);
                    }
                    return false;
                }
            }
        }

        \Yii::$app->cdnApi->reflected_clear_cache($cdnUrls);
        \Yii::$app->cdnApi->inxy_clear_cache($cdnUrls);

        return true;
    }

    public function clearCdnCache()
    {
        $cdnUrls = [];
        foreach($this->getSizesForRemoving() as $size)
        {
            $cdnUrls[] = $this->getHttpFtpFileName($size);
        }

        $res1 = \Yii::$app->cdnApi->reflected_clear_cache($cdnUrls);
        $res2 = \Yii::$app->cdnApi->inxy_clear_cache($cdnUrls);

        return [$res1, $res2];
    }


    public function removeFiles(){
        return $this->removeFromFtp() && $this->removeFromLocal();
    }


    public function ftpProcess()
    {
        return $this->createThumbs() && $this->putToFtpServer() && $this->removeFromLocal();
    }

    public function putToFtpUploadingQueue($priority = 0)
    {
        return \Yii::$app->pheanstalk->addJob("$this->processRoute {$this->owner->id}", 0, $priority);
    }


    public function getIsProcessing(){
        return $this->owner->{$this->statusAttr} == $this->processingStatus;
    }

    protected function getThumbSuffix($size, $defaultExtension)
    {
        if($this->getServerIsVer2()){
            return parent::getThumbSuffix($size, $defaultExtension);
        }else{

            $suffixData = $this->oldSuffixes[$size];

            if($this->getIsThumbRequired($size)){
                return $suffixData[0];
            }else{
                if($size == Image::SIZE_0_PX){
                    return $this->getOldZeroThumbSuffix();
                }
                return ".$defaultExtension";
            }
        }
    }

    protected function getOldZeroThumbSuffix(){
        return $this->oldSuffixes[Image::SIZE_0_PX][0];
    }

    public function getThumbFileName($thumbSize = false)
    {
        if($this->getIsOldServer() && $thumbSize == Image::SIZE_0_PX){
            return $this->getPrefixThumbFolder() . parent::getThumbFileName(Image::SIZE_0_PX);
        }else{
            return parent::getThumbFileName($thumbSize);
        }

    }


    protected function getPrefixThumbFolder()
    {
        $server = $this->getServerConfig();
        if($this->owner->{$this->serverAttr} < 10){
            return 'thumbs/';
        }elseif(!empty($server['hotlink'])){
            return 'hotlink-ok/';
        }else{
            return '';
        }
    }

    public function getIsOldServer(){
        $server = $this->getServerConfig();
        return $this->owner->{$this->serverAttr} < 10 || !empty($server['hotlink']);
    }

    public function createZeroThumb()
    {
        $fullName = $this->getFullFileName();
        $thumbName = $this->getFullFileName(Image::SIZE_0_PX);
        $out = ImageHelper::sqThm($fullName, $thumbName, Image::SIZE_0_PX, Image::SIZE_0_PX);
        if(!empty($out)){
            $this->owner->addError('fileName', "Error while creating zero thumb:" . json_encode($out));
        }
        return file_exists($thumbName);
    }

    public function getServerIsVer2()
    {
        $server = $this->getServerConfig();
        return isset($server['type']) && $server['type'] === self::SERVER_TYPE_VER2;
    }

    public function pickServer()
    {
        $servers = $this->serversForUpload;
        if(@geoip_country_code_by_name(@$_SERVER['REMOTE_ADDR']) == 'CN'){
            return 33;
        }
        else{
            return $servers[array_rand($servers)];
        }
    }



    public function saveFileFromServer($localPath)
    {
        $server = $this->getServerConfig();
        if (isset($server['nfs'])) {
            /* Put file to NFS */
            if (file_exists($this->getFtpFileName()) && is_writable($localPath)) {
                copy($this->getFtpFileName(), $localPath);
            }
        } else {
            try {
                $connId = ftp_connect($server['server']);
                if (!$connId) {
                    throw new \Exception("Coudn't connect to server:" . json_encode($server));
                }

                if (tp_login($connId, $server['username'], $server['password'])) {
                    ftp_pasv($connId, true);
                    $res = ftp_get($connId, $localPath, $this->getFtpFileName(), FTP_BINARY);
                    ftp_close($connId);

                    if (!$res) {
                        throw new \Exception("Can't download file from server:" . json_encode(error_get_last()));
                    }
                } else {
                    throw new \Exception("Coudn't login to server:" . json_encode($server));
                }
            } catch (\Exception $e) {
                LogHelper::log($e, '@runtime/logs/zip/', 'console_downloading_file.log');
                return false;
            }
        }

        return file_exists($localPath);
    }
}
