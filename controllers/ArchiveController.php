<?php

namespace app\controllers;


use app\components\Controller;
use app\models\Archive;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class ArchiveController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['list'],
                        'roles' => ['@']
                    ],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['get'],
                ],
            ],
        ];
    }

    public function actionList()
    {
        $archives = Archive::find()
            ->where(['authorId' => \Yii::$app->user->id])
            ->andWhere(new Expression("DATE_ADD(createTime, INTERVAL " . Archive::EXPIRE_PERIOD . " SECOND) > NOW()"))
            ->all();

        return $this->render('list', compact('archives'));
    }
}