<?php

namespace app\components\authclients\autopost;
use app\components\behaviors\ConsoleAuthClientBehavior;
use app\models\Post;
use app\models\PostBlock;
use app\models\PostToSocialsLog;
use yii\base\Exception;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/**
 */
class Facebook extends \yii\authclient\clients\Facebook
{
    public $pageId;
    public $apiBaseUrl = 'https://graph.facebook.com/v2.5';
    public $scope = "manage_pages,email,publish_pages";
    public  $tokenFileDir = "@runtime/facebook";

    private $_autoPostResult;


    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = ConsoleAuthClientBehavior::className();
        return $behaviors;
    }

    public function fetchPageAccessToken()
    {
        $res = $this->api($this->pageId, 'GET', [
            'fields' => 'access_token',
        ]);
        $token = $this->createToken(['params'=>$res]);
        $this->setAccessToken($token);
        return $token;
    }

    public function autoPost(Post $post)
    {
        $this->fetchPageAccessToken();

        $blocks = $post->getPostBlocks()
            ->where("type=:type", [':type'=>PostBlock::BLOCK_TYPE_VIDEO])
            ->limit(2)->all();

        if(count($blocks) == 1){

            if(!empty($blocks[0]->file_name))
            {
                $this->_autoPostResult = $this->uploadVideo($blocks[0]->getVideoFileName(), [
                    'title' => $post->title,
                    'description' => $post->title . "
                    Be cool! Share & Like! @[{$this->pageId}]",
                    'call_to_action' => Json::encode([
                        'type' => 'WATCH_MORE',
                        'value'=>[
                            'link' => \Yii::$app->urlManager->hostInfo,
                            'link_caption' => "Liked it? then check THIS!",
                        ]
                    ])
                ]);

                return $this->_autoPostResult['success'];

            }
            elseif(!empty($blocks[0]->video_url))
            {
                $this->_autoPostResult = $this->api($this->pageId . "/feed", 'POST', [
                    'message' => $post->title . ' ' . $post->getUrl(true),
                    "link" => $blocks[0]->video_url
                ]);
            }
            else{
                throw new Exception("To post video to facebook it must conain either file_name or video_url");
            }
        }else{
            $this->_autoPostResult = $this->api($this->pageId . "/feed", 'POST', [
                'message' => $post->title,
                "link" => $post->getUrl(true)
            ]);
        }
        return !empty($this->_autoPostResult['id']);
    }

    /**
     * @return mixed
     */
    public function getAutoPostResult()
    {
        return $this->_autoPostResult;
    }


    protected function restoreAccessToken()
    {
        $tokenArr = $this->getTokenFromFile();
        if(!$tokenArr){
            throw new Exception("Can't restore token from file for pinterest");
        }
        $token = $this->createToken(['params'=>$tokenArr]);
        if ($token->getIsExpired()) {
            $token = $this->refreshAccessToken($token);
        }
        return $token;
    }


    public function uploadVideo($filename, $videoData)
    {
        $fb = new \Facebook\Facebook([
            'app_id' => $this->clientId,
            'app_secret' => $this->clientSecret,
            'default_graph_version' => 'v2.3'
        ]);

        $this->loadTokenFromFile();
        $this->fetchPageAccessToken();

        $accessToken = $this->getAccessToken()->token;
        $response['success'] = false;

        try {
            $response = $fb->uploadVideo($this->pageId, $filename, $videoData, $accessToken);
        } catch(\Exception $e) {
           $response['error'] = $e->getMessage();
        }

        return $response;
    }


    protected function apiInternal($accessToken, $url, $method, array $params, array $headers)
    {
        if(strtoupper($method) == 'GET' ){
            $url = $this->composeUrl($url, $params);
        }
        $this->populateCurlRequestData($url, $method, $params, $headers);
        return parent::apiInternal($accessToken, $url,$method, $params, $headers);
    }


}