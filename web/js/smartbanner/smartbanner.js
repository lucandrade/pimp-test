$(function () {

    if (!$.cookie('smartbanner')) {
        $('body').append('<div class="smartbanner visible-xs-block"><span class="close">&times;</span></div>');
        var sb = $('.smartbanner').append(window.smart_banner);

        sb.find('.close').on('click', function () {
            $.cookie('smartbanner', '1', {
                expires: window.smart_expires
            });
            sb.remove();
            return false;
        });

    }
});



