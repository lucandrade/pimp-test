<?php
/**
 * Created by PhpStorm.
 * User: mactra
 * Date: 13.11.15
 * Time: 15:50
 */

namespace app\controllers;

use Yii;
use app\components\Controller;
use app\models\ResetPasswordForm;
use app\models\UserOauth;
use app\models\UserSettingsForm;
use yii\base\InvalidParamException;
use app\models\User;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use app\components\SphinxDataProvider;
use app\models\Post;
use app\models\FilterUserPost;

class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['profile', 'reset-password']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionSocialAccounts()
    {
        return $this->render('social-accounts');
    }


//    public function actionRequestPasswordReset()
//    {
//        $model = new PasswordResetRequestForm();
//
//        if ($model->load(Yii::$app->request->post())) {
//
//            $validations = $this->performAjaxValidation($model);
//
//            if($validations !== false)
//                return $validations;
//
//            if (!$model->validate()){
//                return 'error';
//            }
//
//            $user = User::findOne([
//                'email' => $model->email,
//            ]);
//
//            if($user)
//            {
//                $user->generatePasswordResetToken();
//                if($user->save() && $model->sendEmail($user->password_reset_token))
//                    return 'ok';
//            }
//        }
//
//        return 'error';
//    }

    // todo: move to site controller
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('reset-password', [
            'model' => $model,
        ]);
    }


    public function actionSettings()
    {
        $formModel = new UserSettingsForm();
        if ($formModel->load($_POST)) {
            $validations = $this->performAjaxValidation($formModel);
            if ($validations !== false) {
                return $validations;
            }

            if ($formModel->applySettings()) {
                Yii::$app->session->setFlash('success', 'Settings have been changed');
                return $this->refresh();
            }
        }

        return $this->render('settings', ['model' => $formModel]);
    }

    public function actionSettingAvatar()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $formModel = new UserSettingsForm();
        if ($formModel->load(Yii::$app->request->post()) && $formModel->applyAvatar()) {
            return [
                'status' => 'success',
                'avatar' => $formModel->profileAvatar
            ];
        }

        return [
            'status' => 'error',
            'errors' => $formModel->getErrors()
        ];
    }

    public function actionUnlink($provider)
    {
        if (!in_array($provider, UserOauth::possibleSocialAccounts())) {
            throw new NotFoundHttpException;
        }

        if (\Yii::$app->user->identity->unlinkSocialAccount($provider)) {
            \Yii::$app->session->setFlash('success', 'You successfully unlinked your ' . ucfirst($provider) . " account");
        } else {
            \Yii::$app->session->setFlash('error', "We couldn't unlink your " . ucfirst($provider) . " account");
        }

        return $this->redirect(['user/settings']);
    }

    public function actionSetDefault($provider)
    {
        if (!in_array($provider, UserOauth::possibleSocialAccounts())) {
            throw new NotFoundHttpException;
        }

        \Yii::$app->user->identity->current_oauth_provider = $provider;

        if (\Yii::$app->user->identity->save()) {
            \Yii::$app->session->setFlash('success', 'You set as default your ' . ucfirst($provider) . " account");
        } else {
            \Yii::$app->session->setFlash('error', "We couldn't set as default your " . ucfirst($provider) . " account");
        }

        return $this->redirect(['user/settings']);
    }

    public function actionSetDefaultAvatar()
    {
        $user = Yii::$app->user->identity;
        $user->current_oauth_provider = null;

        if ($user->update(true, ['current_oauth_provider'])) {
            Yii::$app->session->setFlash('success', 'Your profile avatar set as default');
        } else {
            Yii::$app->session->setFlash('error', 'Error set default avatar');
        }
        return $this->redirect(['user/settings']);
    }

    public function actionProfile($id = null, $username = null)
    {
        if(!empty($id)){
            $user = User::findOne($id);
        }else{
            $user = User::findOne(['username' => $username]);
        }


        if (empty($user)) {
            throw new NotFoundHttpException('User not found');
        }

        $dataQuery = Post::find()
            ->from('post p')
            ->with([
                'postBlocks',
                'tags',
                'author'
            ]);

        $filter = new FilterUserPost([
            'sphinxClient' => Yii::$app->sphinxClient->getClient(),
            'user' => $user
        ]);

        $filter->applyClientFilters();

        $defaultSort = $filter->type === 'best' ? ['rating' => SORT_DESC, 'live_at' => SORT_DESC]
            : ['live_at' => SORT_DESC];

        $dataProvider = new SphinxDataProvider([
            'cacheDuration' => 60,
            'query' => $dataQuery,
            'sphinxClient' => $filter->getSphinxClient(),
            'indexName' => $filter->indexName,
            'primaryModelKey' => 'p.id',
            'sphinxQuery' => $filter->getSphinxQuery(),
            'useSqlSortMode' => true,
            'sortingAttributes' => [
                'live_at' => 'ts_live_at',
                'rating' => 'rating'
            ],
            'sort' => [
                'defaultOrder' => $defaultSort,
            ],
            'pagination' => [
                'pageSize' => Yii::$app->params['itemsPerPage'],
                'defaultPageSize' => Yii::$app->params['itemsPerPage']
            ]
        ]);

        return $this->render('public-profile', [
            'filter' => $filter,
            'user' => $user,
            'dataProvider' => $dataProvider
        ]);
    }


}