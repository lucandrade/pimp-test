<h2><center>Terms and Agreements</center></h2>

<div id="tos">
<h3>Terms and Agreements for PimpandHost.com:</h3>
<br />

<p>PimpandHost reserves the right to remove any digital content that compromises
security of our dedicated servers or use excessive bandwidth. </p>


<p>All files are &copy; to their respective owners.<br />
Though PimpandHost staff continously and vigilantly monitors the types of images uploaded to its servers,
PimpandHost is not responsible for the content of any uploaded files, nor is it in affiliation with any
entities that may be represented in the uploaded files.</p>
<br /><br />

<h3>You agree not to use PimpandHost Service to:</h3>
<br />

<p>Upload child pornography. Uploaders' as well as all the viewers' IP addresses will be reported to the proper authorities
such as the FBI and Interpol. With this information, these two agencies will be able to find and arrest the user
in regard to the above-mentioned abuse.</p>


<p>Upload images that infringe on the copyrights of any entity. This includes, but is not limited to, copyrighted photographs,
as well as files for which user lacks usage and distribution permissions. An abuse of copyright law will result in immediate
removal of the images off the PimpandHost service.</p>


<p>Our policy is to consider All DMCA abuse reports and take appropriate action within 24-48 hours. </p>

</div>



<br><br>

<br><br>



