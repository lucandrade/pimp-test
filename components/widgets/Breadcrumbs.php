<?php

namespace app\components\widgets;


class Breadcrumbs extends \yii\widgets\Breadcrumbs
{
    public $itemTemplate = "<li class='fa'>{link}</li>\n";
    public $homeLink = false;
    public $activeItemTemplate = "<li class='fa'><span>{link}</span></li>\n";
}