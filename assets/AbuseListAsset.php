<?php

namespace app\assets;


use yii\web\AssetBundle;

class AbuseListAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/abuse-list.css',
    ];

    public $js = [
        'js/abuse-list.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
        'app\assets\BootboxAsset',
        'app\assets\ShareImagesModalAsset',
        'app\assets\SelectionBarAsset',
        'app\assets\ImageListAsset',
    ];
    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}