<?php

namespace app\assets;


use app\components\AssetBundle;

class DarkroomjsAsset extends AssetBundle
{
	public $sourcePath = '@bower/darkroom';
	public $css = [
		'build/darkroom.css',
	];

	public $js = [
		'build/darkroom.js',
	];

	public $depends = [
		'app\assets\FabricjsAsset',
	];
}
