<?php
namespace app\models;
use yii\helpers\Html;
use yii\base\Model;
use yii\helpers\Url;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => User::className(),
                'message' => 'There is no user with such email.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail($token)
    {
		// todo: get from config
        $subject = "Reset password for " . \Yii::$app->name;
        $link = Url::to(['user/reset-password', 'token' => $token], true);
        $to      = $this->email;
        $from = empty(\Yii::$app->params['resetPasswordEmailFrom'])
            ? \Yii::$app->params['activationEmailFrom']
            : \Yii::$app->params['resetPasswordEmailFrom'];

        if(is_array($from)){
            $from = array_keys($from)[0];
        }

        $message = "Use this link to reset your password: " . $link;
        $headers = "From: reset password " . \Yii::$app->name .  " <$from>" . "\r\n" .
            'X-Mailer: PHP/' . phpversion() . "\r\n";

        $headers .= 'Content-type: text/html; charset=utf-8';

        $ret = mail($to, $subject, $message, $headers);

        return $ret;
    }
}
