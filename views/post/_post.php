<?php

use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use yii\widgets\ListView;
use yii\helpers\Url;
use app\widgets\Adv;

/**
 * @var $model app\models\Post
 * @var $tag app\models\Tag
 *
 */

?>
<div class="item">
    <div class="post list-post" data-isseen="<?= (int)in_array($model->id, $seenIds) ?>"
         data-widget="page"
         data-page-key="post_<?= $model->id ?>"
         data-page="<?= $page ?>">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <div class="post-header-main">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="widget-rating widget-rating-scores">
                                <div data-rating="page"
                                     data-rating-type="scores"
                                     data-rating-key="post_<?= $model->id ?>"
                                     data-rating-value="<?= $model->rating ?>">
                                </div>
                            </div>
                            <h1 class="p-title">
                                <a href="<?= $model->getUrl(); ?>" class="post-title save-history">
                                    <?= Html::encode($model->title) ?>
                                </a>
                            </h1>
                            <?php if (!empty($model->tags)) { ?>
                                <div class="top-tag">
                                <span class="post-tags">
                                    <?= implode(' ', $model->linkedTagList) ?>
                                </span>
                                </div>
                            <?php } ?>
                            <div class="post-admin">
                                <?php if (!empty($model->author)) { ?>
                                    <a class="post-author" href="<?= $model->author->profileUrl ?>">
                                        <i>
                                            <img src="<?= $model->author->avatar ?>"
                                                 alt="<?= $model->author->avatar ?>"
                                                 width="25"
                                                 height="25">
                                        </i>
                                        <?= $model->author->username ?>
                                    </a>
                                <?php } ?>
                                <span class="post-date" title="<?= $model->live_at ?>">
                                <?= $model->liveDate ?>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row collapsing-content">
            <div class="col-xs-12 col-sm-12">
                <div class="collapse in post-content-wrapper"
                     id="collapsePost-<?= $model->id ?>"
                     aria-expanded="true">
                    <div class="post-expand-wrapper">
                        <?php
                        $blockDataProvider = new ArrayDataProvider([
                            'allModels' => $model->getPostBlocks()->all(),
                            'pagination' => false
                        ]);
                        ?>
                        <?php
                        echo ListView::widget([
                            'dataProvider' => $blockDataProvider,
                            'itemView' => '_post_block',
                            'viewParams' => [
                                'postUrl' => $model->getUrl()
                            ],
                            'summary' => false,
                            'emptyText' => false,
                        ]);
                        ?>
                    </div>
                </div>
                <div class="post-bottom">
                    <div class="pull-right manage-right-icons">
                        <?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->is_admin): ?>
                            <a href="<?= Url::to(["admin/post-to-socials"]); ?>"
                               class="share-to-socials"
                               title="share to socials">
                                <i class="fa fa-2x fa-share"></i>
                            </a>
                        <?php endif; ?>

                        <?php if (Yii::$app->user->can('admin') || $model->checkAuthorAccess()) { ?>
                            <a href="<?= $model->getUpdateUrl(); ?>" class="btn-update-post" title="update post">
                                <i class="fa fa-2x fa-edit"></i>&nbsp
                            </a>
                        <?php } ?>
                    </div>
                <span class="widget-rating">
                    <span data-rating="page"
                          data-rating-type="buttons"
                          data-rating-key="post_<?= $model->id ?>"
                          data-rating-value="<?= $model->rating ?>">
                    </span>
                </span>
                    <div class="post-btn-group pull-left">
                        <div class="favo">
                            <?php
                            $iconClass = "fa-heart active";
                            if (Yii::$app->user->isGuest || !Yii::$app->user->identity->isFavoritePost($model->id)) {
                                $iconClass = "fa-heart";
                            }
                            ?>
                            <a class="add-to-favorite"
                               title="add to favorites"
                               href="<?= Yii::$app->user->isGuest ? Url::to(['post/toggle-favorite', 'post_id' => $model->id]) : '#' ?>">
                                <i class="fa <?= $iconClass ?>"></i>
                            </a>
                        </div>
                        <div class="comm">
                            <a class="post-comments" href="<?= $model->commentsUrl ?>" title="comments">
                                <i class="fa fa-comment"></i>
                                <span data-page="comments-count"></span>
                            </a>
                        </div>
                        <div class="mys">
                            <div id="rrssb-<?= $model->id ?>-bottom">
                            <span class="btn"
                                  title="share"
                                  data-rrssb="button"
                                  data-rrssb-container="#rrssb-<?= $model->id ?>-bottom"
                                  data-rrssb-url="<?= $model->getUrl(true) ?>"
                                  data-rrssb-title="<?= $model->title ?>"
                                  data-rrssb-image="<?= $model->postImage ?>">
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$currenPage=Url::current();
if($currenPage=='/best' || $currenPage=='/new'){
    $postCount = (int)$widget->dataProvider->getCount();
    $currentPostIndex = $index + 1;
    if ($currentPostIndex === $postCount ||
        (
            $postCount >= round(Yii::$app->params['itemsPerPage'] / 2, 0, PHP_ROUND_HALF_UP) &&
            $currentPostIndex == round($postCount / 2, 0, PHP_ROUND_HALF_UP)
        )
    ) { ?>
        <div class="item">
            <?= Adv::widget(); ?>
        </div>
<?php }
} ?>


