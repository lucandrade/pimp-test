<?php

namespace app\models;


use app\components\behaviors\FtpImageFile;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use app\helpers\FileHelper;
use app\helpers\ImageHelper;
use app\helpers\HtmlPurifier;

/**
 * Class PostBlock
 *
 * @property int $post_id;
 * @property string $type;
 * @property string $text;
 * @property int $position;
 * @property string $file_name;
 * @property int $file_size;
 * @property int $media_width;
 * @property int $media_height;
 * @property string $video_url;
 * @property string $vid;
 *
 * @package app\models
 */
class PostBlock extends ActiveRecord
{

    const BLOCK_TYPE_TEXT = 1;
    const BLOCK_TYPE_IMAGE = 2;
    const BLOCK_TYPE_VIDEO = 3;
    const VIDEO_TYPE_YOUTUBE = 'youtube';
    const VIDEO_TYPE_COUB = 'coub';
    const VIDEO_TYPE_FACEBOOK = 'facebook';

    const IMAGE_FOLDER = 'posts_uploaded_images';

    const STATUS_NORMAL = 0;
    const STATUS_PROCESSING = 5;


    public static function tableName()
    {
        return 'post_block';
    }

    public static function getTypes()
    {
        return [
            self::BLOCK_TYPE_TEXT => 'text',
            self::BLOCK_TYPE_IMAGE => 'image',
            self::BLOCK_TYPE_VIDEO => 'video'
        ];
    }

    public function getTypeName()
    {
        $types = self::getTypes();
        if (isset($types[$this->type])) {
            return $types[$this->type];
        }
        return null;
    }



    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['ftpFile'] = [
            'class' => FtpImageFile::className(),
            'filenameProperty' => 'file_name',
            'folderHashAttr' => 'post_id',
            'storeFolder' => self::IMAGE_FOLDER,
            'processRoute' => 'post-block/process',
            'thumbSizes' => [200],
            'serversForUpload' => \Yii::$app->params['serversForPosts']
        ];

        return $behaviors;
    }

    public function rules()
    {
        return [
            ['type', 'required'],
            [['type', 'text', 'file_name', 'video_url'], 'trim'],
            ['type', 'in', 'range' => array_keys(self::getTypes())],
            [['video_url'], 'required', 'when' => function ($m) {
                return $m->type === PostBlock::BLOCK_TYPE_VIDEO && empty($m->file_name);
            }],

            ['server', 'required', 'when'=> function($m){
                return $m->type === self::BLOCK_TYPE_IMAGE;
            }]
            ,
            [['text'], function ($attribute) {
                $this->{$attribute} = HtmlPurifier::process($this->{$attribute});
            }],
            [
                'video_url',
                'filter',
                'filter' => function ($value) {
                    if (strpos($value, 'facebook.com') !== false) {
                        preg_match('/https:\/\/www.facebook.com\/([^\/]+)\/videos\/(\d+)\//', $value, $m);
                        if (isset($m[1]) && isset($m[2])) {
                            return 'https://www.facebook.com/' . $m[1] . '/videos/' . $m[2] . '/';
                        }
                    }
                    return $value;
                }
            ],
            [['video_url'], 'url'],
            [['video_url'], 'match', 'pattern' => '/(facebook.com|youtube.com\/v\/|youtube.com\/watch|youtu.be\/|coub.com\/view\/).+/i', 'message' => 'Url not supported'],
            [['video_url'], 'urlExists', 'when' => function ($m) {
                return $m->type === PostBlock::BLOCK_TYPE_VIDEO && empty($m->file_name);
            }],
            ['post_id', 'exist', 'targetClass' => Post::className(), 'targetAttribute' => 'id'],
            [['position', 'server', 'status'], 'integer'],
        ];
    }

    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }

    public function afterDelete()
    {
        if ((int)$this->type === self::BLOCK_TYPE_IMAGE && file_exists($this->mediaFileName)) {
            unlink($this->mediaFileName);
        }

        if ($this->isUploadedVideo()) {
            $client = Yii::$app->authAdminCollection->getClient('ytadmin');
            $client->removeVideo($this->getVideoId());
        }
        parent::afterDelete();
    }

    public function afterSave($insert, $changedAttributes)
    {
        if (
            $this->isUploadedVideo() &&
            in_array('text', array_keys($changedAttributes))
        ) {
            $client = Yii::$app->authAdminCollection->getClient('ytadmin');
            $client->renameVideo($this->getVideoId(), $this->text);
        }


        parent::afterSave($insert, $changedAttributes);
    }

    public function urlExists($attribute)
    {
        parse_str(parse_url($this->{$attribute}, PHP_URL_QUERY), $query);
        $provideMessage = 'Please, provide valid video URL - currently we are supporting Coub and Youtube';
        if (count($query) > 1) {
            return $this->addError($attribute, $provideMessage);
        } else if (count($query) === 1 && !isset($query['v'])) {
            return $this->addError($attribute, $provideMessage);
        }

        $headers = @get_headers($this->{$attribute});
        if (isset($headers[0]) && strpos($headers[0], '200') === false && strpos($headers[0], '302') === false) {
            return $this->addError($attribute, 'Url not exist');
        }
    }

    public function getMediaPath()
    {
        $path = Yii::getAlias('@app/media_protected/postblock/image/');
        $path .= ImageHelper::getPathPrefix($this->id);
        FileHelper::createDirectory($path, 0777);
        return $path;
    }

    public function getVideoPath()
    {
        $path = Yii::getAlias('@app/media_protected/postblock/video/');
        $path .= ImageHelper::getPathPrefix($this->id);
        FileHelper::createDirectory($path, 0777);
        return $path;
    }

    public function getWebmPath()
    {
        $path = Yii::getAlias('@app/web/media/webm/');
        $path .= ImageHelper::getPathPrefix($this->id);
        FileHelper::createDirectory($path, 0777);
        return $path;
    }

    public function getMp4Path()
    {
        $path = Yii::getAlias('@app/web/media/mp4/');
        $path .= ImageHelper::getPathPrefix($this->id);
        FileHelper::createDirectory($path, 0777);
        return $path;
    }

    public function getMediaFileName()
    {
        return $this->mediaPath . $this->id . '.' . pathinfo($this->file_name, PATHINFO_EXTENSION);
    }

    public function getVideoFileName()
    {
        return $this->videoPath . $this->id . '.' . pathinfo($this->file_name, PATHINFO_EXTENSION);
    }

    public function getThumbFileName()
    {
        $path = (int)$this->type === self::BLOCK_TYPE_VIDEO ? $this->videoPath : $this->mediaPath;
        return $path . $this->id . '.png';
    }

    public function getWebmFileName()
    {
        return $this->webmPath . $this->id . '.webm';
    }

    public function getM4vFileName()
    {
        return $this->mp4Path . $this->id . '.m4v';
    }

    public function getWebmSrc()
    {
        if (file_exists($this->webmFileName)) {

            $fileName = ImageHelper::getPathPrefix($this->id);
            $fileName .= basename($this->webmFileName);

            return Url::to('@web/media/webm/' . $fileName, true);
        }

        return null;
    }

    public function getM4vSrc()
    {
        if (file_exists($this->m4vFileName)) {

            $fileName = ImageHelper::getPathPrefix($this->id);
            $fileName .= basename($this->m4vFileName);

            return Url::to('@web/media/mp4/' . $fileName, true);
        }

        return null;
    }

    public function saveLoadedImage($imageFileName, $copy = false)
    {
        $this->file_name = FileHelper::generateFileName($this->file_name, $this->id, 4);
        $newFullName = $this->getFullFileName();

        if (file_exists($imageFileName)) {
            if (
                ($copy && copy($imageFileName, $newFullName)) ||
                rename($imageFileName, $newFullName)
            ) {

                if ($this->isGIF()) {
                    $this->updateGIFJob();
                }

                $this->status = self::STATUS_PROCESSING;

                if($this->updateImageProperties())
                {
                    $this->putToFtpUploadingQueue();
                    return true;
                }

            } else {
                $this->addError('file_name', 'Failed move image file(class ' . __CLASS__ . ' line ' . __LINE__ . ')');
            }

        } else {
            $this->addError('file_name', 'Image file not exist(class ' . __CLASS__ . ' line ' . __LINE__ . ')');
        }

        return false;

    }

    public function updateVideoJob()
    {
        if (empty($this->video_url)) {
            /**
             * @var $pheanstalk \app\components\Pheanstalk;
             */
            $pheanstalk = Yii::$app->pheanstalk;
            $jobId = $pheanstalk->addJob('video/upload ' . $this->id, 60 * 60 * 24 * 30, 10, 'videotube');
            if (is_numeric($jobId)) {
                return true;
            } else {
                $this->addError('file_name', 'Video job not added(class ' . __CLASS__ . ' line ' . __LINE__ . ')');
                return false;
            }
        }

        return true;
    }

    public function updateGIFJob()
    {
        if (!file_exists($this->webmFileName)) {
            /**
             * @var $pheanstalk \app\components\Pheanstalk;
             */
            $pheanstalk = Yii::$app->pheanstalk;
            $jobId = $pheanstalk->addJob('video/convert ' . $this->id, 60 * 60 * 24 * 30, 10, 'videotube');
            if (is_numeric($jobId)) {
                return true;
            } else {
                $this->addError('file_name', 'GIF job not added(class ' . __CLASS__ . ' line ' . __LINE__ . ')');
                return false;
            }
        }

        return true;
    }

    public function saveLoadedVideo($videoFileName, $copy = false)
    {
        if (file_exists($videoFileName)) {

            if (
                ($copy && copy($videoFileName, $this->videoFileName)) ||
                rename($videoFileName, $this->videoFileName)
            ) {

                return $this->updateVideoJob();

            } else {
                $this->addError('file_name', 'Failed to move video file(class ' . __CLASS__ . ' line ' . __LINE__ . ')');
                return false;
            }

        } else {
            $this->addError('file_name', 'Video file not exist(class ' . __CLASS__ . ' line ' . __LINE__ . ')');
            return false;
        }
    }

    public function updateImageProperties()
    {
        $fullName = $this->getFullFileName();
        if (file_exists($fullName)) {
            $this->file_size = filesize($fullName);

            if (in_array(mime_content_type($fullName), ImageHelper::$imageMimeTypes)) {
                $imagine = \yii\imagine\Image::getImagine();
                $source = $imagine->open($fullName);
                $size = $source->getSize();
                $this->media_width = $size->getWidth();
                $this->media_height = $size->getHeight();
            } else {
                $logData = [
                    'postblock_id' => $this->id,
                    'fileName' => $fullName,
                    'mime' => mime_content_type($fullName),
                ];
                ImageHelper::log($logData);
            }

            $this->server = $this->pickServer();

            if ($this->update(true, ['file_size', 'media_width', 'media_height','file_name', 'status', 'server'])) {
                return true;
            } else {
                $this->addError('file_name', 'Error updating image properties(class ' . __CLASS__ . ' line ' . __LINE__ . ')');
                return false;
            }

        } else {
            $this->addError('file_name', 'Media file not found(class ' . __CLASS__ . ' line ' . __LINE__ . ')');
            return false;
        }

    }

    public function getRelativeWidth($toFixWidth = 750)
    {
        if ($this->media_width > $toFixWidth) {
            return $toFixWidth;
        }

        return $this->media_width;
    }

    public function getRelativeHeight($toFixWidth = 750)
    {
        if ($this->media_width > $toFixWidth) {
            return (int)($this->media_height * $toFixWidth / $this->media_width);
        }

        return $this->media_height;
    }

    public function getAdjustedWidth($toFixWidth = 750)
    {
        return $toFixWidth;
    }

    public function getAdjustedHeight($toFixWidth = 750)
    {
        return (int)($this->media_height * $toFixWidth / $this->media_width);
    }

    public function getImgSrc($format = 'original')
    {
        $size = false;
        if($format == 'thumb_50'){
            $size = 200;
        }

        return $this->getHttpFtpFilename($size);

//        if (file_exists($this->mediaFileName)) {
//            if (!in_array($format, ImageHelper::$availableFormats)) {
//                $format = ImageHelper::$defaultSrcFormat;
//            }
//            $src = Url::base(true) . '/media/?';
//            $src .= $format;
//            $src .= '?/postblock/image/';
//            $src .= ImageHelper::getPathPrefix($this->id);
//            $src .= $this->id . '.' . pathinfo($this->file_name, PATHINFO_EXTENSION);
//
//            return $src;
//        }
//
//        return null;
    }

    public function getThumbSrc($format = 'default')
    {
        if (file_exists($this->thumbFileName)) {
            if (!in_array($format, ImageHelper::$availableFormats)) {
                $format = ImageHelper::$defaultSrcFormat;
            }
            $src = Url::base(true) . '/media/?';
            $src .= $format;
            $src .= '?/postblock/' . $this->getTypeName() . '/';
            $src .= ImageHelper::getPathPrefix($this->id);
            $src .= $this->id . '.png';

            return $src;
        }

        return null;
    }

    public function saveVideoThumbnail()
    {
        if (!file_exists($this->thumbFileName)) {
            $img = @imagecreatefromjpeg($this->videoImage);
            if ($img) {
                \yii\imagine\Image::thumbnail($this->videoImage, 640, 480)->save($this->thumbFileName);
            }
        }
        return file_exists($this->thumbFileName);
    }

    public function updateVideoData($videoData)
    {
        $this->video_url = 'https://www.youtube.com/watch?v=';
        $this->video_url .= $videoData->id;
        return $this->update(true, ['video_url']);
    }


    public function getVideoType()
    {
        if (preg_match('/.+(youtube\.com\/).+/', $this->video_url)) {
            return self::VIDEO_TYPE_YOUTUBE;
        } else if (preg_match('/.+(youtu\.be\/).+/', $this->video_url)) {
            return self::VIDEO_TYPE_YOUTUBE;
        } else if (preg_match('/.+(\/coub\.com\/).+/', $this->video_url)) {
            return self::VIDEO_TYPE_COUB;
        } else if (preg_match('/.+(\/www\.facebook\.com\/).+/', $this->video_url)) {
            return self::VIDEO_TYPE_FACEBOOK;
        }

        return null;
    }

    public function getVideoId()
    {
        if ($this->videoType !== self::VIDEO_TYPE_FACEBOOK) {
            if (preg_match('/watch\?v=(.+)$/', $this->video_url, $m)) {
                if (isset($m[1])) {
                    return $m[1];
                }
            } else if (preg_match('/.+\/(.+)$/', $this->video_url, $m)) {
                if (isset($m[1])) {
                    return $m[1];
                }
            }
        }
        return null;
    }

    public function getYoutubeImage($type = 'sddefault')
    {
        $types = [
            'hqdefault',
            'mqdefault',
            'sddefault',
            'maxresdefault'
        ];
        if (!in_array($type, $types)) {
            $type = 'sddefault';
        }

        $url = 'https://i.ytimg.com/vi/' . $this->videoId . '/' . $type . '.jpg';
        if (ImageHelper::urlExist($url)) {
            return $url;
        } else {
            foreach ($types as $defaultType) {
                $url = 'https://i.ytimg.com/vi/' . $this->videoId . '/' . $defaultType . '.jpg';
                if (ImageHelper::urlExist($url)) {
                    return $url;
                }
            }
        }
        return '';
    }

    public function getCoubImage()
    {
        return '';
    }

    public function getFacebookImage()
    {
        return '';
    }

    public function getVideoImage()
    {
        switch ($this->videoType) {
            case self::VIDEO_TYPE_YOUTUBE:
                return $this->youtubeImage;
            case self::VIDEO_TYPE_COUB:
                return $this->coubImage;
            case self::VIDEO_TYPE_FACEBOOK:
                return $this->facebookImage;
        }
    }

    public function getYoutubeUrl()
    {
        return 'https://www.youtube.com/embed/' . $this->videoId;
    }

    public function getCoubUrl()
    {
        return '//coub.com/embed/' . $this->videoId;
    }

    public function getFacebookUrl()
    {
        return $this->video_url;
    }

    public function getVideoUrl()
    {
        switch ($this->videoType) {
            case self::VIDEO_TYPE_YOUTUBE:
                return $this->youtubeUrl;
            case self::VIDEO_TYPE_COUB:
                return $this->coubUrl;
            case self::VIDEO_TYPE_FACEBOOK:
                return $this->facebookUrl;
        }
    }

    public function getGifUrl()
    {
        return 'https://www.youtube.com/embed/' . $this->videoId . '?playlist=' . $this->videoId . '&modestbranding=1&autoplay=1&loop=1&controls=0&rel=0';
    }

    public function isGIF()
    {
        return strtolower(pathinfo($this->file_name, PATHINFO_EXTENSION)) === 'gif';
    }

    public function isUploadedVideo()
    {
        return (int)$this->type === self::BLOCK_TYPE_VIDEO && $this->getVideoId() && !empty($this->file_name);
    }

    public function isProcessing()
    {
        if (
            (int)$this->type === self::BLOCK_TYPE_IMAGE &&
            $this->isGIF() &&
            !file_exists($this->webmFileName)
        ) {
            return true;
        } else if (
            (int)$this->type === self::BLOCK_TYPE_VIDEO &&
            empty($this->videoUrl)
        ) {
            return true;
        }

        return false;
    }

    public function isEmptyPreview()
    {
        if (
            (int)$this->type === self::BLOCK_TYPE_VIDEO &&
            !$this->isUploadedVideo() &&
            empty($this->videoImage)
        ) {
            return true;
        }
        return false;
    }
}