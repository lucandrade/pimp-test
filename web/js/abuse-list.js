$(document).ready(function(){

    var $container = $('#list-abuse-page');
    if($container.length == 0){
       return false;
    }

    $('#submit-batch-report-btn').on('click', function(e){
        e.preventDefault();

        var $me = $(this);
        $me.attr('disabled', 'disabled').html('Loading...');
        var req = {
            text: $('#abuse-batch-report').val()
        };

        $.ajax({
            url:$me.data('url'),
            dataType:"json",
            type: "POST",
            data: {req:JSON.stringify(req)},
            success: function(res){
                window.location.reload(true);
            }
        });
    });


    $('#abuse-list .cancel').on('click', function(e){

        e.preventDefault();
        var $button = $(this), $tr = $button.parents('tr');
        $button.attr('disabled', 'disabled').html('Loading...');
        $.ajax({
            url: $container.data('cancel-url'),
            data: {id: $tr.data('key')},
            type: "POST",
            dataType:"json",
            success: function(res){
                 if(res.status && res.status == 'ok'){
                     $tr.fadeOut('slow', function(){
                         $tr.remove();

                         if($('#abuse-list td').length == 0){
                             $('#reported-images').fadeOut(function(){
                                 $('#reported-images').remove();
                             });
                         }
                     });
                 }else{
                     alert("Some errors occurred while canceling abuse");
                 }
            }
        })
    });


    $('#purge-all-btn').on('click', function(){
        var $me = $(this);
        $me.attr('disabled','disabled').html('Loading...');
        $.ajax({
            url:$me.data('url'),
            type:"POST",
            dataType:'json',
            success:function(res){
                var message, count = parseInt(res.count);
                if(count > 0)
                {
                    message = count + " reported images were purged.";
                    if(count === 1){
                        message  = "Reported image was purged";
                    }
                    $('#reported-images').html("<h3>"+message+"</h3>");
                }else{
                    alert("No images were purged. Please try again later");
                    $me.removeAttr('disabled').html('Purge All');
                }
            },
            error:function(){
                alert("Some errors occurred!");
                $me.removeAttr('disabled').html('Purge All');
            }
        });
    });

});