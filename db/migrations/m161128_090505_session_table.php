<?php

use yii\db\Migration;

class m161128_090505_session_table extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE {{%session}} (
              `id` char(40) COLLATE utf8_unicode_ci NOT NULL,
              `expire` int(11) DEFAULT NULL,
              `data` blob,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
        ");
    }

    public function down()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->execute('DROP TABLE {{%session}}');
        $this->execute('SET FOREIGN_KEY_CHECKS = 1');
    }

}
