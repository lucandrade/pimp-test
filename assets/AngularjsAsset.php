<?php

namespace app\assets;

use yii\web\AssetBundle;

class AngularjsAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
    ];

    public $js = [
        'js/vendor/angular.min.js'
    ];

    public $depends = [
    ];
    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}