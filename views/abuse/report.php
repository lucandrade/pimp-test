<?php
\app\assets\AbuseReportAsset::register($this);
?>

<h1 class="text-center">Report Abuse</h1>

<div id="abuse-report-page" class="text-center">
    <p>By submitting this form, you are alerting the <?= \Yii::$app->name ?>team that this site has content that is in violation of our Terms of Service.</p>
    <p>Submitted content: <b id="abuse-url"><?= $url;?></b></p>
    <button id="abuse-btn" data-url="<?= \yii\helpers\Url::to(['abuse/ajax-report']) ?>" class="btn btn-primary">Submit</button>
</div>
