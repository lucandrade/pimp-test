<?php

namespace app\widgets;


use app\models\Image;
use yii\base\Widget;
use yii\db\ActiveQuery;

class SpecialListThumb extends Widget
{

    /**
     * @var ActiveQuery
     */
    public $queryCount;

    public $thumb;
    public $emptyThumb;

    public $url;
    

    public function run()
    {
        $count = $this->queryCount->count('id');
        return $this->render('SpecialListThumb', compact('count'));
    }


}