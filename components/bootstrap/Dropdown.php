<?php

namespace app\components\bootstrap;


class Dropdown extends \yii\bootstrap\Dropdown
{
    public function publicRenderItems($items, $options = [])
    {
        return $this->renderItems($items, $options);
    }

}