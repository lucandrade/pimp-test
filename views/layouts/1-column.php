<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use app\widgets\CuteCatIcon;

$this->beginContent('@app/views/layouts/main-layout.php');
?>
    <!-- Entries Column -->
    <div class="col-xs-12">
        <?= Alert::widget();?>
        <?= $content ?>
    </div>

<?php $this->endContent(); ?>





