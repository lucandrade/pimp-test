<?php
/**
 * Created by PhpStorm.
 * User: mactra
 * Date: 24.11.15
 * Time: 18:06
 */

namespace app\models;


use Yii;
use Exception;
use yii\db\ActiveRecord;

class JobLog extends ActiveRecord
{

    public static $reconnectionRepeats = 3;

    private static $_defaultRepeats = 0;

    public static function tableName()
    {
        return "JobLog";
    }

    public function rules()
    {
        return [
            ['command', 'string', 'max' => 256],
            ['execute_time', 'double'],
            ['job_id', 'integer'],
            ['command_output', 'safe'],
        ];
    }

    public static function addLog($attributes)
    {
        $log = new self;
        $log->setAttributes($attributes);
        $log->save();
        return $log;
    }

    public static function addedInPeriod($cmd, $period, $timeUnit = "SECOND")
    {
        $condition = 'command = :cmd AND removed_from_queue_at > DATE_SUB(NOW(), INTERVAL :period ' . $timeUnit . ')';
        self::$_defaultRepeats = self::$reconnectionRepeats > self::$_defaultRepeats ? self::$reconnectionRepeats : self::$_defaultRepeats;

        try {

            $count = self::find()
                ->where($condition, [':cmd' => $cmd, ':period' => $period])
                ->count();

            self::$reconnectionRepeats = self::$_defaultRepeats;
            return $count;

        } catch (Exception $e) {

            if (self::$reconnectionRepeats > 0) {

                self::$reconnectionRepeats--;
                Yii::$app->db->close();
                Yii::$app->db->open();

                return self::addedInPeriod($cmd, $period, $timeUnit);

            } else {

                throw $e;

            }
        }
    }

}