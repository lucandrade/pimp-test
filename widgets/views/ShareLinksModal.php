<?php
use yii\helpers\Url;
$topTa = 'codes';
$bottomTa = 'site-codes';
?>

<div class="modal" id="share-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        data-album-codes-url="<?= Url::to(['album/get-share-codes']) ?>"
        data-image-codes-url="<?= Url::to(['image/get-share-codes']) ?>"
>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?= $this->render("ShareCodes"); ?>
            </div>
        </div>
    </div>
</div>