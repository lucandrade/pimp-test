<script>
    window.CKSDKInit = function() {
        CK.init(<?= json_encode($config) ?>);
        CK.share('[data-rrssb="button"]');
        CK.vote('[data-rating="page"]');
        CK.counter('[data-page="comments-count"]');
        CK.comments('[data-comment="comment-widget"]');
    };

    (function(d, s, id){
        if (d.getElementById(id)) {return;}
        var js, jsf = d.getElementsByTagName(s)[0];
        js = d.createElement(s); js.id = id;
        js.src = ("https:" == document.location.protocol ? "https" : "http") + "://<?= \Yii::$app->params['commentsHost'] ?>/1.0/js";
        jsf.parentNode.insertBefore(js, jsf);
    }(document, 'script', 'comments-sdk'));
</script>

