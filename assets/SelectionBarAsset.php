<?php

namespace app\assets;


use yii\web\AssetBundle;

class SelectionBarAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/selection-bar.css',
    ];

    public $js = [
        'js/jquery.selectionBar.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
        'app\assets\ImageSelectorAsset',
    ];

    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}