<?php

use yii\db\Migration;

class m160803_150438_add_abuse_search_table extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE `abuse_search` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `value` varchar(255) DEFAULT '',
              PRIMARY KEY (`id`),
              UNIQUE KEY `value` (`value`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        );

    }

    public function down()
    {
        $this->dropTable("abuse_search");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
