<?php
/**
 * @var \app\models\MainSearchForm $searchForm
 */

use app\assets\PostAsset;
use app\assets\YTExternalAsset;

PostAsset::register($this);
YTExternalAsset::register($this);
$this->title = 'Search results for ' . $searchForm->q;
?>

<?= $this->render("//post/_posts", compact('dataProvider')); ?>