<?php

use app\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $filter string
 * @var $changeFields array
 * @var $modelClass string
 */
$queryParams = Yii::$app->request->getQueryParams();
unset($queryParams['filter']);
$urlWithParams = ArrayHelper::merge([Yii::$app->controller->getRoute()], $queryParams);
$filterFormUrl = Url::to($urlWithParams, true);

$fieldsHtml = [];
$model = new $modelClass;
foreach ($changeFields as $name => $values) {
    $fieldsHtml[] = '<div class="col-md-3">'
        . Html::dropDownList($name, '', ['-' => '-- ' . $model->getAttributeLabel($name) . ' --'] + $values, [
            'class' => 'form-control',
            'data-index-action' => 'change',
            'data-index-message' => 'Are you sure you want to change selected items?'
        ]) . '</div>';
}
?>
<div class='panel panel-default'>

    <div class='panel-body'>
        <div class='row'>
            <div class='col-md-6'>
                <form id="<?= Yii::$app->controller->action->id ?>-filter"
                      action="<?= $filterFormUrl ?>"
                      method="get">
                    <div class='input-group'>
                        <?=
                        Html::textInput('filter', $filter, [
                            'class' => 'form-control',
                            'placeholder' => 'Search for...'
                        ])
                        ?>

                        <?php foreach ($queryParams as $name => $value) { ?>
                            <?= Html::hiddenInput($name, $value, ['class' => 'form-control']) ?>
                        <?php } ?>

                        <span class='input-group-btn'>
                            <?=
                            Html::a('<i class="fa fa-times"></i> Reset', $filterFormUrl, [
                                'class' => 'btn btn-default',
                                'type' => 'reset'
                            ])
                            ?>
                            <?=
                            Html::button('<i class="fa fa-search"></i> Search', [
                                'class' => 'btn btn-default',
                                'type' => 'submit'])
                            ?>
                        </span>
                    </div>
                </form>
            </div>
            <div class='col-md-6 text-right'>
                <?= Html::a('<i class="fa fa-plus"></i> Add new', ['users-create'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>

        <hr>
        {items}
        <hr>

        <div class='row'>
            <?= join("\n", $fieldsHtml) ?>
            <div class='col-md-3 text-right'>
                <?=
                Html::button('<i class="fa fa-trash"></i> Delete', [
                    'class' => 'btn btn-danger delete-fields',
                    'name' => 'delete',
                    'value' => 1,
                    'data-index-action' => 'click',
                    'data-index-message' => 'Are you sure you want to delete selected items?',
                    'data-delete-by-one' => 1
                ])
                ?>

                <?=
                Html::button('<i class="fa fa-remove"></i> Purge', [
                    'class' => 'btn btn-danger purge-fields',
                    'data-index-message' => 'Are you sure you want to completely purge selected items?',
                    'data-url' => Url::to(['admin/users-purge'])
                ])
                ?>
            </div>
        </div>
    </div>

    <div class='panel-footer'>
        <div class='row'>
            <div class='col-md-6'>{summary}</div>
            <div class='col-md-6 text-right'>{pager}</div>
        </div>
    </div>
</div>