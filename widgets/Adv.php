<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;
use yii\base\Exception;

class Adv extends Widget
{
    public $enabled = true;
    public $id = 'z36';

    public function init()
    {
        if (isset(Yii::$app->params['widgets']['adv']['enabled'])) {
            $this->enabled = Yii::$app->params['widgets']['adv']['enabled'];
        }
        if (!isset($this->id)) {
            throw new Exception(self::className() . '::id must be set');
        }
        parent::init();
    }

    public function run()
    {
        if ($this->enabled) {
            return $this->render('adv', [
                'id' => $this->id,
            ]);
        }
    }
}