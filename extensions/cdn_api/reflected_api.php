<?php
 
// To install the Crypt/HMAC dependency:
// sudo apt-get install php-pear
// sudo pear install Crypt_HMAC
 
// To install the Curl dependency:
// sudo apt-get install php5-curl
  
require_once 'Crypt/HMAC.php';
 
function rnapi($verb, $resource, $param = '', $content = NULL)
{
  $keyId = 10821;
  $secretKey = 'SEojbi7L0l2hu5MxYGCXYrHsGEptXa6rASUOJSjCSvFHUYTdMN8BsXzdJDSwf41F';
  $ct = 'text/json';
  $env = 'https://ws.reflected.net:443';
  $requestDate = time();
  $content_json = '';
 
  if (substr($resource, 0, 1) !== '/')
  {
    $resource = '/' . $resource;
  }
 
  $url = $env . $resource;
  if ($param != '')
  {
    $url = $url . '?' . $param;
  }
 
  $ch = curl_init();
  if ($verb == 'POST')
  {
    curl_setopt($ch, CURLOPT_POST, true);
    $ct = 'multipart/form-data';
    if ($content != '')
    {
      $content_json = json_encode($content);
      curl_setopt($ch, CURLOPT_POSTFIELDS, array('content' => $content_json));
    }
  }
  elseif ($verb == 'PUT')
  {
    curl_setopt($ch, CURLOPT_PUT, true);
  }
 
  $content_sha1 = sha1($content_json);
  $str = "$requestDate\n$resource\n$ct\n$verb\n$content_sha1\n";
  $hasher = new \Crypt_HMAC($secretKey, 'sha1');
  $authString = 'RNA ' . $keyId . ':' . hexToBase64($hasher->hash($str));
 
  curl_setopt($ch, CURLOPT_URL, $url);
  $header_array = array(
    "Date: " . $requestDate,
    "RN-Authorization: " . $authString,
    "Content-Type: " . $ct,
    "Content-SHA1: $content_sha1",
  );
  curl_setopt($ch, CURLOPT_HTTPHEADER, $header_array);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
 
  // Uncomment the line below to return response to a variable.
  curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE);
 
  // Uncomment the line below to get HTTP return code
  $code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
 
  $output = curl_exec($ch);
  curl_close($ch);
  return (json_decode($output, true));
}
 
function hexToBase64($str)
{
  $raw = '';
  for ($i = 0; $i < strlen($str); $i += 2)
  {
    $raw .= chr(hexdec(substr($str, $i, 2)));
  }
  return base64_encode($raw);
}
 
?>
