$(function () {
    if (typeof activeNotificationTime !== 'undefined') {
        var titleHolder = $('title').text();
        var notificationHandlerId = activeNotificationTime;
        var showMoreNotificationTimeEnd = $('.show-more-notifications').data('time-end');
        var showMoreNotificationOffset = $('.show-more-notifications').data('offset');

        function updateNotificationTimeoutExpire() {
            return new Date().getTime() - 30000;
        }

        function getCurrentNotificationHandler() {
            var storageNotificationData = localStorage.getItem('storageNotification');
            if (storageNotificationData) {
                return JSON.parse(storageNotificationData);
            }
            return {};
        }

        function isCurrentNotificationHandler() {
            storageNotification = getCurrentNotificationHandler();
            return Object.keys(storageNotification).length > 0 && storageNotification.handlerId === notificationHandlerId;
        }

        function setCurrentNotificationHandler(storageNotification) {
            localStorage.setItem('storageNotification', JSON.stringify(storageNotification));
        }

        function updateStorageNotificationFields(storageNotification) {
            storageNotification.time = new Date().getTime();
            storageNotification.handlerId = notificationHandlerId;
            storageNotification.activeNotificationTime = activeNotificationTime;
            storageNotification.notificationCount = notificationCount;
            storageNotification.notificationListHtml = $('.list-notification').html();
            storageNotification.showMoreNotificationOffset = showMoreNotificationOffset;
            storageNotification.showMoreNotificationTimeEnd = showMoreNotificationTimeEnd;
            setCurrentNotificationHandler(storageNotification);
        }

        function extractStorageNotificationFields(storageNotification) {
            if (updateNotificationTimeoutExpire() < storageNotification.time) {
                //if (activeNotificationTime < storageNotification.activeNotificationTime) {
                activeNotificationTime = storageNotification.activeNotificationTime;
                notificationCount = storageNotification.notificationCount;
                $('.list-notification').html(storageNotification.notificationListHtml);
                showMoreNotificationOffset = storageNotification.showMoreNotificationOffset;
                showMoreNotificationTimeEnd = storageNotification.showMoreNotificationTimeEnd;
            }
            updateNotificationTitle();
            updateNotificationLabel();
        }

        function updateCurrentStorageHandler() {
            var storageNotification = getCurrentNotificationHandler();
            if (Object.keys(storageNotification).length > 0) {
                if (
                    storageNotification.handlerId === notificationHandlerId ||
                    storageNotification.time < updateNotificationTimeoutExpire()
                ) {
                    updateStorageNotificationFields(storageNotification);
                } else {
                    extractStorageNotificationFields(storageNotification);
                }
            } else {
                updateStorageNotificationFields(storageNotification);
            }
            //console.log(isCurrentNotificationHandler());
        }

        updateStorageNotificationFields(getCurrentNotificationHandler());
        setInterval(updateCurrentStorageHandler, 30000);

        function getLastNotificationUrl() {
            var $firstLi = $('.list-notification .bg-default').first();
            if ($firstLi.length > 0) {
                var $postBlock = $firstLi.find('.col-xs-3').last();
                if ($postBlock.length > 0) {
                    return $postBlock.find('a').attr('href');
                }
            }
        }

        function notifyInfoUser(message) {
            var title = 'Owned.com';

            var notificationClick = function () {
                if (typeof getLastNotificationUrl() !== 'undefined') {
                    window.open(getLastNotificationUrl(), '_blank');
                }
            };

            var options = {
                icon: 'static/i/logo.png',
                body: message
            };

            // Let's check if the browser supports notifications
            if (!("Notification" in window)) {
                console.log("This browser does not support desktop notification");
            }
            // Let's check whether notification permissions have already been granted
            else if (Notification.permission === "granted") {
                // If it's okay let's create a notification
                var notification = new Notification(title, options);
                notification.onclick = notificationClick;
            }

            // Otherwise, we need to ask the user for permission
            else if (Notification.permission !== 'denied') {
                Notification.requestPermission(function (permission) {
                    // If the user accepts, let's create a notification
                    if (permission === "granted") {
                        var notification = new Notification(title, options);
                        notification.onclick = notificationClick;
                    }
                });
            }

            // At last, if the user has denied notifications, and you
            // want to be respectful there is no need to bother them any more.
        }

        function updateNotificationTitle() {
            if (notificationCount > 0) {
                $('title').text('(' + notificationCount + ') ' + titleHolder);
            } else {
                $('title').text(titleHolder);
            }
        }

        updateNotificationTitle();

        function updateNotificationLabel() {
            if (notificationCount > 0) {
                $('.header-notification > .dropdown-toggle .label').remove();
                $('.header-notification > .dropdown-toggle span').addClass('text-danger');
                $('.header-notification > .dropdown-toggle').append('<span class="label label-danger">' + notificationCount + '</span>');
            } else {
                $('.header-notification > .dropdown-toggle .label').remove();
                $('.header-notification > .dropdown-toggle span').removeClass('text-danger');
                $('.list-notification .bg-default').removeClass('bg-default');
            }
        }

        function markAllNotifications(markNotificationUrl) {
            if (typeof markNotificationUrl !== 'undefined') {
                var storageNotification = getCurrentNotificationHandler();
                activeNotificationTime = activeNotificationTime < storageNotification.activeNotificationTime ? storageNotification.activeNotificationTime : activeNotificationTime;
                $.ajax({
                    url: markNotificationUrl,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {lastTime: activeNotificationTime},
                    success: function (response) {
                        if (response.status === 'success') {
                            notificationCount = 0;
                            updateNotificationTitle();
                            updateNotificationLabel();
                            $('.list-notification').html('');
                            updateStorageNotificationFields(storageNotification);
                            $('.show-more-notifications').closest('li').remove();
                        }
                    }
                });
            }
        }

        if (typeof updateNotificationUrl !== 'undefined') {
            function getNotificationsUpdate() {
                if (isCurrentNotificationHandler()) {
                    var queryParam = '?lastTime=' + activeNotificationTime;
                    $.ajax({
                        url: updateNotificationUrl + queryParam,
                        type: 'GET',
                        dataType: 'JSON',
                        success: function (response) {
                            if (response.status === 'success') {
                                if (response.activeNotificationCount > 0) {
                                    activeNotificationTime = response.activeNotificationTime;
                                    notificationCount += response.activeNotificationCount;
                                    var notificationText = notificationCount > 1 ? 'notifications' : 'notification';
                                    notifyInfoUser('You have ' + notificationCount + ' ' + notificationText, getLastNotificationUrl());
                                    $('.list-notification').prepend($(response.notificationList).html());
                                    updateStorageNotificationFields(getCurrentNotificationHandler());
                                    updateNotificationTitle();
                                    updateNotificationLabel();
                                }
                            }
                        }
                    });
                }
            }

            var updateNotificationInterval = setInterval(getNotificationsUpdate, 60000);
        }

        $('.mark-notifications').click(function (e) {
            e.preventDefault();
            markAllNotifications($(this).attr('href'));
        });

        $('.show-more-notifications').click(function (e) {
            e.preventDefault();
            var $btn = $(this);
            var showMoreNotificationUrl = $(this).attr('href');
            $btn.addClass('disabled');
            $btn.find('.fa').removeAttr('hidden');
            if (typeof showMoreNotificationUrl !== 'undefined') {
                var storageNotification = getCurrentNotificationHandler();
                $.ajax({
                    url: showMoreNotificationUrl,
                    type: 'GET',
                    dataType: 'JSON',
                    data: {
                        time_end: storageNotification.showMoreNotificationTimeEnd,
                        offset: storageNotification.showMoreNotificationOffset
                    },
                    success: function (response) {
                        if (response.status === 'success' && response.activeNotificationCount > 0) {
                            $('.list-notification').append($(response.notificationList).html());
                            storageNotification.showMoreNotificationOffset += response.activeNotificationCount;
                            $btn.removeClass('disabled');
                            $btn.find('.fa').attr('hidden', 'hidden');
                            updateStorageNotificationFields(storageNotification);
                        } else {
                            $btn.closest('li').remove();
                        }
                    }
                });
            }
        });

        $('.header-notification > .dropdown-toggle').parent().on('hide.bs.dropdown', function (e) {
            if ($(this).find('.dropdown-menu').is(':hover')) {
                e.preventDefault();
            }
        });
    }
});