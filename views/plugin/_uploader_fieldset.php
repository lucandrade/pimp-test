<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
?>
<legend style="font-size: 13px !important; width: 98%">
    <span style="float:left;">Our Image Uploader: (Supports Multiple Images at once).</span>
    <span style="float: right" > <a href="<?=  Url::base(true) ?>" target="_blank">Powered by PimpAndHost.com</a></span>
</legend>
<div id="content2">
    <br />
    <div>
        <div style="float: left;width: 35%;">
            <div id="pah-upload-container">
                <a id="upload_pickfiles" href="#" title="No runtime found." class="btn btn-primary" style="display: inline-block; width: 109px; height: 26px; position: relative; z-index: 1;
                background: no-repeat url(&quot;<?= Url::base(true) . "/static/i/"?>browse_sprite_plugin.png&quot;);"></a>
                <?= Html::fileInput('files', null, [
                    'class'=>'file-input',
                    'accept' => 'image/*',
                    'multiple' => 'multiple',
                    'style' => 'display:none;'])
                ?>
            </div>
        </div>
        <div class="how_to">Need Help in using the plugin?
            <a href="<?=  Url::to(["plugin/index"], true) ?>" target="_blank">Click Here!</a>
            <br/>
            <a style="float:right; margin: -10px 0 10px 0;" href="<?= Url::to(["plugin/index"],true) ?>" target="_blank">Get this plugin</a>
        </div>
    </div>
    <div style="clear:both"></div>
    <div id="msg" style="display:none;font-weight: bold">
        Please Make sure to insert the images into your post by clicking on them in any order you prefer or simply click the "insert all images" button.
        <br/><br/>
        <button id="allimgs" onclick="return insertAll();">Insert all images</button>
        <br /><br />
    </div>
</div>
</div>
<div style="clear:both"></div>
<div class="previews"></div>
<div id="img_move_to_album"></div>