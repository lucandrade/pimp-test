<?php

namespace app\assets;


use yii\web\AssetBundle;

class FullUploaderAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/full-uploader.css'
    ];

    public $js = [
        'js/jquery.fullPahUploader.js'
    ];

    public $depends = [
        'app\assets\UploaderAsset',
        'app\assets\AlbumAsset'

    ];
    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}