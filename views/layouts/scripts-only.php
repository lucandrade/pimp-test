<?php
use app\widgets\MinifyWidget;
?>
<?php $this->beginPage() ?>
<?php $this->beginBody() ?>
<?php MinifyWidget::begin(); ?>

<?= $content ?>

<?php MinifyWidget::end() ?>
<?php $this->endBody() ?>
<?php $this->endPage() ?>
