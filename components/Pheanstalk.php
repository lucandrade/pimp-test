<?php

namespace app\components;

use Yii;
use app\models\JobLog;
use app\models\JobQueue;
use Pheanstalk\PheanstalkInterface;
use yii\base\Component;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class Pheanstalk extends Component
{

    /**
     * @var \Pheanstalk\Pheanstalk
     */
    private $_client;
    public $server = '127.0.0.1';
    public $port = PheanstalkInterface::DEFAULT_PORT;
    public $defaultTube = 'testtube';
    public $defaultPriority = 10;

    public function init()
    {
        $this->_client = new \Pheanstalk\Pheanstalk($this->server, $this->port);
    }

    public function getClient()
    {
        return $this->_client;
    }

    public function addJob($command, $period = 0, $priority = null, $tube = null)
    {

        if (JobQueue::addedInPeriod($command, $period)) {
            return 0;
        }

        $priority = isset($priority) ? $priority : $this->defaultPriority;
        $tube = isset($tube) ? $tube : $this->defaultTube;
        $jobData = Json::encode(['cmd' => $command, 'period' => $period]);

        $res = $this->_client->useTube($tube)->put($jobData, $priority);

        $this->log($res);

        JobQueue::addJob($command);

        return $res;
    }

    public function stats($key = null)
    {
        $stats = $this->_client->stats();

        return $key ? ArrayHelper::getValue($stats, $key) : $stats;
    }

    protected function log($message)
    {
        $logFile = Yii::getAlias("@runtime") . "/logs/pheanstalk.log";

        return file_put_contents(
            $logFile,
            "\n" . date('Y-m-d H:i:s') . str_pad(' ', 40, '=') . "\n" . $message . "\n",
            FILE_APPEND
        );
    }
}