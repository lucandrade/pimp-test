<?php

namespace app\components;

use Yii;
use yii\base\Component;

class RedirectManager extends Component
{

    /**
     * @var array
     *
     *  <IfModule mod_rewrite.c>
     *  RewriteCond %{REQUEST_URI} ^/tag/(.*)$
     *  RewriteRule /(.*)$ /s/tag_$1 [R=301,L]
     *
     *  RewriteCond %{REQUEST_URI} ^/(image|video|gif)/$
     *  RewriteRule /(.*)$ /s/type_%1 [R=301,L]
     *
     *  RewriteCond %{REQUEST_URI} ^/(meme|user)/$
     *  RewriteRule /(.*)$ / [R=301,L]
     *
     *  RewriteCond %{REQUEST_URI} ^/post/create/$
     *  RewriteRule /(.*)$ /post/update [R=301,L]
     *
     *  RewriteCond %{REQUEST_URI} ^/user/(\d+)$
     *  RewriteRule /(.*)$ /u/$1 [R=301,L]
     *
     *  RewriteCond %{REQUEST_URI} ^/author/(\d+)/$
     *  RewriteRule /(.*)/$ /s/user_$1 [R=301,L]
     *
     *   RewriteCond %{REQUEST_URI} ^/search/(.+)/$
     *   RewriteRule /([^/]+)/(.+)$ /s/$1 [R=301,L]
     *   </IfModule>
     */

    public $rules = [
        //can be fucked up by other rules
//        '^/site/botBatchReportAbuse.*$' => 'last',
        
        //@todo: remove?
        '^/rss.xml$' => '/',
        '^/(meme|user)/$' => '/',
        '^/post/create/$' => '/post/update',
        '^/user/(\d+)$' => '/u/$1',
        '^/author/(\d+)/$' => '/s/user_$1',
        '^/search/([^/]+)/(.+)?$' => '/s/$1',

        //pah
        '.*/album/search.*/q/([\w\+\-\%]+)/.*page/(\d+)/?.*$' =>'/s/$1?page=$2',
        '.*/album/search.*/q/([\w\+\-\%]+)/?.*$' => '/s/$1',
        '.*/album/search.*q=([\w\+\-\%]+).*$' =>'/s/$1',
        '.*/album/search/([\w\+\-\%]+).*$' =>'/s/$1',

        '.*/album/dashboard/(\w+)/.*page.?(\d+).*$' => '/album/username/$1?page=$2',
        '.*/album/dashboard/(\w+).*$' => '/album/username/$1',

        '.*/album/.*/id/(\d+)/.*page/(\d+)/?.*$' => '/album/$1?page=$2',
        '.*/album/.*/id/(\d+).*$' => '/album/$1',
        '.*/album/.*show/(\d+).*$' => '/album/$1',


        '.*/album/(\d+)\-.+\.html.*page=(\d+).*$' => '/album/$1?page=$2',
        '.*/album/(\d+)\-.+\.html.*$' => '/album/$1',

        '.*/image/show/(\d+).*$' => '/image/$1',
        '.*/image/show.*/id/(\d+).*$' => '/image/$1',
        '.*/image/(\d+)(.+)\.html.*$' => '/image/$1',

        '.*/site/profile/username/(\w+)/.*page/(\d+).*$' => '/album/username/$1?page=$2',
        '.*/site/profile/username/(\w+)/.+$' => '/album/username/$1',
        '.*/site/profile/username/(\w+).*$' => '/album/username/$1',

        '.*/site/editProfile.*$' => 'user/settings',
        '.*/site/dashboard.*$' => '/album/my',
        '.*/user/register$' => '/signup',
        '.*/site/upload.*$' => '/',
        '.*/site/jsUploadPlugin.*$' => '/plugin/uploader',
        '.*/album/search.*$' => '/',
        '.*/upload/post.*$' => '/',


    ];

    public $multipleRules = [
        '^/tag/([^/]+)(/.*)?$' => [
            'pattern' => ['#^/tag/([^/]+)(/.*)?$#', '#\+#'],
            'replacement' => ['/s/tag_$1', '_']
        ],
    ];

    public function init()
    {
        if (Yii::$app->request->isConsoleRequest) {
            return false;
        }

        if ($url = Yii::$app->request->url) {
            foreach ($this->rules as $rule => $to) {
                if (preg_match('#' . $rule . '#', $url, $matches)) {
                		if($to == 'last') return true;
                    Yii::$app->response->redirect(preg_replace('~' . $rule . '~si', $to, $url), 301);
                    Yii::$app->end();
                }
            }

            foreach ($this->multipleRules as $rule => $replaces) {
                if (preg_match('#' . $rule . '#', $url)) {
                    Yii::$app->response->redirect(preg_replace($replaces['pattern'], $replaces['replacement'], $url), 301);
                    Yii::$app->end();
                }
            }
        }

        return true;
    }
}