<?php

namespace app\assets;

use yii\web\AssetBundle;

class ResponsiveTable extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/plugins/responsive-tables.css',
    ];
    public $js = [
        'js/plugins/responsive-tables.js',
    ];
    public $depends = [
        'yii\web\YiiAsset'
    ];
    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}