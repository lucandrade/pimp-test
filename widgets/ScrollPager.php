<?php

namespace app\widgets;

use app\assets\InfiniteAjaxScrollAsset;

class ScrollPager extends \kop\y2sp\ScrollPager
{
    protected function registerAssets()
    {
        InfiniteAjaxScrollAsset::register($this->view);
    }
}