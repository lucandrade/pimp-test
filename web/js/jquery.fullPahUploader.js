(function($){

    $.fn.fullPahUploader = function(method){
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method "' +  method + '" is not found in jquery.fullPahUploader' );
        }
    };

    var $container, settings, $showUrlBtn, $selectionBar, $uploadContainer;

    var methods = {
        init:function(options){

            settings = {};
            settings = $.extend(true, {} ,defaultSettigns, options);
            $(this).data('settings', settings);

            $container = $(this);

            removeSettings.url = $container.data('removeurl');
            $showUrlBtn = $container.find('.show-url-upload');
            $showUrlBtn.removeClass('disabled-link');

            $uploadContainer = $container.find('.uploader-container');
            var pahUploaderOptions = $.extend(true,{}, defaultPahUploaderOptions, settings.uploaderOptions);
            $uploadContainer.pahUploader(pahUploaderOptions);
            var $fileinput = $uploadContainer.pahUploader('getPluginUploader');
            $fileinput.data('fu-settings', settings);

            $showUrlBtn.on('click', function(e){
                e.preventDefault();
                if($(this).hasClass('disabled-link')){
                    return false;
                }
                var expanded = $(this).data('expanded');
                if(!expanded)
                {
                    $(this).next().slideDown();
                    $(this).data('expanded', true);
                    $(this).data('hidedtext', $(this).text());
                    $(this).text('Cancel');
                }else
                {
                    $(this).next().slideUp();
                    $(this).data('expanded', false);
                    $(this).text($(this).data('hidedtext'));
                }
            });

            $(this)
                .off('click', '.url-upload-btn')
                .on('click', '.url-upload-btn', function(e){
                    var $inputGroup = $(this).parents('.url-input-group'),
                        urls = $inputGroup.find('.image-urls').val(),
                        $button = $(this);

                    $button.attr('disabled', 'disabled');
                    $button.text('Loading');

                    urls = urls.replace(/\n+/g,'\n').replace(/(^\n|\n$)/g,'');
                    $button.parents('.upload-control-container').trigger('changefpu');
                    $inputGroup.find('.image-urls').val('');

                    if(!urls.match(/\w/)){
                        $inputGroup.addClass('has-error');
                        $inputGroup.find('.help-block').text("Field cannot be empty");
                        return false;
                    }
                    else{
                        if($inputGroup.hasClass('has-error')){
                            $inputGroup.removeClass('has-error').find('.help-block').text("");
                        }
                    }

                    if(settings.loadAlbumPage ){
                        loadAlbumPage(function(albumId){
                            $('#selection-bar').show();
                            processUrls(urls, $button, albumId);
                        });
                    }else {
                        processUrls(urls, $button, settings.albumId)
                    }
                });
        },

        reInit:function(options){
            var dataSettings = $(this).data('settings');
            settings = $.extend(true, {}, dataSettings, options || {});
            $container = $(this);
            $uploadContainer = $container.find('.uploader-container');
            var pahUploaderOptions = $.extend({}, defaultPahUploaderOptions, settings.uploaderOptions);
            $uploadContainer.pahUploader('reInitElems', pahUploaderOptions);
        }

    };

    this.isInited = false;
    var defaultSettigns = {
        containerId:"uploader-modal",
        enableSelectionBar: false,
        enableImageSelector: true,
        loadAlbumPage : false,
        maxFileSize:5000000,
        initAlbumPage:true,
    };

    var eventsNames = {
        hidden:"hidden.up",
    };

    var removeSettings = {
        successRemoved:function(ids, $wrappers){

            $wrappers.parents('.preview-container').fadeOut('slow', function(){
                $wrappers.imageSelector('unselectAll');
                $(this).remove();
                if($container.find('.previews .image-wrapper').length == 0){
                    $selectionBar.hide();
                }
            });
        },
        getIds: function ($selectedWrappers){
            var ids = [];
            $selectedWrappers.each(function(){
                ids.push($(this).data('key'));
            });

            return ids;
        }
    };


    var addFile = function(data, albumId){
        var $previewsContainer = $(settings.uploaderOptions.previewsContainer);

        if(!data.formData){
            data.formData = {};
        }

        if(albumId && !data.formData.albumId){
            data.formData.albumId = albumId;
        }

        if (data.files) {
            $.each(data.files, function () {

                var file = this;

                var matches = file.type.match(/^image\/(gif|jpe?g|png)$/i);
                if(!matches){
                    file.error = {
                        type:"common-error",
                        title:"Error: file '" + file.name + "' is not image",
                        description:"Only images in formats png/jpeg/gif are allowed"
                    }

                }

                if(file.size > settings.maxFileSize){
                    file.error = {
                        title:"Error: file '" + file.name + "' is too big",
                        description:"Size "+ parseInt(file.size/1000) + " kB (Maximum allowed:" + parseInt(settings.maxFileSize/1000) + "kB)"
                    }
                }

                file.fileId = Math.random().toString(36).replace(/[^a-z]+/g, '');
                data.formData.fileId = file.fileId;


                var $previewContainer = createPreviewContainer(file);
                $previewsContainer.prepend($previewContainer);

                if (!$previewContainer.data('stop-submit'))
                {
                    data.submit().done(function(submitData){
                        if (submitData.files)
                        {
                            var file = submitData.files[0];
                            var $previewContainer = $('#preview_' + file.fileId);
                            completePreviewContainer(submitData.files[0], $previewContainer);
                        }
                        else{
                            alert("Coudn't upload picture");
                        }
                    });
                }
            });
        }
    };

    var prepareForUploading = function(e, data){
        e.stopPropagation();
        var locSettings = $(this).data('fu-settings');
        if(locSettings.loadAlbumPage){
            var $this = $(this);
            loadAlbumPage(function(albumId){
                settings.albumId = albumId;
                $('#selection-bar').show();
                $this.fileupload('add', {files:data.files});
            });

            $('#' + settings.containerId).find('.upload-control-container').trigger('changefpu');
            return false;
        }
        else{
            $('#' + settings.containerId).find('.upload-control-container').trigger('changefpu');
        }
    };


    var defaultPahUploaderOptions = {

        fileuploadOptions:{


            change: prepareForUploading,

            drop: prepareForUploading,

            add:function(e, data){
                addFile(data, settings.albumId);
            },

            progress:function(e,data){
                if(data.files && data.files[0] && data.files[0].fileId){
                    var $previewContainer = $('#preview_' + data.files[0].fileId);
                    $previewContainer.updateProgress(data.loaded, data.total, data.bitrate);
                }else{
                    throw "Wrong data response for progress:"+ JSON.stringify(data);
                }
            }
        }
    };

    var loadAlbumPage = function(callback){
        $.ajax({
            url:settings.albumCreateUrl,
            type:"POST",
            dataType:"json",
            success: function(res){
                history.pushState(null, null, res.url);

                var commentSdkIsLoaded = $('#comments-sdk').length > 0;
                $('#main-container').html(res.html);

                if(commentSdkIsLoaded){
                    CKSDKInit();
                }

                if(settings.initAlbumPage){
                    pahIniter.albumPage.ready();
                }

                pahIniter.imageList.ready();
                pahIniter.widgets.xeditable();
                shareModalWindow.reInit();

                $('body').on('load', '#comments-sdk', function(){
                   alert('SDK loaded!');
                });


                callback(res.albumId);

            }
        })
    };

    $.fn.updateProgress = function(loaded, total, bitrate){
        var progress = parseInt(loaded / total * 100, 10);
        var $progress = $(this).find(".progress");

        var bitrate = $uploadContainer.pahUploader('formatBitrate', bitrate);

        $progress.find('.progress-percent').text(progress + "%");
        $progress.find('.progress-loaded').text(parseInt(loaded / 1000) + "kB");
        $progress.find('.progress-bitrate').text(bitrate.value);
        $progress.find('.progress-bitrate-units').text(bitrate.units);

        $progress.show();
        $progress.find(".bar").css(
            'width',
            progress + '%'
        );
        return $(this);
    };

    var completePreviewContainer = function(file, $previewContainer)
    {
        if(!file.error && !file.image.errors){

            var $wrapper = $previewContainer.find('.im-wr')
                .data('page-url', file.image.pageUrl)
                .attr('href', file.image.pageUrl)
                .data('src', file.image.fileUrl)
                .data('edit-url', file.image.editUrl)
                .data('filename', file.original_name)
                .data('size', file.size)
                .addClass('image-wrapper');


            $previewContainer.data('key', file.image.id);

            var hoverBarContent =  '<i title="Download image (disabled while processing)" class="fa fa-download icon-disabled"></i>'+
                '<i title="Remove image" class="fa fa-trash-o"></i>' +
                '<i title="Edit image (disabled while processing)" class="fa fa-edit icon-disabled"></i>' +
                '<i title="Share image" class="fa fa-share-alt"></i>' +
                '<i title="See full image" class="fa fa-picture-o"></i>';

            var hoverInfoContent  = "<div class='content-header'>" + file.original_name + "</div>"+
                "<div class='content-info'>Size "+ parseInt(file.size/1000) + " kB</div>";

            if(settings.enableImageSelector){

                $wrapper.imageSelector({
                    events:{
                        onSelected:eventsNames.onSelected,
                        onUnselected:eventsNames.onUnselected
                    },

                    enableSelection:true,

                    hoverBarContent: hoverBarContent,

                    hoverInfoContent:function($wrapper){
                        if($previewContainer.find('.content-title').length == 0)
                        {
                            return hoverInfoContent;
                        }
                    },
                    removeSettings:removeSettings
                });
            }else{
                var $hoverDonor = $('<div/>').addClass("hover-donor").hide();
                $hoverDonor
                    .append("<div class='hover-bar-content'>" + hoverBarContent + "</div>")
                    .append("<div class='hover-info-content'>" + hoverInfoContent + "</div>");
                $previewContainer.append($hoverDonor);

            }

            pahIniter.widgets.albumMetrics.addTotalCount(1);
            pahIniter.widgets.albumMetrics.addTotalSize(file.size);

            $('#selection-bar').selectionBar('updateIcon');
            $previewContainer.find('.image-wrapper').trigger("onAppended");

        }else{
            var error;
            if(file.error){
                error = file.error
            }else {
                if(file.image && file.image.errors){
                    error = {
                        title:"Errors while uploading",
                        description:JSON.stringify(file.image.errors).replace(/[\{\}\[\]]/g,"")
                    };
                }else{
                    error = {
                        title:"Unknown error",
                        description:"Unknown error"
                    };
                }
            }
            $uploadContainer.pahUploader('addError', $previewContainer, error);
        }

        $previewContainer.find('.absolute-progress').fadeOut("slow");
    };



    var createPreviewContainer = function(file){
        var $previewContainer = $("<div></div>").addClass("image-block col-md-4 col-xs-4 col-xxs-6 col-lg-3");
        var $imageWrapper;

        if(file.error && Object.keys(file.error).length > 0)
        {
            $imageWrapper = $("<div></div>").addClass('image-wrapper-error center-cropped');
            if(file.error.type && file.error.type == 'common-error'){
                $imageWrapper.addClass('common-error')
            }
        }
        else
        {
            $imageWrapper = $("<a></a>").addClass('im-wr center-cropped');
            $previewContainer.attr('id','preview_' + file.fileId);
            var $progress = $(
                '<div class="absolute-progress">' +
                '<div class="progress">' +
                '<div class="progress-text">' +
                '<span class="progress-percent">0%</span> - ' +
                '<span class="progress-loaded">0kb</span>' +
                ' (<span class="progress-bitrate">0</span>' +
                '<span class="progress-bitrate-units">kb</span>)'+
                '</div>' +
                '<div class="bar"></div>' +
                '   </div>' +
                '</div>');


            $previewContainer.append($progress);
        }

        $previewContainer.append("<div class='ar'><div class='grid-content'></div></div>");

        $previewContainer.find(".grid-content").append($imageWrapper);

        if(!file.error || !file.error.type || file.error.type != 'common-error'){

            if(file instanceof File)
            {
                var reader = new FileReader();
                reader.onload = function (event) {
                    $imageWrapper.css('background-image', "url("+ event.target.result +")");
                };
                reader.readAsDataURL(file);
            }else{
                $imageWrapper.css('background-image', "url("+ file.sourceUrl +")");
            }
        }

        if(file.error){
            $uploadContainer.pahUploader('addError', $previewContainer, file.error);
        }

        return $previewContainer;
    };


    var preparePreviewController = function(url){
        var file = {};
        file.sourceUrl = url;
        file.fileId = Math.random().toString(36).replace(/[^a-z]+/g, '');
        var $previewContainer = createPreviewContainer(file),
            $previewsContainer = $(settings.uploaderOptions.previewsContainer);

        $previewsContainer.prepend($previewContainer);
        $previewContainer.updateProgress(getRandom(3,11), 100, 1000/getRandom(1,8));

        return file.fileId;
    };

    var processUrls = function(urls, $button, albumId){

        $button.removeAttr('disabled');
        $button.text('Submit');
        urls = urls.split("\n");

        $.each(urls, function(){
            if(this == ''){
                return;
            }

            var url = this.toString();
            var data = {url:url};
            data.fileId = preparePreviewController(url);


            if(albumId){
                data.albumId = albumId
            }
            $.ajax({
                url:$button.data('url'),
                data:data,
                dataType:"json",
                type:"POST",
                success:function(res){
                    if(res.status && res.status == 'ok' && res.file)
                    {
                        var file = res.file;
                        var $previewContainer = $('#preview_' + file.fileId),
                            times = getRandom(1,8),
                            i = 0,
                            step = Math.floor((100)/times);

                        if(!file.error){

                            var timerId = setInterval(function(){
                                i++;
                                var percent = i*step;
                                var loaded = file.size * percent / 100;
                                if(loaded > file.size)
                                {
                                    $previewContainer.updateProgress(file.size, file.size, file.size/getRandom(1,8));
                                    completePreviewContainer(res.file, $previewContainer);
                                    clearInterval(timerId);
                                }else{
                                    $previewContainer.updateProgress(loaded, file.size, file.size/getRandom(1,8));
                                }
                            }, getRandom(100, 300));
                        }else{
                            completePreviewContainer(file, $previewContainer);
                        }

                    }
                    else{
                        if(res.status && res.status == 'error'){
                            alert(res.messages);
                        }else{
                            alert("Coudn't upload file for some reason. Please try again later");
                        }

                    }
                },

                error:function(jqXHR, textStatus, errorThrown){
                    console.log(jqXHR, textStatus, errorThrown);
                    var file = {
                        error: {
                            type:"common-error",
                            title:textStatus,
                            description:errorThrown
                        }
                    };

                    addUploadedFileToPreview(file);
                }
            });
        });

        $button.parents('.url-input-group').find('.image-urls').val('');
    };

    var addUploadedFileToPreview = function(file)
    {
        file.fileId = Math.random().toString(36).replace(/[^a-z]+/g, '');
        var $previewContainer = createPreviewContainer(file),
            $previewsContainer = $(settings.uploaderOptions.previewsContainer);

        $previewsContainer.prepend($previewContainer);

        var times = getRandom(1,8),
            step = Math.floor((100)/times);

        for(var i = 0; i <= times;i++){
            var percent = i*step;
            var loaded = file.size * percent / 100;
            $previewContainer.delay(getRandom(100, 800)).updateProgress(loaded, file.size, file.size/getRandom(1,8));
        }

        completePreviewContainer(file, $previewContainer);
    };

    var getRandom = function(min, max){
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };

    this.settings = $.extend(true, {}, settings);
})(jQuery);