<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Url;
$this->beginContent('@app/views/layouts/clean.php');
?>
	<?= $this->render('includes/_header') ?>

	<div class="container">
		<div id='main-container' class="row">
			<?= $content ?>
		</div>

		<?php
			$route = \Yii::$app->controller->getRoute();
			$fullUploaderOptions = [
				'loadAlbumPage' => true,
				'initAlbumPage' => $route !== 'album/item',
			];

			if($route == 'site/index')
			{
				$fullUploaderOptions['uploaderOptions'] = [
					'js' => [
						'fileuploadOptions' => [
							'dropZone' => null
						]
					]
				];
			}



			echo \app\widgets\UploaderPopup::widget([
				'fullUploaderOptions' => $fullUploaderOptions
			]);
		?>

		<input type="file" id="main-page-file-input" style="display: none" multiple
			   data-url="<?= Url::to(['image/upload-file']) ?>" accept="image/*">

		<?= $this->render('includes/_footer')?>
	</div>
<?php $this->endContent(); ?>
