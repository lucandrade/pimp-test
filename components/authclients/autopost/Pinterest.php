<?php

namespace app\components\authclients\autopost;
use app\models\Post;
use yii\authclient\OAuth2;
use yii\base\Exception;
use app\components\behaviors\ConsoleAuthClientBehavior;

/**
 */
class Pinterest extends OAuth2
{
    /**
     * @inheritdoc
     */
    public $authUrl = 'https://api.pinterest.com/oauth/';
    /**
     * @inheritdoc
     */
    public $tokenUrl = 'https://api.pinterest.com/v1/oauth/token';
    /**
     * @inheritdoc
     */
    public $apiBaseUrl = 'https://api.pinterest.com/v1';
    /**
     * @inheritdoc
     */
    public $scope = 'read_public,write_public';

    public  $tokenFileDir = "@runtime/pinterest";


    private $_autoPostResult;

    /**
     * @var string name of the board pins will be added to
     */
    public $boardName;

    public function init()
    {
        if(empty($this->boardName)){
            throw new Exception("bordName for pinterest shoud be set");
        }
        parent::init();
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = ConsoleAuthClientBehavior::className();
        return $behaviors;
    }

    public function fetchPageAccessToken()
    {
        $res = $this->api($this->pageId, 'GET', [
            'fields' => 'access_token',
        ]);
        $token = $this->createToken(['params'=>$res]);
        $this->setAccessToken($token);
        return $res;
    }

    public function autoPost(Post $post)
    {
        $token = $this->restoreAccessToken();
        $this->setAccessToken($token);
        $postData = $post->getSocialPostingData();

        $user = $this->api('me/','GET', ['fields'=>'username']);
        if(empty($user['data']['username'])){
            throw new Exception("Can't retrieve user username from pinterest api");
        }
        $this->_autoPostResult = $this->api('pins/', 'POST', [
            'board'=> $user['data']['username'] . "/" . $this->boardName,
            'note' => $postData['title'],
            'link' => $postData['url'],
            'image_url' => $postData['media_url']
        ]);

        return !empty($this->_autoPostResult['data']['id']);
    }


    public function getAutoPostResult()
    {
        return $this->_autoPostResult;
    }

    protected function restoreAccessToken()
    {
        $tokenArr = $this->getTokenFromFile();
        if(!$tokenArr){
            throw new Exception("Can't restore token from file for pinterest");
        }
        $token = $this->createToken(['params'=>$tokenArr]);
        if ($token->getIsExpired()) {
            $token = $this->refreshAccessToken($token);
        }

        return $token;
    }

    protected function apiInternal($accessToken, $url, $method, array $params, array $headers)
    {
        if(strtoupper($method) == 'GET' ){
            $url = $this->composeUrl($url, $params);
        }
        $this->populateCurlRequestData($url, $method, $params, $headers);
        return parent::apiInternal($accessToken, $url,$method, $params, $headers);
    }

}