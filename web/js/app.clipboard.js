jQuery(document).ready(function () {
    var clipboardCodes = new Clipboard('[data-clipboard-target="#site-codes"]');
    var clipboardLink = new Clipboard('[data-clipboard-target="#album-link"]');
    var inputLink = jQuery('#album-link');
    var inputCodes = jQuery('#site-codes');
    var timerCodes;
    var timerLink;
    var button;

    clipboardLink.on('success', function(e) {
        inputLink.addClass('highlighted');
        button = jQuery(e.trigger);
        button.attr('data-original-title', 'Copied');
        button.tooltip('show');
        if (timerCodes) {
            clearTimeout(timerCodes);
        }
        timerCodes = setTimeout(function () {
            button.attr('data-original-title', '');
            button.tooltip('hide');
            inputLink.removeClass('highlighted');
        }, 1000);
        e.clearSelection();
    });

    clipboardCodes.on('success', function(e) {
        inputCodes.scrollTop(0).addClass('highlighted');
        button = jQuery(e.trigger);
        button.attr('data-original-title', 'Copied');
        button.tooltip('show');
        if (timerLink) {
            clearTimeout(timerLink);
        }
        timerLink = setTimeout(function () {
            inputCodes.removeClass('highlighted')
            button.attr('data-original-title', '');
            button.tooltip('hide');
        }, 1000);
        e.clearSelection();
    });
    clipboardCodes.on('error', function(e) {
        //
    });
});
