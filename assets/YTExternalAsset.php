<?php

namespace app\assets;

use yii\web\AssetBundle;

class YTExternalAsset extends AssetBundle
{
    public $css = [
    ];
    public $js = [
        'https://www.youtube.com/iframe_api',
    ];
    public $depends = [
    ];
	public $jsOptions = [
	];
	public $cssOptions=[
	];
}