<?php
namespace app\controllers\admin;

class RestoreAction extends PostGridAction
{
    public $appliedActionName = 'restored';

    protected function processModel($model)
    {
        return $model->restore();
    }
}