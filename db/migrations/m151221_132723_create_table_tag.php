<?php

use yii\db\Schema;
use yii\db\Migration;

class m151221_132723_create_table_tag extends Migration
{
    public function up()
    {
        $this->createTable('{{%tag}}',[
            'id'   => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING,
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%tag}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
