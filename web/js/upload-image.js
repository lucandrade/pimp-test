$(document).ready(function(){

    var $uploadContainer = $('#upload-container');

    var $createAlbumForm =  $('#create-album-form');
    var $createAlbumModal = $('#create-album-modal');

    var $albumInput = $('#input-album-id');

    if($albumInput.length > 0){
        $albumInput.on('change', function(){
            var $this = $(this);
            if($this.val() == '0'){
                $createAlbumModal.modal('show');
            }
        });

        $albumInput.on('focus', function(){
            $(this).data('old-value', $(this).val());
        })
    }

    $createAlbumModal.on('hide.bs.modal', function(){
        var oldValue = $albumInput.data('old-value');
        $albumInput.val(oldValue);
    });

    $('#btn-create-album').on('click', function(){
        $createAlbumForm.data('yiiActiveForm').submitting = true;
        $createAlbumForm.yiiActiveForm('validate');
        $(this).attr('disabled','disabled');
        return false;
    });

    $createAlbumForm.on('submit', function(){
       return false;
    });

    $createAlbumForm.on('afterValidate', function(event, messages, errorAttributes){

        $createAlbumForm.data('yiiActiveForm').submitting = false;
        if(errorAttributes.length == 0){
           $.ajax({
               url:$createAlbumForm.attr('action'),
               data:$createAlbumForm.serialize(),
               type:"POST",
               dataType:"json",
               success:function(data){
                   if(data.status && data.status == 'ok')
                   {
                       $albumInput.append("<option value='"+data.id+"'>"+data.title+"</option>");
                       $albumInput.val(data.id);
                       $albumInput.data('old-value', data.id);
                       $createAlbumForm.yiiActiveForm('resetForm');
                       $createAlbumModal.modal('hide');
                       $createAlbumForm.find('input').val('');

                   }else{
                       alert("Something was goind wrong while album creating");
                   }
               },
               complete:function(){
                   $('#btn-create-album').removeAttr('disabled');
               }
           });
       }

        return false;
    });

    $uploadContainer.pahUploader({
        'albumIdSource':'#input-album-id',
        'url' : "http://posts.pah.loc/picture/upload-file",
    });
});