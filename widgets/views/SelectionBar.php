<div id="<?= $this->context->id ?>" class="selection-bar" <?= $this->context->show ? ""  : "style='display:none'" ?>>

    <div id="select-all" title="Select all images">
        <i class="fa fa-check"></i>
    </div>
    <label id="select-all-label" class=""  for="select-all">Select All</label>

    <div class="status-text">
        <span class="count"></span>
             <span class="help-text">&nbsp;image<span class="s"></span>&nbsp;selected
             </span>
    </div>

    <div class="icons">
        <?php
            foreach($icons as $name => $icon){
                if(!isset($icon['show']) || $icon['show'] != false)
                {
                    echo \app\helpers\Html::tag("div",
                        "<i class='fa {$icon['icon']}'></i>",
                        [
                            'title' => $icon['title'],
                            'class' => 'icon-wrapper pah-color ' . $name
                        ]
                    );
                }
            }
        ?>
    </div>
</div>