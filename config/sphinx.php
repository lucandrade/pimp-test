<?php

require(__DIR__ . '/../vendor/autoload.php');

$localConfigFile = __DIR__ . '/../config/local.php';
$localConfig = $localConfig = file_exists($localConfigFile) ? require($localConfigFile) : [];
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../config/bootstrap.php');

new yii\console\Application(
    yii\helpers\ArrayHelper::merge(
        require(__DIR__ . '/../config/common.php'),
        require(__DIR__ . '/../config/console.php'),
        isset($localConfig['common']) ? $localConfig['common'] : [],
        isset($localConfig['console']) ? $localConfig['console'] : []
    )
);

$username = \Yii::$app->components['db']['username'];
$password = \Yii::$app->components['db']['password'];

if (!preg_match_all('~((dbname|host)=(\w+))~si', \Yii::$app->components['db']['dsn'], $matches)) {
    die('cant recognize db DSN');
}

foreach ($matches[2] as $k => $v) {
    $$v = $matches[3][$k];
}

if (!isset($host) || !isset($dbname)) {
    die('no db or host recognized in DSN');
}

function getMainSearchQuery($where = '')
{
    $query = [];
    $query[] = 'SELECT i.id, i.id as id_attr, i.title as i_title_attr,  UNIX_TIMESTAMP(i.createTime) AS ts_created_at,';
    $query[] = 'UNIX_TIMESTAMP(i.updateTime) AS ts_updated_at, i.status as i_status_attr, i.authorId as i_authorId_attr,';
    $query[] = 'a.id as a_id_attr, a.title as a_title_attr';
    $query[] = 'FROM image i ';
    $query[] = 'LEFT JOIN album a ON i.albumId = a.id';
    $query[] = 'WHERE i.id>=$start AND i.id<=$end AND (i.status=' . \app\models\Image::STATUS_NORMAL . ' OR i.status=' . \app\models\Image::STATUS_PROCESSING . ")";
    $query[] =  $where;
    return implode(' ', $query);
}



function getPostsSearchQuery($where = '')
{
    $query = [];
    $query[] = 'SELECT *, p.id AS id_attr, p.is_adult AS is_adult_attr, p.user_id AS user_id_attr, UNIX_TIMESTAMP(created_at) AS ts_created_at,';
    $query[] = 'UNIX_TIMESTAMP(updated_at) AS ts_updated_at, UNIX_TIMESTAMP(live_at) AS ts_live_at,';
    $query[] = '(SELECT GROUP_CONCAT(pb.text SEPARATOR " ") FROM post_block pb WHERE pb.post_id=id_attr';
    $query[] = 'AND pb.type="' . \app\models\PostBlock::BLOCK_TYPE_TEXT . '" ORDER BY pb.position ASC ) AS fullcontent_attr,';
    $query[] = '(SELECT GROUP_CONCAT(tag.name SEPARATOR " ") FROM post_to_tag ptt, tag';
    $query[] = 'WHERE ptt.post_id=p.id AND tag.id = ptt.tag_id ), (SELECT CONCAT("tag_",';
    $query[] = 'GROUP_CONCAT(REPLACE(tag.name, " ", "_") SEPARATOR " tag_")) FROM post_to_tag ptt, tag';
    $query[] = 'WHERE ptt.post_id=p.id AND tag.id = ptt.tag_id), (SELECT CONCAT("user_", REPLACE(user.id, " ", "_"))';
    $query[] = 'FROM user WHERE user.id=p.user_id AND user.status= '. \app\models\User::STATUS_ACTIVE.') FROM post p';
    $query[] = 'WHERE id>=$start AND id<=$end AND is_draft=0 AND status=' . \app\models\Post::STATUS_ACTIVE;
    $query[] = $where;
    return implode(' ', $query);
}
?>
<?php ob_start() ?>


    source pah_dbconnect
    {
        type                            = mysql
        sql_host                        = <?php echo $host ?>

        sql_user                        = <?php echo $username ?>

        sql_pass                        = <?php echo $password ?>

        sql_db                          = <?php echo $dbname ?>

    }



    source pah_main_search_source : pah_dbconnect
    {
        sql_query_range               = SELECT MIN(id),MAX(id) FROM image
        sql_range_step                = 1000

        sql_attr_uint                 = id_attr
        sql_attr_uint                 = a_id_attr
        sql_field_string              = i_title_attr
        sql_field_string              = a_title_attr
        sql_attr_uint                 = i_status_attr
        sql_attr_uint                 = i_authorid_attr
        sql_attr_timestamp            = ts_created_at
        sql_attr_timestamp            = ts_updated_at

        sql_query_info                = SELECT * FROM image WHERE id=$id

    }

    source pah_main_search : pah_main_search_source
    {
        sql_query                     = <?php echo getMainSearchQuery() ?>

    }

    source pah_main_search_delta : pah_main_search_source
    {
        sql_query                     = <?php echo getMainSearchQuery('AND i.createTime <= NOW() AND i.createTime > NOW() - INTERVAL 48 HOUR') ?>

    }

    source pah_main_search_future : pah_main_search_source
    {
        sql_query                     = <?php echo getMainSearchQuery('AND i.createTime > NOW()') ?>

    }

    index pah_main_search
    {
        source                  = pah_main_search
        path                    = <?php echo \Yii::$app->params['sphinxPaths']['main_search']; ?>

        docinfo                 = extern
        charset_type            = utf-8
        charset_table           = U+FF10..U+FF19->0..9, U+FF21..U+FF3A->a..z, U+FF41..U+FF5A->a..z, 0..9,A..Z->a..z, a..z
        mlock                   = 0
        morphology              = stem_enru
        min_word_len            = 1
        html_strip              = 1
        enable_star             = 1
        min_infix_len           = 1

    }

    index pah_main_search_delta : pah_main_search
    {
        source = pah_main_search_delta
        path = <?php echo \Yii::$app->params['sphinxPaths']['main_search_delta']; ?>

    }

    index pah_main_search_future : pah_main_search
    {
        source = pah_main_search_future
        path = <?php echo \Yii::$app->params['sphinxPaths']['main_search_future']; ?>

    }


    source pah_posts_search_source : pah_dbconnect
    {
        sql_query_range               = SELECT MIN(id),MAX(id) FROM post
        sql_range_step                = 1000
    
        sql_field_string              = fullcontent_attr
        sql_attr_uint                 = id_attr
        sql_attr_uint                 = is_adult_attr
        sql_attr_uint                 = user_id_attr
        sql_attr_float                = rating
        sql_attr_timestamp            = ts_created_at
        sql_attr_timestamp            = ts_updated_at
        sql_attr_timestamp            = ts_live_at
    
        sql_query_info                = SELECT * FROM post WHERE id=$id

    }

    source pah_posts_search : pah_posts_search_source
    {
        sql_query                     = <?php echo getPostsSearchQuery('AND live_at <= NOW()') ?>

    }

    source pah_posts_search_delta : pah_posts_search_source
    {
        sql_query                     = <?php echo getPostsSearchQuery('AND live_at <= NOW() AND live_at > NOW() - INTERVAL 48 HOUR') ?>

    }

    source pah_posts_search_future : pah_posts_search_source
    {
        sql_query                     = <?php echo getPostsSearchQuery('AND live_at > NOW()') ?>

    }

    index pah_posts_search
    {
        source                  = pah_posts_search
        path                    = <?php echo \Yii::$app->params['sphinxPaths']['posts_search']; ?>

        docinfo                 = extern
        charset_type            = utf-8
        charset_table           = U+FF10..U+FF19->0..9, U+FF21..U+FF3A->a..z, U+FF41..U+FF5A->a..z, 0..9,A..Z->a..z, a..z
        mlock                   = 0
        morphology              = stem_enru
        min_word_len            = 1
        html_strip              = 1
        enable_star             = 1
        min_infix_len           = 1
    }

    index pah_posts_search_delta : pah_posts_search
    {
        source = pah_posts_search_delta
        path = <?php echo \Yii::$app->params['sphinxPaths']['posts_search_delta']; ?>

    }

    index pah_posts_search_future : pah_posts_search
    {
        source = pah_posts_search_future
        path = <?php echo \Yii::$app->params['sphinxPaths']['posts_search_future']; ?>

    }





<?php
$buffer = ob_get_clean();
echo trim($buffer);
?>
