<?php
namespace app\extensions;

use yii\base\Component;

class SphinxWrapper extends Component
{

    private $_client;
    public $host;
    public $timeout = 3000;
    public $port;
    public $arrayResult = true;


    public function init()
    {
        include("sphinxclient/sphinxapi.php");

        $this->_client = new \SphinxClient();
        $this->_client->SetServer($this->host, $this->port);
        $this->_client->SetConnectTimeout($this->timeout);
        $this->_client->SetArrayResult($this->arrayResult);
        $this->_client->SetMatchMode(SPH_MATCH_EXTENDED);
    }


    public function getClient()
    {
        return $this->_client;
    }

    public function getIds($result)
    {
        $ids = [];
        if(!empty($result['matches']))
        {
            foreach($result['matches'] as $match){
                $ids[] = $match['id'];
            }

        }
        return $ids;
    }

}