<?php
/**
 * Configuration file for the "yii asset" console command.
 */

// In the console environment, some path aliases may not exist. Please define these:
Yii::setAlias('@webroot', dirname(__DIR__) . '/web');
Yii::setAlias('@web', '/');

return [
    // java compressor
    //    'jsCompressor' => 'java -jar tools/compiler.jar --js {from} --js_output_file {to}',
    //    'cssCompressor' => 'java -jar tools/yuicompressor.jar --type css {from} -o {to}',

    // nodejs gulp
    'bundles' => [
        'yii\widgets\ActiveFormAsset',
        'yii\grid\GridViewAsset',
        'yii\validators\ValidationAsset',
        'app\assets\AdminAsset',
        'app\assets\GCExternalAsset'
    ],
    // Asset bundle for compression output:
    'targets' => [
        'all' => [
			'class' => 'yii\web\AssetBundle',
            'basePath' => '@webroot/assets',
            'baseUrl' => '@web/assets',
            'js' => 'js/all-{hash}.js',
            'css' => 'css/all-{hash}.css'
        ],
    ],
    // Asset manager configuration:
    'assetManager' => [
        'basePath' => '@webroot/assets',
        'baseUrl' => '@web/assets'
    ],
];