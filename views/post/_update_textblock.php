<?php
/**
 * @var $this yii\web\View
 * @var $model app\models\PostBlock
 */

use yii\bootstrap\Html;
use yii\helpers\HtmlPurifier;

?>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="post-block-manage">
            <button type="button" class="js-remove btn btn-sm btn-danger pull-right">
                <i class="fa fa-trash"></i>
            </button>
            <span class="drag-handle text-muted h4">&#9776;</span>
            &nbsp;
            <button type="button" class="btn btn-sm btn-default add-post-item">
                <i class="fa fa-plus"></i>
            </button>
        </div>
        <div class="form-group">
            <?= Html::activeHiddenInput($model, '[' . $model->id . ']id', ['class' => 'form-control']) ?>
            <?= Html::activeHiddenInput($model, '[' . $model->id . ']position', ['class' => 'form-control']) ?>
            <div class="medium-eitor">
                <div class="medium-editor-content">
                    <?= HtmlPurifier::process($model->text) ?>
                </div>
            </div>
            <?= Html::activeTextarea($model, '[' . $model->id . ']text', ['class' => 'form-control', 'rows' => 3, 'hidden' => 'hidden']) ?>
        </div>
    </div>
</div>
