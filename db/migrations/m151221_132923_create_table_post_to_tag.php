<?php

use yii\db\Schema;
use yii\db\Migration;

class m151221_132923_create_table_post_to_tag extends Migration
{
    public function up()
    {
        $this->createTable('{{%post_to_tag}}',[
            'post_id' => Schema::TYPE_INTEGER,
            'tag_id'  => Schema::TYPE_INTEGER,
        ]);
        $this->createIndex('post_id_tag_id','{{%post_to_tag}}', ['post_id','tag_id'], true);
    }

    public function down()
    {
        $this->dropTable('{{%post_to_tag}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
