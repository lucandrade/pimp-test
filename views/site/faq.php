<?php
/**
 * @var $this \yii\web\View
 */

use yii\helpers\Url;

?>
<div>
    <h1>Signing Up</h1>
    <p>If you would like to submit posts, or leave comments you have to sign up. You can do so by going here.</p>
    <p><a href="<?= Url::to(['site/signup'], true) ?>"><?= Url::to(['site/signup'], true) ?></a></p>
    <p>Make sure you have a working email because you will need to confirm it's actually a working email, and you are a
        real person. if your cat wants to signup, well, he or she will also have to have their own working email. The
        signup
        process is pretty straight forward, add your email, pick a name and a password. If you've used a computer some
        time
        in your life you shouldn't have a problem. If you're new to the internet, well, glad you picked us first but you
        still need all of the above.</p>

    <h1>Posting</h1>
    <p>This is where things may be a little tricky for some of you, others may have no problem. We have several ways you
        can add content, whether it be videos from youtube, or pictures you took of your cousin when he had a little too
        much to drink and you drew schlongs on his face (try not to upload those though, we're trying to keep the site
        SFW
        so you can send stuff yo grandma and not give her a heart-attack in the process).</p>
    <p>Now, onto posting.</p>
    <ol class="list-unstyled numeric-list">
        <li>
            - Add a Title, make it catchy, if its your cat doing something funny, be witty about it. Don't just say
            "Whiskers on Catnip" say "Whiskers traveling to a parallel universe on that catnip" or something even better
            than that lame one I just used as an example.
        </li>
        <li>
            - Add the content, you can embed a YouTube video just by clicking the play button icon, adding the link,
            and hitting add, eezy peezy! You can add images by clicking the center icon and add up to 100 pics. You can
            also add some text by clicking the first icon if you want to give a description of each picture.
        </li>
        <li>
            - You can rearrange the stuff simply by moving your mouse over the 3 bars on the left side and dragging it
            into the spot you would like. You can also delete stuff you didn't mean to add, or click the + icon and add
            something else above it.
        </li>
        <li>
            - Add tags, this helps people find the content you post. Don't go insane and post like 400 tags that have
            nothing to do with the post. It just pisses people off.
        </li>
        <li>
            - "Is Author" if you took the pic, click that. If you jacked it from somewhere else because you thought it
            was funny and needed to be shared, don't click it.
        </li>
        <li>- "Is Adult" look, no porn. But if something is a little risque but no nudity, share that shiznit.</li>
        <li>
            - Save Draft, if you're about to miss your favorite tv show and still want to post it, just hit save
            draft. Come
            back later and finish it and submit it. It'll still be there ready and waiting like that girl you once had
            who never called back because well... she wasn't impressed by your skills in the sack.
        </li>
        <li>
            - "Preview" it is what it says it is, preview the post before setting it live for the world to see. I'd
            suggest using it and going over everything you wrote just so the grummer nazes don't attack you. See what I
            did there?
        </li>
    </ol>
    <p>
        Finally, click Save post. When you click this it'll go to the "New" section, if it does well there it may even
        make it to the homepage. If it does you get points, which leads us to the next subject...
    </p>
    <p>How to take a bath in hot chocolate and why it's great for your skin.</p>
    <p>Wait. Crap. Wrong tutorial.</p>
    <p>Ah, yeah, onto the points system.</p>
    <p>See that up and down with a number between it on the left side of every post? That's an upvote and downvote
        button.
        You click that when you like or dislike something, the more upvotes something you post gets, the more points you
        accumulate. The more you accumulate the more people like you and the less you need to go outside because you
        have
        people that like you right here on the site. Well, not really. They just like you for your content. Needless to
        say,
        #upvotesmatter , its a new movement gaining traction, so I hear. The more points you get, the more badges you
        get.
        Eventually we'll give you prizes as the site grows if you consistently post good stuff. If you post horrible
        stuff
        no one likes, well, stop. Find better content.
    </p>

    <h1>Sharing</h1>
    <p>This is actually extremely important if you're posting content to share it to your social networks, emailing it
        to
        your friends, your mom, your uncle who you rarely talk to but like to keep in touch with because he sends money
        every year for Christmas and your birthday. Ect. On the left side of each post you'll see the google, twitter
        and
        facebook icons, click them, share away. If you want to share to others just copy and paste the URL.
    </p>

    <h1>Favorite Posts</h1>
    <p>See the star under the sharing options? This allows you to favorite the stuff you really like so you can easily
        find
        it again in the future.
    </p>

    <h1>Commenting</h1>
    <p>This is where the fun really happens, when people start commenting, arguing, trolling, ect. Or posting titles
        that
        are actually better than the ones the original poster made. Mostly its just to use words to express your love
        for
        how great the content is, or how much you hated it. Which usually leads to someone else saying the opposite,
        then
        you two bicker as others join in, that's why we included the reply option to each comment, so the comments can
        be
        nested instead of it looking like a bunch of random comments to people that posted something halfway up the
        page.
    </p>
    <p>We also allow you to vote up/vote down comments. If a comment gets enough downvotes it will be hidden so others
        aren't subjected to the stupidity of a few morons who just like to type crap online.</p>
    <p>We also allow you to sort the comments by Best (Highest rated) and Newest.</p>

    <h1>Quick Scroll</h1>
    <p>See those up/down arrows in the bottom right hand of the page? Those help you skip to the next post without
        having
        to scroll, the one pointing Up with the bar above it jumps back to the top of the page.
    </p>

    <h1>Search</h1>
    <p>I feel like I shouldn't have to explain this, but if you're new to the internet and want to find something, this
        is
        how you find it on our site. Also, there is a theory going around if you type in a certain series of letters and
        numbers the screen literally opens a time portal that allows you to search through time. I tried it once, and I
        must
        say, the Dinosaurs weren't really all they're made out to be. Made fun of a T-Rex for his little arms and all he
        did
        was turn his back and walk away crying, like a lil bitch.
    </p>

    <p>Well, that's how you use the site. We'll be updating this as new features are added. So if you don't know how to
        do
        something that's new on the site, just check back here. We'll walk you through it.
    </p>
</div>