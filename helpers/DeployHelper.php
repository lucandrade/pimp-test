<?php

namespace app\helpers;


class DeployHelper
{
    public static function getLogFile()
    {
        return \Yii::getAlias("@runtime") . "/logs/deploy.log";
    }

    public static function runPull($branch = null, $dir = null)
    {
        if (empty($branch)) {
            $branch = !empty(\Yii::$app->params['autoDeployBranch']) ? \Yii::$app->params['autoDeployBranch'] : "master";
        }

        self::prepareDir($dir);
        $out = ConsoleHelper::exec("git pull origin $branch", self::getLogFile());

        return $out;
    }

    public static function runAfterPullScripts($dir = null)
    {
        $out = [];
        self::prepareDir($dir);
        $logFile = self::getLogFile();
        $out = array_merge($out, ConsoleHelper::exec("composer install", $logFile));
        $out = array_merge($out, ConsoleHelper::exec("./yii migrate --interactive=0", $logFile));
        $out = array_merge($out, ConsoleHelper::exec("./yii minify config/assets-config/minify.php config/generated-assets/asset.php", $logFile));
        $out = array_merge($out, ConsoleHelper::exec("./yii minify config/assets-config/editor-asset.php config/generated-assets/editor-asset.php", $logFile));
        $out = array_merge($out, ConsoleHelper::exec("./yii minify config/assets-config/minify-uploader-plugin.php config/generated-assets/uploader-plugin.php", $logFile));
        $out = array_merge($out, ConsoleHelper::exec("./yii worker/stop", $logFile));
        $out = array_merge($out, ConsoleHelper::exec("./yii worker/start", $logFile));

        return $out;
    }

    public static function prepareDir($dir)
    {
        if (empty($dir)) {
            $dir = !empty(\Yii::$app->params['deployDir']) ? \Yii::$app->params['deployDir']
                : \Yii::getAlias("@app");
        }

        chdir($dir);
        return $dir;
    }
}