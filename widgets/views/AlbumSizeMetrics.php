<span class="post-date">
    Total images : <span id="total-images-count"><?= $this->context->count ?></span>
</span>
<span class="post-date">
    Total size: <span id="total-album-size" data-total-size="<?= $this->context->size ?>">
    <?= \app\helpers\FileHelper::humanFileSize($this->context->size) ?></span>
</span>