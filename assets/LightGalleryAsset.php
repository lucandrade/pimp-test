<?php

namespace app\assets;

use yii\web\AssetBundle;

class LightGalleryAsset extends AssetBundle
{
	public $sourcePath = '@bower/lightgallery';
	public $css = [
		'dist/css/lightgallery.css',
        'dist/css/lg-transitions.css'
	];

    public $js = [
        'dist/js/lightgallery.js',
        'demo/js/lg-zoom.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];
}
