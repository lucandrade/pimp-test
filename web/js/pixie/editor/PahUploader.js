'use strict';

angular.module('ImageEditor')

.service("pahUploader", ['canvas', function(canvas){

    var uploader = $('.uploader-container'),
        title = $('.img-preview').data('title');

    uploader.pahUploader({
        clicker : false,
        fileInput : '.file-input',
        showAfterUploadingInfo : false,
        previewsContainer: '#status-container #previews-container',
        renderPreviewImage:false,
    });


    this.getUploader = function(){
        return uploader;
    };

    this.uploadImage = function(format, quality, name, submitDone, albumId){
        canvas.fabric.deactivateAll();
        canvas.fabric.renderAll();

        canvas.zoom(1);

        canvas.fabric.lowerCanvasEl.toBlob(function(blob) {
            blob.name = name +"." + format;
            var data = {
                files:[blob],
                submitDone: submitDone
            };

            if(albumId){
                data.formData = {
                    albumId:albumId
                }
            }
            uploader.pahUploader('getPluginUploader').fileupload("add", data);
        }, 'image/'+format, quality);
    }
}]);