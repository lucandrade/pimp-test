<?php

use yii\db\Schema;
use yii\db\Migration;

class m160118_110747_add_table_user_seen_post extends Migration
{

    public $table = 'user_seen_post';
    public $userRef = 'user_id';
    public $postRef = 'post_id';

    public function up()
    {
        $this->createTable($this->table, [
            $this->userRef => $this->integer(11),
            $this->postRef => $this->integer(11),
        ]);
        $this->addPrimaryKey('pk_user_seen_posts', $this->table, [$this->userRef, $this->postRef]);

        $this->createIndex($this->userRef . "_index", $this->table, $this->userRef);
        $this->createIndex($this->postRef. "_index", $this->table, $this->postRef);

        $this->addForeignKey($this->userRef . "_user_seen_post_fk", $this->table, $this->userRef,'user', 'id');
        $this->addForeignKey($this->postRef . "_user_seen_post_fk", $this->table, $this->postRef,'post', 'id');

    }

    public function down()
    {
        $this->dropForeignKey($this->postRef . "_user_seen_post_fk", $this->table);
        $this->dropForeignKey($this->userRef . "_user_seen_post_fk", $this->table);

        $this->dropTable($this->table);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
