<?php
namespace app\db\fixtures;

use yii\test\ActiveFixture;

class ImageFixture extends ActiveFixture {

	public $modelClass = 'app\models\Image';
	public $depends = [
		'app\db\fixtures\UserOauthFixture',
		'app\db\fixtures\AlbumFixture'
	];
}