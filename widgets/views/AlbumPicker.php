<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
$isGuest = \Yii::$app->user->isGuest;
?>

<?php \yii\bootstrap\Modal::begin([
    'id'=>'album-picker-modal',
    'header' => '<h4>Move to</h4>',
    'toggleButton' => false,
    'footer' => $isGuest ? null : "<button type='button' disabled class='btn btn-primary' id='btn-move-album'>Move</button>"
]); ?>

<?php
    if(!$isGuest){
        echo \yii\helpers\Html::dropDownList('destination-album', $this->context->defaultAlbumId, $albumIds,
        [
            'prompt' => 'Select album...',
            'class' => 'form-control',
            'id' => 'destination-album-id-list'
        ]);
    }
?>

    <?php $form = ActiveForm::begin([
        'id' => 'create-album-form',
        'options' => [
            'class' => 'form-inline'
        ],
        'action' => $this->context->url
    ]);

    $newAlbum = new \app\models\Album();
    $newAlbum->setScenario(\app\models\Album::SCENARIO_CREATING_BY_USER);
    ?>

    <?= $form->field($newAlbum, 'title')->textInput(['id'=>'new-title-input'])->label("New Album:");  ?>
    <?= \yii\bootstrap\Html::button("Create", ['class'=>'btn btn-primary', 'id' => 'btn-create-album']); ?>


    <?php ActiveForm::end(); ?>

<?php \yii\bootstrap\Modal::end(); ?>