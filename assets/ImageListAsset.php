<?php

namespace app\assets;

use yii\web\AssetBundle;

class ImageListAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/responsive-grid.css',
        'css/image-list.css'
    ];

    public $js = [
        'js/image-list.js'
    ];

    public $depends = [
    ];
    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}