<?php

use yii\rbac\Item;
use app\components\RatingRule;

$ratingRule = new RatingRule();

return [

    'addPosts' => [
        'type' => Item::TYPE_PERMISSION,
        'description' => 'Adding posts',
        'ruleName' => $ratingRule->name,
    ],

    'addComments' => [
        'type' => Item::TYPE_PERMISSION,
        'description' => 'Adding comments',
        'ruleName' => $ratingRule->name,
    ],

    'addCommentImages' => [
        'type' => Item::TYPE_PERMISSION,
        'description' => 'Adding comment images',
        'ruleName' => $ratingRule->name,
    ],

    'addVideo' => [
        'type' => Item::TYPE_PERMISSION,
        'description' => 'Adding video',
        'ruleName' => $ratingRule->name,
    ],

    'combineTags' => [
        'type' => Item::TYPE_PERMISSION,
        'description' => 'Combine tags',
        'ruleName' => $ratingRule->name,
    ],

    'guest' => [
        'type' => Item::TYPE_ROLE,
        'description' => 'Guest',
    ],

    'user' => [
        'type' => Item::TYPE_ROLE,
        'description' => 'User',
        'children' => [
            'guest',
            'addPosts',
            'addComments',
            'addCommentImages',
            'addVideo',
            'combineTags',
            'ruleName' => $ratingRule->name
        ],
    ],

    'admin' => [
        'type' => Item::TYPE_ROLE,
        'description' => 'Admin',
        'children' => [
            'user'
        ],
    ],

];