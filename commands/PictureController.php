<?php

namespace app\commands;


use app\commands\actions\Process;
use app\models\Image;
use yii\base\Exception;
use yii\console\Controller;
use yii\helpers\Console;
use app\helpers\LogHelper;
use yii\helpers\FileHelper;

class PictureController extends Controller
{

    public function actions()
    {
        return [
            'process' => [
                'class' => Process::className(),
                'modelClass' => Image::className(),
                'entityName' => 'image'
            ]
        ];
    }


    public function actionPurge($id)
    {
        $image = $this->loadImage($id);
        if($image){
            if($image->purge()){
                echo $this->ansiFormat("Image # '$id' was successfully purged from db, file system and ftp", Console::FG_GREEN);
                LogHelper::log("Image # '$id' was successfully purged from db, file system and ftp",'@runtime/images/', 'purging.log');
            }else{
                echo $this->ansiFormat("Image # '$id' was NOT purged because of " . json_encode($image->getErrors()), Console::FG_RED);
                LogHelper::log("Image # '$id' was NOT purged because of " . json_encode($image->getErrors()),'@runtime/images/', 'purging.log');
            }
        }
    }


    protected function getImagesQueryByReq($req)
    {
        $parts = explode('-', $req);
//         $ids = [];
//         if(isset($parts[1])){
//             for($i = $parts[0]; $i<=$parts[1]; $i++){
//                 $ids[] = $i;
//             }
//         }else{
//             $ids = [$parts[0]];
//         }
        $id_from = $parts[0];
        if(isset($parts[1])){
	        $id_to = $parts[1];
        }else{
	        $id_to = $id_from;
        }
        if(!$id_from) {
					die("\n id 'from' not set");
        }
        if($id_to < $id_from) {
					die("\n id 'to' is less then id 'from'");
        }

        $query = Image::find()->where([
            'status' => [
                Image::STATUS_NORMAL,
                Image::STATUS_DELETED,
                Image::STATUS_PRIVATE
            ]
        ])
        ->andWhere(['>=', 'id', $id_from]) 
        ->andWhere(['<=', 'id', $id_to]) 
        ;

        return $query;
    }



    public function actionClearCdn($req)
    {
        $query = $this->getImagesQueryByReq($req);
        $totalFound = $query->count('id');
        $totalProcessed = 0;

        echo $this->ansiFormat("\nClearing CDN started... found $totalFound image to reconvert...\n");

        $limit = 50;
        $offset = 0;

        do{
            /**
             * @var $images Image[]
             */
            $images = $query->limit($limit)->offset($offset)->all();
            $offset += $limit;

            foreach($images as $image){

                list($reflected, $inxy) = $image->clearCdnCache();
                if(empty($reflected)){
                    echo $this->ansiFormat("\nReflected results empty for image #$image->id" , Console::FG_RED);
                }
                if(empty($inxy)){
                    echo $this->ansiFormat("\nInxy results empty for image #$image->id" , Console::FG_RED);
                }

                $totalProcessed++;
                echo $this->ansiFormat("$totalProcessed/$totalFound images were processed\n");
            }

        }while(count($images) === $limit);

        echo $this->ansiFormat("\n\n$totalProcessed / $totalFound images  cleared cnd cache!", Console::FG_GREEN);
    }

    public function actionReconvert($req)
    {
        $query = $this->getImagesQueryByReq($req);

        $totalFound = $query->count('id');
        $totalConverted = 0;

        echo $this->ansiFormat("\nReconverting started... found $totalFound image to reconvert...\n");

        $limit = 50;
        $offset = 0;

        do{
            /**
             * @var $images Image[]
             */
            $images = $query->limit($limit)->offset($offset)->all();
            $offset += $limit;

            foreach($images as $image){
                if($this->reconvertImage($image))
                {
                    $totalConverted++;
                    echo $this->ansiFormat("\r$totalConverted/$totalFound images were reconverted...");
                }
                else{
                    echo $this->ansiFormat("\nCan't convert image #$image->id : ".json_encode($image->getErrors()) . "\n", Console::FG_RED);
                }

            }

        }while(count($images) === $limit);

        echo $this->ansiFormat("\n$totalConverted / $totalFound images were reconverted!");
    }


    protected function reconvertImage(Image $image)
    {
        $dir = \Yii::getAlias("@runtime/tmp/")  . md5($this->id . time());
        FileHelper::createDirectory($dir);
        $tmp = $dir ."/". $image->fileName;
        try{
            $fileSaved = $image->saveFileFromServer($tmp);
        }
        catch(Exception $e){
            echo $this->ansiFormat("\nCoudn't download image file for reconverting from ftp:" . $e->getMessage(), Console::FG_RED);
            return false;
        }

        if(!$fileSaved){
            echo $this->ansiFormat("Coudn't save file from ftp to " . $tmp, Console::FG_RED);
            return false;
        }

        if ($image->saveFile([], $tmp)){

            return $image->putToFtpUploadingQueue();
        }else{
            $this->ansiFormat("\n Coudn't save file to media_protected for next uploading to ftp, errors:" . json_encode($image->getErrors()), Console::FG_RED);
            return false;
        }
    }



    /**
     * @param $id
     * @return Image
     */
    protected function loadImage($id)
    {
        $image = Image::findOne(['id' => $id]);
        if(!$image)
        {
            LogHelper::log("Image with id '$id' not found", '@runtime/logs/images/', 'purging.log');
            echo $this->ansiFormat("Image with id '$id' not found", Console::FG_RED);
            return null;
        }

        return $image;
    }

}