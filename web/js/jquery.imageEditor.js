(function($){
    $.fn.imageEditor = function(method){
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method "' +  method + '" is not found in jquery.imageEditor' );
        }
    };

    var defaultSettings = {
        modalIdPrefix:"image-editor-modal"

    };
    var settings = {};
    var editorIdAttr = 'editor-id';
    var darkroom, $this,
        modalId = false;




    var initSettings = function(options){
        $this = $(this);
        settings = $.extend(defaultSettings, options || {});
        if($this.data(editorIdAttr)){
            modalId = settings.modalIdPrefix+$this.data(editorIdAttr);
        }
    };

    var methods = {
        init:function(options){
            initSettings.call(this, options);
            initModal();
        }
    };

    var initModal = function(){

        var editorId = $this.data(editorIdAttr) || false,
            editorIsInited = $this.data("editor-inited") || false;

        if(!editorId)
        {
            editorId = Math.random().toString(36).replace(/[^a-z]+/g, '');
            $this.data(editorIdAttr, editorId);
            modalId = settings.modalIdPrefix + editorId;
            var code  =
                '<div class="modal fade" id="' + modalId + '" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">' +
                    '<div class="modal-dialog" role="document">' +
                        '<div class="modal-content">' +
                            '<div class="modal-header">' +
                                '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                                '<h4 class="modal-title" id="myModalLabel">Edit image</h4>' +
                            '</div>' +
                            '<div class="modal-body">' +
                                '<div><img id="main-img" class="img-responsive"></div>'+
                                '<div><h2 id="indicator" class="text-center">Loading...&nbsp;<i class="fa fa-spin fa-spinner"></i></h2></div>'+
                            '</div>' +
                            '<div class="modal-footer">' +
                                '<button type="button" class="btn btn-default"  data-dismiss="modal">Close</button>' +
                                '<button type="button" class="btn btn-primary" disabled id="save-img-btn">Save image</button>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>';

            $('body').append(code);

            var $mainImage =  $('#'+modalId+' #main-img');

            $('body').on('click', '#' + modalId + ' #save-img-btn', function(){
                var $button = $(this);
                if(darkroom && darkroom instanceof Darkroom){


                    $button.attr('disabled', 'disabled');
                    $('#'+modalId).modal('hide');
                    var data = darkroom.sourceImage.toDataURL({ format: 'jpeg', quality: 1}),
                        blob = dataURItoBlob(data);

                    blob.name = $this.data('filename');
                    settings.uploader.pahUploader('getPluginUploader').fileupload("add",{files:[blob]});
                }
            });

            $mainImage.on('load', function(){
                $('#'+modalId + " #indicator").hide();
                $('#' + modalId + ' #save-img-btn').removeAttr('disabled');
            });

            $mainImage.hide();
            $mainImage.attr('src', settings.imgUrl);
        }

        $('#'+modalId).modal('show');

        if(!editorIsInited){
            $this.data('editor-inited', true);
            darkroom = new Darkroom('#'+modalId+' #main-img', {
                // Canvas initialization size
                minWidth: 100,
                minHeight: 100,
                maxWidth: 500,
                maxHeight: 500,

                // Plugins options
                plugins: {
                    crop: {
                        minHeight: 50,
                        minWidth: 50
                    },
                    resize:{
                        maxResizeCanvasWidth:900,
                        maxResizeCanvasHeight:900
                    },
                    save: false // disable plugin
                },

                // Post initialization method
                initialize: function() {

                }
            });
        }
    };


    var  dataURItoBlob = function(dataURI, callback) {
        // convert base64 to raw binary data held in a string
        // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
        var byteString = atob(dataURI.split(',')[1]);

        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

        // write the bytes of the string to an ArrayBuffer
        var ab = new ArrayBuffer(byteString.length);
        var ia = new Uint8Array(ab);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        // write the ArrayBuffer to a blob, and you're done
        var bb = new Blob([ab], {type:mimeString});
        return bb;
    }

})(jQuery);