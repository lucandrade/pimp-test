<?php

namespace app\assets;


use yii\web\AssetBundle;

class ShareImagesModalAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/share-images-modal.css',
    ];

    public $js = [
        'js/share-image-modal.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
        'app\assets\ShareCodesAsset',
    ];

    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}