<?php

use yii\db\Migration;

/**
 * Handles adding password to table `album`.
 */
class m160627_093154_add_password_to_album extends Migration
{
    public $table = 'album';
    public $col = 'password';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn($this->table, $this->col, $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn($this->table, $this->col);
    }
}
