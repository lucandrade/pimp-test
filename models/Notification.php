<?php

namespace app\models;

use app\helpers\CommentsService;
use app\helpers\Html;
use Yii;
use yii\base\InvalidParamException;
use yii\db\Expression;

use app\models\interfaces\CommentsServiceModel;
use yii\base\Exception;

/**
 * This is the model class for table "notification".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $type_id
 * @property integer $caller_id
 * @property integer $owner_id
 * @property integer $parent_model_type
 * @property integer $status
 * @property string $created_at
 *
 * @property User $owner
 * @property User $caller
 */
class Notification extends \yii\db\ActiveRecord
{
    const TYPE_POST_COMMENTS = 1;
    const TYPE_POST_UPVOTED = 2;
    const TYPE_POST_DOWNVOTED = 3;
    const TYPE_COMMENTS_REPLY = 4;
    const TYPE_COMMENTS_AT = 5;

    const STATUS_ACTIVE = 1;
    const STATUS_READED = 2;

    const PARENT_MODEL_POST = 1;
    const PARENT_MODEL_ALBUM = 2;
    const PARENT_MODEL_IMAGE = 3;
    const PARENT_MODEL_COMMENT = 4;

    const PARENT_ALIAS_ALBUM = 'album';
    const PARENT_ALIAS_IMAGE = 'image';
    const PARENT_ALIAS_POST = 'post';

    const GUEST_USER_NAME = 'Guest';

    public $count;
    public static $batchSize = 10;
    private static $_activeNotificationCount;

    public static function tableName()
    {
        return 'notification';
    }


    public function rules()
    {
        return [
            [['owner_id'], 'required'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            [['type', 'owner_id', 'status', 'caller_id'], 'integer'],
            ['type_id', 'string'],
            [
                'type',
                'in',
                'range' => [
                    self::TYPE_POST_COMMENTS,
                    self::TYPE_POST_UPVOTED,
                    self::TYPE_POST_DOWNVOTED,
                    self::TYPE_COMMENTS_REPLY,
                    self::TYPE_COMMENTS_AT
                ]
            ],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_READED]],
            [['owner_id', 'caller_id'], 'exist', 'targetClass' => User::className(), 'targetAttribute' => 'id'],
            [['text'], 'filter', 'filter' => function ($value) {
                return filter_var($value, FILTER_SANITIZE_STRING);
            }],
            ['parent_model_type', 'in', 'range' => array_values(CommentsService::aliasesTypes())],
            [['created_at'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'type_id' => 'Type ID',
            'owner_id' => 'Owner ID',
            'caller_id' => 'Caller ID',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCaller()
    {
        return $this->hasOne(User::className(), ['id' => 'caller_id']);
    }

    public function getActiveRelatedCaller()
    {
        $relatedNotification = self::find()
            ->where([
                'notification.owner_id' => $this->owner_id,
                'notification.type' => $this->type,
                'notification.type_id' => $this->type_id,
                'notification.status' => self::STATUS_ACTIVE
            ])
            ->andWhere(['<>', 'notification.id', $this->id])
            ->joinWith(['caller'])
            ->orderBy(['notification.created_at' => SORT_DESC])
            ->one();

        if ($relatedNotification === null) {
            return null;
        }
        return $relatedNotification->caller;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getActiveNotificationQuery()
    {
        return self::find()
            ->select('*')
            ->addSelect(['count' => new Expression('COUNT(type_id)')])
            ->where([
                'status' => self::STATUS_ACTIVE,
                'owner_id' => Yii::$app->user->id
            ])
            ->orderBy(['created_at' => SORT_DESC])
            ->groupBy(['type', 'type_id']);
    }

    public static function getActiveNotificationCount()
    {
        if (self::$_activeNotificationCount === null) {
            self::$_activeNotificationCount = (int)self::getActiveNotificationQuery()->count();
        }

        return self::$_activeNotificationCount;
    }

    public function getPostTypeList()
    {
        return [
            self::TYPE_POST_COMMENTS,
            self::TYPE_POST_UPVOTED,
            self::TYPE_POST_DOWNVOTED
        ];
    }

    public function getCommentTypeList()
    {
        return [
            self::TYPE_COMMENTS_AT,
            self::TYPE_COMMENTS_REPLY
        ];
    }

    public function isLinkedComment()
    {
        return in_array($this->type, $this->commentTypeList) && $this->post;
    }

    public function isLinkedPost()
    {
        return in_array($this->type, $this->postTypeList) && $this->post;
    }

    public function getUrl()
    {
        if ($this->isLinkedPost()) {
            return $this->post->getUrl(true) . $this->text;
        } else if ($this->isLinkedComment()) {
            return $this->post->getUrl(true) . $this->text;
        }
        return null;
    }

    public function getNoticeSuffix()
    {
        $suffix = '';
        if ($this->count > 1 && ($rCaller = $this->activeRelatedCaller)) {
            if ((int)$this->count === 2) {
                $suffix .= ' and ' . $rCaller->getUsernameLink();
            } else {
                $suffix .= ', ' . $rCaller->getUsernameLink();
                $users = ($this->count - 2) > 1 ? 'users' : 'user';
                $suffix .= ' and ' . ($this->count - 2) . ' ' . $users;
            }
        }
        return $suffix;
    }

    public function getNotice()
    {
        $title = '';
        $usernameLink = isset($this->caller) ? $this->caller->getUsernameLink() : self::GUEST_USER_NAME;
        switch ($this->type) {
            case self::TYPE_POST_COMMENTS:
                $title .= $usernameLink . $this->noticeSuffix . ' commented your ' . $this->getParentObjectLink();
                break;
            case self::TYPE_POST_UPVOTED:
                $title .= $usernameLink . $this->noticeSuffix . ' upvoted your ' . $this->getParentObjectLink();
                break;
            case self::TYPE_POST_DOWNVOTED:
                $title .= $usernameLink . $this->noticeSuffix . ' downvoted your ' .  $this->getParentObjectLink();
                break;
            case self::TYPE_COMMENTS_AT:
                $title .= $usernameLink . $this->noticeSuffix . ' have mentioned you in comment';
                break;
            case self::TYPE_COMMENTS_REPLY:
                $title .= $usernameLink . $this->noticeSuffix . ' have replied to your comment';
                break;
        }
        return $title;
    }

    public function getParentModelAlias()
    {
        return CommentsService::getAliasByType($this->parent_model_type);
    }

    public function getParentObjectLink()
    {
        list($parentId, $modelClass) = CommentsService::resolveKey($this->type_id);

        $alias = $this->getParentModelAlias();
        $model = $modelClass::findOne(['id' => $parentId]);

        if($model && $model->getIsActive()){
            return Html::a($alias, $model->getUrl());
        }else{
            return $alias;
        }

    }
}
