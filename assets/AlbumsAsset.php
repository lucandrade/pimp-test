<?php

namespace app\assets;


use yii\web\AssetBundle;

class AlbumsAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/user-albums.css',
        'css/responsive-grid.css'
    ];

    public $js = [
        'js/user-albums.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
        'app\assets\ImageSelectorAsset',
        'app\assets\AlbumLockerAsset',
    ];
    public $jsOptions = [
    ];
    public $cssOptions = [
    ];
}